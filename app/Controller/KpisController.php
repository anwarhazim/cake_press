<?php
class KpisController extends AppController
{
	var $uses = false;
	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {


	}

	public function correction()
    {

		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->Session->setFlash('Your request update successfully sent to HRIS Team for verifications. Please wait for any update.', 'success');
			$this->redirect(array('action' => 'correction'));

        }
    }

	public function kpiplanning()
	{
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->Session->setFlash('Your form successfully sent to your superiour to evalute. Please wait for any update.', 'success');
			$this->redirect(array('action' => 'kpiplanning'));

        }

	}

	public function addnewkpiplanning()
	{

	}

	public function midyearreview()
	{
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->Session->setFlash('Your form successfully sent to your superiour to evalute. Please wait for any update.', 'success');
			$this->redirect(array('action' => 'midyearreview'));

        }

	}

	public function addnewmidyearreview() {

	}



    public function incumbentdata()
    {

	}

	public function yearendreview() {

	}

	public function addnewendyearreview() {

	}

	public function competencyassessment() 
	{
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->Session->setFlash('Your form successfully sent to your superiour to evalute. Please wait for any update.', 'success');
			$this->redirect(array('action' => 'competencyassessment'));

        }
	}

	public function backup()
	{
		
	}

	public function merit()
	{
		
	}

	public function overview()
	{
		
	}

	public function declaration()
	{
		
	}

}
