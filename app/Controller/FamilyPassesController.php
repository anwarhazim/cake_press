<?php
class FamilyPassesController extends AppController
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	public $uses = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
	}

	public function index()
    {
		$this->loadModel('FamilyPass');
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

		$roles = $this->UserRole->getRoleByUserId($staff['Staff']['user_id']);

        $familypasstypes = array();

        if (in_array(14, $roles))//HCD Family Pass Prasarana
        {
            $familypasstypes[] = 1;
        }

        if (in_array(15, $roles))//HCD Family Pass Bus
        {
            $familypasstypes[] = 2;
        }

        if (in_array(16, $roles))//HCD Family Pass Rail
        {
            $familypasstypes[] = 3;
        }

		$conditions = array();

		$conditions['conditions'][] = array(
                                            'FamilyPass.organisation_type_id' => $familypasstypes
											);

        $conditions['order'] = array('FamilyPass.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['FamilyPass'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Applicant.reference_no LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(FamilyPass.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(FamilyPass.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['FamilyPass'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate('FamilyPass');

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['FamilyPass']['modified'] = date("d-m-Y",strtotime($details[$i]['FamilyPass']['modified']));

            $details[$i]['FamilyPass']['created'] = date("d-m-Y",strtotime($details[$i]['FamilyPass']['created']));

			$details[$i]['FamilyPass']['id'] = $this->Utility->encrypt($details[$i]['FamilyPass']['id'], 'fmlypss');

			$applyby = $this->Staff->findById($details[$i]['Applicant']['staff_id']);
			$details[$i]['ApplyBy'] = $applyby['Staff'];

        }
        
        $auth = $this->Utility->getUserAuth(54,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

		$this->set(compact('details'));
	}

	public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('National');
        $this->loadModel('Gender');
		$this->loadModel('FamilyPass');
        $this->loadModel('Applicant');
        $this->loadModel('FamilyPassType');
        $this->loadModel('FamilyPassReason');
        $this->loadModel('Organisation');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'fmlypss');

        $detail = $this->FamilyPass->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['Applicant']['staff_id']))
        {
            $applyby = $this->Staff->findStaffSummaryById($detail['Applicant']['staff_id']);

            if(!empty($applyby))
            {
                $detail['ApplyBy'] = $applyby['Staff'];

                if(!empty($applyby['Staff']['avatar']))
                {
                    $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
                }

                $detail['ApplyBy']['avatar'] = $img;
            }
        }

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['ApprovedBy']['id']))
        {
            $ApprovedBy = $this->Staff->findStaffSummaryById($detail['ApprovedBy']['id']);
        
            if(!empty($ApprovedBy['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$verifiedby['Staff']['avatar']."'/>";
            }
        }
        $detail['ApprovedBy']['avatar'] = $img;

        $detail['ApprovedBy']['day_by_text'] = date('D', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['day_by_num'] = date('d', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['month'] = date('m', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['year'] = date('Y', strtotime($detail['FamilyPass']['approved']));

        $detail['ApprovedBy']['hour'] = date('h', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['minute'] = date('i', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['format'] = date('A', strtotime($detail['FamilyPass']['approved']));

        if(!empty($detail['FamilyPass']['family_pass_pick_date']))
        {
            $detail['FamilyPass']['family_pass_pick_date'] = date("d-m-Y", strtotime($detail['FamilyPass']['family_pass_pick_date']));
        }

        $apply = $this->Staff->findById($detail['Applicant']['staff_id']);
        if(!empty($apply))
        {
            $detail['Staff'] = $apply['Staff'];
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->FamilyPass->validate = array(
                                                'note' => array(
                                                    'notBlank' => array(
                                                            'rule' => 'notBlank',
                                                            'message' => 'You need to leave a note!'
                                                        )
                                                ),
                                            );   

            switch ($data['Submit']) 
            {
                case 'draf':

                    $this->FamilyPass->set($data);
                    if($this->FamilyPass->validates())
                    {
                        $data['FamilyPass']['status_id'] = 2;  
                        $data['FamilyPass']['modified_by'] = $staff['Staff']['id'];                
                        $data['FamilyPass']['modified'] = date('Y-m-d H:i:s');

                        $this->FamilyPass->create();
                        $this->FamilyPass->save($data);

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'view/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully verified.', 'error');
                    }

                    break;

                case 'close':

                    $this->FamilyPass->set($data);
                    if($this->FamilyPass->validates())
                    {
                        $data['FamilyPass']['is_active'] = 99;  
                        $data['FamilyPass']['family_pass_status_id'] = 2;  
                        $data['FamilyPass']['family_pass_pick_id'] = $staff['Staff']['id'];

                        $this->FamilyPass->create();
                        $this->FamilyPass->save($data);

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'view/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully verified.', 'error');
                    }

                    break;

                }
        }
        
        switch ($detail['FamilyPass']['modul_id']) 
        {
            case 17:// spouse
                $spouse = $this->Spouse->findById($detail['Applicant']['key_id']);

                if(empty($spouse))
                {
                    $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
                    $this->redirect('/');
                }
                else
                {

                    $detail['Spouse'] = $spouse['Spouse'];
                    $detail['Spouse']['marriage_date'] =  date("d-m-Y", strtotime($detail['Spouse']['marriage_date']));
                    
                    $detail['Attachment'] = $spouse['Attachment'];
                }

                break;

            case 20:// spouse
                $children = $this->Children->findById($detail['Applicant']['key_id']);

                if(empty($children))
                {
                    $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
                    $this->redirect('/');
                }
                else
                {

                    $detail['Children'] = $children['Children'];
                    $detail['Children']['date_of_birth'] =  date("d-m-Y", strtotime($detail['Children']['date_of_birth']));
                    
                    $detail['Attachment'] = $children['Attachment'];
                }

                break;
        }

        $this->request->data = $detail; 

        if(!empty($detail))
        {
            $detail['FamilyPass']['id'] = $this->Utility->encrypt($detail['FamilyPass']['id'], 'fmlypss');

            $detail['FamilyPass']['day_by_text'] = date('D', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['day_by_num'] = date('d', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['month'] = date('m', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['year'] = date('Y', strtotime($detail['FamilyPass']['created']));

            $detail['FamilyPass']['hour'] = date('h', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['minute'] = date('i', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['format'] = date('A', strtotime($detail['FamilyPass']['created']));
        }

        $familypasstypes = $this->FamilyPassType->find('list');
        $familypassreasons = $this->FamilyPassReason->find('list');
        $organisations = $this->Organisation->find('list');

		$days = array(
			'Mon' => 'Mon',
			'Tue' => 'Tue',
			'Wed' => 'Wed',
			'Thu' => 'Thu',
			'Fri' => 'Fri',
			'Sat' => 'Sat',
			'Sun' => 'Sun',
		);

        $disabled = '';
        if($detail['FamilyPass']['status_id'] > 2)
        {
            $disabled =  'disabled';
        }

        $approved =  false;
        if($detail['FamilyPass']['status_id'] == 2)
        {
            $approved =  true;
        }

        $pickups = false;
        if($detail['FamilyPass']['status_id'] == 10)
        {
            $pickups =  true;
        }

        $disabled_pickup = '';
        if($detail['FamilyPass']['is_active'] > 1)
        {
            $disabled_pickup =  'disabled';
        }

        $auth = $this->Utility->getUserAuth(54, $person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $nationals = $this->National->find('list');
        $genders = $this->Gender->find('list');

        $path = Router::url('/documents/', true);

        $this->set(compact(
			'key',
			'detail',
			'applyBy',
			'days',
            'approved',
            'pickups',
            'disabled',
            'disabled_pickup',
            'familypasstypes',
            'familypassreasons',
            'races', 
            'religions', 
            'nationals',
            'genders',
            'path',
            'organisations'
		));
    }

    public function approved($key = null)
    {
        $this->loadModel('Staff');
		$this->loadModel('FamilyPass');
        $this->loadModel('Applicant');
        $this->loadModel('FamilyPassType');
        $this->loadModel('FamilyPassReason');
        $this->loadModel('Notification');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'fmlypss');

        $detail = $this->FamilyPass->findById($id);

        $auth = $this->Utility->getUserAuth(54, $person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

       
        $this->FamilyPass->validate = array(
                                        'family_pass_serial_no' => array(
                                            'notBlank' => array(
                                                'rule' => 'notBlank',
                                                'message' => 'The Serial No. field is required.'
                                            ),
                                        ),
                                        'note' => array(
                                            'notBlank' => array(
                                                    'rule' => 'notBlank',
                                                    'message' => 'You need to leave a note!'
                                                )
                                        ),
                                    );        

        $this->FamilyPass->set($detail);
        if($this->FamilyPass->validates())
        {
            $data = array();

            $data['FamilyPass']['id'] = $detail['FamilyPass']['id'];
            $data['FamilyPass']['status_id'] = 10;
            $data['FamilyPass']['is_active'] = 1;
            $data['FamilyPass']['family_pass_status_id'] = 1;
            $data['FamilyPass']['approved_by'] = $staff['Staff']['id'];
            $data['FamilyPass']['approved'] = date('Y-m-d H:i:s');

            $this->FamilyPass->create();
            $this->FamilyPass->save($data);

            $subject = 'Your Family Pass for Reference No. '. $detail['Applicant']['reference_no'] . " have successfully approved.";
            $body = $detail['FamilyPass']['note'];
            $body .= "<br/>";
            $body .= "Please collect your Family Pass at Prasarana HR in Bangsar.";
            $body .= "<br/>";
            $body .= "Click <a href='".Router::url('/MyFamilyPasses/view/'. $key , true)."'>here</a> to view detail.";
    
            $notification = array();

            $notification['Notification']['sender_id'] = $staff['Staff']['id'];
            $notification['Notification']['recipient_id'] = $detail['Applicant']['staff_id'];
            $notification['Notification']['label'] = 'info-warning';
            $notification['Notification']['proses'] = 1;
            $notification['Notification']['is_read'] = 1;
            $notification['Notification']['subject'] = $subject;
            $notification['Notification']['body'] = $body;
            $notification['Notification']['created_by'] = $staff['Staff']['id'];
            $notification['Notification']['modified_by'] = $staff['Staff']['id'];

            $this->Notification->create();
            $this->Notification->save($notification);

            $this->Session->setFlash('Information succesfully update.', 'success');
            $this->redirect(array('action' => 'view/'.$key));
        }
        else
        {
            $this->Session->setFlash('Error! Information not successfully verified.', 'error');
            $this->redirect(array('action' => 'view/'.$key));
        }
    }

    public function reject($key = null)
    {
        $this->loadModel('Staff');
		$this->loadModel('FamilyPass');
        $this->loadModel('Applicant');
        $this->loadModel('FamilyPassType');
        $this->loadModel('FamilyPassReason');
        $this->loadModel('Notification');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'fmlypss');

        $detail = $this->FamilyPass->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $auth = $this->Utility->getUserAuth(54, $person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }
        
        $this->FamilyPass->validate = array(
                                        'note' => array(
                                            'notBlank' => array(
                                                    'rule' => 'notBlank',
                                                    'message' => 'You need to leave a note!'
                                                )
                                        ),
                                    );

        $this->FamilyPass->set($detail);
        if($this->FamilyPass->validates())
        {
            $data = array();

            $data['FamilyPass']['id'] = $detail['FamilyPass']['id'];
            $data['FamilyPass']['status_id'] = 12;
            $data['FamilyPass']['is_active'] = 99;
            $data['FamilyPass']['approved_by'] = $staff['Staff']['id'];
            $data['FamilyPass']['approved'] = date('Y-m-d H:i:s');

            $data['FamilyPass']['family_pass_serial_no'] = null;

            $this->FamilyPass->create();
            $this->FamilyPass->save($data);

            $subject = 'Your Family Pass for Reference No. '. $detail['Applicant']['reference_no'] . " have been rejected";
            $body = $detail['FamilyPass']['note'];
            $body .= "<br/>";
            $body .= "Click <a href='".Router::url('/MyFamilyPasses/view/'. $key , true)."'>here</a> to view detail.";
    
            $notification = array();

            $notification['Notification']['sender_id'] = $staff['Staff']['id'];
            $notification['Notification']['recipient_id'] = $detail['Applicant']['staff_id'];
            $notification['Notification']['label'] = 'info-warning';
            $notification['Notification']['proses'] = 1;
            $notification['Notification']['is_read'] = 1;
            $notification['Notification']['subject'] = $subject;
            $notification['Notification']['body'] = $body;
            $notification['Notification']['created_by'] = $staff['Staff']['id'];
            $notification['Notification']['modified_by'] = $staff['Staff']['id'];

            $this->Notification->create();
            $this->Notification->save($notification);

            $this->Session->setFlash('Information succesfully updated.', 'success');
            $this->redirect(array('action' => 'view/'.$key));
        }
        else
        {
            $this->Session->setFlash('Error! Information not successfully verified.', 'error');
            $this->redirect(array('action' => 'view/'.$key));
        }
    }
}
