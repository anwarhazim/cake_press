<?php

class AppController extends Controller 
{
	public $components = array(
	    'Session',
	    'Auth' => array(
	        		'loginRedirect' => array('controller' => 'Notifications', 'action' => 'index'),
	        		'logoutRedirect' => array('controller' => 'Users', 'action' => 'login'),
	        		'authError' => 'You must login to access this system.',
					'loginError' => 'Invalid username and password. Please try again!',
					'authenticate' => array(
											'Form' => array(
												'scope' => array('is_active' => 1, 'status_id' => 10)
											),
										),
					),
    );

    public function beforeFilter() 
	{	
	    $user = $this->Auth->user();
	    if($user)
	    {
	    	$this->isAuthorized($user);
		}
	}

    public function isAuthorized($user) 
	{
		// Here is where we should verify the role and give access based on role
		if($user)
		{			
			$this->loadModel('Utility');

			$path = Router::url('/', true);

			$session = $this->Utility->getUserDetails($user['id']);
			$session['Path']['url'] = $path;
			$this->set("session", $session);

		    return true;
		} 
		else 
		{
			return false;
		}
	}
}