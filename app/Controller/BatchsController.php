<?php
class BatchsController extends AppController
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	public $uses = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
	}

	public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $roles = $this->UserRole->getRoleByUserId($staff['Staff']['user_id']);

        $vouchertypes = array();

        if (in_array(17, $roles))//HCD Medical Prasarana
        {
            $vouchertypes[] = 1;
        }

        if (in_array(18, $roles))//HCD Medical Bus
        {
            $vouchertypes[] = 2;
        }

        if (in_array(19, $roles))//HCD Medical Rail
        {
            $vouchertypes[] = 3;
        }

		$conditions = array();

		$conditions['conditions'][] = array(
                                            'Batch.organisation_type_id' => $vouchertypes
											);

        $conditions['order'] = array('Batch.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Batch'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Batch.name LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Batch.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Batch.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Batch'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Batch']['modified'] = date("d-m-Y",strtotime($details[$i]['Batch']['modified']));

            $details[$i]['Batch']['created'] = date("d-m-Y",strtotime($details[$i]['Batch']['created']));

			$details[$i]['Batch']['id'] = $this->Utility->encrypt($details[$i]['Batch']['id'], 'btch');
        }
        
        $auth = $this->Utility->getUserAuth(56,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

		$this->set(compact('details'));
	}

	public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'btch');

        $detail = $this->Batch->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        $createdby = $this->Staff->findStaffSummaryById($detail['CreatedBy']['id']);

        if(!empty($createdby))
        {
            $detail['CreatedBy'] = $createdby['Staff'];

            if(!empty($createdby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$createdby['Staff']['avatar']."'/>";
            }

            $detail['CreatedBy']['avatar'] = $img;
        }

        if(!empty($detail))
        {
            $detail['Batch']['day_by_text'] = date('D', strtotime($detail['Batch']['created']));
            $detail['Batch']['day_by_num'] = date('d', strtotime($detail['Batch']['created']));
            $detail['Batch']['month'] = date('m', strtotime($detail['Batch']['created']));
            $detail['Batch']['year'] = date('Y', strtotime($detail['Batch']['created']));

            $detail['Batch']['hour'] = date('h', strtotime($detail['Batch']['created']));
            $detail['Batch']['minute'] = date('i', strtotime($detail['Batch']['created']));
            $detail['Batch']['format'] = date('A', strtotime($detail['Batch']['created']));
        }

        $this->request->data = $detail;

        $disabled = "disabled";

        $days = array(
                    'Mon' => 'Mon',
                    'Tue' => 'Tue',
                    'Wed' => 'Wed',
                    'Thu' => 'Thu',
                    'Fri' => 'Fri',
                    'Sat' => 'Sat',
                    'Sun' => 'Sun',
                );

        $this->set(compact(
			'key',
            'detail',
			'days',
            'disabled'
		));
    }

    public function edit($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'btch');

        $detail = $this->Batch->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->Batch->set($data);
            if($this->Batch->validates())
            {
                $data['Batch']['modified_by'] = $staff['Staff']['id'];
                $data['Batch']['modified'] = date('Y-m-d H:i:s');

                $this->Batch->create();
                $this->Batch->save($data);

                $this->Session->setFlash('Batch successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }

        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        $createdby = $this->Staff->findStaffSummaryById($detail['CreatedBy']['id']);

        if(!empty($createdby))
        {
            $detail['CreatedBy'] = $createdby['Staff'];

            if(!empty($createdby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$createdby['Staff']['avatar']."'/>";
            }

            $detail['CreatedBy']['avatar'] = $img;
        }

        if(!empty($detail))
        {
            $detail['Batch']['day_by_text'] = date('D', strtotime($detail['Batch']['created']));
            $detail['Batch']['day_by_num'] = date('d', strtotime($detail['Batch']['created']));
            $detail['Batch']['month'] = date('m', strtotime($detail['Batch']['created']));
            $detail['Batch']['year'] = date('Y', strtotime($detail['Batch']['created']));

            $detail['Batch']['hour'] = date('h', strtotime($detail['Batch']['created']));
            $detail['Batch']['minute'] = date('i', strtotime($detail['Batch']['created']));
            $detail['Batch']['format'] = date('A', strtotime($detail['Batch']['created']));
        }
        
        
        $this->request->data = $detail;

        $disabled = "";

        $days = array(
                'Mon' => 'Mon',
                'Tue' => 'Tue',
                'Wed' => 'Wed',
                'Thu' => 'Thu',
                'Fri' => 'Fri',
                'Sat' => 'Sat',
                'Sun' => 'Sun',
            );

        $this->set(compact(
			'key',
            'detail',
			'days',
            'disabled'
		));
    }

    public function lists()
    {
        $this->loadModel('Staff');
        $this->loadModel('Medical');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $key = "";
        if(!empty($this->request->params['named']['key']))
        {
            $key = $this->request->params['named']['key'];
        }

        if(empty($key))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'btch');

        $detail = $this->Batch->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $conditions = array();

		$conditions['conditions'][] = array(
                                            'Medical.batch_id' => $id
											);

        $conditions['order'] = array('Medical.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Medical'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Medical.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Medical.ic LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Medical.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Medical.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Medical'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate('Medical');

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Medical']['modified'] = date("d-m-Y",strtotime($details[$i]['Medical']['modified']));

            $details[$i]['Medical']['created'] = date("d-m-Y",strtotime($details[$i]['Medical']['created']));

			$details[$i]['Medical']['id'] = $this->Utility->encrypt($details[$i]['Medical']['id'], 'mdcl');
        }
        
        $auth = $this->Utility->getUserAuth(56,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

        $this->set(compact(
            'key',
            'details'
		));
    }

    public function medical_view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Medical');
        $this->loadModel('Gender');
        $this->loadModel('Bank');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'mdcl');

        $detail = $this->Medical->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        $applyby = $this->Staff->findStaffSummaryById($detail['Medical']['staff_id']);

        if(!empty($applyby))
        {
            $detail['ApplyBy'] = $applyby['Staff'];

            if(!empty($applyby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
            }

            $detail['ApplyBy']['avatar'] = $img;
        }

        if(!empty($detail['Medical']['dob']))
        {
            $detail['Medical']['dob'] = date("d-m-Y", strtotime($detail['Medical']['dob']));
        }

        $detail['Medical']['batch_id'] = $this->Utility->encrypt($detail['Medical']['batch_id'], 'btch');

        $this->request->data = $detail;

        $disabled = "disabled";

        $genders = $this->Gender->find('list');
        $banks = $this->Bank->find('list');

        $days = array(
                'Mon' => 'Mon',
                'Tue' => 'Tue',
                'Wed' => 'Wed',
                'Thu' => 'Thu',
                'Fri' => 'Fri',
                'Sat' => 'Sat',
                'Sun' => 'Sun',
            );

        $this->set(compact(
			'key',
            'detail',
            'genders',
            'banks',
			'applyby',
			'days',
            'approved',
            'pickups',
            'disabled'
		));
    }

    public function medical_edit($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Medical');
        $this->loadModel('Gender');
        $this->loadModel('Bank');
        $this->loadModel('MedicalStatus');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'mdcl');

        $detail = $this->Medical->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->Medical->set($data);
            if($this->Medical->validates())
            {
                $data['Medical']['modified_by'] = $staff['Staff']['id'];
                $data['Medical']['modified'] = date('Y-m-d H:i:s');

                $this->Medical->create();
                $this->Medical->save($data);
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        $applyby = $this->Staff->findStaffSummaryById($detail['Medical']['staff_id']);

        if(!empty($applyby))
        {
            $detail['ApplyBy'] = $applyby['Staff'];

            if(!empty($applyby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
            }

            $detail['ApplyBy']['avatar'] = $img;
        }

        if(!empty($detail['Medical']['dob']))
        {
            $detail['Medical']['dob'] = date("d-m-Y", strtotime($detail['Medical']['dob']));
        }

        $detail['Medical']['batch_id'] = $this->Utility->encrypt($detail['Medical']['batch_id'], 'btch');
        
        $this->request->data = $detail;

        $disabled = "";

        $genders = $this->Gender->find('list');
        $banks = $this->Bank->find('list');
        $medicalstatuses = $this->MedicalStatus->find('list');

        $days = array(
                'Mon' => 'Mon',
                'Tue' => 'Tue',
                'Wed' => 'Wed',
                'Thu' => 'Thu',
                'Fri' => 'Fri',
                'Sat' => 'Sat',
                'Sun' => 'Sun',
            );

        $this->set(compact(
			'key',
            'detail',
            'genders',
            'banks',
            'applyby',
            'medicalstatuses',
			'days',
            'approved',
            'pickups',
            'disabled'
		));
    }

    public function download($key=null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Medical');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'btch');

        $detail = $this->Batch->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $details = $this->Medical->find('all', array(
                                                    'conditions' => array(
                                                                        'Medical.batch_id' => $id,
                                                                        'Medical.is_active' => 1,
                                                                    )
                                        ));
        
        $temps = array();

        $temps[0] = array(
            'status' =>  "Status",
            'policy_no' =>  "Policy No",
            'effective_date' =>  "Seniority Date (MM/DD/YYYY)",
            'expiry_date' =>  "Expiry Date",
            'agent_code' =>  "Agent Code",
            'corporate_name' =>  "Corporate Name",
            'emp_no' =>  "Emp No",
            'emp_name'  =>  "Emp Name",
            'emp_ic'  =>  "Emp IC",
            'emp_dob'  =>  "Emp DOB",
            'emp_gender'  =>  "Emp Gender",
            'benefit_plan'  =>  "Benefit Plan",
            'cost_centre' => "Cost Centre",
            'branch'  =>  "Branch",
            'department'  =>  "Department",
            'entitled_date'  =>  "Entitled Date (MM/DD/YYYY)",
            'resigned_date'  =>  "Resigned Date (MM/DD/YYYY)", 
            'member_name'  =>  "Member Name",
            'member_ic'  =>  "Member IC",
            'member_dob'  =>  "Member DOB",
            'member_gender'  =>  "Member Gender",
            'relationship'  =>  "Relationship (Employee/ Spouse/ Son/ Daughter)",
            'bank_name'  =>  "Bank Name",
            'bank_account_no'  =>  "Bank Account No.",
            'email'  =>  "Email",
            'remarks'  =>  "Remarks",
        );

        if(!empty($details))
        {
            $i = 1;
            foreach ($details as $medical) 
            {
                $emp = $this->Staff->findById($medical['Medical']['staff_id']);

                $start_date = "";
                $end_date = "";
                $member_dob = "";
                $bank_name = "";

                if(!empty($emp['Staff']['job_start_date']))
                {
                    $start_date = date("m/d/Y",strtotime($emp['Staff']['job_start_date'])); 
                }

                if(!empty($emp['Staff']['job_end_date']))
                {
                    $end_date = date("m/d/Y",strtotime($emp['Staff']['job_end_date']));
                }

                if(!empty($medical['Medical']['dob']))
                {
                    $member_dob = date("m/d/Y",strtotime($medical['Medical']['dob']));
                }

                if(!empty($medical['Bank']['name']))
                {
                    $bank_name = $medical['Bank']['name'];
                }
                
                $temps[$i]['status'] = $medical['MedicalStatus']['name'];
                $temps[$i]['policy_no'] = "";
                $temps[$i]['effective_date'] =  "";
                $temps[$i]['expiry_date'] =  "";
                $temps[$i]['agent_code'] =  "";
                $temps[$i]['corporate_name'] = $emp['OrganisationType']['name'];
                $temps[$i]['emp_no'] = $emp['Staff']['staff_no'];
                $temps[$i]['emp_name'] = $emp['Staff']['name'];
                $temps[$i]['emp_ic'] = $emp['Staff']['ic_no']; 
                $temps[$i]['emp_dob'] = date("m/d/Y",strtotime($emp['Staff']['date_of_birth']));  
                $temps[$i]['emp_gender'] = $emp['Gender']['name']; 
                $temps[$i]['benefit_plan'] = "";
                $temps[$i]['cost_centre'] = "";
                $temps[$i]['branch'] = $emp['Division']['name'];
                $temps[$i]['department'] = $emp['Department']['name']; 
                $temps[$i]['entitled_date'] = $start_date; 
                $temps[$i]['resigned_date'] = $end_date; 
                $temps[$i]['member_name'] = $medical['Medical']['name'];
                $temps[$i]['member_ic'] = $medical['Medical']['ic'];
                $temps[$i]['member_dob'] = $member_dob;
                $temps[$i]['member_gender'] = $medical['Gender']['name'];
                $temps[$i]['relationship'] = $medical['MedicalRelationship']['name'];
                $temps[$i]['bank_name'] = $bank_name;
                $temps[$i]['bank_account_no'] = $medical['Medical']['bank_account_no'];
                $temps[$i]['email']  = $medical['Medical']['email'];
                $temps[$i]['remarks']  = $medical['Medical']['remarks']; 

                $i++;
            }
        }

        $this->response->download($detail['Batch']['name'].".csv");
		
		$this->set(compact('temps'));

		$this->layout = 'ajax';
    }
}
