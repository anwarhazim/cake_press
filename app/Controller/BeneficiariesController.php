<?php
class BeneficiariesController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Beneficiary.staff_id' => $staff['Staff']['id'],
                                            'Beneficiary.is_delete' => 99,
                                        );

        $conditions['order'] = array('Beneficiary.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Beneficiary'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Beneficiary.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Beneficiary.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Beneficiary.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Beneficiary'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Beneficiary']['modified'] = date("d-m-Y",strtotime($details[$i]['Beneficiary']['modified']));

            $details[$i]['Beneficiary']['created'] = date("d-m-Y",strtotime($details[$i]['Beneficiary']['created']));

            $details[$i]['Beneficiary']['id'] = $this->Utility->encrypt($details[$i]['Beneficiary']['id'], 'bnf');
            
        }

        $this->set(compact('details'));
    }

    public function add()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $detail = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Beneficiary']['staff_id'] = $detail['Staff']['id'];
            $data['Beneficiary']['is_delete'] = 99;
            $data['Beneficiary']['modified_by'] = $detail['Staff']['id'];
            $data['Beneficiary']['modified'] = date('Y-m-d H:i:s');
            $data['Beneficiary']['created_by'] = $detail['Staff']['id'];
            $data['Beneficiary']['created'] = date('Y-m-d H:i:s');

            $this->Beneficiary->create();
            if($this->Beneficiary->save($data))
            {
                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }
    }

    public function view($key = null)
    {
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bnf');

        $detail = $this->Beneficiary->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            $this->request->data = $detail;
        }

        $disabled = 'disabled';

        $this->set(compact('key', 'disabled'));
    }

    public function edit($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bnf');

        $detail = $this->Beneficiary->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Beneficiary']['id'] = $id;
            $data['Beneficiary']['modified_by'] = $staff['Staff']['id'];
            $data['Beneficiary']['modified'] = date('Y-m-d H:i:s');

            $this->Beneficiary->create();
            if($this->Beneficiary->save($data))
            {
                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }

        $this->set(compact('key'));
    }

    public function delete($key=null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bnf');

        $detail = $this->Beneficiary->findById($id);

        //$directory = $path.$detail['Attachment']['path'];

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            $data = array();

            $data['Beneficiary']['id'] = $id;
            $data['Beneficiary']['is_delete'] = 1;
            $data['Beneficiary']['modified_by'] = $staff['Staff']['id'];
            $data['Beneficiary']['modified'] = date('Y-m-d H:i:s');

            $this->Beneficiary->create();
            if($this->Beneficiary->save($data))
            {
                $this->Session->setFlash('Information successfully deleted.', 'success');
                $this->redirect(array('action' => '/'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully deleted. Please try again!', 'error');
                $this->redirect(array('action' => '/'));
            }

        }
    }
}