<?php
class StaffsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'view', 'administrator', 'edit', 'delete');
    }

    public function index()
    {
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Staff.is_active' => 1,
                                        );

        $conditions['order'] = array('User.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Staff'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Staff.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Staff.ic_no LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Staff.staff_no LIKE' => '%' . $value . '%')
                        );
						
						$conditions['conditions']['OR'][] = array(
                            array('Staff.email LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Staff.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Staff.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Staff'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Staff');

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Staff']['modified'] = date("d-m-Y",strtotime($details[$i]['User']['modified']));

            $details[$i]['Staff']['created'] = date("d-m-Y",strtotime($details[$i]['User']['created']));

            $details[$i]['Staff']['id'] = $this->Utility->encrypt($details[$i]['Staff']['id'], 'stf');
        }

        $this->set(compact('details'));
    }

    public function view($key)
    {
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('JobDesignation');
        $this->loadModel('OrganisationType');
        $this->loadModel('JobType');
        $this->loadModel('Location');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            $detail['Staff']['date_of_birth'] =  date("d-m-Y", strtotime($detail['Staff']['date_of_birth']));
            $this->request->data = $detail;
        }

        $disabled = "disabled";

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $maritalstatus = $this->MaritalStatus->find('list');
        $banks = $this->Bank->find('list');
        $jobdesignations = $this->JobDesignation->find('list');
        $organisationtypes = $this->OrganisationType->find('list');
        $jobtypes = $this->JobType->find('list');
        $locations = $this->Location->find('list');

        $this->set(compact('key', 'disabled', 'genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks', 'jobdesignations', 'organisationtypes', 'jobtypes', 'locations'));        
    }

    public function administrator($key)
    {
        $this->loadModel('Status');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->Staff->set($data);
            if($this->Staff->validates())
            {
                $data['Staff']['id'] = $id;
                switch ($data['Staff']['status_id']) 
                {
                    case 3:
                        $data['Staff']['assign'] = date('Y-m-d H:i:s');
                        break;
                    case 4:
                        $data['Staff']['verified'] = date('Y-m-d H:i:s');
                        break;
                    case 5:
                        $data['Staff']['approved'] = date('Y-m-d H:i:s');
                        break;
                    default:
                        $data['Staff']['modified'] = date('Y-m-d H:i:s');
                        break;
                }

                $this->Staff->create();
                $this->Staff->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'administrator/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }

        }
        else
        {
            $this->request->data = $detail;
        }

        $statuses = $this->Status->find('list', array(
                                                    'conditions' => array('id' => array(1, 10, 11))
                                                ));

        $this->set(compact('key', 'statuses'));        
    }

    public function edit($key)
    {
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('JobDesignation');
        $this->loadModel('OrganisationType');
        $this->loadModel('JobType');
        $this->loadModel('Location');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Staff']['id'] = $id;                
            $data['Staff']['modified'] = date('Y-m-d H:i:s');

            $this->Staff->set($data);
            if($this->Staff->validates())
            {
                $this->Staff->create();
                $this->Staff->save($data);

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        else
        {
            $detail['Staff']['date_of_birth'] =  date("d-m-Y", strtotime($detail['Staff']['date_of_birth']));
            $this->request->data = $detail;
        }

        $disabled = "disabled";

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $maritalstatus = $this->MaritalStatus->find('list');
        $banks = $this->Bank->find('list');
        $jobdesignations = $this->JobDesignation->find('list');
        $organisationtypes = $this->OrganisationType->find('list');
        $jobtypes = $this->JobType->find('list');
        $locations = $this->Location->find('list');

        $this->set(compact('key', 'disabled', 'genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks', 'jobdesignations', 'organisationtypes', 'jobtypes', 'locations'));        
    }

    public function add()
    {
        $this->loadModel('User');
        $this->loadModel('UserRole');
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('JobDesignation');
        $this->loadModel('OrganisationType');
        $this->loadModel('JobType');
        $this->loadModel('Location');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->Staff->set($data);
            if($this->Staff->validates())
            {
                $password = 'Pass@word1';

                $user = array();
                $user['User']['username'] = $data['Staff']['staff_no'];
                $user['User']['password'] = $password;
                $user['User']['status_id'] = 1;
                $user['User']['is_active'] = 1; 

                $this->User->create();
                $this->User->save($user);
                $user_id = $this->User->id;

                $userrole = array();
                $userrole['UserRole']['user_id'] = $user_id;
                $userrole['UserRole']['role_id'] = 2;
                $userrole['UserRole']['created_by'] = $staff['Staff']['id'];
                $userrole['UserRole']['modified_by'] = $staff['Staff']['id'];
                $this->UserRole->create();
                $this->UserRole->save($userrole);

                $data['Staff']['user_id'] = $user_id;
                $data['Staff']['is_flag'] = 1;
                $data['Staff']['is_type'] = 99;
                $data['Staff']['status_id'] = 1;
                $data['Staff']['is_active'] = 1;
                $data['Staff']['is_agreed'] = 1;
                $data['Staff']['agreed'] = date('Y-m-d H:i:s');

                $this->Staff->create();
                $this->Staff->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $maritalstatus = $this->MaritalStatus->find('list');
        $banks = $this->Bank->find('list');
        $jobdesignations = $this->JobDesignation->find('list');
        $organisationtypes = $this->OrganisationType->find('list');
        $jobtypes = $this->JobType->find('list');
        $locations = $this->Location->find('list');

        $this->set(compact('genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks', 'jobdesignations', 'organisationtypes', 'jobtypes', 'locations'));
    }

    public function delete($key)
    {
        $this->loadModel('Utility');

        $this->autoRender = false;

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $data['Staff']['id'] = $detail['Staff']['id'];
        $data['Staff']['is_active'] = 99;

        $this->Staff->create();
        $this->Staff->save($data);

        $this->Session->setFlash('Information successfully deleted!', 'success');
        $this->redirect(array('action' => '/'));
    }

    public function profile()
    {
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            $staff['Staff']['date_of_birth'] =  date("d-m-Y", strtotime($staff['Staff']['date_of_birth']));
            $this->request->data = $staff;
        }

        $disabled = "disabled";

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $maritalstatus = $this->MaritalStatus->find('list');
        $banks = $this->Bank->find('list');

        $update = $this->Staff->findIfUpdateByStaffId($staff['Staff']['id']);

        $this->set(compact('key', 'disabled', 'update', 'genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks'));        
    }

    public function update_staff()
    {
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Staff']['id'] = $staff['Staff']['id'];

            $this->Staff->set($data);
            if($this->Staff->validates())
            {
                if($data['Staff']['is_flag'] == 99)
                {
                    $history = array();
                    $exclude = array('id', 'lft', 'rght');

                    foreach ($data['Staff'] as $index => $value) 
                    {
                        if(!in_array($index, $exclude))
                        {
                            $history['Staff'][$index] = $value;
                        }
                        $exclude[] = $index;
                    }

                    foreach ($staff['Staff'] as $index => $value) 
                    {
                        if(!in_array($index, $exclude))
                        {
                            $history['Staff'][$index] = $value;
                        }
                    }

                    $history['Staff']['parent_id'] = $data['Staff']['id'];
                    $history['Staff']['is_active'] = 1;
                    $history['Staff']['is_flag'] = 1;
                    $history['Staff']['status_id'] = 2;
                    $history['Staff']['created_by'] = $staff['Staff']['id'];
                    $history['Staff']['created'] = date('Y-m-d H:i:s');
                    $history['Staff']['modified_by'] = $staff['Staff']['id'];
                    $history['Staff']['modified'] = date('Y-m-d H:i:s');

                    //remove validation
                    unset($this->Staff->validate['staff_no']);
                    unset($this->Staff->validate['ic_no']);
                    unset($this->Staff->validate['email']);

                    //$this->Staff->recover();

                    $this->Staff->create();
                    $this->Staff->save($history);

                    $staff_id = $this->Staff->id;

                    // reset //
                    $data = array();
                    $data['Staff']['id'] = $staff['Staff']['id'];
                    $data['Staff']['is_active'] = 99;
                    $data['Staff']['modified_by'] = $staff['Staff']['id'];
                    $data['Staff']['modified'] = date('Y-m-d H:i:s');

                    $this->Staff->create();
                    $this->Staff->save($data);

                    /* start spouse */
                        $spouses = $this->Spouse->find('all', array(
                                                                    'conditions' => array('Spouse.staff_id' => $staff['Staff']['id'], 'Spouse.is_active' => 1)
                                                                ));

                        if(!empty($spouses))
                        {
                            foreach ($spouses as $spouse) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($spouse['Spouse'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Spouse'][$index] = $value;
                                    }
                                }

                                $history['Spouse']['staff_id'] = $staff_id;
                                $history['Spouse']['parent_id'] = $spouse['Spouse']['id'];
                                $history['Spouse']['is_active'] = 1;

                                //remove validation
                                unset($this->Spouse->validate['ic_no']);

                                //$this->Children->recover();

                                $this->Spouse->create();
                                $this->Spouse->save($history);
                                $spouse_id = $this->Spouse->id;

                                $spouse['Spouse']['is_active'] = 99;
                                $spouse['Spouse']['modified_by'] = $staff['Staff']['id'];
                                $spouse['Spouse']['modified'] = date('Y-m-d H:i:s');

                                $this->Spouse->create();
                                $this->Spouse->save($spouse);

                                foreach ($spouse['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $spouse_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end spouse */

                    /* start Children */
                        $childrens = $this->Children->find('all', array(
                                                                    'conditions' => array('Children.staff_id' => $staff['Staff']['id'], 'Children.is_active' => 1)
                                                                ));

                        if(!empty($childrens))
                        {
                            foreach ($childrens as $children) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($children['Children'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Children'][$index] = $value;
                                    }
                                }

                                $history['Children']['staff_id'] = $staff_id;
                                $history['Children']['parent_id'] = $children['Children']['id'];
                                $history['Children']['is_active'] = 1;

                                //remove validation
                                unset($this->Children->validate['ic']);

                                //$this->Children->recover();

                                $this->Children->create();
                                $this->Children->save($history);
                                $children_id = $this->Children->id;

                                $children['Children']['is_active'] = 99;
                                $children['Children']['modified_by'] = $staff['Staff']['id'];
                                $children['Children']['modified'] = date('Y-m-d H:i:s');

                                $this->Children->create();
                                $this->Children->save($children);

                                foreach ($children['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $children_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Children */
                    /* start Death */
                        $deaths = $this->Death->find('all', array(
                                                                    'conditions' => array('Death.staff_id' => $staff['Staff']['id'], 'Death.is_active' => 1)
                                                                ));

                        if(!empty($deaths))
                        {
                            foreach ($deaths as $death) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($death['Death'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Death'][$index] = $value;
                                    }
                                }

                                $history['Death']['staff_id'] = $staff_id;
                                $history['Death']['parent_id'] = $death['Death']['id'];
                                $history['Death']['is_active'] = 1;

                                //remove validation
                                unset($this->Death->validate['ic']);

                                //$this->Death->recover();

                                $this->Death->create();
                                $this->Death->save($history);
                                $death_id = $this->Death->id;

                                $death['Death']['is_active'] = 99;
                                $death['Death']['modified_by'] = $staff['Staff']['id'];
                                $death['Death']['modified'] = date('Y-m-d H:i:s');

                                $this->Death->create();
                                $this->Death->save($death);

                                foreach ($death['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $death_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Death */
                    /* start Education */
                        $educations = $this->Education->find('all', array(
                                                                    'conditions' => array('Education.staff_id' => $staff['Staff']['id'], 'Education.is_active' => 1)
                                                                ));

                        if(!empty($educations))
                        {
                            foreach ($educations as $education) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($education['Education'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Education'][$index] = $value;
                                    }
                                }

                                $history['Education']['staff_id'] = $staff_id;
                                $history['Education']['parent_id'] = $education['Education']['id'];
                                $history['Education']['is_active'] = 1;

                                //remove validation
                                unset($this->Education->validate['ic']);

                                //$this->Education->recover();

                                $this->Education->create();
                                $this->Education->save($history);
                                $education_id = $this->Education->id;

                                $education['Education']['is_active'] = 99;
                                $education['Education']['modified_by'] = $staff['Staff']['id'];
                                $education['Education']['modified'] = date('Y-m-d H:i:s');

                                $this->Education->create();
                                $this->Education->save($education);

                                foreach ($education['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $education_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Education */
                    /* start Skill */
                        $skills = $this->Skill->find('all', array(
                                                                    'conditions' => array('Skill.staff_id' => $staff['Staff']['id'], 'Skill.is_active' => 1)
                                                                ));

                        if(!empty($skills))
                        {
                            foreach ($skills as $skill) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($skill['Skill'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Skill'][$index] = $value;
                                    }
                                }

                                $history['Skill']['staff_id'] = $staff_id;
                                $history['Skill']['parent_id'] = $skill['Skill']['id'];
                                $history['Skill']['is_active'] = 1;

                                //remove validation
                                unset($this->Skill->validate['ic']);

                                //$this->Skill->recover();

                                $this->Skill->create();
                                $this->Skill->save($history);
                                $skill_id = $this->Skill->id;

                                $skill['Skill']['is_active'] = 99;
                                $skill['Skill']['modified_by'] = $staff['Staff']['id'];
                                $skill['Skill']['modified'] = date('Y-m-d H:i:s');

                                $this->Skill->create();
                                $this->Skill->save($skill);

                                foreach ($education['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $skill_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Skill */
                }
                else
                {
                    $data['Staff']['status_id'] = 2;
                    $data['Staff']['modified_by'] = $staff['Staff']['id'];
                    $data['Staff']['modified'] = date('Y-m-d H:i:s');

                    $this->Staff->create();
                    $this->Staff->save($data);
                }

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'update_staff'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        else
        {
            $staff['Staff']['date_of_birth'] =  date("d-m-Y", strtotime($staff['Staff']['date_of_birth']));
            $this->request->data = $staff;
        }

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');

        if($staff['Staff']['marital_status_id'] == 1)
        {
            $maritalstatus = $this->MaritalStatus->find('list');
        }
        else
        {
            $maritalstatus = $this->MaritalStatus->find('list', array(
                                                                'conditions' => array('id !=' => 1)
                                                            ));
        }

        $banks = $this->Bank->find('list');

        $update = $this->Staff->findIfUpdateByStaffId($staff['Staff']['id']);
        if($update == false)
        {
            $this->Session->setFlash('You cannot make any changes within this period. Please contact system administrator for help.', 'info');
            $this->redirect(array('action' => 'profile'));
        }

        $this->set(compact('genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks'));        
    }

    public function employment()
    {
        $this->loadModel('JobDesignation');
        $this->loadModel('JobGrade');
        $this->loadModel('OrganisationType');
        $this->loadModel('Organisation');
        $this->loadModel('JobType');
        $this->loadModel('Location');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $detail = $this->Staff->findStaffByUserId($person['id']);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            if(!empty($detail['Staff']['entry_date']))
            {
                $detail['Staff']['entry_date'] =  date("d-m-Y", strtotime($detail['Staff']['entry_date']));
            }

            if(!empty($detail['Staff']['job_start_date']))
            {
                $detail['Staff']['job_start_date'] =  date("d-m-Y", strtotime($detail['Staff']['job_start_date']));
            }

            if(!empty($detail['Staff']['job_end_date']))
            {
                $detail['Staff']['job_end_date'] =  date("d-m-Y", strtotime($detail['Staff']['job_end_date']));
            }

            $this->request->data = $detail;
        }

        $disabled = "disabled";

        $jobdesignations = $this->JobDesignation->find('list');
        $jobgrades = $this->JobGrade->find('list');
        $organisationtypes = $this->OrganisationType->find('list');

        $organisations = $this->Organisation->find('list', array(
                                                            'conditions' => array('is_active' => 1, 'organisation_type_id ' => $detail['Staff']['organisation_type_id'] , 'organisation_category_id' => 1)
                                                        ));

        $divisions = $this->Organisation->find('list', array(
                                                            'conditions' => array('is_active' => 1, 'organisation_type_id ' => $detail['Staff']['organisation_type_id'], 'organisation_category_id' => 2)
                                                        ));

        $departments = $this->Organisation->find('list', array(
                                                        'conditions' => array('is_active' => 1, 'organisation_type_id ' => $detail['Staff']['organisation_type_id'], 'organisation_category_id' => 3)
                                                    ));

        $sections = $this->Organisation->find('list', array(
                                                        'conditions' => array('is_active' => 1, 'organisation_type_id ' => $detail['Staff']['organisation_type_id'], 'organisation_category_id' => 4)
                                                    ));

        $units = $this->Organisation->find('list', array(
                                                        'conditions' => array('is_active' => 1, 'organisation_type_id ' => $detail['Staff']['organisation_type_id'], 'organisation_category_id' => 5)
                                                    ));
        
        $subunits = $this->Organisation->find('list', array(
                                                        'conditions' => array('is_active' => 1, 'organisation_type_id ' => $detail['Staff']['organisation_type_id'], 'organisation_category_id' => 6)
                                                    ));

        $jobtypes = $this->JobType->find('list');
        $locations = $this->Location->find('list');

        $this->set(compact('disabled', 'jobdesignations', 'jobgrades', 'organisationtypes', 'organisations', 'divisions', 'departments', 'sections', 'units', 'subunits', 'jobtypes', 'locations'));        

    }

    public function contact()
    {
        $this->loadModel('State');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            $this->request->data = $staff;
        }

        $disabled = "disabled";

        $states = $this->State->find('list');

        $update = false;

        $update = $this->Staff->findIfUpdateByStaffId($staff['Staff']['id']);

        $this->set(compact('disabled', 'states', 'update'));        
    }

    public function update_contact()
    {
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Staff']['id'] = $staff['Staff']['id'];

            $this->Staff->set($data);
            if($this->Staff->validates())
            {
                if($data['Staff']['is_flag'] == 99)
                {
                    $history = array();
                    $exclude = array('id', 'lft', 'rght');

                    foreach ($data['Staff'] as $index => $value) 
                    {
                        if(!in_array($index, $exclude))
                        {
                            $history['Staff'][$index] = $value;
                        }
                        $exclude[] = $index;
                    }

                    foreach ($staff['Staff'] as $index => $value) 
                    {
                        if(!in_array($index, $exclude))
                        {
                            $history['Staff'][$index] = $value;
                        }
                    }

                    $history['Staff']['parent_id'] = $data['Staff']['id'];
                    $history['Staff']['is_active'] = 1;
                    $history['Staff']['is_flag'] = 1;
                    $history['Staff']['status_id'] = 2;
                    $history['Staff']['created_by'] = $staff['Staff']['id'];
                    $history['Staff']['created'] = date('Y-m-d H:i:s');
                    $history['Staff']['modified_by'] = $staff['Staff']['id'];
                    $history['Staff']['modified'] = date('Y-m-d H:i:s');

                    //remove validation
                    unset($this->Staff->validate['staff_no']);
                    unset($this->Staff->validate['ic_no']);
                    unset($this->Staff->validate['email']);

                    $this->Staff->recover();

                    $this->Staff->create();
                    $this->Staff->save($history);

                    $staff_id = $this->Staff->id;

                    // reset //
                    $data = array();
                    $data['Staff']['id'] = $staff['Staff']['id'];
                    $data['Staff']['is_active'] = 99;
                    $data['Staff']['modified_by'] = $staff['Staff']['id'];
                    $data['Staff']['modified'] = date('Y-m-d H:i:s');

                    $this->Staff->create();
                    $this->Staff->save($data);

                    /* start Children */
                        $childrens = $this->Children->find('all', array(
                                                                    'conditions' => array('Children.staff_id' => $staff['Staff']['id'], 'Children.is_active' => 1)
                                                                ));

                        if(!empty($childrens))
                        {
                            foreach ($childrens as $children) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($children['Children'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Children'][$index] = $value;
                                    }
                                }

                                $history['Children']['staff_id'] = $staff_id;
                                $history['Children']['parent_id'] = $children['Children']['id'];
                                $history['Children']['is_active'] = 1;

                                //remove validation
                                unset($this->Children->validate['ic']);

                                $this->Children->recover();

                                $this->Children->create();
                                $this->Children->save($history);
                                $children_id = $this->Children->id;

                                $children['Children']['is_active'] = 99;
                                $children['Children']['modified_by'] = $staff['Staff']['id'];
                                $children['Children']['modified'] = date('Y-m-d H:i:s');

                                $this->Children->create();
                                $this->Children->save($children);

                                foreach ($children['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $children_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Children */
                    /* start Death */
                        $deaths = $this->Death->find('all', array(
                                                                    'conditions' => array('Death.staff_id' => $staff['Staff']['id'], 'Death.is_active' => 1)
                                                                ));

                        if(!empty($deaths))
                        {
                            foreach ($deaths as $death) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($death['Death'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Death'][$index] = $value;
                                    }
                                }

                                $history['Death']['staff_id'] = $staff_id;
                                $history['Death']['parent_id'] = $death['Death']['id'];
                                $history['Death']['is_active'] = 1;

                                //remove validation
                                unset($this->Death->validate['ic']);

                                $this->Death->recover();

                                $this->Death->create();
                                $this->Death->save($history);
                                $death_id = $this->Death->id;

                                $death['Death']['is_active'] = 99;
                                $death['Death']['modified_by'] = $staff['Staff']['id'];
                                $death['Death']['modified'] = date('Y-m-d H:i:s');

                                $this->Death->create();
                                $this->Death->save($death);

                                foreach ($death['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $death_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Death */
                    /* start Education */
                        $educations = $this->Education->find('all', array(
                                                                    'conditions' => array('Education.staff_id' => $staff['Staff']['id'], 'Education.is_active' => 1)
                                                                ));

                        if(!empty($educations))
                        {
                            foreach ($educations as $education) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($education['Education'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Education'][$index] = $value;
                                    }
                                }

                                $history['Education']['staff_id'] = $staff_id;
                                $history['Education']['parent_id'] = $education['Education']['id'];
                                $history['Education']['is_active'] = 1;

                                //remove validation
                                unset($this->Education->validate['ic']);

                                $this->Education->recover();

                                $this->Education->create();
                                $this->Education->save($history);
                                $education_id = $this->Education->id;

                                $education['Education']['is_active'] = 99;
                                $education['Education']['modified_by'] = $staff['Staff']['id'];
                                $education['Education']['modified'] = date('Y-m-d H:i:s');

                                $this->Education->create();
                                $this->Education->save($education);

                                foreach ($education['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $education_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Education */
                    /* start Skill */
                        $skills = $this->Skill->find('all', array(
                                                                    'conditions' => array('Skill.staff_id' => $staff['Staff']['id'], 'Skill.is_active' => 1)
                                                                ));

                        if(!empty($skills))
                        {
                            foreach ($skills as $skill) 
                            {
                                $history = array();
                                $exclude = array('id', 'lft', 'rght');

                                foreach ($skill['Skill'] as $index => $value) 
                                {
                                    if(!in_array($index, $exclude))
                                    {
                                        $history['Skill'][$index] = $value;
                                    }
                                }

                                $history['Skill']['staff_id'] = $staff_id;
                                $history['Skill']['parent_id'] = $skill['Skill']['id'];
                                $history['Skill']['is_active'] = 1;

                                //remove validation
                                unset($this->Skill->validate['ic']);

                                $this->Skill->recover();

                                $this->Skill->create();
                                $this->Skill->save($history);
                                $skill_id = $this->Skill->id;

                                $skill['Skill']['is_active'] = 99;
                                $skill['Skill']['modified_by'] = $staff['Staff']['id'];
                                $skill['Skill']['modified'] = date('Y-m-d H:i:s');

                                $this->Skill->create();
                                $this->Skill->save($skill);

                                foreach ($education['Attachment'] as $value) 
                                {
                                    $history = array();
                                    $history['Attachment']['name'] = $value['name'];
                                    $history['Attachment']['path'] = $value['path'];
                                    $history['Attachment']['modul_id'] = $value['modul_id'];
                                    $history['Attachment']['key_id'] = $skill_id;
                                    $history['Attachment']['created_by'] =  $value['created_by'];
                                    $history['Attachment']['created'] =  $value['created'];
                                    $history['Attachment']['modified_by'] = $value['modified_by'];
                                    $history['Attachment']['modified'] =  $value['modified'];

                                    $this->Attachment->create();
                                    $this->Attachment->save($history);
                                }

                            }
                            
                        }
                    /* end Skill */
                }
                else
                {
                    $data['Staff']['modified_by'] = $staff['Staff']['id'];
                    $data['Staff']['modified'] = date('Y-m-d H:i:s');

                    $this->Staff->create();
                    $this->Staff->save($data);
                }
                
                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'update_contact'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        else
        {
            $this->request->data = $staff;
        }

        $states = $this->State->find('list');

        $update = $this->Staff->findIfUpdateByStaffId($staff['Staff']['id']);
        if($update == false)
        {
            $this->Session->setFlash('You cannot make any changes within this period. Please contact system administrator for help.', 'info');
            $this->redirect(array('action' => 'contact'));
        }

        $this->set(compact('states'));        
    }

}