<?php
class MedicalsController extends AppController
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	public $uses = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
	}

	public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $roles = $this->UserRole->getRoleByUserId($staff['Staff']['user_id']);

        $vouchertypes = array();

        if (in_array(17, $roles))//HCD Medical Prasarana
        {
            $vouchertypes[] = 1;
        }

        if (in_array(18, $roles))//HCD Medical Bus
        {
            $vouchertypes[] = 2;
        }

        if (in_array(19, $roles))//HCD Medical Rail
        {
            $vouchertypes[] = 3;
        }

		$conditions = array();

		$conditions['conditions'][] = array(
                                            'Medical.is_flag' => 1,
                                            'Medical.organisation_type_id' => $vouchertypes
											);

        $conditions['order'] = array('Medical.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Medical'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Medical.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Medical.ic LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Medical.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Medical.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Medical'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Medical']['modified'] = date("d-m-Y",strtotime($details[$i]['Medical']['modified']));

            $details[$i]['Medical']['created'] = date("d-m-Y",strtotime($details[$i]['Medical']['created']));

			$details[$i]['Medical']['id'] = $this->Utility->encrypt($details[$i]['Medical']['id'], 'mdcl');
        }
        
        $auth = $this->Utility->getUserAuth(56,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

		$this->set(compact('details'));
	}

	public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Gender');
        $this->loadModel('Bank');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'mdcl');

        $detail = $this->Medical->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        $applyby = $this->Staff->findStaffSummaryById($detail['Medical']['staff_id']);

        if(!empty($applyby))
        {
            $detail['ApplyBy'] = $applyby['Staff'];

            if(!empty($applyby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
            }

            $detail['ApplyBy']['avatar'] = $img;
        }

        if(!empty($detail['Medical']['dob']))
        {
            $detail['Medical']['dob'] = date("d-m-Y", strtotime($detail['Medical']['dob']));
        }

        $this->request->data = $detail;

        $disabled = "disabled";

        $genders = $this->Gender->find('list');
        $banks = $this->Bank->find('list');

        $days = array(
                'Mon' => 'Mon',
                'Tue' => 'Tue',
                'Wed' => 'Wed',
                'Thu' => 'Thu',
                'Fri' => 'Fri',
                'Sat' => 'Sat',
                'Sun' => 'Sun',
            );

        $this->set(compact(
			'key',
            'detail',
            'genders',
            'banks',
			'applyby',
			'days',
            'approved',
            'pickups',
            'disabled'
		));
    }

    public function edit($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Gender');
        $this->loadModel('Bank');
        $this->loadModel('MedicalStatus');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'mdcl');

        $detail = $this->Medical->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->Medical->set($data);
            if($this->Medical->validates())
            {
                $data['Medical']['modified_by'] = $staff['Staff']['id'];
                $data['Medical']['modified'] = date('Y-m-d H:i:s');

                $this->Medical->create();
                $this->Medical->save($data);
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        $applyby = $this->Staff->findStaffSummaryById($detail['Medical']['staff_id']);

        if(!empty($applyby))
        {
            $detail['ApplyBy'] = $applyby['Staff'];

            if(!empty($applyby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
            }

            $detail['ApplyBy']['avatar'] = $img;
        }

        if(!empty($detail['Medical']['dob']))
        {
            $detail['Medical']['dob'] = date("d-m-Y", strtotime($detail['Medical']['dob']));
        }

        $this->request->data = $detail;

        $disabled = "";

        $genders = $this->Gender->find('list');
        $banks = $this->Bank->find('list');
        $medicalstatuses = $this->MedicalStatus->find('list');

        $days = array(
                'Mon' => 'Mon',
                'Tue' => 'Tue',
                'Wed' => 'Wed',
                'Thu' => 'Thu',
                'Fri' => 'Fri',
                'Sat' => 'Sat',
                'Sun' => 'Sun',
            );

        $this->set(compact(
			'key',
            'detail',
            'genders',
            'banks',
            'applyby',
            'medicalstatuses',
			'days',
            'approved',
            'pickups',
            'disabled'
		));
    }

    public function add()
    {
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Batch');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);
        
        $roles = $this->UserRole->getRoleByUserId($staff['Staff']['user_id']);

        $vouchertypes = array();

        if (in_array(17, $roles))//HCD Medical Prasarana
        {
            $vouchertypes[] = 1;
        }

        if (in_array(18, $roles))//HCD Medical Bus
        {
            $vouchertypes[] = 2;
        }

        if (in_array(19, $roles))//HCD Medical Rail
        {
            $vouchertypes[] = 3;
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->Batch->set($data);
            if($this->Batch->validates())
            {
                $data['Batch']['organisation_type_id'] = $staff['Staff']['organisation_type_id'];
                $data['Batch']['modified_by'] = $staff['Staff']['id'];
                $data['Batch']['created_by'] = $staff['Staff']['id'];

                $medicals = $this->Medical->find('all', array(
                                                        'conditions' => array('Medical.is_active' => 1, 
                                                                                'Medical.is_flag' => 1,
                                                                                'Medical.organisation_type_id' => $vouchertypes
                                                                            )
                                                        ));

                if(!empty($medicals))
                {
                    $this->Batch->create();
                    $this->Batch->save($data);
                    
                    $batch_id = $this->Batch->id;

                
                    foreach ($medicals as $medical) 
                    {
                        $mycare = array();

                        $mycare['Medical']['id'] = $medical['Medical']['id'];
                        $mycare['Medical']['batch_id'] = $batch_id;
                        $mycare['Medical']['is_flag'] = 99;

                        $this->Medical->create();
                        $this->Medical->save($mycare);
                    }

                    $this->Session->setFlash('Batch successfully created. Please go to the list of batch to download MiCARE file.', 'success');
                    $this->redirect(array('action' => 'add'));
                }
                else
                {
                    $this->Session->setFlash('Error! No data available. Please try again!', 'error');
                    $this->redirect(array('action' => 'add'));
                }
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }

    }
}
