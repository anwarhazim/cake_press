<?php
class MyMarriedVouchersController extends AppController
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	public $uses = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
	}

	public function index()
    {
		$this->loadModel('Voucher');
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

		$conditions = array();

		$conditions['conditions'][] = array(
                                            'Voucher.Voucher_type_id' => 1,
                                            'Applicant.staff_id' => $staff['Staff']['list_id']
											);

        $conditions['order'] = array('Voucher.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Voucher'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Applicant.reference_no LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Voucher.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Voucher.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Voucher'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate('Voucher');

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Voucher']['modified'] = date("d-m-Y",strtotime($details[$i]['Voucher']['modified']));

            $details[$i]['Voucher']['created'] = date("d-m-Y",strtotime($details[$i]['Voucher']['created']));

			$details[$i]['Voucher']['id'] = $this->Utility->encrypt($details[$i]['Voucher']['id'], 'vcr');

			$applyby = $this->Staff->findById($details[$i]['Applicant']['staff_id']);
			$details[$i]['ApplyBy'] = $applyby['Staff'];

		}

		$this->set(compact('details'));

	}

	public function view($key = null)
    {
        $this->loadModel('Staff');
		$this->loadModel('Voucher');
        $this->loadModel('Applicant');
        $this->loadModel('PaymentType');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('National');
        $this->loadModel('Spouse');
        $this->loadModel('VoucherStatus');
        $this->loadModel('Organisation');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'vcr');

        $detail = $this->Voucher->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        $spouse = $this->Spouse->findById($detail['Applicant']['key_id']);

        if(empty($spouse))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {

            $detail['Spouse'] = $spouse['Spouse'];
            $detail['Spouse']['marriage_date'] =  date("d-m-Y", strtotime($detail['Spouse']['marriage_date']));
            
            $detail['Attachment'] = $spouse['Attachment'];
        }

        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['Applicant']['staff_id']))
        {
            $applyby = $this->Staff->findStaffSummaryById($detail['Applicant']['staff_id']);

            if(!empty($applyby))
            {
                $detail['ApplyBy'] = $applyby['Staff'];

                if(!empty($applyby['Staff']['avatar']))
                {
                    $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
                }

                $detail['ApplyBy']['avatar'] = $img;
            }
        }

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['ApprovedBy']['id']))
        {
            $ApprovedBy = $this->Staff->findStaffSummaryById($detail['ApprovedBy']['id']);
        
            if(!empty($ApprovedBy['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$verifiedby['Staff']['avatar']."'/>";
            }
        }
        $detail['ApprovedBy']['avatar'] = $img;

        $detail['ApprovedBy']['day_by_text'] = date('D', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['day_by_num'] = date('d', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['month'] = date('m', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['year'] = date('Y', strtotime($detail['Voucher']['approved']));

        $detail['ApprovedBy']['hour'] = date('h', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['minute'] = date('i', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['format'] = date('A', strtotime($detail['Voucher']['approved']));

        if(!empty($detail['Voucher']['vouchers_pick_date']))
        {
            $detail['Voucher']['vouchers_pick_date'] = date("d-m-Y", strtotime($detail['Voucher']['vouchers_pick_date']));
        }

        $this->request->data = $detail;

        if(!empty($detail))
        {
            $detail['Voucher']['id'] = $this->Utility->encrypt($detail['Voucher']['id'], 'vcr');

            $detail['Voucher']['day_by_text'] = date('D', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['day_by_num'] = date('d', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['month'] = date('m', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['year'] = date('Y', strtotime($detail['Voucher']['created']));

            $detail['Voucher']['hour'] = date('h', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['minute'] = date('i', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['format'] = date('A', strtotime($detail['Voucher']['created']));
        }

        $apply = $this->Staff->findById($detail['Applicant']['staff_id']);
        if(!empty($apply))
        {
            $detail['Staff'] = $apply['Staff'];
        }

        $paymenttypes = $this->PaymentType->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $nationals = $this->National->find('list');
        $voucherstatuses = $this->VoucherStatus->find('list');
        $organisations = $this->Organisation->find('list');

		$days = array(
			'Mon' => 'Mon',
			'Tue' => 'Tue',
			'Wed' => 'Wed',
			'Thu' => 'Thu',
			'Fri' => 'Fri',
			'Sat' => 'Sat',
			'Sun' => 'Sun',
		);

        $disabled =  'disabled';

        $approved =  false;
        if($detail['Voucher']['status_id'] == 2)
        {
            $approved =  true;
        }

        $pickups = false;
        if($detail['Voucher']['status_id'] == 10)
        {
            $pickups =  true;
        }

        $disabled_pickup =  'disabled';

        $this->set(compact(
			'key',
			'detail',
			'applyBy',
			'days',
            'approved',
            'pickups',
            'disabled',
            'disabled_pickup',
            'paymenttypes',
            'voucherstatuses',
            'organisations',
            'races',
            'religions',
            'nationals'
		));
    }

    public function prints($key=null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Organisation');
		$this->loadModel('Voucher');
        $this->loadModel('Applicant');
        $this->loadModel('PaymentType');
        $this->loadModel('VoucherStatus');
        $this->loadModel('Spouse');
        $this->loadModel('Gender');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'vcr');

        $detail = $this->Voucher->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($detail['Voucher']['approved']))
        {
            $detail['Voucher']['approved'] =  date("d-m-Y", strtotime($detail['Voucher']['approved']));
        }

        if(!empty($detail['Applicant']['staff_id']))
        {
            $applyby = $this->Staff->findById($detail['Applicant']['staff_id']);
            $detail['ApplyBy'] = $applyby['Staff'];
            $detail['Organisation'] = $applyby['Organisation'];
        }

        $spouse = $this->Spouse->findById($detail['Applicant']['key_id']);

        if(empty($spouse))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {

            $detail['Spouse'] = $spouse['Spouse'];
            $detail['Spouse']['marriage_date'] =  date("d-m-Y", strtotime($detail['Spouse']['marriage_date']));
            
            $detail['Attachment'] = $spouse['Attachment'];

            $detail['Religion'] = $spouse['Religion'];
            $detail['Race'] = $spouse['Race'];
            $detail['National'] = $spouse['National'];
        }

        $baseURL = Router::url('/', true);

        $html = '';
        $html .= '<html>';
        $html .= '<body>';
        $html .= '<h6 class="text-center no-margin grey">#'. $detail['Applicant']['reference_no'] .': Spouse Voucher</h6>';
        $html .= '<table style="width: 100%; border: none; padding-top:8px;">
                    <tr>';
        $html .=        '<td style="text-align: left; width: 100%"></td>';
        $html .=    '</tr>
				</table>';
		$html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
		$html .=        '<td colspan="2" width="100%">Staff Information</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td width="30%">Name</td>';
		$html .=        '<td width="70%">'.$detail['ApplyBy']['name'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Staff No</td>';
		$html .=        '<td>'.$detail['ApplyBy']['staff_no'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Organisation</td>';
		$html .=        '<td>'.$detail['Organisation']['name'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Date of Apply</td>';
		$html .=        '<td>'.date("d-m-Y", strtotime($detail['Applicant']['created'])).'</td>';
		$html .=    '</tr>';
		$html .=    '</tbody>';
		$html .= '</table>';
		$html .= '<table style="width: 100%; border: none; padding-top:8px;">
                    <tr>';
        $html .=        '<td style="text-align: left; width: 100%"></td>';
        $html .=    '</tr>
                </table>';
        $html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
		$html .=        '<td colspan="2" width="100%">Spouse Information</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td width="30%">Name</td>';
		$html .=        '<td width="70%">'.$detail['Spouse']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Marriage Date</td>';
		$html .=        '<td>'.$detail['Spouse']['marriage_date'].'</td>';
        $html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>IC No.</td>';
		$html .=        '<td>'.$detail['Spouse']['ic_new'].'</td>';
		$html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Passport No.</td>';
		$html .=        '<td>'.$detail['Spouse']['passport_no'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Religion</td>';
		$html .=        '<td>'.$detail['Religion']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Race</td>';
		$html .=        '<td>'.$detail['Race']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Nationality</td>';
		$html .=        '<td>'.$detail['National']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Occupation</td>';
		$html .=        '<td>'.$detail['Spouse']['occupation'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Employer</td>';
		$html .=        '<td>'.$detail['Spouse']['employer'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Employer Address</td>';
		$html .=        '<td>'.$detail['Spouse']['employer_address'].'</td>';
        $html .=    '</tr>';
		$html .=    '</tbody>';
        $html .= '</table>';
        $html .= '<table style="width: 100%; border: none; padding-top:8px;">
                    <tr>';
        $html .=        '<td style="text-align: left; width: 100%"></td>';
        $html .=    '</tr>
                </table>';
        $html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
		$html .=        '<td colspan="2" width="100%">Office Use Only</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td width="30%">Status</td>';
		$html .=        '<td width="70%">'.$detail['Status']['name'].'</td>';
        $html .=    '</tr>';
        if(!empty($detail['ApprovedBy']['name']))
        {
		$html .=    '<tr>';
		$html .=        '<td width="30%">Approved By</td>';
		$html .=        '<td width="70%">'.$detail['ApprovedBy']['name'].'</td>';
        $html .=    '</tr>';
        }
        if(!empty($detail['Voucher']['approved']))
        {
		$html .=    '<tr>';
		$html .=        '<td>Approved at</td>';
		$html .=        '<td>'.$detail['Voucher']['approved'].'</td>';
        $html .=    '</tr>';
        }
		$html .=    '</tbody>';
        $html .= '</table>';
        $html .= '</html>';

        $receipt_name = "#". $detail['Applicant']['reference_no'] .": Married Voucher ".  $detail['ApplyBy']['name'] ." .pdf";

        App::import('Vendor', 'MPDF', array('file' => 'mpdf/vendor/autoload.php'));

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDisplayMode('fullpage');
        $stylesheet = file_get_contents($baseURL.'/css/report.css');
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($html,2);
		$mpdf->SetHTMLFooter('
            <table width="100%" style="vertical-align: bottom; color: #000000; font-weight: bold; font-size: 5pt;">
                <tr>
                    <td width="33%"><span style="font-weight: bold;">{PAGENO}/{nbpg}</td>
                    <td width="33%" align="center" style="font-weight: bold; color: #c6c6c6;">This is computer generated document no signature required</td>
                    <td width="33%" style="text-align: right; ">{DATE j-m-Y}</td>
                </tr>
            </table>
            ');
        $mpdf->Output($receipt_name,'I');
        exit;

    }

}
