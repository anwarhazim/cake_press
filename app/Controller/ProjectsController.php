<?php
class ProjectsController extends AppController
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['order'] = array('Project.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Project'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Project.name LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Project.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Project.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Project'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Project']['modified'] = date("d-m-Y",strtotime($details[$i]['Project']['modified']));

            $details[$i]['Project']['created'] = date("d-m-Y",strtotime($details[$i]['Project']['created']));

            $details[$i]['Project']['id'] = $this->Utility->encrypt($details[$i]['Project']['id'], 'pro');

        }

        $this->set(compact('details'));
    }

    public function add()
    {
		$this->loadModel('Staff');

		$person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
			$data = $this->request->data;

			$this->Project->set($data);
            if($this->Project->validates())
            {
				$data['Project']['modified_by'] = $staff['Staff']['id'];
				$data['Project']['modified'] = date('Y-m-d H:i:s');
				$data['Project']['created_by'] = $staff['Staff']['id'];
				$data['Project']['created'] = date('Y-m-d H:i:s');

                $this->Project->create();
				$this->Project->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }
    }

    public function edit($key = null)
    {
		$this->loadModel('Staff');
        $this->loadModel('Utility');


		$person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'pro');

        $detail = $this->Project->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$data['Project']['id'] = $id;

			$this->Project->set($data);
            if($this->Project->validates())
            {
				$data['Project']['modified_by'] = $staff['Staff']['id'];
				$data['Project']['modified'] = date('Y-m-d H:i:s');

                $this->Project->create();
				$this->Project->save($data);

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }

        $this->set(compact('key'));
    }
}
