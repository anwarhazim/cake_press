<?php
class NotificationsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Notification.recipient_id' => $staff['Staff']['list_id'],
                                        );

        $conditions['order'] = array('Notification.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Notification'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Notification.subject LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Notification.body LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Notification.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Notification.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Notification'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $sender = $this->Staff->findStaffSummaryById($details[$i]['Notification']['sender_id']);
            
            $details[$i]['Notification']['id'] = $this->Utility->encrypt($details[$i]['Notification']['id'], 'ntf');

            $details[$i]['Notification']['modified'] = $this->Utility->datetime($details[$i]['Notification']['modified']);

            $details[$i]['Notification']['created'] = $this->Utility->datetime($details[$i]['Notification']['created']);

            $details[$i]['Sender']['name'] = $this->Utility->strlen($details[$i]['Sender']['name'], 20);
            
            if(!empty($sender['Staff']['avatar']))
            {
                $details[$i]['Sender']['avatar'] = 'avatars/'.$sender['Staff']['avatar'];
            }
            else
            {
                $details[$i]['Sender']['avatar'] = '/img/users/default-avatar.jpg';
            }

            $details[$i]['Notification']['body'] = $this->Utility->strlen($details[$i]['Notification']['body'], 40);

            
        }

        $this->set(compact('details'));
    }

    public function read($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'ntf');

        $detail = $this->Notification->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $data = array();
        $data['Notification']['id'] = $detail['Notification']['id'];
        $data['Notification']['is_read'] = 99;

        $this->Notification->create();
        $this->Notification->save($data);

        $this->set(compact('key', 'detail'));        
    }

    public function getNotificationByStaffId($staff_id = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $notifications = $this->Notification->find('all', array(
                                                        'conditions' => array('Notification.recipient_id' => $staff['Staff']['id'], 'Notification.is_read' => 1),
                                                    ));

        $details = array();

        if(!empty($notifications))
        {
            foreach ($notifications as $notification) 
            {
                $sender = $this->Staff->findStaffSummaryById($notification['Notification']['sender_id']);
    
                $img = "";
                $baseURL = Router::url('/', true);
                if(!empty($sender['Staff']['avatar']))
                {
                    $img = "<img class='img-circle img-sm' src='".$baseURL."avatars/".$sender['Staff']['avatar']."'/>";
                }
                else
                {
                    $img = "<img class='img-circle img-sm' src='".$baseURL."img/users/default-avatar.jpg'/>";
                }
                
                $details[$notification['Notification']['id']] = '<li class="media"><div class="media-left">'. $img .'</div>
                                                                    <div class="media-body">
                                                                            <a href="'.$baseURL.'Users/read/'.$this->Utility->encrypt($notification['Notification']['id'], 'ntf').'" class="media-heading">
                                                                                <span class="text-semibold">'.$sender['Staff']['name'].'</span>
                                                                                <span class="media-annotation pull-right">'.$this->Utility->datetime($notification['Notification']['created']).'</span>
                                                                            </a>
                                                                            <span class="text-muted">'.$this->Utility->strlen($notification['Notification']['subject'], 40).'</span>
                                                                        </div>
                                                                    </li>';


            }
        }
        else
        {
            $details[] = '<li class="media">
                                <div class="media-body">
                                    <span class="text-muted">No notification...</span>
                                </div>
                            </li>';
        }

        $myJSON = json_encode($details);

        return $myJSON;
    }

    public function getNotificationCountByStaffId($staff_id = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $count = $this->Notification->find('count', array(
                                                        'conditions' => array('Notification.recipient_id' => $staff['Staff']['id'], 'Notification.is_read' => 1),
                                                    ));
        
        return $count;
    }

}