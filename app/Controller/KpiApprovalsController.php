<?php
class KpiApprovalsController extends AppController
{
	var $uses = false;
	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {

	}

	public function correction()
    {

		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->Session->setFlash('Your request update successfully sent to HRIS Team for verifications. Please wait for any update.', 'success');
			$this->redirect(array('action' => 'correction'));

        }
    }

    public function organization() 
    {

	}

    public function add()
    {

    }
    
    public function staff()
    {

    }

    public function incumbentdata()
    {

    }

    public function addnewmidyearreview()
    {

    }

    public function addnewkpiplanning()
    {
        
    }
    
}
