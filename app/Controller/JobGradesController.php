<?php
class JobGradesController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'add', 'edit', 'getJobGradeByDesignationId');
    }

    public function index()
    {
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['order'] = array('JobGrade.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['JobGrade'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('JobGrade.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(JobGrade.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(JobGrade.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['JobGrade'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['JobGrade']['modified'] = date("d-m-Y",strtotime($details[$i]['JobGrade']['modified']));

            $details[$i]['JobGrade']['created'] = date("d-m-Y",strtotime($details[$i]['JobGrade']['created']));

            $details[$i]['JobGrade']['id'] = $this->Utility->encrypt($details[$i]['JobGrade']['id'], 'jbg');
            
        }

        $this->set(compact('details'));
    }

    public function add()
    {
        $this->loadModel('JobDesignation');

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->JobGrade->create();
            if($this->JobGrade->save($data))
            {
                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $jobdesignations = $this->JobDesignation->find('list');

        $this->set(compact('jobdesignations'));
    }

    public function edit($key = null)
    {
        $this->loadModel('JobDesignation');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'jbg');

        $detail = $this->JobGrade->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['JobGrade']['id'] = $id;

            $this->JobGrade->create();
            if($this->JobGrade->save($data))
            {
                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }

        $jobdesignations = $this->JobDesignation->find('list');

        $this->set(compact('key', 'jobdesignations'));
    }

    public function getJobGradeByDesignationId($designation_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $organisations = $this->JobGrade->find('list',
                                                        array(
                                                            'conditions' => array(
                                                                                'JobGrade.job_designation_id'=>$designation_id, 
                                                                            ),
                                                            'contain' => false,
                                                            'order' => array('JobGrade.name ASC'),
                                                    ));

        $myJSON = json_encode($organisations);

        return $myJSON;
    }   
}