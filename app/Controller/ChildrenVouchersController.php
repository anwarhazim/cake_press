<?php
class ChildrenVouchersController extends AppController
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	public $uses = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
	}

	public function index()
    {
		$this->loadModel('Voucher');
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

		$roles = $this->UserRole->getRoleByUserId($staff['Staff']['user_id']);

        $vouchertypes = array();

        if (in_array(8, $roles))//HCD voucher Children Prasarana
        {
            $vouchertypes = array(1, 2, 3);
        }

        if (in_array(9, $roles))//HCD voucher Children Bus
        {
            $vouchertypes[] = 2;
        }

        if (in_array(10, $roles))//HCD voucher Children Rail
        {
            $vouchertypes[] = 3;
        }

		$conditions = array();

		$conditions['conditions'][] = array(
                                            'Voucher.Voucher_type_id' => 2,
                                            'Voucher.organisation_type_id' => $vouchertypes
											);

        $conditions['order'] = array('Voucher.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Voucher'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Applicant.reference_no LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Voucher.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Voucher.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Voucher'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate('Voucher');

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Voucher']['modified'] = date("d-m-Y",strtotime($details[$i]['Voucher']['modified']));

            $details[$i]['Voucher']['created'] = date("d-m-Y",strtotime($details[$i]['Voucher']['created']));

			$details[$i]['Voucher']['id'] = $this->Utility->encrypt($details[$i]['Voucher']['id'], 'vcr');

			$applyby = $this->Staff->findById($details[$i]['Applicant']['staff_id']);
			$details[$i]['ApplyBy'] = $applyby['Staff'];

        }
        
        $auth = $this->Utility->getUserAuth(50,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

		$this->set(compact('details'));
	}

	public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Organisation');
        $this->loadModel('Children');
        $this->loadModel('Gender');
		$this->loadModel('Voucher');
        $this->loadModel('Applicant');
        $this->loadModel('PaymentType');
        $this->loadModel('VoucherStatus');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'vcr');

        $detail = $this->Voucher->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $children = $this->Children->findById($detail['Applicant']['key_id']);

        if(empty($children))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {

            $detail['Children'] = $children['Children'];
            $detail['Children']['date_of_birth'] =  date("d-m-Y", strtotime($detail['Children']['date_of_birth']));
            
            $detail['Attachment'] = $children['Attachment'];
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['Applicant']['staff_id']))
        {
            $applyby = $this->Staff->findStaffSummaryById($detail['Applicant']['staff_id']);

            if(!empty($applyby))
            {
                $detail['ApplyBy'] = $applyby['Staff'];

                if(!empty($applyby['Staff']['avatar']))
                {
                    $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
                }

                $detail['ApplyBy']['avatar'] = $img;
            }
        }

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['ApprovedBy']['id']))
        {
            $ApprovedBy = $this->Staff->findStaffSummaryById($detail['ApprovedBy']['id']);
        
            if(!empty($ApprovedBy['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$verifiedby['Staff']['avatar']."'/>";
            }
        }
        $detail['ApprovedBy']['avatar'] = $img;

        $detail['ApprovedBy']['day_by_text'] = date('D', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['day_by_num'] = date('d', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['month'] = date('m', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['year'] = date('Y', strtotime($detail['Voucher']['approved']));

        $detail['ApprovedBy']['hour'] = date('h', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['minute'] = date('i', strtotime($detail['Voucher']['approved']));
        $detail['ApprovedBy']['format'] = date('A', strtotime($detail['Voucher']['approved']));

        if(!empty($detail['Voucher']['vouchers_pick_date']))
        {
            $detail['Voucher']['vouchers_pick_date'] = date("d-m-Y", strtotime($detail['Voucher']['vouchers_pick_date']));
        }

        $apply = $this->Staff->findById($detail['Applicant']['staff_id']);
        if(!empty($apply))
        {
            $detail['Staff'] = $apply['Staff'];
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            switch ($data['Submit']) 
            {
                case 'draf':

                    $this->Voucher->set($data);
                    if($this->Voucher->validates())
                    {
                        $data['Voucher']['status_id'] = 2;  
                        $data['Voucher']['modified_by'] = $staff['Staff']['id'];                
                        $data['Voucher']['modified'] = date('Y-m-d H:i:s');

                        $this->Voucher->create();
                        $this->Voucher->save($data);

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'view/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully verified.', 'error');
                    }

                    break;

                case 'close':

                    $this->Voucher->set($data);
                    if($this->Voucher->validates())
                    {
                        $data['Voucher']['is_active'] = 99;  
                        $data['Voucher']['voucher_status_id'] = 2;  
                        $data['Voucher']['voucher_pick_id'] = $staff['Staff']['id'];

                        $this->Voucher->create();
                        $this->Voucher->save($data);

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'view/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully verified.', 'error');
                    }

                    break;

                }
        }
        else
        {
            $this->request->data = $detail;
        }

        if(!empty($detail))
        {
            $detail['Voucher']['id'] = $this->Utility->encrypt($detail['Voucher']['id'], 'vcr');

            $detail['Voucher']['day_by_text'] = date('D', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['day_by_num'] = date('d', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['month'] = date('m', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['year'] = date('Y', strtotime($detail['Voucher']['created']));

            $detail['Voucher']['hour'] = date('h', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['minute'] = date('i', strtotime($detail['Voucher']['created']));
            $detail['Voucher']['format'] = date('A', strtotime($detail['Voucher']['created']));
        }

        $paymenttypes = $this->PaymentType->find('list', array(
                                                            'conditions' => array('PaymentType.id' => array(1,3,4)),
                                                        ));

        $voucherstatuses = $this->VoucherStatus->find('list');
        $organisations = $this->Organisation->find('list');

        $genders = $this->Gender->find('list');

        $path = Router::url('/documents/', true);

		$days = array(
			'Mon' => 'Mon',
			'Tue' => 'Tue',
			'Wed' => 'Wed',
			'Thu' => 'Thu',
			'Fri' => 'Fri',
			'Sat' => 'Sat',
			'Sun' => 'Sun',
		);

        $disabled = '';
        if($detail['Voucher']['status_id'] > 2)
        {
            $disabled =  'disabled';
        }

        $approved =  false;
        if($detail['Voucher']['status_id'] == 2)
        {
            $approved =  true;
        }

        $pickups = false;
        if($detail['Voucher']['status_id'] == 10)
        {
            $pickups =  true;
        }

        $disabled_pickup = '';
        if($detail['Voucher']['is_active'] > 1)
        {
            $disabled_pickup =  'disabled';
        }

        $auth = $this->Utility->getUserAuth(50,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

        $this->set(compact(
			'key',
			'detail',
			'applyBy',
			'days',
            'approved',
            'pickups',
            'disabled',
            'disabled_pickup',
            'paymenttypes',
            'voucherstatuses', 
            'path', 
            'genders',
            'organisations'
		));
    }

    public function approved($key = null)
    {
        $this->loadModel('Staff');
		$this->loadModel('Voucher');
        $this->loadModel('Applicant');
        $this->loadModel('PaymentType');
        $this->loadModel('VoucherStatus');
        $this->loadModel('Notification');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'vcr');

        $detail = $this->Voucher->findById($id);

        $auth = $this->Utility->getUserAuth(50,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $this->Voucher->set($detail);
        if($this->Voucher->validates())
        {
            $data = array();

            $data['Voucher']['id'] = $detail['Voucher']['id'];
            $data['Voucher']['status_id'] = 10;
            $data['Voucher']['is_active'] = 1;
            $data['Voucher']['approved_by'] = $staff['Staff']['id'];
            $data['Voucher']['approved'] = date('Y-m-d H:i:s');

            $this->Voucher->create();
            $this->Voucher->save($data);

            $subject = 'Your Voucher for Reference No. '. $detail['Applicant']['reference_no'] . " have successfully approved.";
            $body = $detail['Voucher']['note'];
            $body .= "<br/>";
            $body .= "Please download and submit the attachment into your SAP system.";
            $body .= "<br/>";
            $body .= "Click <a href='".Router::url('/MyChildrenVouchers/view/'. $key , true)."'>here</a> to view detail.";
    
            $notification = array();

            $notification['Notification']['sender_id'] = $staff['Staff']['id'];
            $notification['Notification']['recipient_id'] = $detail['Applicant']['staff_id'];
            $notification['Notification']['label'] = 'info-warning';
            $notification['Notification']['proses'] = 1;
            $notification['Notification']['is_read'] = 1;
            $notification['Notification']['subject'] = $subject;
            $notification['Notification']['body'] = $body;
            $notification['Notification']['created_by'] = $staff['Staff']['id'];
            $notification['Notification']['modified_by'] = $staff['Staff']['id'];

            $this->Notification->create();
            $this->Notification->save($notification);

            $this->Session->setFlash('Information succesfully updated.', 'success');
            $this->redirect(array('action' => 'view/'.$key));
        }
        else
        {
            $this->Session->setFlash('Error! Information not successfully verified.', 'error');
            $this->redirect(array('action' => 'view/'.$key));
        }
    }

    public function reject($key = null)
    {
        $this->loadModel('Staff');
		$this->loadModel('Voucher');
        $this->loadModel('Applicant');
        $this->loadModel('PaymentType');
        $this->loadModel('VoucherStatus');
        $this->loadModel('Notification');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'vcr');

        $detail = $this->Voucher->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $auth = $this->Utility->getUserAuth(50,$person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }
        
        $this->Voucher->validate = array(
                                        'note' => array(
                                            'notBlank' => array(
                                                    'rule' => 'notBlank',
                                                    'message' => 'You need to leave a note!'
                                                )
                                        ),
                                    );

        $this->Voucher->set($detail);
        if($this->Voucher->validates())
        {
            $data = array();

            $data['Voucher']['id'] = $detail['Voucher']['id'];
            $data['Voucher']['status_id'] = 12;
            $data['Voucher']['is_active'] = 99;
            $data['Voucher']['approved_by'] = $staff['Staff']['id'];
            $data['Voucher']['approved'] = date('Y-m-d H:i:s');

            $this->Voucher->create();
            $this->Voucher->save($data);

            $subject = 'Your Voucher for Reference No. '. $detail['Applicant']['reference_no'] . " have been rejected";
            $body = $detail['Voucher']['note'];
            $body .= "<br/>";
            $body .= "Click <a href='".Router::url('/MyChildrenVouchers/view/'. $key , true)."'>here</a> to view detail.";
    
            $notification = array();

            $notification['Notification']['sender_id'] = $staff['Staff']['id'];
            $notification['Notification']['recipient_id'] = $detail['Applicant']['staff_id'];
            $notification['Notification']['label'] = 'info-warning';
            $notification['Notification']['proses'] = 1;
            $notification['Notification']['is_read'] = 1;
            $notification['Notification']['subject'] = $subject;
            $notification['Notification']['body'] = $body;
            $notification['Notification']['created_by'] = $staff['Staff']['id'];
            $notification['Notification']['modified_by'] = $staff['Staff']['id'];

            $this->Notification->create();
            $this->Notification->save($notification);

            $this->Session->setFlash('Information succesfully updated.', 'success');
            $this->redirect(array('action' => 'view/'.$key));
        }
        else
        {
            $this->Session->setFlash('Error! Information not successfully verified.', 'error');
            $this->redirect(array('action' => 'view/'.$key));
        }
    }
}
