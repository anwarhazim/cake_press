<?php
class DeathsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Death.staff_id' => $staff['Staff']['id'],
                                        );

        $conditions['order'] = array('Death.date_of_death'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Death'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Death.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Death.ic LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Death.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Death.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Death'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Death']['date_of_death'] = date("d-m-Y",strtotime($details[$i]['Death']['date_of_death']));

            $details[$i]['Death']['modified'] = date("d-m-Y",strtotime($details[$i]['Death']['modified']));

            $details[$i]['Death']['created'] = date("d-m-Y",strtotime($details[$i]['Death']['created']));

            $details[$i]['Death']['id'] = $this->Utility->encrypt($details[$i]['Death']['id'], 'dth');
        }

        $update = $this->Death->findIfUpdateByStaffId($staff['Staff']['id']);

        $this->set(compact('details', 'update'));
    }

    public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Relationship');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $path = Router::url('/documents/', true);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'chld');

        $detail = $this->Death->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;
        
        }
        else
        {
            $detail['Death']['date_of_death'] =  date("d-m-Y", strtotime($detail['Death']['date_of_death']));

            $this->request->data = $detail;
        }
        
        $disabled = "disabled";

        $relationships = $this->Relationship->find('list');

        $update = $this->Death->findIfUpdateByStaffId($staff['Staff']['id']);

        $this->set(compact('key', 'detail', 'path', 'relationships', 'disabled', 'update'));
    }

    public function edit($key)
    {
        $this->loadModel('Staff');
        $this->loadModel('Relationship');
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $path = Router::url('/documents/', true);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'dth');

        $detail = $this->Death->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Death']['id'] = $detail['Death']['id'];
            $data['Death']['is_flag'] = 1;
            $data['Death']['is_active'] = 1;
            $data['Death']['is_delete'] = 99;
            $data['Death']['status_id'] = 2;
            $data['Death']['modified_by'] = $staff['Staff']['id'];
            $data['Death']['modified'] = date('Y-m-d H:i:s');

            $this->Death->set($data);
            if($this->Death->validates())
            {
                $this->Death->create();
                $this->Death->save($data);

                $path = WWW_ROOT . 'documents'.DS;

                foreach ($data['Death']['attachments'] as $attachment) 
                {
                    if($attachment['error'] == 0)
                    {
                        // <!-- start create personal forder by staff no -->

                        //check and create folder by staff no
                        $filename_main = $path.$staff['Staff']['staff_no'];
                        $this->Utility->createFolder($filename_main);

                        //check and create folder by moduls
                        $filename_main = $filename_main.DS.'DEATHS';
                        $this->Utility->createFolder($filename_main);

                        // <!-- end create personal forlder by staff no -->

                        $file = array();

                        $file['Attachment']['key_id'] = $detail['Death']['id'];
                        $file['Attachment']['name'] = $attachment['name'];
                        $file['Attachment']['modul_id'] = 26; // check table moduls to check modul id
                        $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                        $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                        $file['Attachment']['created_by'] = $staff['Staff']['id'];
                        $file['Attachment']['created'] = date('Y-m-d H:i:s');

                        $this->Attachment->create();
                        $this->Attachment->save($file);

                        $attachment_id = $this->Attachment->id;

                        $temp = explode(".", $attachment['name']);
                        $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                        move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);

                        $file = array();

                        $file['Attachment']['id'] = $attachment_id;
                        $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'DEATHS'.DS.$newfilename;

                        $this->Attachment->create();
                        $this->Attachment->save($file);
                    }
                }

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        else
        {
            $detail['Death']['date_of_death'] =  date("d-m-Y", strtotime($detail['Death']['date_of_death']));

            $this->request->data = $detail;
        }

        $relationships = $this->Relationship->find('list');

        $update = $this->Death->findIfUpdateByStaffId($staff['Staff']['id']);
        if($update == false)
        {
            $this->Session->setFlash('You cannot make any changes within this period. Please contact system administrator for help', 'info');
            $this->redirect(array('action' => '/'));
        }

        $this->set(compact('key', 'detail', 'path', 'relationships'));
    }

    public function add()
    {
        $this->loadModel('Staff');
        $this->loadModel('Relationship');
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Death']['staff_id'] = $staff['Staff']['id'];
            $data['Death']['is_flag'] = 1;
            $data['Death']['is_active'] = 1;
            $data['Death']['is_delete'] = 99;
            $data['Death']['is_status'] = 1;
            $data['Death']['status_id'] = 1;
            $data['Death']['modified_by'] = $staff['Staff']['id'];
            $data['Death']['modified'] = date('Y-m-d H:i:s');
            $data['Death']['created_by'] = $staff['Staff']['id'];
            $data['Death']['created'] = date('Y-m-d H:i:s');
            
            $this->Death->set($data);
            if($this->Death->validates())
            {
                $this->Death->create();
                $this->Death->save($data);

                $death_id = $this->Death->id;

                $path = WWW_ROOT . 'documents'.DS;

                foreach ($data['Death']['attachments'] as $attachment) 
                {
                    // <!-- start create personal forder by staff no -->

                    //check and create folder by staff no
                    $filename_main = $path.$staff['Staff']['staff_no'];
                    $this->Utility->createFolder($filename_main);

                    //check and create folder by moduls
                    $filename_main = $filename_main.DS.'DEATHS';
                    $this->Utility->createFolder($filename_main);

                    // <!-- end create personal forlder by staff no -->

                    $file = array();

                    $file['Attachment']['key_id'] = $death_id;
                    $file['Attachment']['name'] = $attachment['name'];
                    $file['Attachment']['modul_id'] = 26; // check table moduls to check modul id
                    $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                    $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                    $file['Attachment']['created_by'] = $staff['Staff']['id'];
                    $file['Attachment']['created'] = date('Y-m-d H:i:s');

                    $this->Attachment->create();
                    $this->Attachment->save($file);

                    $attachment_id = $this->Attachment->id;

                    $temp = explode(".", $attachment['name']);
                    $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                    move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);

                    $file = array();

                    $file['Attachment']['id'] = $attachment_id;
                    $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'DEATHS'.DS.$newfilename;

                    $this->Attachment->create();
                    $this->Attachment->save($file);
                }

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }

        $relationships = $this->Relationship->find('list');

        $update = $this->Death->findIfUpdateByStaffId($staff['Staff']['id']);
        if($update == false)
        {
            $this->Session->setFlash('You cannot make any changes within this period. Please contact system administrator for help', 'info');
            $this->redirect(array('action' => '/'));
        }

        $this->set(compact('relationships'));
    }

    public function attachment($id=null)
    {
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $path = WWW_ROOT . 'documents'.DS;

        if(empty($id))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $detail = $this->Attachment->findById($id);

        $directory = $path.$detail['Attachment']['path'];

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            $key = $this->Utility->encrypt($detail['Attachment']['key_id'], 'dth');

            if($this->Attachment->delete($id))
            {
                $directory = $path.$detail['Attachment']['path'];
                unlink($directory);

                $this->Session->setFlash('Attachment successfully deleted.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Attachment not successfully deleted. Please try again!', 'error');
                $this->redirect(array('action' => 'edit/'.$key));
            }
        }
    }

    public function delete($key=null)
    {
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $path = WWW_ROOT . 'documents'.DS;

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'dth');

        $detail = $this->Death->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            if($detail['Death']['status_id'] < 3)
            {
                if($this->Death->delete($id))
                {
                    foreach ($detail['Attachment'] as $attachment) 
                    {
                        $this->Attachment->delete($attachment['id']);

                        $directory = $path.$attachment['path'];
                        unlink($directory);
                    }

                    $this->Session->setFlash('Information successfully deleted.', 'success');
                    $this->redirect(array('action' => '/'));
                }
                else
                {
                    $this->Session->setFlash('Error! Information not successfully deleted. Please try again!', 'error');
                    $this->redirect(array('action' => '/'));
                }
            }
            else
            {
                $this->Session->setFlash('Error! You cannot delete this information. Please contact system administrator for help.', 'error');
                $this->redirect(array('action' => '/'));
            }

        }
    }
}