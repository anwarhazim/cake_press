<?php
class MyFamilyPassesController extends AppController
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	public $uses = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
	}

	public function index()
    {
		$this->loadModel('FamilyPass');
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

		$conditions = array();

		$conditions['conditions'][] = array(
                                            'Applicant.staff_id' => $staff['Staff']['list_id']
											);

        $conditions['order'] = array('FamilyPass.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['FamilyPass'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Applicant.reference_no LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(FamilyPass.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(FamilyPass.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['FamilyPass'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate('FamilyPass');

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['FamilyPass']['modified'] = date("d-m-Y",strtotime($details[$i]['FamilyPass']['modified']));

            $details[$i]['FamilyPass']['created'] = date("d-m-Y",strtotime($details[$i]['FamilyPass']['created']));

			$details[$i]['FamilyPass']['id'] = $this->Utility->encrypt($details[$i]['FamilyPass']['id'], 'fmlypss');

			$applyby = $this->Staff->findById($details[$i]['Applicant']['staff_id']);
			$details[$i]['ApplyBy'] = $applyby['Staff'];

		}

		$this->set(compact('details'));

	}

	public function view($key = null)
    {
        $this->loadModel('Staff');
		$this->loadModel('FamilyPass');
        $this->loadModel('Applicant');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('FamilyPassType');
        $this->loadModel('FamilyPassReason');
        $this->loadModel('Organisation');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'fmlypss');

        $detail = $this->FamilyPass->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
            $this->redirect('/');
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['Applicant']['staff_id']))
        {
            $applyby = $this->Staff->findStaffSummaryById($detail['Applicant']['staff_id']);

            if(!empty($applyby))
            {
                $detail['ApplyBy'] = $applyby['Staff'];

                if(!empty($applyby['Staff']['avatar']))
                {
                    $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
                }

                $detail['ApplyBy']['avatar'] = $img;
            }
        }

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['ApprovedBy']['id']))
        {
            $ApprovedBy = $this->Staff->findStaffSummaryById($detail['ApprovedBy']['id']);
        
            if(!empty($ApprovedBy['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$verifiedby['Staff']['avatar']."'/>";
            }
        }
        $detail['ApprovedBy']['avatar'] = $img;

        $detail['ApprovedBy']['day_by_text'] = date('D', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['day_by_num'] = date('d', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['month'] = date('m', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['year'] = date('Y', strtotime($detail['FamilyPass']['approved']));

        $detail['ApprovedBy']['hour'] = date('h', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['minute'] = date('i', strtotime($detail['FamilyPass']['approved']));
        $detail['ApprovedBy']['format'] = date('A', strtotime($detail['FamilyPass']['approved']));

        if(!empty($detail['FamilyPass']['family_pass_pick_date']))
        {
            $detail['FamilyPass']['family_pass_pick_date'] = date("d-m-Y", strtotime($detail['FamilyPass']['family_pass_pick_date']));
        }

        $apply = $this->Staff->findById($detail['Applicant']['staff_id']);
        if(!empty($apply))
        {
            $detail['Staff'] = $apply['Staff'];
        }

        switch ($detail['Applicant']['modul_id']) 
        {
            case 17:
                
                $spouse = $this->Spouse->findById($detail['Applicant']['key_id']);

                if(empty($spouse))
                {
                    $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
                    $this->redirect('/');
                }
                else
                {
        
                    $detail['Spouse'] = $spouse['Spouse'];
                    $detail['Spouse']['marriage_date'] =  date("d-m-Y", strtotime($detail['Spouse']['marriage_date']));
                    
                    $detail['Attachment'] = $spouse['Attachment'];
                }

                break;
            case 20:

                $children = $this->Children->findById($detail['Applicant']['key_id']);

                if(empty($children))
                {
                    $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
                    $this->redirect('/');
                }
                else
                {

                    $detail['Children'] = $children['Children'];
                    $detail['Children']['date_of_birth'] =  date("d-m-Y", strtotime($detail['Children']['date_of_birth']));
                    
                    $detail['Attachment'] = $children['Attachment'];
                }

                break;
        }

        $this->request->data = $detail;

        if(!empty($detail))
        {
            $detail['FamilyPass']['id'] = $this->Utility->encrypt($detail['FamilyPass']['id'], 'fmlypss');

            $detail['FamilyPass']['day_by_text'] = date('D', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['day_by_num'] = date('d', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['month'] = date('m', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['year'] = date('Y', strtotime($detail['FamilyPass']['created']));

            $detail['FamilyPass']['hour'] = date('h', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['minute'] = date('i', strtotime($detail['FamilyPass']['created']));
            $detail['FamilyPass']['format'] = date('A', strtotime($detail['FamilyPass']['created']));
        }

        $familypasstypes = $this->FamilyPassType->find('list');
        $familypassreasons = $this->FamilyPassReason->find('list');
        $organisations = $this->Organisation->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $nationals = $this->National->find('list');
        $genders = $this->Gender->find('list');

		$days = array(
			'Mon' => 'Mon',
			'Tue' => 'Tue',
			'Wed' => 'Wed',
			'Thu' => 'Thu',
			'Fri' => 'Fri',
			'Sat' => 'Sat',
			'Sun' => 'Sun',
		);

        $disabled =  'disabled';

        $approved =  false;
        if($detail['FamilyPass']['status_id'] == 2)
        {
            $approved =  true;
        }

        $pickups = false;
        if($detail['FamilyPass']['status_id'] == 10)
        {
            $pickups =  true;
        }

        $disabled_pickup =  'disabled';

        $this->set(compact(
			'key',
			'detail',
			'applyBy',
			'days',
            'approved',
            'pickups',
            'disabled',
            'disabled_pickup',
            'familypasstypes',
            'familypassreasons',
            'organisations',
            'races',
            'religions',
            'nationals',
            'genders'
		));
    }

    public function add()
    {
        
    }

    public function prints($key=null)
    {
        $this->loadModel('Staff');
        $this->loadModel('FamilyPass');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('National');
        $this->loadModel('Organisation');
		$this->loadModel('Voucher');
        $this->loadModel('Applicant');
        $this->loadModel('PaymentType');
        $this->loadModel('VoucherStatus');
        $this->loadModel('Death');
        $this->loadModel('Gender');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'fmlypss');

        $detail = $this->FamilyPass->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($detail['FamilyPass']['family_pass_pick_date']))
        {
            $detail['FamilyPass']['family_pass_pick_date'] = date("d-m-Y", strtotime($detail['FamilyPass']['family_pass_pick_date']));
        }

        if(!empty($detail['FamilyPass']['approved']))
        {
            $detail['FamilyPass']['approved'] =  date("d-m-Y", strtotime($detail['FamilyPass']['approved']));
        }

        if(!empty($detail['Applicant']['staff_id']))
        {
            $applyby = $this->Staff->findById($detail['Applicant']['staff_id']);
            $detail['ApplyBy'] = $applyby['Staff'];
            $detail['Organisation'] = $applyby['Organisation'];
        }

        switch ($detail['Applicant']['modul_id']) 
        {
            case 17:
                
                $spouse = $this->Spouse->findById($detail['Applicant']['key_id']);

                if(empty($spouse))
                {
                    $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
                    $this->redirect('/');
                }
                else
                {
        
                    $detail['Spouse'] = $spouse['Spouse'];
                    $detail['Spouse']['marriage_date'] =  date("d-m-Y", strtotime($detail['Spouse']['marriage_date']));
                    
                    $detail['Attachment'] = $spouse['Attachment'];

                    $detail['Religion'] = $spouse['Religion'];
                    $detail['Race'] = $spouse['Race'];
                    $detail['National'] = $spouse['National'];
                }

                break;
            case 20:

                $children = $this->Children->findById($detail['Applicant']['key_id']);

                if(empty($children))
                {
                    $this->Session->setFlash('We cannot find any in our record. Please contact administrator for help.', 'error');
                    $this->redirect('/');
                }
                else
                {

                    $detail['Children'] = $children['Children'];
                    $detail['Children']['date_of_birth'] =  date("d-m-Y", strtotime($detail['Children']['date_of_birth']));
                    
                    $detail['Attachment'] = $children['Attachment'];

                    $detail['Gender'] = $children['Gender'];
                }

                break;
        }

        $baseURL = Router::url('/', true);

        $html = '';
        $html .= '<html>';
        $html .= '<body>';
        $html .= '<h6 class="text-center no-margin grey">#'. $detail['Applicant']['reference_no'] .': Family Pass</h6>';
        $html .= '<table style="width: 100%; border: none; padding-top:8px;">
                    <tr>';
        $html .=        '<td style="text-align: left; width: 100%"></td>';
        $html .=    '</tr>
				</table>';
		$html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
		$html .=        '<td colspan="2" width="100%">Staff Information</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td width="30%">Name</td>';
		$html .=        '<td width="70%">'.$detail['ApplyBy']['name'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Staff No</td>';
		$html .=        '<td>'.$detail['ApplyBy']['staff_no'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Organisation</td>';
		$html .=        '<td>'.$detail['Organisation']['name'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Date of Apply</td>';
		$html .=        '<td>'.date("d-m-Y", strtotime($detail['Applicant']['created'])).'</td>';
		$html .=    '</tr>';
		$html .=    '</tbody>';
		$html .= '</table>';
		$html .= '<table style="width: 100%; border: none; padding-top:8px;">
                    <tr>';
        $html .=        '<td style="text-align: left; width: 100%"></td>';
        $html .=    '</tr>
                </table>';
        if(!empty($detail['Spouse']))
        {
        $html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
        $html .=        '<td colspan="2" width="100%">Spouse Information</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td width="30%">Name</td>';
        $html .=        '<td width="70%">'.$detail['Spouse']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Marriage Date</td>';
        $html .=        '<td>'.$detail['Spouse']['marriage_date'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>IC No.</td>';
        $html .=        '<td>'.$detail['Spouse']['ic_new'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Passport No.</td>';
        $html .=        '<td>'.$detail['Spouse']['passport_no'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Religion</td>';
        $html .=        '<td>'.$detail['Religion']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Race</td>';
        $html .=        '<td>'.$detail['Race']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Nationality</td>';
        $html .=        '<td>'.$detail['National']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Occupation</td>';
        $html .=        '<td>'.$detail['Spouse']['occupation'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Employer</td>';
        $html .=        '<td>'.$detail['Spouse']['employer'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Employer Address</td>';
        $html .=        '<td>'.$detail['Spouse']['employer_address'].'</td>';
        $html .=    '</tr>';
        $html .=    '</tbody>';
        $html .= '</table>';
        }

        if(!empty($detail['Children']))
        {
        $html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
        $html .=        '<td colspan="2" width="100%">Children Information</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td width="30%">Name</td>';
        $html .=        '<td width="70%">'.$detail['Children']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>MyKID / IC No.</td>';
        $html .=        '<td>'.$detail['Children']['ic'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Gender</td>';
        $html .=        '<td>'.$detail['Gender']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Date of Birth</td>';
        $html .=        '<td>'.$detail['Children']['date_of_birth'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Occupation</td>';
        $html .=        '<td>'.$detail['Children']['occupation'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
        $html .=        '<td>Institution/ Employer</td>';
        $html .=        '<td>'.$detail['Children']['institution_employer'].'</td>';
        $html .=    '</tr>';
        $html .=    '</tbody>';
        $html .= '</table>';
        }
        $html .= '<table style="width: 100%; border: none; padding-top:8px;">
                     <tr>';
        $html .=        '<td style="text-align: left; width: 100%"></td>';
        $html .=    '</tr>
                </table>';

        $html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
		$html .=        '<td colspan="2" width="100%">Family Pass Information</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td width="30%">Reference No</td>';
		$html .=        '<td width="70%">'.$detail['Applicant']['reference_no'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>No. Serial</td>';
		$html .=        '<td>'.$detail['FamilyPass']['family_pass_serial_no'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Type</td>';
		$html .=        '<td>'.$detail['FamilyPassType']['name'].'</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td>Reason</td>';
		$html .=        '<td>'.$detail['FamilyPassReason']['name'].'</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=        '<td>Date of Apply</td>';
		$html .=        '<td>'.date("d-m-Y", strtotime($detail['FamilyPass']['created'])).'</td>';
		$html .=    '</tr>';
		$html .=    '</tbody>';
        $html .= '</table>';
        
        $html .= '<table style="width: 100%; border: none; padding-top:8px;">
                    <tr>';
        $html .=        '<td style="text-align: left; width: 100%"></td>';
        $html .=    '</tr>
                </table>';
        $html .= '<table width="100%" border="1">';
        $html .=    '<tbody>';
        $html .=    '<tr>';
		$html .=        '<td colspan="2" width="100%">Office Use Only</td>';
        $html .=    '</tr>';
        $html .=    '<tr>';
		$html .=        '<td width="30%">Status</td>';
		$html .=        '<td width="70%">'.$detail['Status']['name'].'</td>';
        $html .=    '</tr>';
        if(!empty($detail['ApprovedBy']['name']))
        {
		$html .=    '<tr>';
		$html .=        '<td width="30%">Approved By</td>';
		$html .=        '<td width="70%">'.$detail['ApprovedBy']['name'].'</td>';
        $html .=    '</tr>';
        }
        if(!empty($detail['FamilyPass']['approved']))
        {
		$html .=    '<tr>';
		$html .=        '<td>Approved at</td>';
		$html .=        '<td>'.$detail['FamilyPass']['approved'].'</td>';
        $html .=    '</tr>';
        }
		$html .=    '</tbody>';
        $html .= '</table>';
        $html .= '</html>';

        $receipt_name = "#". $detail['Applicant']['reference_no'] .": Family Pass ".  $detail['ApplyBy']['name'] ." .pdf";

        App::import('Vendor', 'MPDF', array('file' => 'mpdf/vendor/autoload.php'));

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetDisplayMode('fullpage');
        $stylesheet = file_get_contents($baseURL.'/css/report.css');
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($html,2);
		$mpdf->SetHTMLFooter('
            <table width="100%" style="vertical-align: bottom; color: #000000; font-weight: bold; font-size: 5pt;">
                <tr>
                    <td width="33%"><span style="font-weight: bold;">{PAGENO}/{nbpg}</td>
                    <td width="33%" align="center" style="font-weight: bold; color: #c6c6c6;">This is computer generated document no signature required</td>
                    <td width="33%" style="text-align: right; ">{DATE j-m-Y}</td>
                </tr>
            </table>
            ');
        $mpdf->Output($receipt_name,'I');
        exit;

    }

}
