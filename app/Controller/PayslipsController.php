<?php
class PayslipsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Payslip.staff_no' => $staff['Staff']['staff_no'],
                                        );

        $conditions['group'] = array('wage_month, wage_year');
        $conditions['order'] = array('Payslip.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Payslip'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Payslip.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Payslip.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Payslip'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Payslip']['modified'] = date("d-m-Y",strtotime($details[$i]['Payslip']['modified']));

            $details[$i]['Payslip']['created'] = date("d-m-Y",strtotime($details[$i]['Payslip']['created']));

            $details[$i]['Payslip']['id'] = $this->Utility->encrypt($details[$i]['Payslip']['id'], 'psl');
            
        }

        $this->set(compact('details'));
    }

    public function add()
    {
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->JobDesignation->create();
            if($this->JobDesignation->save($data))
            {
                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Information not successfully saved.', 'error');
            }
        }
    }

    public function edit($key = null)
    {
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'jbd');

        $detail = $this->JobDesignation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['JobDesignation']['id'] = $id;

            $this->JobDesignation->create();
            if($this->JobDesignation->save($data))
            {
                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }

        $this->set(compact('key'));
    }

    public function view($wage_month = null, $wage_year = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('PayslipCategory');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $incomes = $this->Payslip->find('all', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'PayslipCategory.payslip_type_id' => array(1, 5)),
        ));
        
        $gross_pay = $this->Payslip->find('first', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'Payslip.wage_category' => 'GP'),
        ));

        $net_pay = $this->Payslip->find('first', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'Payslip.wage_category' => 'NP'),
        ));

        $deductions = $this->Payslip->find('all', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'PayslipCategory.payslip_type_id' => array(2, 4)),
        ));

        $employer_contr = $this->Payslip->find('all', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'PayslipCategory.payslip_type_id' => array(3)),
        ));

        $count = 0;

        if($count < count($incomes))
        {
            $count = count($incomes);
        }

        if($count < count($deductions))
        {
            $count = count($deductions);
        }

        if($count < count($employer_contr))
        {
            $count = count($employer_contr);
        }

        $this->set(compact(
            'staff', 
            'count', 
            'incomes', 
            'deductions', 
            'employer_contr', 
            'net_pay', 
            'wage_month', 
            'wage_year',
            'gross_pay',
            'net_pay'
        ));
    }

    public function payslip($wage_month = null, $wage_year = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('PayslipCategory');
        
        ini_set('memory_limit', '256M');
        $baseURL = Router::url('/', true);
        $path = WWW_ROOT;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $incomes = $this->Payslip->find('all', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'PayslipCategory.payslip_type_id' => array(1, 5)),
        ));
        
        $gross_pay = $this->Payslip->find('first', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'Payslip.wage_category' => 'GP'),
        ));

        $net_pay = $this->Payslip->find('first', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'Payslip.wage_category' => 'NP'),
        ));

        $deductions = $this->Payslip->find('all', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'PayslipCategory.payslip_type_id' => array(2, 4)),
        ));

        $employer_contr = $this->Payslip->find('all', array(
            'conditions' => array('Payslip.staff_no' => $staff['Staff']['staff_no'], 'Payslip.wage_month' => $wage_month, 'Payslip.wage_year' => $wage_year, 'PayslipCategory.payslip_type_id' => array(3)),
        ));

        $count = 0;

        if($count < count($incomes))
        {
            $count = count($incomes);
        }

        if($count < count($deductions))
        {
            $count = count($deductions);
        }

        if($count < count($employer_contr))
        {
            $count = count($employer_contr);
        }

        $this->set(compact(
            'staff', 
            'path', 
            'baseURL',
            'count', 
            'incomes', 
            'deductions', 
            'employer_contr', 
            'net_pay', 
            'wage_month', 
            'wage_year',
            'gross_pay',
            'net_pay'
        ));

        $this->layout = 'default';
    }

    public function reversed()
    {
        $this->autoRender = false;
        $this->loadModel('PayslipCategory');

        $categories = $this->Payslip->find('all', array(
            'conditions' => array('Payslip.wage_category' => 'NP'),
            'group' => 'wage_code'
            
        ));

        foreach ($categories as $category) 
        {

            $cats = $this->PayslipCategory->find('all', array(
                        'conditions' => array('PayslipCategory.wage_code' => $category['Payslip']['wage_code'])
                    ));
            
            foreach ($cats as $cat) 
            {
                $data = array();

                $data['PayslipCategory']['id'] = $cat['PayslipCategory']['id'];
                $data['PayslipCategory']['payslip_type_id'] = 6;
                
                $this->PayslipCategory->create();
                $this->PayslipCategory->save($data);
            }
            
        }

        debug('Done');
        die;

    }

}