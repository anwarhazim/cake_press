<?php
class UsersController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('login', 'forgot', 'setup');
    }

    public function login()
    {
        $this->loadModel('Staff');

        if($this->request->is('post') || $this->request->is('put'))
        {
            if ($this->Auth->login()) 
            {          
                $person = $this->Auth->user();
                
                $staff = $this->Staff->findStaffByUserId($person['id']);

                $session = "";

                $data = array();
                $data['User']['id'] = $person['id'];
                $data['User']['session'] = $session;

                $this->User->create();
                $this->User->save($data);

                $this->Session->setFlash('Welcome, '.$staff['Staff']['name'], 'success');
                $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
            }
            else
            {
                $this->Session->setFlash('You have entered an invalid Staff No. or invalid Password. Please try again!', 'error');
            }
        } 
        
        $this->layout = 'blank';
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function forgot()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $check = $this->Staff->find('first', array(
                'conditions' => array('Staff.is_active' => 1, 'Staff.email' => $data['User']['email'])
            ));

            if(!empty($check))
            {
                $session = $this->Utility->generateRandomString();
                $session = $this->Utility->signature($session.$check['Staff']['id']);

                $reset = array();
                $reset['User']['id'] = $check['Staff']['user_id'];
                $reset['User']['session'] = $session;

                $this->User->create();
                $this->User->save($reset);

                $baseURL = Router::url('/', true);

                $subject = 'Reset Password PRESS';
                $from = 'press.admin@prasarana.com.my';
                $to = $check['Staff']['email'];

                $headers = "From: " . strip_tags($check['Staff']['email']) . "\r\n";
                $headers .= "Reply-To: ". strip_tags($check['Staff']['email']) . "\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $message = '<p>Please click <a href="'.$baseURL.'Users/setup/' . $session . '">here</a> to reset your new password.</p>';
                $message .= '<p>For any enquiries, please contact PRESS System Administrator.</p>';

                $Email = new CakeEmail('smtp');
                $Email->from(array( $from => 'PRESS'));
                $Email->to($to);
                $Email->template('default', null);
                $Email->emailFormat('html');
                $Email->subject($subject);
                $Email->viewVars(array('baseURL' => $baseURL, 'message' => $message,));
                $Email->send();

                $this->Session->setFlash('A link has been sent to  '.$check['Staff']['email'], 'success');
                $this->redirect(array('action' => 'forgot'));
            }
            else
            {
                $this->Session->setFlash('Your email address is not in our record.', 'error');
            }
            
        }
        
        $this->layout = 'blank';
    }

    public function password()
    {
        $this->loadModel('Staff');

        $path = Router::url('/', true);

        $person = $this->Auth->user();

        $detail = $this->User->find('first', array(
            'conditions' => array('User.is_active' => 1, 'User.id'=> $person['id'])
        ));

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['User']['id'] = $detail['User']['id'];

            $this->User->set($data);
            if($this->User->validates())
            {
                $this->User->create();
                $this->User->save($data);

                $this->Session->setFlash('Your new password successfully updated.', 'success');
                $this->redirect(array('action' => '/password'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }

        $this->set(compact('path'));
    }

    public function setup($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $detail = $this->User->find('first', array(
            'conditions' => array('User.is_active' => 1, 'User.session' => $key)
        ));
        

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['User']['id'] = $detail['User']['id'];

            $this->User->set($data);
            if($this->User->validates())
            {
                $data['User']['session'] = '';
                $this->User->create();
                $this->User->save($data);

                $this->Session->setFlash('Your new password successfully updated.', 'success');
                $this->redirect(array('action' => '/'));
            }
            else
            {
                $this->Session->setFlash('Error! Permission denied. Please try again!', 'error');
            }
            
        }
        
        $this->layout = 'blank';
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Status');
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Staff.is_active' => 1,
                                        );

        $conditions['order'] = array('User.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['User'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Staff.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('User.username LIKE' => '%' . $value . '%')
                        );
						
						$conditions['conditions']['OR'][] = array(
                            array('Staff.email LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Staff.ic_new LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(User.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(User.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Staff'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Staff');

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['User']['modified'] = date("d-m-Y",strtotime($details[$i]['User']['modified']));

            $details[$i]['User']['created'] = date("d-m-Y",strtotime($details[$i]['User']['created']));

            $details[$i]['Staff']['id'] = $this->Utility->encrypt($details[$i]['Staff']['id'], 'stf');

            $status = $this->Status->findById($details[$i]['User']['status_id']);
            $details[$i]['Status'] = $status['Status'];
        }

        $this->set(compact('details'));
    }

    public function administrator($key)
    {
        $this->loadModel('Staff');
        $this->loadModel('Role');
        $this->loadModel('UserRole');
        $this->loadModel('Status');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $role_selected = array();

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $userroles =  $this->UserRole->find('all', array(
                                                        'conditions' => array('UserRole.user_id' => $detail['User']['id'])
                                                    ));

        foreach ($userroles as $userrole) 
        {
            $role_selected[] = $userrole['UserRole']['role_id'];
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;
            $data['User']['id'] = $detail['User']['id'];

            $this->User->set($data);
            if($this->User->validates())
            {

                $this->User->create();
                $this->User->save($data);

                $user_roles = array();
                $user_roles = $data['User']['roles'];

                $userrole_excluded = array();

                foreach ($userroles as $userrole) 
                {
                    $userrole_excluded[] = $userrole['UserRole']['role_id'];
                }

                $max = count($userrole_excluded);
                for ($i = 0; $i < $max; $i++) 
                { 
                    if (!in_array($userrole_excluded[$i], $user_roles)) 
                    {
                        $this->UserRole->deleteAll(array('UserRole.user_id' => $detail['User']['id'], 'UserRole.role_id' => $userrole_excluded[$i]), false);
                    }
                }

                $max = count($user_roles);
                for ($i = 0; $i < $max; $i++) 
                { 
                    if (in_array($data['User']['roles'][$i], $role_selected)) 
                    {
                        unset($data['User']['roles'][$i]);
                    }
                }
        
                $role_choose = array();
                foreach ($data['User']['roles'] as $role_id) 
                {
                    if (!in_array($role_id, $role_choose)) 
                    {
                        $role = array();
                        $role['UserRole']['user_id'] = $detail['User']['id'];
                        $role['UserRole']['role_id'] = $role_id;
                        $role['UserRole']['created_by'] = $staff['Staff']['id'];
                        $role['UserRole']['modified_by'] = $staff['Staff']['id'];

                        $this->UserRole->create();
                        $this->UserRole->save($role);

                        $role_choose[] = $role_id;
                    }
                }

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'administrator/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }

        }
        else
        {
            $this->request->data = $detail;
        }

        $roles = $this->Role->find('list');
        $statuses = $this->Status->find('list', array(
                                                'conditions' => array('id' => array(1, 10, 11))
                                            ));

        $this->set(compact('key', 'roles', 'role_selected', 'statuses'));        
    }

    public function view($key)
    {
        $this->loadModel('Staff');
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('JobDesignation');
        $this->loadModel('OrganisationType');
        $this->loadModel('JobType');
        $this->loadModel('Location');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            $detail['Staff']['date_of_birth'] =  date("d-m-Y", strtotime($detail['Staff']['date_of_birth']));
            $this->request->data = $detail;
        }

        $disabled = "disabled";

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $maritalstatus = $this->MaritalStatus->find('list');
        $banks = $this->Bank->find('list');
        $jobdesignations = $this->JobDesignation->find('list');
        $organisationtypes = $this->OrganisationType->find('list');
        $jobtypes = $this->JobType->find('list');
        $locations = $this->Location->find('list');

        $this->set(compact('key', 'disabled', 'genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks', 'jobdesignations', 'organisationtypes', 'jobtypes', 'locations'));        
    }

    public function inactive($key)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $this->autoRender = false;

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $data['User']['id'] = $detail['User']['id'];
        $data['User']['status_id'] = 11;

        $this->User->create();
        $this->User->save($data);

        $this->Session->setFlash('Information successfully inactive.', 'success');
        $this->redirect(array('action' => '/'));
    }

    public function active($key)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $this->autoRender = false;

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'stf');

        $detail = $this->Staff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $data['User']['id'] = $detail['User']['id'];
        $data['User']['status_id'] = 10;

        $this->User->create();
        $this->User->save($data);

        $this->Session->setFlash('Information successfully inactive.', 'success');
        $this->redirect(array('action' => '/'));
    }

    public function profile()
    {
        $this->loadModel('Staff');
        $this->loadModel('Notification');
        $this->loadModel('Utility');

        $path = Router::url('/', true);

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Notification.recipient_id' => $staff['Staff']['id'],
                                        );

        $conditions['order'] = array('Notification.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Notification'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Notification.subject LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Notification.body LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Notification.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Notification.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Notification'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Notification');

        for ($i=0; $i < count($details); $i++) 
        { 
            $sender = $this->Staff->findStaffSummaryById($details[$i]['Notification']['sender_id']);

            $details[$i]['Notification']['id'] = $this->Utility->encrypt($details[$i]['Notification']['id'], 'ntf');

            $details[$i]['Notification']['modified'] = $this->Utility->datetime($details[$i]['Notification']['modified']);

            $details[$i]['Notification']['created'] = $this->Utility->datetime($details[$i]['Notification']['created']);

            $details[$i]['Sender']['name'] = $this->Utility->strlen($details[$i]['Sender']['name'], 20);

            $img = "";
            $baseURL = Router::url('/', true);
            if(!empty($sender['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-xs' src='".$baseURL."avatars/".$sender['Staff']['avatar']."'/>";
            }
            else
            {
                $img = "<img class='img-circle img-xs' src='".$baseURL."img/users/default-avatar.jpg'/>";
            }

            $details[$i]['Sender']['avatar'] = $img;

            $details[$i]['Notification']['body'] = strip_tags($this->Utility->strlen($details[$i]['Notification']['body'], 40));
        }

        $this->set(compact('details', 'path'));
    }

    public function read($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Notification');
        $this->loadModel('Utility');

        $path = Router::url('/', true);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'ntf');

        $detail = $this->Notification->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $sender = $this->Staff->findStaffSummaryById($detail['Notification']['sender_id']);

        $img = "";
        $baseURL = Router::url('/', true);
        if(!empty($sender['Staff']['avatar']))
        {
            $img = "<img class='img-circle img-md' src='".$baseURL."avatars/".$sender['Staff']['avatar']."'/>";
        }
        else
        {
            $img = "<img class='img-circle img-md' src='".$baseURL."img/users/default-avatar.jpg'/>";
        }

        $detail['Sender']['avatar'] = $img;

        $data = array();
        $data['Notification']['id'] = $detail['Notification']['id'];
        $data['Notification']['is_read'] = 99;

        $this->Notification->create();
        $this->Notification->save($data);

        $this->set(compact('key', 'detail', 'path'));        
    }

    public function avatar()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $path = Router::url('/avatars/', true);
        $localhost = Router::url('/', true);

        $person = $this->Auth->user();
        $detail = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $path = WWW_ROOT . 'avatars'.DS;
            
            $filename_main = $path.$detail['Staff']['staff_no'];
            $this->Utility->createFolder($filename_main);

            $this->User->set($data);
            if($this->User->validates())
            {
                $file = array();

                $temp = explode(".", $data['User']['avatar']['name']);
                $newfilename = $this->Utility->encrypt($detail['Staff']['user_id'], 'AtCh') . '.' . end($temp);
                move_uploaded_file($data['User']['avatar']['tmp_name'], $filename_main.DS.$newfilename);

                $file = array();

                $file['User']['id'] = $detail['Staff']['user_id'];
                $file['User']['avatar'] = $detail['Staff']['staff_no'].DS.$newfilename;
                $file['User']['modified_by'] = $detail['Staff']['id'];
                $file['User']['modified'] = date('Y-m-d H:i:s');
                
                $this->User->create();
                $this->User->save($file);

                $this->Session->setFlash('Your profile picture successfully uploaded.', 'success');
                $this->redirect('avatar');
            }
            else
            {
                $this->Session->setFlash('There was an error uploading your profile picture.', 'error');
            }
                
        }

        $this->set(compact('path', 'localhost'));

    }

}