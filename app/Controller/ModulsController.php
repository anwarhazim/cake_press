<?php
class ModulsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        //$this->Auth->allow();
    }

    public function index()
    {
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['order'] = array('Modul.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Modul'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Modul.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Modul.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Modul.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Modul'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Modul']['modified'] = date("d-m-Y",strtotime($details[$i]['Modul']['modified']));

            $details[$i]['Modul']['created'] = date("d-m-Y",strtotime($details[$i]['Modul']['created']));

            $details[$i]['Modul']['id'] = $this->Utility->encrypt($details[$i]['Modul']['id'], 'mdl');
            
        }

        $this->set(compact('details'));
    }

    public function add()
    {
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Modul']['is_active'] = 1;
            $this->Modul->create();
            if($this->Modul->save($data))
            {
                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $is_nav = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_full = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_type = array(
                    1=>'HEADER',
                    99=>'MENU'
                );

        $this->set(compact('is_nav', 'is_full', 'is_type'));
    }

    public function edit($key = null)
    {
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'mdl');

        $detail = $this->Modul->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Modul']['id'] = $id;

            $this->Modul->create();
            if($this->Modul->save($data))
            {
                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }

        $is_nav = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_full = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_type = array(
                    1=>'HEADER',
                    99=>'MENU'
                );

        $is_active = array(
                    1=>'ACTIVE',
                    99=>'INACTIVE'
                );

        $this->set(compact('key', 'is_nav', 'is_full', 'is_active', 'is_type'));
    }

    public function coordination()
    {
        $this->loadModel('Utility');

        if ($this->request->is('post') || $this->request->is('put')) 
        {
            $data = $this->request->data;
            $this->Utility->getNestableSave($data, count($data), 'Modul', '', 1);
        }

        $moduls = $this->Modul->find('threaded',
                                                        array(
                                                            'conditions' => array('Modul.is_active' => 1,),
                                                            'contain' => false,
                                                            'order' => array('Modul.order ASC'),
                                                    ));
		
        $nestable = "";
      
        $nestable = $this->Utility->getNestable($moduls, count($moduls), 'Modul', "", 'name');
    
        $this->set(compact('nestable'));
    }

    public function roles()
    {
        $this->loadModel('Role');
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['order'] = array('Role.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Role'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Role.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Role.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Role.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Role'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Role');

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Role']['modified'] = date("d-m-Y",strtotime($details[$i]['Role']['modified']));

            $details[$i]['Role']['created'] = date("d-m-Y",strtotime($details[$i]['Role']['created']));

            $details[$i]['Role']['id'] = $this->Utility->encrypt($details[$i]['Role']['id'], 'rol');

            if(empty($details[$i]['Setting']))
            {
                $details[$i]['Status']['name'] = 'UNASSIGN';
            }
            else
            {
                $details[$i]['Status']['name'] = 'ASSIGN';
            }
        }

        $this->set(compact('details'));
    }

    public function assign($key = null)
    {
        $this->loadModel('Role');
        $this->loadModel('Setting');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'rol');

        $detail = $this->Role->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            if ($this->request->is('post') || $this->request->is('put')) 
            {
                $jsfields = $this->request->data('jsfields');

                $settings = $this->Setting->find('all',
                            array(
                                'conditions' => array('Setting.role_id' => $id),
                            ));
                
                if($settings)
                {
                    $this->Setting->deleteAll(array('Setting.role_id' => $id), false);
                }

                if($jsfields)
                {

                    $jsArray = explode(',', $jsfields);

                    foreach ($jsArray as $modul_id) 
                    {
                        $data = array();
                        $data['Setting']['role_id'] = $id;
                        $data['Setting']['modul_id'] = $modul_id;

                        $this->Setting->create();
                        $this->Setting->save($data);
                    }
                }

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'assign/'.$key));
            }
        }

        $moduls = $this->Modul->find('threaded',
            array(
                'conditions' => array(
                                    'Modul.is_active' => 1,
                                ),
                'contain' => false,
                'order' => array('Modul.order ASC'),
          ));

        $tree = '';
        $setList = '';
        $sets = '';

        $tree = $this->Utility->getTreeModul($moduls, count($moduls), '');

        $settings = $this->Setting->find('list',
                                            array(
                                            'conditions' => array('Setting.role_id' => $id),
                                            'fields' => 'Setting.modul_id',
                                        ));

        foreach ($settings as $key)
        {
            $setList .= $sets . '' . $key . '';
            $sets = ',';
        }

        $this->set(compact('key', 'tree', 'setList', 'detail'));
    }

}