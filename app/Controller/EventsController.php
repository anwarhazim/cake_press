<?php
class EventsController extends AppController
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['order'] = array('Event.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Event'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Event.name LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Event.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Event.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Event'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Event']['modified'] = date("d-m-Y",strtotime($details[$i]['Event']['modified']));

            $details[$i]['Event']['created'] = date("d-m-Y",strtotime($details[$i]['Event']['created']));

            $details[$i]['Event']['event_start_date'] =  date("d-m-Y", strtotime($details[$i]['Event']['event_start_date']));
            
            $details[$i]['Event']['event_end_date'] =  date("d-m-Y", strtotime($details[$i]['Event']['event_end_date']));

            $details[$i]['Event']['id'] = $this->Utility->encrypt($details[$i]['Event']['id'], 'eve');

        }

        $this->set(compact('details'));
    }

    public function add()
    {
		$this->loadModel('Staff');
		$this->loadModel('CategoryEvent');

		$person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
			$data = $this->request->data;

			$this->Event->set($data);
            if($this->Event->validates())
            {
				$data['Event']['modified_by'] = $staff['Staff']['id'];
				$data['Event']['modified'] = date('Y-m-d H:i:s');
				$data['Event']['created_by'] = $staff['Staff']['id'];
				$data['Event']['created'] = date('Y-m-d H:i:s');

                $this->Event->create();
				$this->Event->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $categoryevents = $this->CategoryEvent->find('list');
        $this->set(compact('categoryevents'));
    }

    public function edit($key = null)
    {
		$this->loadModel('Staff');
        $this->loadModel('Utility');
        $this->loadModel('CategoryEvent');

		$person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'eve');

        $detail = $this->Event->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$data['Event']['id'] = $id;

			$this->Event->set($data);
            if($this->Event->validates())
            {
				$data['Event']['modified_by'] = $staff['Staff']['id'];
				$data['Event']['modified'] = date('Y-m-d H:i:s');

                $this->Event->create();
				$this->Event->save($data);

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $detail['Event']['event_start_date'] =  date("d-m-Y", strtotime($detail['Event']['event_start_date']));
            $detail['Event']['event_end_date'] =  date("d-m-Y", strtotime($detail['Event']['event_end_date']));
            $this->request->data = $detail;
        }

        $categoryevents = $this->CategoryEvent->find('list');
        $this->set(compact('key', 'categoryevents'));
    }
}
