<?php
class ApplicantsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Applicant.staff_id' => $staff['Staff']['list_id'],
                                            'Applicant.parent_id' => ''
                                        );

        $conditions['order'] = array('Applicant.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Applicant'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Applicant.reference_no LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Applicant.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Applicant.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Applicant'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Applicant']['id'] = $this->Utility->encrypt($details[$i]['Applicant']['id'], 'app');

            $details[$i]['Applicant']['modified'] = date("d-m-Y",strtotime($details[$i]['Applicant']['modified']));

            $details[$i]['Applicant']['created'] = date("d-m-Y",strtotime($details[$i]['Applicant']['created']));
        }

        $this->set(compact('details'));
    }

    public function updates()
    {
        $this->loadModel('Staff');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Status');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);
        
        $details = array();

        $i = 0;

        if(!empty($staff['Staff']))
        {
            if($staff['Staff']['is_flag'] == 1 && $staff['Staff']['status_id'] <= 2)
            {
                $details['Update'][$i]['id'] = '';
                $details['Update'][$i]['modul'] = 'Personal Information';
                $details['Update'][$i]['view'] = '/Staffs/profile/';
                $details['Update'][$i]['edit'] = '/Staffs/update_staff/';
                $details['Update'][$i]['status_id'] = $staff['Staff']['status_id'];

                $status = $this->Status->findById($staff['Staff']['status_id']);
                $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";

                $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($staff['Staff']['modified']));

                $i++;
            }
        }

        $educations = $this->Education->find('all', array(
                                                'conditions' => array('Education.staff_id' => $staff['Staff']['id'], 'Education.is_active' => 1, 'Education.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'institution_id', 'qualification_id', 'result', 'modified'),
                                            ));

        if(!empty($educations))
        {
            foreach ($educations as $education) 
            {
                if($education['Education']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($education['Education']['id'], 'edu');
                    $details['Update'][$i]['modul'] = 'Educations';
                    $details['Update'][$i]['view'] = '/Educations/view/';
                    $details['Update'][$i]['edit'] = '/Educations/edit/';
                    $details['Update'][$i]['status_id'] = $education['Education']['status_id'];

                    $status = $this->Status->findById($education['Education']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";

                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($education['Education']['modified']));

                    $i++;
                }
            }
        }

        $skills = $this->Skill->find('all', array(
                                                'conditions' => array('Skill.staff_id' => $staff['Staff']['id'], 'Skill.is_active' => 1, 'Skill.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'membership_no', 'from_year', 'to_year', 'status_id', 'is_status', 'is_delete', 'note', 'modified'),
                                            ));

        if(!empty($skills))
        {
            foreach ($skills as $skill) 
            {
                if($skill['Skill']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($skill['Skill']['id'], 'skl');
                    $details['Update'][$i]['modul'] = 'Professional Skills';
                    $details['Update'][$i]['view'] = '/Skills/view/';
                    $details['Update'][$i]['edit'] = '/Skills/edit/';
                    $details['Update'][$i]['status_id'] = $skill['Skill']['status_id'];

                    $status = $this->Status->findById($skill['Skill']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($skill['Skill']['modified']));

                    $i++;
                }
            }
        }

        $spouses = $this->Spouse->find('all', array(
                                                'conditions' => array('Spouse.staff_id' => $staff['Staff']['id'], 'Spouse.is_active' => 1, 'Spouse.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'is_status', 'name', 'ic_new', 'marriage_date', 'status_id', 'passport_no', 'race_id', 'religion_id', 'national_id', 'occupation', 'employer', 'employer_address', 'office_no', 'mobile_no', 'income_tax_no', 'income_tax_branch', 'note', 'modified'),
                                            ));

        if(!empty($spouses))
        {
            foreach ($spouses as $spouse) 
            {
                if($spouse['Spouse']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($spouse['Spouse']['id'], 'spou');
                    $details['Update'][$i]['modul'] = 'Spouses';
                    $details['Update'][$i]['view'] = '/Spouses/view/';
                    $details['Update'][$i]['edit'] = '/Spouses/edit/';
                    $details['Update'][$i]['status_id'] = $spouse['Spouse']['status_id'];

                    $status = $this->Status->findById($spouse['Spouse']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($spouse['Spouse']['modified']));

                    $i++;
                }
            }
        }

        $childrens = $this->Children->find('all', array(
                                                'conditions' => array('Children.staff_id' => $staff['Staff']['id'], 'Children.is_active' => 1, 'Children.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', ' ic', 'is_status', 'date_of_birth', 'status_id', 'gender_id', 'occupation', 'institution_employer', 'note', 'modified'),
                                            ));

        if(!empty($childrens))
        {
            foreach ($childrens as $children) 
            {
                if($children['Children']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($children['Children']['id'], 'chld');
                    $details['Update'][$i]['modul'] = 'Child Information';
                    $details['Update'][$i]['view'] = '/Childrens/view/';
                    $details['Update'][$i]['edit'] = '/Childrens/edit/';
                    $details['Update'][$i]['status_id'] = $children['Children']['status_id'];

                    $status = $this->Status->findById($children['Children']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($children['Children']['modified']));

                    $i++;
                }
            }
        }

        $deaths = $this->Death->find('all', array(
                                            'conditions' => array('Death.staff_id' => $staff['Staff']['id'], 'Death.is_active' => 1, 'Death.is_delete' => 99),
                                            'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'ic', ' relationship_id', 'date_of_death', 'cause_of_death', 'status_id', 'note', 'modified'),
                                        ));

        if(!empty($deaths))
        {
            foreach ($deaths as $death) 
            {
                if($death['Death']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($death['Death']['id'], 'dth');
                    $details['Update'][$i]['modul'] = 'Family Member Death';
                    $details['Update'][$i]['view'] = '/Deaths/view/';
                    $details['Update'][$i]['edit'] = '/Deaths/edit/';
                    $details['Update'][$i]['status_id'] = $death['Death']['status_id'];

                    $status = $this->Status->findById($death['Death']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($death['Death']['modified']));

                    $i++;
                }
            }
        }

        $this->set(compact('details')); 
    }

    public function submit()
    {
        $this->loadModel('Staff');
        $this->loadModel('Applicant');
        $this->loadModel('UserRole');
        $this->loadModel('Status');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Notification');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $hristeams = $this->UserRole->getStaffByRole(3);

        if(empty($hristeams))
        {
            $this->Session->setFlash('We cannot find any officer on duty. Please contact system administrator for help.', 'error');
            $this->redirect('updates');
        }
        
        $details = array();

        $i = 0;
        $str_length = 6;

        if(!empty($staff['Staff']))
        {
            if(($staff['Staff']['is_flag'] == 1) && ($staff['Staff']['status_id'] < 3) )
            {
                $details['Update'][$i]['id'] = '';
                $details['Update'][$i]['modul_id'] = 4;
                $details['Update'][$i]['view'] = '/Staffs/profile/';
                $details['Update'][$i]['edit'] = '/Staffs/update_staff/';

                $i++;
            }
        }

        //check for education if null, cannot proside to other proses
        /* Start */
        $check = $this->Education->find('count', array(
                                                            'conditions' => array('Education.staff_id' => $staff['Staff']['id'], 'Education.is_active' => 1),
                                                        ));
        if($check == 0)
        {
            $this->Session->setFlash("You don't have any data regarding education. Please fill up your latest education before sent to HRIS Team for verifications.", 'error');
            $this->redirect(array('action' => 'updates'));
        }
        /* End */

        $educations = $this->Education->find('all', array(
                                                    'conditions' => array('Education.staff_id' => $staff['Staff']['id'], 'Education.is_active' => 1, 'Education.is_delete' => 99),
                                                    'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'institution_id', 'qualification_id', 'result', 'modified'),
                                                ));

        if(!empty($educations))
        {
            foreach ($educations as $education) 
            {
                if($education['Education']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $education['Education']['id'];
                    $details['Update'][$i]['modul_id'] = 8;
                    $details['Update'][$i]['view'] = '/Educations/view/';
                    $details['Update'][$i]['edit'] = '/Educations/edit/';

                    $i++;
                }
            }
        }

        $skills = $this->Skill->find('all', array(
                                            'conditions' => array('Skill.staff_id' => $staff['Staff']['id'], 'Skill.is_active' => 1, 'Skill.is_delete' => 99),
                                            'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'membership_no', 'from_year', 'to_year', 'status_id', 'is_status', 'is_delete', 'note', 'modified'),
                                        ));

        if(!empty($skills))
        {
            foreach ($skills as $skill) 
            {
                if($skill['Skill']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $skill['Skill']['id'];
                    $details['Update'][$i]['modul_id'] = 11;
                    $details['Update'][$i]['view'] = '/Skills/view/';
                    $details['Update'][$i]['edit'] = '/Skills/edit/';

                    $i++;
                }
            }
        }

        $spouses = $this->Spouse->find('all', array(
                                            'conditions' => array('Spouse.staff_id' => $staff['Staff']['id'], 'Spouse.is_active' => 1, 'Spouse.is_delete' => 99),
                                            'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'is_status', 'name', 'ic_new', 'marriage_date', 'status_id', 'passport_no', 'race_id', 'religion_id', 'national_id', 'occupation', 'employer', 'employer_address', 'office_no', 'mobile_no', 'income_tax_no', 'income_tax_branch', 'note', 'modified'),
                                        ));

        if(!empty($spouses))
        {
            foreach ($spouses as $spouse) 
            {
                if($spouse['Spouse']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $spouse['Spouse']['id'];
                    $details['Update'][$i]['modul_id'] = 17;
                    $details['Update'][$i]['view'] = '/Spouses/view/';
                    $details['Update'][$i]['edit'] = '/Spouses/edit/';

                    $i++;
                } 
            }
        }

        $childrens = $this->Children->find('all', array(
                                                'conditions' => array('Children.staff_id' => $staff['Staff']['id'], 'Children.is_active' => 1, 'Children.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', ' ic', 'is_status', 'date_of_birth', 'status_id', 'gender_id', 'occupation', 'institution_employer', 'note', 'modified'),
                                            ));

        if(!empty($childrens))
        {
            foreach ($childrens as $children) 
            {
                if($children['Children']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $children['Children']['id'];
                    $details['Update'][$i]['modul_id'] = 20;
                    $details['Update'][$i]['view'] = '/Childrens/view/';
                    $details['Update'][$i]['edit'] = '/Childrens/edit/';

                    $i++;
                }
            }
        }

        $deaths = $this->Death->find('all', array(
                                            'conditions' => array('Death.staff_id' => $staff['Staff']['id'], 'Death.is_active' => 1, 'Death.is_delete' => 99),
                                            'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'ic', ' relationship_id', 'date_of_death', 'cause_of_death', 'status_id', 'note', 'modified'),
                                        ));

        if(!empty($deaths))
        {
            foreach ($deaths as $death) 
            {
                if($death['Death']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $death['Death']['id'];
                    $details['Update'][$i]['modul_id'] = 26;
                    $details['Update'][$i]['view'] = '/Deaths/view/';
                    $details['Update'][$i]['edit'] = '/Deaths/edit/';

                    $i++;
                }
            }
        }

        if(!empty($details))
        {
            //create and submit applicant 
            $applicant_parent = array();

            $applicant_parent['Applicant']['staff_id'] = $staff['Staff']['id'];
            $applicant_parent['Applicant']['modul_id'] = 1;
            $applicant_parent['Applicant']['status_id'] = 1;
            $applicant_parent['Applicant']['view'] = '/Hris/view/';
            $applicant_parent['Applicant']['edit'] = '/Hris/edit/';

            $this->Applicant->create();
            $this->Applicant->save($applicant_parent);
            $applicant_parent_id = $this->Applicant->id;

            $applicant_parent['Applicant']['id'] = $applicant_parent_id;
            $applicant_parent['Applicant']['key_id'] = $applicant_parent_id;
            $applicant_parent['Applicant']['reference_no'] = substr("000000{$applicant_parent_id}", -$str_length);

            $this->Applicant->create();
            $this->Applicant->save($applicant_parent);

            foreach ($details['Update'] as $update) 
            {
                $applicant_child = array();

                $applicant_child['Applicant']['staff_id'] = $staff['Staff']['id'];
                $applicant_child['Applicant']['modul_id'] = $update['modul_id'];
                $applicant_child['Applicant']['status_id'] = 1;
                $applicant_child['Applicant']['view'] = $update['view'];
                $applicant_child['Applicant']['edit'] = $update['edit'];
                $applicant_child['Applicant']['key_id'] = $update['id'];
                $applicant_child['Applicant']['parent_id'] = $applicant_parent_id;
                $applicant_child['Applicant']['created_by'] = $staff['Staff']['id'];
                $applicant_child['Applicant']['modified_by'] = $staff['Staff']['id'];

                $this->Applicant->create();
                $this->Applicant->save($applicant_child);
                $applicant_child_id = $this->Applicant->id;

                $applicant_child['Applicant']['id'] = $applicant_child_id;
                $applicant_child['Applicant']['reference_no'] = substr("000000{$applicant_child_id}", -$str_length);

                $this->Applicant->create();
                $this->Applicant->save($applicant_child);
            }

            //update status for each modul related from draf to pending
            foreach ($details['Update'] as $update) 
            {   
                $data = array();

                switch ($update['modul_id']) 
                {
                    case 4:

                        $data['Staff']['id'] = $staff['Staff']['id'];
                        $data['Staff']['status_id'] = 3;

                        $this->Staff->create();
                        $this->Staff->save($data);

                        break;
                    
                    case 8:

                        $data['Education']['id'] = $update['id'];
                        $data['Education']['status_id'] = 3;

                        $this->Education->create();
                        $this->Education->save($data);
                        
                        break;

                    case 11:

                        $data['Skill']['id'] = $update['id'];
                        $data['Skill']['status_id'] = 3;

                        $this->Skill->create();
                        $this->Skill->save($data);
                        
                        break;
                    
                    case 17:

                        $data['Spouse']['id'] = $update['id'];
                        $data['Spouse']['status_id'] = 3;

                        $this->Spouse->create();
                        $this->Spouse->save($data);
                        
                        break;

                    case 20:

                        $data['Children']['id'] = $update['id'];
                        $data['Children']['status_id'] = 3;

                        $this->Children->create();
                        $this->Children->save($data);
                        
                        break;
                    
                    case 26:

                        $data['Death']['id'] = $update['id'];
                        $data['Death']['status_id'] = 3;

                        $this->Death->create();
                        $this->Death->save($data);
                        
                        break;
                }
            }


            $subject = $staff['Staff']['name']. " have send Request Update for verification";
            //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
            $body = "<br/>";
            $body .= "Click <a href='".Router::url('/Hris/view/'.$this->Utility->encrypt($applicant_parent_id, 'app'), true)."'>here</a> to view ".$staff['Staff']['name']." request update.";

            foreach ($hristeams['HRIS'] as $hristeam) 
            {
                $notification = array();

                $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                $notification['Notification']['recipient_id'] = $hristeam['id'];
                $notification['Notification']['label'] = 'info-warning';
                $notification['Notification']['proses'] = 1;
                $notification['Notification']['is_read'] = 1;
                $notification['Notification']['subject'] = $subject;
                $notification['Notification']['body'] = $body;
                $notification['Notification']['created_by'] = $staff['Staff']['id'];
                $notification['Notification']['modified_by'] = $staff['Staff']['id'];

                $this->Notification->create();
                $this->Notification->save($notification);
            }

            $this->Session->setFlash('Your request update successfully sent to HRIS Team for verifications. Please wait for any update.', 'success');
            $this->redirect(array('action' => 'updates'));
        }

    }

    public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $details = $this->Applicant->find('threaded',
                                                        array(
                                                            'conditions' => array(
                                                                                'Applicant.staff_id' => $staff['Staff']['id'],
                                                                                'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                            ),
                                                            'contain' => false,
                                                    ));

        if(!empty($detail))
        {
            $detail['Applicant']['id'] = $this->Utility->encrypt($detail['Applicant']['id'], 'app');

            $detail['Applicant']['day_by_text'] = date('D', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['day_by_num'] = date('d', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['month'] = date('m', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['year'] = date('Y', strtotime($detail['Applicant']['created']));

            $detail['Applicant']['hour'] = date('h', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['minute'] = date('i', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['format'] = date('A', strtotime($detail['Applicant']['created']));
            
            $detail['AssignTo']['day_by_num'] = date('d', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['month'] = date('m', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['year'] = date('Y', strtotime($detail['Applicant']['assign']));

            $detail['AssignTo']['hour'] = date('h', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['minute'] = date('i', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['format'] = date('A', strtotime($detail['Applicant']['assign']));

            $detail['Applicant']['assign'] = date("d-m-Y",strtotime($detail['Applicant']['assign']));
        }

        $baseURL = Router::url('/', true);

        $assignto = $this->Staff->findStaffSummaryById($detail['AssignTo']['id']);
        $img = "";
        if(!empty($assignto['Staff']['avatar']))
        {
            $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$assignto['Staff']['avatar']."'/>";
        }
        else
        {
            $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";
        }
        $detail['AssignTo']['avatar'] = $img;

        $statues = array();
        for ($i=0; $i < count($details) ; $i++) 
        { 
            $statues[$i] = $details[$i]['Applicant']['status_id'];
            $details[$i]['Applicant']['id'] = $this->Utility->encrypt($details[$i]['Applicant']['id'], 'app');
            $details[$i]['Applicant']['modified'] = date("d-m-Y",strtotime($details[$i]['Applicant']['modified']));
        }

        $revised = false;
        if (in_array(5, $statues))
        {   
            $revised = true;
        }

        $days = array(
                    'Mon' => 'Mon',
                    'Tue' => 'Tue',
                    'Wed' => 'Wed',
                    'Thu' => 'Thu',
                    'Fri' => 'Fri',
                    'Sat' => 'Sat',
                    'Sun' => 'Sun',
                );
        
        $this->set(compact('detail', 'details', 'days', 'revised'));
    }

    public function getUpdateByStaffId($staff_id = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Status');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);
        
        $details = array();

        $i = 0;

        if(!empty($staff['Staff']))
        {
            if($staff['Staff']['is_flag'] == 1 && $staff['Staff']['status_id'] <= 2)
            {
                $details['Update'][$i]['id'] = '';
                $details['Update'][$i]['modul'] = 'Personal Information';
                $details['Update'][$i]['view'] = '/Staffs/profile/';
                $details['Update'][$i]['edit'] = '/Staffs/update_staff/';
                $details['Update'][$i]['status_id'] = $staff['Staff']['status_id'];

                $status = $this->Status->findById($staff['Staff']['status_id']);
                $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";

                $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($staff['Staff']['modified']));

                $i++;
            }
        }

        $educations = $this->Education->find('all', array(
                                                'conditions' => array('Education.staff_id' => $staff['Staff']['id'], 'Education.is_active' => 1, 'Education.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'institution_id', 'qualification_id', 'result', 'modified'),
                                            ));

        if(!empty($educations))
        {
            foreach ($educations as $education) 
            {
                if($education['Education']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($education['Education']['id'], 'edu');
                    $details['Update'][$i]['modul'] = 'Educations';
                    $details['Update'][$i]['view'] = '/Educations/view/';
                    $details['Update'][$i]['edit'] = '/Educations/edit/';
                    $details['Update'][$i]['status_id'] = $education['Education']['status_id'];

                    $status = $this->Status->findById($education['Education']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";

                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($education['Education']['modified']));

                    $i++;
                }
            }
        }

        $skills = $this->Skill->find('all', array(
                                                'conditions' => array('Skill.staff_id' => $staff['Staff']['id'], 'Skill.is_active' => 1, 'Skill.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'membership_no', 'from_year', 'to_year', 'status_id', 'is_status', 'is_delete', 'note', 'modified'),
                                            ));

        if(!empty($skills))
        {
            foreach ($skills as $skill) 
            {
                if($skill['Skill']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($skill['Skill']['id'], 'skl');
                    $details['Update'][$i]['modul'] = 'Professional Skills';
                    $details['Update'][$i]['view'] = '/Skills/view/';
                    $details['Update'][$i]['edit'] = '/Skills/edit/';
                    $details['Update'][$i]['status_id'] = $skill['Skill']['status_id'];

                    $status = $this->Status->findById($skill['Skill']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($skill['Skill']['modified']));

                    $i++;
                }
            }
        }

        $spouses = $this->Spouse->find('all', array(
                                                'conditions' => array('Spouse.staff_id' => $staff['Staff']['id'], 'Spouse.is_active' => 1, 'Spouse.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'is_status', 'name', 'ic_new', 'marriage_date', 'status_id', 'passport_no', 'race_id', 'religion_id', 'national_id', 'occupation', 'employer', 'employer_address', 'office_no', 'mobile_no', 'income_tax_no', 'income_tax_branch', 'note', 'modified'),
                                            ));

        if(!empty($spouses))
        {
            foreach ($spouses as $spouse) 
            {
                if($spouse['Spouse']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($spouse['Spouse']['id'], 'spou');
                    $details['Update'][$i]['modul'] = 'Spouses';
                    $details['Update'][$i]['view'] = '/Spouses/view/';
                    $details['Update'][$i]['edit'] = '/Spouses/edit/';
                    $details['Update'][$i]['status_id'] = $spouse['Spouse']['status_id'];

                    $status = $this->Status->findById($spouse['Spouse']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($spouse['Spouse']['modified']));

                    $i++;
                }
            }
        }

        $childrens = $this->Children->find('all', array(
                                                'conditions' => array('Children.staff_id' => $staff['Staff']['id'], 'Children.is_active' => 1, 'Children.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', ' ic', 'is_status', 'date_of_birth', 'status_id', 'gender_id', 'occupation', 'institution_employer', 'note', 'modified'),
                                            ));

        if(!empty($childrens))
        {
            foreach ($childrens as $children) 
            {
                if($children['Children']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($children['Children']['id'], 'chld');
                    $details['Update'][$i]['modul'] = 'Child Information';
                    $details['Update'][$i]['view'] = '/Childrens/view/';
                    $details['Update'][$i]['edit'] = '/Childrens/edit/';
                    $details['Update'][$i]['status_id'] = $children['Children']['status_id'];

                    $status = $this->Status->findById($children['Children']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($children['Children']['modified']));

                    $i++;
                }
            }
        }

        $deaths = $this->Death->find('all', array(
                                            'conditions' => array('Death.staff_id' => $staff['Staff']['id'], 'Death.is_active' => 1, 'Death.is_delete' => 99),
                                            'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'ic', ' relationship_id', 'date_of_death', 'cause_of_death', 'status_id', 'note', 'modified'),
                                        ));

        if(!empty($deaths))
        {
            foreach ($deaths as $death) 
            {
                if($death['Death']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($death['Death']['id'], 'dth');
                    $details['Update'][$i]['modul'] = 'Family Member Death';
                    $details['Update'][$i]['view'] = '/Deaths/view/';
                    $details['Update'][$i]['edit'] = '/Deaths/edit/';
                    $details['Update'][$i]['status_id'] = $death['Death']['status_id'];

                    $status = $this->Status->findById($death['Death']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($death['Death']['modified']));

                    $i++;
                }
            }
        }

        $updates = array();

        if(!empty($details))
        {
            foreach ($details['Update'] as $key => $detail) 
            {
                $updates[$key] = '<li class="media">
                                        <div class="media-body">
                                            <a href="'.Router::url($detail['view'].$detail['id'], true).'" class="media-heading">
                                                <span class="text-semibold">'.$detail['modul'].'</span>
                                                <span class="media-annotation pull-right">'.$detail['modified'].'</span>
                                            </a>
                                        </div>
                                    </li>';
            }
        }
        else
        {
            $updates[] = '<li class="media">
                                <div class="media-body">
                                    <span class="text-muted">No changes have been made yet...</span>
                                </div>
                            </li>';
        }

        $myJSON = json_encode($updates);

        return $myJSON;
    }

    public function getUpdateCountByStaffId($staff_id = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Status');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);
        
        $details = array();

        $i = 0;

        if(!empty($staff['Staff']))
        {
            if($staff['Staff']['is_flag'] == 1 && $staff['Staff']['status_id'] <= 2)
            {
                $details['Update'][$i]['id'] = '';
                $details['Update'][$i]['modul'] = 'Personal Information';
                $details['Update'][$i]['view'] = '/Staffs/profile/';
                $details['Update'][$i]['edit'] = '/Staffs/update_staff/';
                $details['Update'][$i]['status_id'] = $staff['Staff']['status_id'];

                $status = $this->Status->findById($staff['Staff']['status_id']);
                $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";

                $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($staff['Staff']['modified']));

                $i++;
            }
        }

        $educations = $this->Education->find('all', array(
                                                'conditions' => array('Education.staff_id' => $staff['Staff']['id'], 'Education.is_active' => 1, 'Education.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'institution_id', 'qualification_id', 'result', 'modified'),
                                            ));

        if(!empty($educations))
        {
            foreach ($educations as $education) 
            {
                if($education['Education']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($education['Education']['id'], 'edu');
                    $details['Update'][$i]['modul'] = 'Educations';
                    $details['Update'][$i]['view'] = '/Educations/view/';
                    $details['Update'][$i]['edit'] = '/Educations/edit/';
                    $details['Update'][$i]['status_id'] = $education['Education']['status_id'];

                    $status = $this->Status->findById($education['Education']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";

                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($education['Education']['modified']));

                    $i++;
                }
            }
        }

        $skills = $this->Skill->find('all', array(
                                                'conditions' => array('Skill.staff_id' => $staff['Staff']['id'], 'Skill.is_active' => 1, 'Skill.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'membership_no', 'from_year', 'to_year', 'status_id', 'is_status', 'is_delete', 'note', 'modified'),
                                            ));

        if(!empty($skills))
        {
            foreach ($skills as $skill) 
            {
                if($skill['Skill']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($skill['Skill']['id'], 'skl');
                    $details['Update'][$i]['modul'] = 'Professional Skills';
                    $details['Update'][$i]['view'] = '/Skills/view/';
                    $details['Update'][$i]['edit'] = '/Skills/edit/';
                    $details['Update'][$i]['status_id'] = $skill['Skill']['status_id'];

                    $status = $this->Status->findById($skill['Skill']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($skill['Skill']['modified']));

                    $i++;
                }
            }
        }

        $spouses = $this->Spouse->find('all', array(
                                                'conditions' => array('Spouse.staff_id' => $staff['Staff']['id'], 'Spouse.is_active' => 1, 'Spouse.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'is_status', 'name', 'ic_new', 'marriage_date', 'status_id', 'passport_no', 'race_id', 'religion_id', 'national_id', 'occupation', 'employer', 'employer_address', 'office_no', 'mobile_no', 'income_tax_no', 'income_tax_branch', 'note', 'modified'),
                                            ));

        if(!empty($spouses))
        {
            foreach ($spouses as $spouse) 
            {
                if($spouse['Spouse']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($spouse['Spouse']['id'], 'spou');
                    $details['Update'][$i]['modul'] = 'Spouses';
                    $details['Update'][$i]['view'] = '/Spouses/view/';
                    $details['Update'][$i]['edit'] = '/Spouses/edit/';
                    $details['Update'][$i]['status_id'] = $spouse['Spouse']['status_id'];

                    $status = $this->Status->findById($spouse['Spouse']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($spouse['Spouse']['modified']));

                    $i++;
                }
            }
        }

        $childrens = $this->Children->find('all', array(
                                                'conditions' => array('Children.staff_id' => $staff['Staff']['id'], 'Children.is_active' => 1, 'Children.is_delete' => 99),
                                                'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', ' ic', 'is_status', 'date_of_birth', 'status_id', 'gender_id', 'occupation', 'institution_employer', 'note', 'modified'),
                                            ));

        if(!empty($childrens))
        {
            foreach ($childrens as $children) 
            {
                if($children['Children']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($children['Children']['id'], 'chld');
                    $details['Update'][$i]['modul'] = 'Child Information';
                    $details['Update'][$i]['view'] = '/Childrens/view/';
                    $details['Update'][$i]['edit'] = '/Childrens/edit/';
                    $details['Update'][$i]['status_id'] = $children['Children']['status_id'];

                    $status = $this->Status->findById($children['Children']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($children['Children']['modified']));

                    $i++;
                }
            }
        }

        $deaths = $this->Death->find('all', array(
                                            'conditions' => array('Death.staff_id' => $staff['Staff']['id'], 'Death.is_active' => 1, 'Death.is_delete' => 99),
                                            'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'ic', ' relationship_id', 'date_of_death', 'cause_of_death', 'status_id', 'note', 'modified'),
                                        ));

        if(!empty($deaths))
        {
            foreach ($deaths as $death) 
            {
                if($death['Death']['status_id'] <= 2)
                {
                    $details['Update'][$i]['id'] = $this->Utility->encrypt($death['Death']['id'], 'dth');
                    $details['Update'][$i]['modul'] = 'Family Member Death';
                    $details['Update'][$i]['view'] = '/Deaths/view/';
                    $details['Update'][$i]['edit'] = '/Deaths/edit/';
                    $details['Update'][$i]['status_id'] = $death['Death']['status_id'];

                    $status = $this->Status->findById($death['Death']['status_id']);
                    $details['Update'][$i]['status'] = "<spam class='label label-primary'>".$status['Status']['name']."</spam>";
                    
                    $details['Update'][$i]['modified'] = date("d-m-Y",strtotime($death['Death']['modified']));

                    $i++;
                }
            }
        }

        $count = 0;
        if(!empty($details['Update']))
        {
            $count = count($details['Update']);
        }
        

        return $count;
    }

    public function details($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('JobDesignation');
        $this->loadModel('OrganisationType');
        $this->loadModel('JobType');
        $this->loadModel('Location');
        $this->loadModel('Qualification');
        $this->loadModel('Relationship');
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $parent = $this->Applicant->findById($detail['Applicant']['parent_id']);
        if(empty($parent))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($parent))
        {
            $parent['AssignTo']['day_by_num'] = date('d', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['month'] = date('m', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['year'] = date('Y', strtotime($parent['Applicant']['assign']));

            $parent['AssignTo']['hour'] = date('h', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['minute'] = date('i', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['format'] = date('A', strtotime($parent['Applicant']['assign']));

            $parent['Applicant']['assign'] = date("d-m-Y",strtotime($parent['Applicant']['assign']));

            $parent['VerifiedBy']['day_by_num'] = date('d', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['month'] = date('m', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['year'] = date('Y', strtotime($parent['Applicant']['verified']));

            $parent['VerifiedBy']['hour'] = date('h', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['minute'] = date('i', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['format'] = date('A', strtotime($parent['Applicant']['verified']));

            $parent['Applicant']['verified'] = date("d-m-Y",strtotime($parent['Applicant']['verified']));

            $parent['ApprovedBy']['day_by_num'] = date('d', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['month'] = date('m', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['year'] = date('Y', strtotime($parent['Applicant']['approved']));

            $parent['ApprovedBy']['hour'] = date('h', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['minute'] = date('i', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['format'] = date('A', strtotime($parent['Applicant']['approved']));

            $parent['Applicant']['approved'] = date("d-m-Y",strtotime($parent['Applicant']['approved']));
        }

        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($parent['AssignTo']['id']))
        {
            $assignto = $this->Staff->findStaffSummaryById($parent['AssignTo']['id']);
            if(!empty($assignto['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$assignto['Staff']['avatar']."'/>";
            }
        }
        $parent['AssignTo']['avatar'] = $img;

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($parent['VerifiedBy']['id']))
        {
            $verifiedby = $this->Staff->findStaffSummaryById($parent['VerifiedBy']['id']);
        
            if(!empty($verifiedby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$verifiedby['Staff']['avatar']."'/>";
            }
        }
        $parent['VerifiedBy']['avatar'] = $img;

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($parent['ApprovedBy']['id']))
        {
            $approvedby = $this->Staff->findStaffSummaryById($parent['ApprovedBy']['id']);
        
            if(!empty($approvedby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$approvedby['Staff']['avatar']."'/>";
            }
        }
        $parent['ApprovedBy']['avatar'] = $img;

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            switch ($data['Submit']) 
            {
                case 'staff':

                    $this->Staff->set($data);
                    if($this->Staff->validates())
                    {
                        $data['Staff']['modified_by'] = $staff['Staff']['id'];
                        $data['Staff']['modified'] = date('Y-m-d H:i:s');

                        $this->Staff->create();
                        $this->Staff->save($data);

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'details/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
                    }

                    break;
                case 'education':
                        
                    $this->Education->set($data);
                    if($this->Education->validates())
                    {
                        $data['Education']['modified_by'] = $staff['Staff']['id'];
                        $data['Education']['modified'] = date('Y-m-d H:i:s');

                        $this->Education->create();
                        $this->Education->save($data);

                        $path = WWW_ROOT . 'documents'.DS;

                        foreach ($data['Education']['attachments'] as $attachment) 
                        {
                            if($attachment['error'] == 0)
                            {
                                // <!-- start create personal forder by staff no -->
        
                                //check and create folder by staff no
                                $filename_main = $path.$staff['Staff']['staff_no'];
                                $this->Utility->createFolder($filename_main);
        
                                //check and create folder by moduls
                                $filename_main = $filename_main.DS.'EDUCATIONS';
                                $this->Utility->createFolder($filename_main);
        
                                // <!-- end create personal forlder by staff no -->
        
                                $file = array();
        
                                $file['Attachment']['key_id'] = $data['Education']['id'];
                                $file['Attachment']['name'] = $attachment['name'];
                                $file['Attachment']['modul_id'] = 8; // check table moduls to check modul id
                                $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                                $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                                $file['Attachment']['created_by'] = $staff['Staff']['id'];
                                $file['Attachment']['created'] = date('Y-m-d H:i:s');
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
        
                                $attachment_id = $this->Attachment->id;
        
                                $temp = explode(".", $attachment['name']);
                                $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                                move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);
        
                                $file = array();
        
                                $file['Attachment']['id'] = $attachment_id;
                                $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'EDUCATIONS'.DS.$newfilename;
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
                            }
                        }

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'details/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
                    }
        
                        break;
                case 'skill':

                    $this->Skill->set($data);
                    if($this->Skill->validates())
                    {
                        $data['Skill']['modified_by'] = $staff['Staff']['id'];
                        $data['Skill']['modified'] = date('Y-m-d H:i:s');

                        $this->Skill->create();
                        $this->Skill->save($data);

                        $path = WWW_ROOT . 'documents'.DS;

                        foreach ($data['Skill']['attachments'] as $attachment) 
                        {
                            if($attachment['error'] == 0)
                            {
                                // <!-- start create personal forder by staff no -->
        
                                //check and create folder by staff no
                                $filename_main = $path.$staff['Staff']['staff_no'];
                                $this->Utility->createFolder($filename_main);
        
                                //check and create folder by moduls
                                $filename_main = $filename_main.DS.'SKILLS';
                                $this->Utility->createFolder($filename_main);
        
                                // <!-- end create personal forlder by staff no -->
        
                                $file = array();
        
                                $file['Attachment']['key_id'] = $data['Skill']['id'];
                                $file['Attachment']['name'] = $attachment['name'];
                                $file['Attachment']['modul_id'] = 11; // check table moduls to check modul id
                                $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                                $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                                $file['Attachment']['created_by'] = $staff['Staff']['id'];
                                $file['Attachment']['created'] = date('Y-m-d H:i:s');
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
        
                                $attachment_id = $this->Attachment->id;
        
                                $temp = explode(".", $attachment['name']);
                                $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                                move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);
        
                                $file = array();
        
                                $file['Attachment']['id'] = $attachment_id;
                                $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'SkillS'.DS.$newfilename;
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
                            }
                        }

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'details/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
                    }


                    break;

                case 'spouse':

                    $this->Spouse->set($data);
                    if($this->Spouse->validates())
                    {
                        $data['Spouse']['modified_by'] = $staff['Staff']['id'];
                        $data['Spouse']['modified'] = date('Y-m-d H:i:s');

                        $this->Spouse->create();
                        $this->Spouse->save($data);

                        $path = WWW_ROOT . 'documents'.DS;

                        foreach ($data['Spouse']['attachments'] as $attachment) 
                        {
                            if($attachment['error'] == 0)
                            {
                                // <!-- start create personal forder by staff no -->
        
                                //check and create folder by staff no
                                $filename_main = $path.$staff['Staff']['staff_no'];
                                $this->Utility->createFolder($filename_main);
        
                                //check and create folder by moduls
                                $filename_main = $filename_main.DS.'SPOUSES';
                                $this->Utility->createFolder($filename_main);
        
                                // <!-- end create personal forlder by staff no -->
        
                                $file = array();
        
                                $file['Attachment']['key_id'] = $data['Spouse']['id'];
                                $file['Attachment']['name'] = $attachment['name'];
                                $file['Attachment']['modul_id'] = 17; // check table moduls to check modul id
                                $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                                $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                                $file['Attachment']['created_by'] = $staff['Staff']['id'];
                                $file['Attachment']['created'] = date('Y-m-d H:i:s');
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
        
                                $attachment_id = $this->Attachment->id;
        
                                $temp = explode(".", $attachment['name']);
                                $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                                move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);
        
                                $file = array();
        
                                $file['Attachment']['id'] = $attachment_id;
                                $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'SPOUSES'.DS.$newfilename;
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
                            }
                        }

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'details/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
                    }


                    break;

                case 'children':

                    $this->Children->set($data);
                    if($this->Children->validates())
                    {
                        $data['Children']['modified_by'] = $staff['Staff']['id'];
                        $data['Children']['modified'] = date('Y-m-d H:i:s');

                        $this->Children->create();
                        $this->Children->save($data);

                        $path = WWW_ROOT . 'documents'.DS;

                        foreach ($data['Children']['attachments'] as $attachment) 
                        {
                            if($attachment['error'] == 0)
                            {
                                // <!-- start create personal forder by staff no -->
        
                                //check and create folder by staff no
                                $filename_main = $path.$staff['Staff']['staff_no'];
                                $this->Utility->createFolder($filename_main);
        
                                //check and create folder by moduls
                                $filename_main = $filename_main.DS.'CHILDRENS';
                                $this->Utility->createFolder($filename_main);
        
                                // <!-- end create personal forlder by staff no -->
        
                                $file = array();
        
                                $file['Attachment']['key_id'] = $data['Children']['id'];
                                $file['Attachment']['name'] = $attachment['name'];
                                $file['Attachment']['modul_id'] = 20; // check table moduls to check modul id
                                $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                                $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                                $file['Attachment']['created_by'] = $staff['Staff']['id'];
                                $file['Attachment']['created'] = date('Y-m-d H:i:s');
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
        
                                $attachment_id = $this->Attachment->id;
        
                                $temp = explode(".", $attachment['name']);
                                $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                                move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);
        
                                $file = array();
        
                                $file['Attachment']['id'] = $attachment_id;
                                $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'CHILDRENS'.DS.$newfilename;
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
                            }
                        }

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'details/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
                    }


                    break;

                case 'death':

                    $this->Death->set($data);
                    if($this->Death->validates())
                    {
                        $data['Death']['modified_by'] = $staff['Staff']['id'];
                        $data['Death']['modified'] = date('Y-m-d H:i:s');

                        $this->Death->create();
                        $this->Death->save($data);

                        $path = WWW_ROOT . 'documents'.DS;

                        foreach ($data['Death']['attachments'] as $attachment) 
                        {
                            if($attachment['error'] == 0)
                            {
                                // <!-- start create personal forder by staff no -->
        
                                //check and create folder by staff no
                                $filename_main = $path.$staff['Staff']['staff_no'];
                                $this->Utility->createFolder($filename_main);
        
                                //check and create folder by moduls
                                $filename_main = $filename_main.DS.'DEATHS';
                                $this->Utility->createFolder($filename_main);
        
                                // <!-- end create personal forlder by staff no -->
        
                                $file = array();
        
                                $file['Attachment']['key_id'] = $data['Death']['id'];
                                $file['Attachment']['name'] = $attachment['name'];
                                $file['Attachment']['modul_id'] = 26; // check table moduls to check modul id
                                $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                                $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                                $file['Attachment']['created_by'] = $staff['Staff']['id'];
                                $file['Attachment']['created'] = date('Y-m-d H:i:s');
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
        
                                $attachment_id = $this->Attachment->id;
        
                                $temp = explode(".", $attachment['name']);
                                $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                                move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);
        
                                $file = array();
        
                                $file['Attachment']['id'] = $attachment_id;
                                $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'DEATHS'.DS.$newfilename;
        
                                $this->Attachment->create();
                                $this->Attachment->save($file);
                            }
                        }

                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'details/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
                    }


                    break;
            }
        }

        $parent_key = $this->Utility->encrypt($detail['Applicant']['parent_id'], 'app');
        $detail['Applicant']['day_by_text'] = date('D', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['day_by_num'] = date('d', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['month'] = date('m', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['year'] = date('Y', strtotime($detail['Applicant']['created']));

        $detail['Applicant']['hour'] = date('h', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['minute'] = date('i', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['format'] = date('A', strtotime($detail['Applicant']['created']));

        $detail['Applicant']['id'] = $this->Utility->encrypt($detail['Applicant']['id'], 'app');

        switch ($detail['Applicant']['modul_id']) 
        {
            case 4: //Personal Details
                $person = $this->Staff->findById($detail['Applicant']['staff_id']);
                $person['Staff']['date_of_birth'] =  date("d-m-Y", strtotime($person['Staff']['date_of_birth']));
                $this->request->data = $person;
                break;
            case 8: //Education
                $education = $this->Education->findById($detail['Applicant']['key_id']);
                $education['Education']['duration_from'] =  date("d-m-Y", strtotime($education['Education']['duration_from']));
                $education['Education']['duration_to'] =  date("d-m-Y", strtotime($education['Education']['duration_to']));
                $this->request->data = $education;
                break;

            case 11: //Skill
                $skill = $this->Skill->findById($detail['Applicant']['key_id']);
                $skill['Skill']['from_year'] =  date("d-m-Y", strtotime($skill['Skill']['from_year']));
                $skill['Skill']['to_year'] =  date("d-m-Y", strtotime($skill['Skill']['to_year']));
                $this->request->data = $skill;
                break;

            case 17: //Spouse
                $spouse = $this->Spouse->findById($detail['Applicant']['key_id']);
                $spouse['Spouse']['marriage_date'] =  date("d-m-Y", strtotime($spouse['Spouse']['marriage_date']));
                $this->request->data = $spouse;
                break;

            case 20: //Children
                $children = $this->Children->findById($detail['Applicant']['key_id']);
                $children['Children']['date_of_birth'] =  date("d-m-Y", strtotime($children['Children']['date_of_birth']));
                $this->request->data = $children;
                break;

            case 26: //Death
                $death = $this->Death->findById($detail['Applicant']['key_id']);
                $death['Death']['date_of_death'] =  date("d-m-Y", strtotime($death['Death']['date_of_death']));
                $this->request->data = $death;
                break;
        }

        $disabled = '';
        if($detail['Applicant']['status_id'] != 5)
        {
            $disabled = 'disabled';
        }
        
        $days = array(
                    'Mon' => 'Mon',
                    'Tue' => 'Tue',
                    'Wed' => 'Wed',
                    'Thu' => 'Thu',
                    'Fri' => 'Fri',
                    'Sat' => 'Sat',
                    'Sun' => 'Sun',
                );
        
        $path = Router::url('/documents/', true);

        $statuses = array(
                        1 => 'ACTIVE',
                        99 => 'INACTIVE',
                    );

        $team_checked = false;
        if($parent['Applicant']['status_id'] == 4 )
        {
            $team_checked = true;
        }

        $approved = false;
        if($parent['Applicant']['status_id'] == 7 )
        {
            $approved = true;
        }

        $assignto = false;
        if($parent['Applicant']['assign_to'] == $staff['Staff']['id'])
        {
            $assignto = true;
        }

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $maritalstatus = $this->MaritalStatus->find('list');
        $banks = $this->Bank->find('list');
        $jobdesignations = $this->JobDesignation->find('list');
        $organisationtypes = $this->OrganisationType->find('list');
        $jobtypes = $this->JobType->find('list');
        $locations = $this->Location->find('list');
        $qualifications = $this->Qualification->find('list');
        $relationships = $this->Relationship->find('list');

        $this->set(compact('key', 'parent_key', 'detail', 'parent', 'disabled', 'days', 'path', 'statuses', 'team_checked', 'approved', 'genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks', 'jobdesignations', 'organisationtypes', 'jobtypes', 'locations', 'qualifications', 'relationships', 'assignto'));
    }

    public function send_revised_team($key = null) 
    {
        $this->loadModel('Staff');
        $this->loadModel('Notification');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($detail))
        {
            $data = array();

            $data['Applicant']['id'] = $detail['Applicant']['id'];
            $data['Applicant']['status_id'] = 6;

            $this->Applicant->create();
            $this->Applicant->save($data);
        }

        $details = $this->Applicant->find('threaded',
                                                array(
                                                    'conditions' => array(
                                                                        'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                        'Applicant.status_id' => 5,
                                                                    ),
                                                    'contain' => false,
                                            ));

        for ($i=0; $i < count($details) ; $i++) 
        { 
            switch ($details[$i]['Applicant']['modul_id']) 
            {
                case 4:
                    # Staff
                    $data = array();
                    $data['Applicant']['id'] = $details[$i]['Applicant']['id'];
                    $data['Applicant']['status_id'] = 6;
        
                    $this->Applicant->create();
                    $this->Applicant->save($data);

                    $personal = array();
                    $personal['Staff']['id'] = $detail['Applicant']['staff_id'];
                    $personal['Staff']['status_id'] = 6;

                    $this->Staff->create();
                    $this->Staff->save($personal);

                    break;
                case 8:
                    # Education

                    $data = array();
                    $data['Applicant']['id'] = $details[$i]['Applicant']['id'];
                    $data['Applicant']['status_id'] = 6;
        
                    $this->Applicant->create();
                    $this->Applicant->save($data);

                    $education = array();
                    $education['Education']['id'] = $details[$i]['Applicant']['key_id'];
                    $education['Education']['status_id'] = 6;

                    $this->Education->create();
                    $this->Education->save($education);

                    break;
                case 11:
                    # Skill

                    $data = array();
                    $data['Applicant']['id'] = $details[$i]['Applicant']['id'];
                    $data['Applicant']['status_id'] = 6;
        
                    $this->Applicant->create();
                    $this->Applicant->save($data);

                    $skill = array();
                    $skill['Skill']['id'] = $details[$i]['Applicant']['key_id'];
                    $skill['Skill']['status_id'] = 6;

                    $this->Skill->create();
                    $this->Skill->save($skill);

                    break;
                case 17:
                    # Spouse

                    $data = array();
                    $data['Applicant']['id'] = $details[$i]['Applicant']['id'];
                    $data['Applicant']['status_id'] = 6;
        
                    $this->Applicant->create();
                    $this->Applicant->save($data);

                    $spouse = array();
                    $spouse['Spouse']['id'] = $details[$i]['Applicant']['key_id'];
                    $spouse['Spouse']['status_id'] = 6;

                    $this->Spouse->create();
                    $this->Spouse->save($spouse);

                    break;  
                    
                case 20:
                    # Children

                    $data = array();
                    $data['Applicant']['id'] = $details[$i]['Applicant']['id'];
                    $data['Applicant']['status_id'] = 6;
        
                    $this->Applicant->create();
                    $this->Applicant->save($data);

                    $children = array();
                    $children['Children']['id'] = $details[$i]['Applicant']['key_id'];
                    $children['Children']['status_id'] = 6;

                    $this->Children->create();
                    $this->Children->save($children);

                    break;

                case 26:
                    # Death

                    $data = array();
                    $data['Applicant']['id'] = $details[$i]['Applicant']['id'];
                    $data['Applicant']['status_id'] = 6;
        
                    $this->Applicant->create();
                    $this->Applicant->save($data);

                    $death = array();
                    $death['Death']['id'] = $details[$i]['Applicant']['key_id'];
                    $death['Death']['status_id'] = 6;

                    $this->Death->create();
                    $this->Death->save($death);

                    break;
            }
        }

        $subject = $detail['ApplyBy']['name']." update for " . $detail['Applicant']['reference_no'] . "  has to be revised";
        //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
        $body = "<br/>";
        $body .= "Click <a href='".Router::url('/Hris/view/'. $key , true)."'>here</a> to view the changes.";

        $notification = array();

        $notification['Notification']['sender_id'] = $staff['Staff']['id'];
        $notification['Notification']['recipient_id'] = $detail['AssignTo']['id'];
        $notification['Notification']['label'] = 'info-warning';
        $notification['Notification']['proses'] = 1;
        $notification['Notification']['is_read'] = 1;
        $notification['Notification']['subject'] = $subject;
        $notification['Notification']['body'] = $body;
        $notification['Notification']['created_by'] = $staff['Staff']['id'];
        $notification['Notification']['modified_by'] = $staff['Staff']['id'];

        $this->Notification->create();
        $this->Notification->save($notification);

        $this->Session->setFlash('Revised successfully send', 'success');
        $this->redirect(array('action' => 'view/'.$key));
    }

    public function attachment($id=null, $key=null)
    {
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $path = WWW_ROOT . 'documents'.DS;

        if(empty($id))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $detail = $this->Attachment->findById($id);

        $directory = $path.$detail['Attachment']['path'];

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            if($this->Attachment->delete($id))
            {
                $directory = $path.$detail['Attachment']['path'];
                unlink($directory);

                $this->Session->setFlash('Attachment successfully deleted.', 'success');
                $this->redirect(array('action' => 'details/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Attachment not successfully deleted. Please try again!', 'error');
                $this->redirect(array('action' => 'details/'.$key));
            }
        }
    }

}