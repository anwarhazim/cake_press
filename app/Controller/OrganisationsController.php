<?php
class OrganisationsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'add', 'edit', 'coordination', 'getOrganisationByOrganisationTypeId', 'getDivisionByOrganisationId', 'getDepartmentByDivisionId', 'getSectionByDepartmentId', 'getUnitBySectiontId', 'getSubUnitByUnitId');
    }

    public function index()
    {
        $this->loadModel('Utility');

        $person = $this->Auth->user();

        $conditions = array();

        $conditions['order'] = array('Organisation.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Organisation'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Organisation.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Organisation.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Organisation.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Organisation'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Organisation']['modified'] = date("d-m-Y",strtotime($details[$i]['Organisation']['modified']));

            $details[$i]['Organisation']['created'] = date("d-m-Y",strtotime($details[$i]['Organisation']['created']));

            $details[$i]['Organisation']['id'] = $this->Utility->encrypt($details[$i]['Organisation']['id'], 'org');
            
        }

        $this->set(compact('details'));
    }

    public function add()
    {
        $this->loadModel('OrganisationType');

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Organisation']['is_active'] = 1;

            $this->Organisation->create();
            if($this->Organisation->save($data))
            {
                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $organisationtypes = $this->OrganisationType->find('list');

        $this->set(compact('organisationtypes'));
    }

    public function edit($key = null)
    {
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'org');

        $detail = $this->Organisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Organisation']['id'] = $id;

            $this->Organisation->create();
            if($this->Organisation->save($data))
            {
                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }

        $this->set(compact('key'));
    }

    public function coordination()
    {
        $this->loadModel('Utility');

        if ($this->request->is('post') || $this->request->is('put')) 
        {
            $data = $this->request->data;
            $this->Utility->getNestableSave($data, count($data), 'Organisation', '', 1);
        }

        $organisations = $this->Organisation->find('threaded',
                                                        array(
                                                            'conditions' => array('Organisation.is_active' => 1,),
                                                            'contain' => false,
                                                            'order' => array('Organisation.order ASC'),
                                                    ));
		
        $nestable = "";
      
        $nestable = $this->Utility->getNestable($organisations, count($organisations), 'Organisation', "", 'name');
    
        $this->set(compact('nestable'));
    }

    public function getOrganisationByOrganisationTypeId($organisation_type_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $organisations = $this->Organisation->find('list',
                                                        array(
                                                            'conditions' => array(
                                                                                'Organisation.organisation_type_id'=>$organisation_type_id, 
                                                                                'Organisation.organisation_category_id'=>1, 
                                                                                'Organisation.is_active'=>1,
                                                                            ),
                                                            'contain' => false,
                                                            'order' => array('Organisation.name ASC'),
                                                    ));

        $myJSON = json_encode($organisations);

        return $myJSON;
    }

    public function getDivisionByOrganisationId($organisation_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $organisations = $this->Organisation->find('list',
                                                        array(
                                                            'conditions' => array(
                                                                                'Organisation.parent_id'=>$organisation_id, 
                                                                                'Organisation.organisation_category_id'=>2, 
                                                                                'Organisation.is_active'=>1,
                                                                            ),
                                                            'contain' => false,
                                                            'order' => array('Organisation.name ASC'),
                                                    ));

        $myJSON = json_encode($organisations);

        return $myJSON;
    }

    public function getDepartmentByDivisionId($division_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $organisations = $this->Organisation->find('list',
                                                        array(
                                                            'conditions' => array(
                                                                                'Organisation.parent_id'=>$division_id, 
                                                                                'Organisation.organisation_category_id'=>3, 
                                                                                'Organisation.is_active'=>1,
                                                                            ),
                                                            'contain' => false,
                                                            'order' => array('Organisation.name ASC'),
                                                    ));

        $myJSON = json_encode($organisations);

        return $myJSON;
    }

    public function getSectionByDepartmentId($department_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $organisations = $this->Organisation->find('list',
                                                        array(
                                                            'conditions' => array(
                                                                                'Organisation.parent_id'=>$department_id, 
                                                                                'Organisation.organisation_category_id'=>4, 
                                                                                'Organisation.is_active'=>1,
                                                                            ),
                                                            'contain' => false,
                                                            'order' => array('Organisation.name ASC'),
                                                    ));

        $myJSON = json_encode($organisations);

        return $myJSON;
    }

    public function getUnitBySectiontId($section_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $organisations = $this->Organisation->find('list',
                                                        array(
                                                            'conditions' => array(
                                                                                'Organisation.parent_id'=>$section_id, 
                                                                                'Organisation.organisation_category_id'=>5, 
                                                                                'Organisation.is_active'=>1,
                                                                            ),
                                                            'contain' => false,
                                                            'order' => array('Organisation.name ASC'),
                                                    ));

        $myJSON = json_encode($organisations);

        return $myJSON;
    }

    public function getSubUnitByUnitId($unit_id = null)
    {
        $this->layout = false;
        $this->autoRender = false;

        $organisations = $this->Organisation->find('list',
                                                        array(
                                                            'conditions' => array(
                                                                                'Organisation.parent_id'=>$unit_id, 
                                                                                'Organisation.organisation_category_id'=>6, 
                                                                                'Organisation.is_active'=>1,
                                                                            ),
                                                            'contain' => false,
                                                            'order' => array('Organisation.name ASC'),
                                                    ));

        $myJSON = json_encode($organisations);

        return $myJSON;
    }   
}