<?php
class HrisController  extends AppController 
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');
    public $uses = array();

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Applicant');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Applicant.parent_id' => ''
                                        );

        $conditions['order'] = array('Applicant.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Applicant'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions'][] = array(
                            array('Applicant.reference_no LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Applicant.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Applicant.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Applicant'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Applicant');

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Applicant']['id'] = $this->Utility->encrypt($details[$i]['Applicant']['id'], 'app');

            $details[$i]['Applicant']['modified'] = date("d-m-Y",strtotime($details[$i]['Applicant']['modified']));

            $details[$i]['Applicant']['created'] = date("d-m-Y",strtotime($details[$i]['Applicant']['created']));
        }

        $this->set(compact('details'));
    }

    public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Applicant');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $roles = $this->UserRole->getRoleByUserId($staff['Staff']['user_id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        $statues = array();

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['ApplyBy']['id']))
        {
            $applyby = $this->Staff->findStaffSummaryById($detail['ApplyBy']['id']);

            if(!empty($applyby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
            }
        }
        $detail['ApplyBy']['avatar'] = $img;

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['AssignTo']['id']))
        {
            $assignto = $this->Staff->findStaffSummaryById($detail['AssignTo']['id']);
        
            if(!empty($assignto['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$assignto['Staff']['avatar']."'/>";
            }
        }
        $detail['AssignTo']['avatar'] = $img;

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['VerifiedBy']['id']))
        {
            $verifiedby = $this->Staff->findStaffSummaryById($detail['VerifiedBy']['id']);
        
            if(!empty($verifiedby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$verifiedby['Staff']['avatar']."'/>";
            }
        }
        $detail['VerifiedBy']['avatar'] = $img;

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($detail['ApprovedBy']['id']))
        {
            $approvedby = $this->Staff->findStaffSummaryById($detail['ApprovedBy']['id']);
        
            if(!empty($approvedby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$approvedby['Staff']['avatar']."'/>";
            }
        }
        $detail['ApprovedBy']['avatar'] = $img;

        $details = $this->Applicant->find('threaded',
                                                        array(
                                                            'conditions' => array(
                                                                                'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                            ),
                                                            'contain' => false,
                                                    ));

        if(!empty($detail))
        {
            $detail['Applicant']['id'] = $this->Utility->encrypt($detail['Applicant']['id'], 'app');

            $detail['Applicant']['day_by_text'] = date('D', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['day_by_num'] = date('d', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['month'] = date('m', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['year'] = date('Y', strtotime($detail['Applicant']['created']));

            $detail['Applicant']['hour'] = date('h', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['minute'] = date('i', strtotime($detail['Applicant']['created']));
            $detail['Applicant']['format'] = date('A', strtotime($detail['Applicant']['created']));

            $detail['AssignTo']['day_by_num'] = date('d', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['month'] = date('m', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['year'] = date('Y', strtotime($detail['Applicant']['assign']));

            $detail['AssignTo']['hour'] = date('h', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['minute'] = date('i', strtotime($detail['Applicant']['assign']));
            $detail['AssignTo']['format'] = date('A', strtotime($detail['Applicant']['assign']));

            $detail['Applicant']['assign'] = date("d-m-Y",strtotime($detail['Applicant']['assign']));

            $detail['VerifiedBy']['day_by_num'] = date('d', strtotime($detail['Applicant']['verified']));
            $detail['VerifiedBy']['month'] = date('m', strtotime($detail['Applicant']['verified']));
            $detail['VerifiedBy']['year'] = date('Y', strtotime($detail['Applicant']['verified']));

            $detail['VerifiedBy']['hour'] = date('h', strtotime($detail['Applicant']['verified']));
            $detail['VerifiedBy']['minute'] = date('i', strtotime($detail['Applicant']['verified']));
            $detail['VerifiedBy']['format'] = date('A', strtotime($detail['Applicant']['verified']));

            $detail['Applicant']['verified'] = date("d-m-Y",strtotime($detail['Applicant']['verified']));

            $detail['ApprovedBy']['day_by_num'] = date('d', strtotime($detail['Applicant']['approved']));
            $detail['ApprovedBy']['month'] = date('m', strtotime($detail['Applicant']['approved']));
            $detail['ApprovedBy']['year'] = date('Y', strtotime($detail['Applicant']['approved']));

            $detail['ApprovedBy']['hour'] = date('h', strtotime($detail['Applicant']['approved']));
            $detail['ApprovedBy']['minute'] = date('i', strtotime($detail['Applicant']['approved']));
            $detail['ApprovedBy']['format'] = date('A', strtotime($detail['Applicant']['approved']));

            $detail['Applicant']['approved'] = date("d-m-Y",strtotime($detail['Applicant']['approved']));

            $summary = $this->Staff->findStaffSummaryById($detail['Applicant']['staff_id']); 

            $detail['CreatedBy'] = $summary['Staff'];
        }

        for ($i=0; $i < count($details) ; $i++) 
        { 
            $details[$i]['Applicant']['id'] = $this->Utility->encrypt($details[$i]['Applicant']['id'], 'app');
            $details[$i]['Applicant']['modified'] = date("d-m-Y",strtotime($details[$i]['Applicant']['modified']));
        
            $statues[] = $details[$i]['Applicant']['status_id'];
        }

        $days = array(
                    'Mon' => 'Mon',
                    'Tue' => 'Tue',
                    'Wed' => 'Wed',
                    'Thu' => 'Thu',
                    'Fri' => 'Fri',
                    'Sat' => 'Sat',
                    'Sun' => 'Sun',
                );
        
        $verified = true;
        $correction_team = false;
        $team_checked = false;
        $correction_leader = false;
        $approved = true;

        if($detail['Applicant']['status_id'] == 4 || $detail['Applicant']['status_id'] == 6 || $detail['Applicant']['status_id'] == 8)
        {
            $team_checked = true;
        }

        if (in_array(4, $statues))
        {   
            $verified = false;
        }
        elseif (in_array(6, $statues))
        {   
            $verified = false;
        }
        elseif (in_array(8, $statues))
        {   
            $verified = false;
        }
        else
        {
            if (in_array(5, $statues))
            {
                $verified = false;
                $correction_team = true;
            }
        }

        foreach ($statues as $status) 
        {
           if($status < 10)
           {
                $approved = false;
                break;
           }
        }

        if (!in_array(7, $statues))
        {
            foreach ($statues as $status) 
            {
                if($status == 8)
                {
                        $correction_leader = true;
                        break;
                }
            }
        }

        $assignto = false;
        if($detail['Applicant']['assign_to'] == $staff['Staff']['id'])
        {
            $assignto = true;
        }

        $hrleader = false;
        if (in_array(4, $roles))
        {   
            $hrleader = true;
        }

        // debug($team_checked);
        // debug($assignto);
        // die;

        $this->set(compact('key', 'detail', 'details', 'days', 'verified', 'correction_team', 'team_checked', 'approved', 'assignto', 'hrleader', 'correction_leader'));
    }

    public function accept($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $details = $this->Applicant->find('threaded',
                                                        array(
                                                            'conditions' => array(
                                                                                'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                            ),
                                                            'contain' => false,
                                                    ));

        for ($i=0; $i < count($details) ; $i++) 
        { 
            $data = array();

            $data['Applicant']['id'] = $details[$i]['Applicant']['id'];
            $data['Applicant']['status_id'] = 4;
            $data['Applicant']['assign_to'] = $staff['Staff']['id'];
            $data['Applicant']['assign'] = date('Y-m-d H:i:s');
            
            $this->Applicant->create();
            $this->Applicant->save($data);

            switch ($details[$i]['Applicant']['modul_id']) 
            {
                case 4:
                    # staff
                    $data = array();

                    $data['Staff']['id'] = $details[$i]['Applicant']['staff_id'];
                    $data['Staff']['status_id'] = 4;

                    $this->Staff->create();
                    $this->Staff->save($data);

                    break;
                case 8:
                    # education

                    $data = array();

                    $data['Education']['id'] = $details[$i]['Applicant']['key_id'];
                    $data['Education']['status_id'] = 4;

                    $this->Education->create();
                    $this->Education->save($data);

                    break;
                case 11:
                    # skill

                    $data = array();

                    $data['Skill']['id'] = $details[$i]['Applicant']['key_id'];
                    $data['Skill']['status_id'] = 4;

                    $this->Skill->create();
                    $this->Skill->save($data);

                    break;
                case 17:
                    # spouse

                    $data = array();

                    $data['Spouse']['id'] = $details[$i]['Applicant']['key_id'];
                    $data['Spouse']['status_id'] = 4;

                    $this->Spouse->create();
                    $this->Spouse->save($data);

                    break;
                case 20:
                    # children

                    $data = array();

                    $data['Children']['id'] = $details[$i]['Applicant']['key_id'];
                    $data['Children']['status_id'] = 4;

                    $this->Children->create();
                    $this->Children->save($data);

                    break;
                case 26:
                    # death

                    $data = array();

                    $data['Death']['id'] = $details[$i]['Applicant']['key_id'];
                    $data['Death']['status_id'] = 4;

                    $this->Death->create();
                    $this->Death->save($data);

                    break;

            }
        }

        if(!empty($detail))
        {
            $data = array();

            $data['Applicant']['id'] = $detail['Applicant']['id'];
            $data['Applicant']['status_id'] = 4;
            $data['Applicant']['assign_to'] = $staff['Staff']['id'];
            $data['Applicant']['assign'] = date('Y-m-d H:i:s');

            $this->Applicant->create();
            $this->Applicant->save($data);
        }

        $this->Session->setFlash('Information successfully updated.', 'success');
        $this->redirect(array('action' => 'view/'.$key));
    }

    public function send_correction_team($key = null) 
    {
        $this->loadModel('Staff');
        $this->loadModel('Notification');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($detail))
        {
            $data = array();

            $data['Applicant']['id'] = $detail['Applicant']['id'];
            $data['Applicant']['status_id'] = 5;

            $this->Applicant->create();
            $this->Applicant->save($data);
        }

        $details = $this->Applicant->find('threaded',
                                                array(
                                                    'conditions' => array(
                                                                        'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                        'Applicant.status_id' => 5,
                                                                    ),
                                                    'contain' => false,
                                            ));

        for ($i=0; $i < count($details) ; $i++) 
        { 
            switch ($details[$i]['Applicant']['modul_id']) 
            {
                case 4:
                    # Staff

                    $personal = array();
                    $personal['Staff']['id'] = $detail['Applicant']['staff_id'];
                    $personal['Staff']['status_id'] = 5;

                    $this->Staff->create();
                    $this->Staff->save($personal);

                    break;
                case 8:
                    # Education

                    $education = array();
                    $education['Education']['id'] = $details[$i]['Applicant']['key_id'];
                    $education['Education']['status_id'] = 5;

                    $this->Education->create();
                    $this->Education->save($education);

                    break;
                case 11:
                    # Skill

                    $skill = array();
                    $skill['Skill']['id'] = $details[$i]['Applicant']['key_id'];
                    $skill['Skill']['status_id'] = 5;

                    $this->Skill->create();
                    $this->Skill->save($skill);

                    break;
                case 17:
                    # Spouse

                    $spouse = array();
                    $spouse['Spouse']['id'] = $details[$i]['Applicant']['key_id'];
                    $spouse['Spouse']['status_id'] = 5;

                    $this->Spouse->create();
                    $this->Spouse->save($spouse);

                    break;  
                    
                case 20:
                    # Children

                    $children = array();
                    $children['Children']['id'] = $details[$i]['Applicant']['key_id'];
                    $children['Children']['status_id'] = 5;

                    $this->Children->create();
                    $this->Children->save($children);

                    break;

                case 26:
                    # Death

                    $death = array();
                    $death['Death']['id'] = $details[$i]['Applicant']['key_id'];
                    $death['Death']['status_id'] = 5;

                    $this->Death->create();
                    $this->Death->save($death);

                    break;
            }
        }

        $subject = "Your request update for " . $detail['Applicant']['reference_no'] . " has a correction";
        //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
        $body = "<br/>";
        $body .= "Click <a href='".Router::url('/Applicants/view/'. $key , true)."'>here</a> to view your update.";

        $notification = array();

        $notification['Notification']['sender_id'] = $staff['Staff']['id'];
        $notification['Notification']['recipient_id'] = $detail['ApplyBy']['id'];
        $notification['Notification']['label'] = 'info-warning';
        $notification['Notification']['proses'] = 1;
        $notification['Notification']['is_read'] = 1;
        $notification['Notification']['subject'] = $subject;
        $notification['Notification']['body'] = $body;
        $notification['Notification']['created_by'] = $staff['Staff']['id'];
        $notification['Notification']['modified_by'] = $staff['Staff']['id'];

        $this->Notification->create();
        $this->Notification->save($notification);

        $this->Session->setFlash('Correction successfully sent!', 'success');
        $this->redirect(array('action' => 'view/'.$key));
    }

    public function send_correction_leader($key=null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Notification');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($detail))
        {
            $data = array();

            $data['Applicant']['id'] = $detail['Applicant']['id'];
            $data['Applicant']['status_id'] = 8;

            $this->Applicant->create();
            $this->Applicant->save($data);
        }

        $details = $this->Applicant->find('threaded',
                                                array(
                                                    'conditions' => array(
                                                                        'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                        'Applicant.status_id' => 8,
                                                                    ),
                                                    'contain' => false,
                                            ));

        for ($i=0; $i < count($details) ; $i++) 
        { 
            switch ($details[$i]['Applicant']['modul_id']) 
            {
                case 4:
                    # Staff

                    $personal = array();
                    $personal['Staff']['id'] = $detail['Applicant']['staff_id'];
                    $personal['Staff']['status_id'] = 8;

                    $this->Staff->create();
                    $this->Staff->save($personal);

                    break;
                case 8:
                    # Education

                    $education = array();
                    $education['Education']['id'] = $details[$i]['Applicant']['key_id'];
                    $education['Education']['status_id'] = 8;

                    $this->Education->create();
                    $this->Education->save($education);

                    break;
                case 11:
                    # Skill

                    $skill = array();
                    $skill['Skill']['id'] = $details[$i]['Applicant']['key_id'];
                    $skill['Skill']['status_id'] = 8;

                    $this->Skill->create();
                    $this->Skill->save($skill);

                    break;
                case 17:
                    # Spouse

                    $spouse = array();
                    $spouse['Spouse']['id'] = $details[$i]['Applicant']['key_id'];
                    $spouse['Spouse']['status_id'] = 8;

                    $this->Spouse->create();
                    $this->Spouse->save($spouse);

                    break;  
                    
                case 20:
                    # Children

                    $children = array();
                    $children['Children']['id'] = $details[$i]['Applicant']['key_id'];
                    $children['Children']['status_id'] = 8;

                    $this->Children->create();
                    $this->Children->save($children);

                    break;

                case 26:
                    # Death

                    $death = array();
                    $death['Death']['id'] = $details[$i]['Applicant']['key_id'];
                    $death['Death']['status_id'] = 8;

                    $this->Death->create();
                    $this->Death->save($death);

                    break;
            }
        }

        $subject = "Update for " . $detail['Applicant']['reference_no'] . " has a correction";
        //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
        $body = "<br/>";
        $body .= "Click <a href='".Router::url('/Hris/view/'. $key , true)."'>here</a> to view ".$detail['Applicant']['reference_no']." correction.";

        $notification = array();

        $notification['Notification']['sender_id'] = $staff['Staff']['id'];
        $notification['Notification']['recipient_id'] = $detail['AssignTo']['id'];
        $notification['Notification']['label'] = 'info-warning';
        $notification['Notification']['proses'] = 1;
        $notification['Notification']['is_read'] = 1;
        $notification['Notification']['subject'] = $subject;
        $notification['Notification']['body'] = $body;
        $notification['Notification']['created_by'] = $staff['Staff']['id'];
        $notification['Notification']['modified_by'] = $staff['Staff']['id'];

        $this->Notification->create();
        $this->Notification->save($notification);

        $this->Session->setFlash('Correction successfully sent!', 'success');
        $this->redirect(array('action' => 'view/'.$key));
    }
    
    public function send_verified($key = null) 
    {
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Notification');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $details = $this->Applicant->find('threaded',
                                                        array(
                                                            'conditions' => array(
                                                                                'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                            ),
                                                            'contain' => false,
                                                    ));

        for ($i=0; $i < count($details) ; $i++) 
        { 
            switch ($details[$i]['Applicant']['modul_id']) 
            {
                case 4:
                    # Staff

                    $personal = array();
                    $personal['Staff']['id'] = $detail['Applicant']['staff_id'];
                    $personal['Staff']['status_id'] = 7;

                    $this->Staff->create();
                    $this->Staff->save($personal);

                    break;
                case 8:
                    # Education

                    $education = array();
                    $education['Education']['id'] = $details[$i]['Applicant']['key_id'];
                    $education['Education']['status_id'] = 7;

                    $this->Education->create();
                    $this->Education->save($education);

                    break;
                case 11:
                    # Skill

                    $skill = array();
                    $skill['Skill']['id'] = $details[$i]['Applicant']['key_id'];
                    $skill['Skill']['status_id'] = 7;

                    $this->Skill->create();
                    $this->Skill->save($skill);

                    break;
                case 17:
                    # Spouse

                    $spouse = array();
                    $spouse['Spouse']['id'] = $details[$i]['Applicant']['key_id'];
                    $spouse['Spouse']['status_id'] = 7;

                    $this->Spouse->create();
                    $this->Spouse->save($spouse);

                    break;  
                    
                case 20:
                    # Children

                    $children = array();
                    $children['Children']['id'] = $details[$i]['Applicant']['key_id'];
                    $children['Children']['status_id'] = 7;

                    $this->Children->create();
                    $this->Children->save($children);

                    break;

                case 26:
                    # Death

                    $death = array();
                    $death['Death']['id'] = $details[$i]['Applicant']['key_id'];
                    $death['Death']['status_id'] = 7;

                    $this->Death->create();
                    $this->Death->save($death);

                    break;
            }
        }

        $hrisleaders = $this->UserRole->getStaffByRole(4);

        if(empty($hrisleaders))
        {
            $this->Session->setFlash('We cannot find officer on duty. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($detail))
        {
            $data = array();

            $data['Applicant']['id'] = $detail['Applicant']['id'];
            $data['Applicant']['status_id'] = 7;
            $data['Applicant']['verified_by'] = $staff['Staff']['id'];
            $data['Applicant']['verified'] = date('Y-m-d H:i:s');

            $this->Applicant->create();
            $this->Applicant->save($data);
        }

        $subject = $detail['ApplyBy']['name'] . " has sent the request update for approval";
        //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
        $body = "<br/>";
        $body .= "Click <a href='".Router::url('/Hris/view/'. $key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." request update.";

        foreach ($hrisleaders['HRIS'] as $hrisleader) 
        {
            $notification = array();

            $notification['Notification']['sender_id'] = $staff['Staff']['id'];
            $notification['Notification']['recipient_id'] = $hrisleader['id'];
            $notification['Notification']['label'] = 'info-warning';
            $notification['Notification']['proses'] = 1;
            $notification['Notification']['is_read'] = 1;
            $notification['Notification']['subject'] = $subject;
            $notification['Notification']['body'] = $body;
            $notification['Notification']['created_by'] = $staff['Staff']['id'];
            $notification['Notification']['modified_by'] = $staff['Staff']['id'];

            $this->Notification->create();
            $this->Notification->save($notification);
        }

        $this->Session->setFlash('Your request update is successfully sent to HRIS Leader for approval.', 'success');
        $this->redirect(array('action' => 'view/'.$key));
    }
    
    public function send_approve($key = null) 
    {
        $this->loadModel('Staff');
        $this->loadModel('UserRole');
        $this->loadModel('Notification');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Voucher');
        $this->loadModel('FamilyPass');
        $this->loadModel('Medical');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($detail))
        {
            $data = array();

            $data['Applicant']['id'] = $detail['Applicant']['id'];
            $data['Applicant']['approved_by'] = $staff['Staff']['id'];
            $data['Applicant']['approved'] = date('Y-m-d H:i:s');
            $data['Applicant']['status_id'] = 10;

            $this->Applicant->create();
            $this->Applicant->save($data);

            $applicant_key = $this->Utility->encrypt($detail['Applicant']['id'], 'app');

            /* notification */
            $subject = $detail['ApplyBy']['name'] . ", your update successfully approved.";
            //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
            $body = "<br/>";
            $body .= "Click <a href='".Router::url('/Applicants/view/'. $applicant_key , true)."'>here</a> to view.";
            
            $notification = array();

            $notification['Notification']['sender_id'] = $staff['Staff']['id'];
            $notification['Notification']['recipient_id'] = $detail['ApplyBy']['id'];
            $notification['Notification']['label'] = 'info-warning';
            $notification['Notification']['proses'] = 1;
            $notification['Notification']['is_read'] = 1;
            $notification['Notification']['subject'] = $subject;
            $notification['Notification']['body'] = $body;
            $notification['Notification']['created_by'] = $staff['Staff']['id'];
            $notification['Notification']['modified_by'] = $staff['Staff']['id'];

            $this->Notification->create();
            $this->Notification->save($notification);
            /* notification */
        }
        
        //create function vheck for spouse
        $applyby = $this->Staff->findById($detail['Applicant']['staff_id']);

        $staff_ids = $this->Staff->getListStaffIdByUserId($applyby['Staff']['user_id']);
        $staff_historys = array();
        if(!empty($staff_ids))
		{
			$i = 0;
			foreach ($staff_ids as $staff_key => $staff_value) 
			{
				$staff_historys[$i] = $staff_key;
				$i++;
			}
		}
        
        // $count  = $this->Spouse->checkSpouseIfExistByStaffId($staff_historys);
        
        $details = $this->Applicant->find('threaded',
                                                        array(
                                                            'conditions' => array(
                                                                                'Applicant.parent_id' => $detail['Applicant']['id'],
                                                                            ),
                                                            'contain' => false,
                                                    ));

        for ($i=0; $i < count($details) ; $i++) 
        { 
            switch ($details[$i]['Applicant']['modul_id']) 
            {
                case 4:
                    # Staff

                    $personal = array();
                    $personal['Staff']['id'] = $detail['Applicant']['staff_id'];
                    $personal['Staff']['status_id'] = 10;

                    $this->Staff->create();
                    $this->Staff->save($personal);

                    //medical
                    $check_medical = $this->Medical->find('first', array(
                                                            'conditions' => array(
                                                                                'Medical.ic' => $applyby['Staff']['ic_no'],
                                                                                'Medical.is_flag' => 99,
                                                                                'Medical.is_active' => 1,
                                                                            )
                                                ));
                    if(empty($check_medical))
                    {
                        $medical = array();
                        $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                        $medical['Medical']['organisation_type_id'] = $applyby['Staff']['organisation_type_id'];
                        $medical['Medical']['name'] = $applyby['Staff']['name'];
                        $medical['Medical']['ic'] = $applyby['Staff']['ic_no'];
                        $medical['Medical']['dob'] = $applyby['Staff']['date_of_birth'];
                        $medical['Medical']['gender_id'] = $applyby['Staff']['gender_id'];
                        $medical['Medical']['medical_relationship_id'] = 1;
                        $medical['Medical']['email'] = $applyby['Staff']['email'];
                        $medical['Medical']['medical_status_id'] = 1;
                        $medical['Medical']['is_flag'] = 1;
                        $medical['Medical']['is_active'] = 1;
                        $medical['Medical']['created_by'] = $staff['Staff']['id'];
                        $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                        $this->Medical->create();
                        $this->Medical->save($medical);
                    }
                    else
                    {

                        $medical = array();
                        $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                        $medical['Medical']['organisation_type_id'] = $applyby['Staff']['organisation_type_id'];
                        $medical['Medical']['name'] = $applyby['Staff']['name'];
                        $medical['Medical']['ic'] = $applyby['Staff']['ic_no'];
                        $medical['Medical']['dob'] = $applyby['Staff']['date_of_birth'];
                        $medical['Medical']['gender_id'] = $applyby['Staff']['gender_id'];
                        $medical['Medical']['medical_relationship_id'] = 1;
                        $medical['Medical']['bank_id'] = $applyby['Staff']['bank_id'];
                        $medical['Medical']['bank_account_no'] = $applyby['Staff']['bank_account_no'];
                        $medical['Medical']['email'] = $applyby['Staff']['email'];
                        $medical['Medical']['medical_status_id'] = 2;
                        $medical['Medical']['is_flag'] = 1;
                        $medical['Medical']['is_active'] = 1;
                        $medical['Medical']['created_by'] = $staff['Staff']['id'];
                        $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                        $this->Medical->create();
                        $this->Medical->save($medical);

                        //history
                        $medical = array();
                        $medical['Medical']['id'] = $check_medical['Medical']['id'];
                        $medical['Medical']['is_active'] = 99;

                        $this->Medical->create();
                        $this->Medical->save($medical);
                    }
                    //medical

                    break;
                case 8:
                    # Education

                    $education = array();
                    $education['Education']['id'] = $details[$i]['Applicant']['key_id'];
                    $education['Education']['status_id'] = 10;

                    $this->Education->create();
                    $this->Education->save($education);

                    break;
                case 11:
                    # Professional Skill

                    $skill = array();
                    $skill['Skill']['id'] = $details[$i]['Applicant']['key_id'];
                    $skill['Skill']['status_id'] = 10;

                    $this->Skill->create();
                    $this->Skill->save($skill);

                    break;
                case 17:
                    # spouse

                    $spouse = array();
                    $spouse['Spouse']['id'] = $details[$i]['Applicant']['key_id'];
                    $spouse['Spouse']['status_id'] = 10;

                    $this->Spouse->create();
                    $this->Spouse->save($spouse);

                    $spouse_detail = $this->Spouse->findById($details[$i]['Applicant']['key_id']);

                    switch ($applyby['OrganisationType']['id']) 
                    {
                        case 1://prasarana
                            $hris_spouses = $this->UserRole->getStaffByRole(5);

                            if(!empty($hris_spouses))
                            {
                                $voucher = array();

                                $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $voucher['Voucher']['voucher_type_id'] = 1;
                                $voucher['Voucher']['organisation_type_id'] = 1;
                                $voucher['Voucher']['status_id'] = 1;
                                $voucher['Voucher']['is_active'] = 1;
                                $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                $this->Voucher->create();
                                $this->Voucher->save($voucher);
                                $voucher_id = $this->Voucher->id;

                                $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/MarriedVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }
                            }

                            $applicant_ids = $this->Applicant->getApplicantByStaffIdModulId($staff_historys, 17);
                            $familypass_ids = $this->FamilyPass->findFamilyPassByApplicantIdModulId($applicant_ids, 17);

                            if(count($familypass_ids) == 0)
                            {
                                //here
                                $family_pass_spouses = $this->UserRole->getStaffByRole(14);

                                $family_pass = array();

                                $family_pass['FamilyPass']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $family_pass['FamilyPass']['modul_id'] = 17;
                                $family_pass['FamilyPass']['key_id'] = $details[$i]['Applicant']['key_id'];
                                $family_pass['FamilyPass']['family_pass_type_id'] = 1;
                                $family_pass['FamilyPass']['family_pass_reason_id'] = 1;
                                $family_pass['FamilyPass']['organisation_type_id'] = 1;
                                $family_pass['FamilyPass']['status_id'] = 1;
                                $family_pass['FamilyPass']['is_active'] = 1;
                                $family_pass['FamilyPass']['created_by'] = $staff['Staff']['id'];
                                $family_pass['FamilyPass']['modified_by'] = $staff['Staff']['id'];

                                $this->FamilyPass->create();
                                $this->FamilyPass->save($family_pass);
                                $family_pass_id = $this->FamilyPass->id;

                                $family_pass_key = $this->Utility->encrypt($family_pass_id, 'fmlypss');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a family pass that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/FamilyPasses/view/'. $family_pass_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($family_pass_spouses['HRIS'] as $family_pass_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $family_pass_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }

                            }

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $spouse_detail['Spouse']['ic_new'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 1;
                                $medical['Medical']['name'] = $spouse_detail['Spouse']['name'];
                                $medical['Medical']['ic'] = $spouse_detail['Spouse']['ic_new'];
                                if(!empty($spouse_detail['Spouse']['dob']))
                                {
                                    $medical['Medical']['dob'] = $spouse_detail['Spouse']['dob'];
                                }
                                switch ($applyby['Staff']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['gender_id'] = 2;
                                        break;
                                    case 2:
                                        $medical['Medical']['gender_id'] = 1;
                                        break;
                                }
                                $medical['Medical']['medical_relationship_id'] = 2;
                                $medical['Medical']['medical_status_id'] = 1;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            else
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 1;
                                $medical['Medical']['name'] = $spouse_detail['Spouse']['name'];
                                $medical['Medical']['ic'] = $spouse_detail['Spouse']['ic_new'];
                                if(!empty($spouse_detail['Spouse']['dob']))
                                {
                                    $medical['Medical']['dob'] = $spouse_detail['Spouse']['dob'];
                                }
                                switch ($applyby['Staff']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['gender_id'] = 2;
                                        break;
                                    case 2:
                                        $medical['Medical']['gender_id'] = 1;
                                        break;
                                }
                                $medical['Medical']['medical_relationship_id'] = 2;
                                $medical['Medical']['medical_status_id'] = 2;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);

                                //history
                                $medical = array();
                                $medical['Medical']['id'] = $check_medical['Medical']['id'];
                                $medical['Medical']['is_active'] = 99;

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical

                            break;
                        
                        case 2://rapid bus
                            $hris_spouses = $this->UserRole->getStaffByRole(6);

                            if(!empty($hris_spouses))
                            {
                                $voucher = array();

                                $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $voucher['Voucher']['voucher_type_id'] = 1;
                                $voucher['Voucher']['organisation_type_id'] = 2;
                                $voucher['Voucher']['status_id'] = 1;
                                $voucher['Voucher']['is_active'] = 1;
                                $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                $this->Voucher->create();
                                $this->Voucher->save($voucher);
                                $voucher_id = $this->Voucher->id;

                                $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/MarriedVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }
                            }

                            $applicant_ids = $this->Applicant->getApplicantByStaffIdModulId($staff_historys, 17);
                            $familypass_ids = $this->FamilyPass->findFamilyPassByApplicantIdModulId($applicant_ids, 17);

                            if(count($familypass_ids) == 0)
                            {
                                $family_pass_spouses = $this->UserRole->getStaffByRole(15);

                                $family_pass = array();

                                $family_pass['FamilyPass']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $family_pass['FamilyPass']['modul_id'] = 17;
                                $family_pass['FamilyPass']['key_id'] = $details[$i]['Applicant']['key_id'];
                                $family_pass['FamilyPass']['family_pass_type_id'] = 1;
                                $family_pass['FamilyPass']['family_pass_reason_id'] = 1;
                                $family_pass['FamilyPass']['organisation_type_id'] = 2;
                                $family_pass['FamilyPass']['status_id'] = 1;
                                $family_pass['FamilyPass']['is_active'] = 1;
                                $family_pass['FamilyPass']['created_by'] = $staff['Staff']['id'];
                                $family_pass['FamilyPass']['modified_by'] = $staff['Staff']['id'];

                                $this->FamilyPass->create();
                                $this->FamilyPass->save($family_pass);
                                $family_pass_id = $this->FamilyPass->id;

                                $family_pass_key = $this->Utility->encrypt($family_pass_id, 'fmlypss');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a family pass that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/FamilyPasses/view/'. $family_pass_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($family_pass_spouses['HRIS'] as $family_pass_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $family_pass_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }

                            }

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $spouse_detail['Spouse']['ic_new'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 2;
                                $medical['Medical']['name'] = $spouse_detail['Spouse']['name'];
                                $medical['Medical']['ic'] = $spouse_detail['Spouse']['ic_new'];
                                if(!empty($spouse_detail['Spouse']['dob']))
                                {
                                    $medical['Medical']['dob'] = $spouse_detail['Spouse']['dob'];
                                }
                                switch ($applyby['Staff']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['gender_id'] = 2;
                                        break;
                                    case 2:
                                        $medical['Medical']['gender_id'] = 1;
                                        break;
                                }
                                $medical['Medical']['medical_relationship_id'] = 2;
                                $medical['Medical']['medical_status_id'] = 1;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            else
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 2;
                                $medical['Medical']['name'] = $spouse_detail['Spouse']['name'];
                                $medical['Medical']['ic'] = $spouse_detail['Spouse']['ic_new'];
                                if(!empty($spouse_detail['Spouse']['dob']))
                                {
                                    $medical['Medical']['dob'] = $spouse_detail['Spouse']['dob'];
                                }
                                switch ($applyby['Staff']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['gender_id'] = 2;
                                        break;
                                    case 2:
                                        $medical['Medical']['gender_id'] = 1;
                                        break;
                                }
                                $medical['Medical']['medical_relationship_id'] = 2;
                                $medical['Medical']['medical_status_id'] = 2;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);

                                //history
                                $medical = array();
                                $medical['Medical']['id'] = $check_medical['Medical']['id'];
                                $medical['Medical']['is_active'] = 99;

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical

                            break;
                        
                        case 3://rapid rail
                            $hris_spouses = $this->UserRole->getStaffByRole(7);

                            if(!empty($hris_spouses))
                            {
                                $voucher = array();

                                $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $voucher['Voucher']['voucher_type_id'] = 1;
                                $voucher['Voucher']['organisation_type_id'] = 3;
                                $voucher['Voucher']['status_id'] = 1;
                                $voucher['Voucher']['is_active'] = 1;
                                $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                $this->Voucher->create();
                                $this->Voucher->save($voucher);
                                $voucher_id = $this->Voucher->id;

                                $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/MarriedVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }
                            }

                            $applicant_ids = $this->Applicant->getApplicantByStaffIdModulId($staff_historys, 17);
                            $familypass_ids = $this->FamilyPass->findFamilyPassByApplicantIdModulId($applicant_ids, 17);

                            if(count($familypass_ids) == 0)
                            {
                                $family_pass_spouses = $this->UserRole->getStaffByRole(16);

                                $family_pass = array();

                                $family_pass['FamilyPass']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $family_pass['FamilyPass']['modul_id'] = 17;
                                $family_pass['FamilyPass']['key_id'] = $details[$i]['Applicant']['key_id'];
                                $family_pass['FamilyPass']['family_pass_type_id'] = 1;
                                $family_pass['FamilyPass']['family_pass_reason_id'] = 1;
                                $family_pass['FamilyPass']['organisation_type_id'] = 1;
                                $family_pass['FamilyPass']['status_id'] = 1;
                                $family_pass['FamilyPass']['is_active'] = 1;
                                $family_pass['FamilyPass']['created_by'] = $staff['Staff']['id'];
                                $family_pass['FamilyPass']['modified_by'] = $staff['Staff']['id'];

                                $this->FamilyPass->create();
                                $this->FamilyPass->save($family_pass);
                                $family_pass_id = $this->FamilyPass->id;

                                $family_pass_key = $this->Utility->encrypt($family_pass_id, 'fmlypss');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a family pass that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/FamilyPasses/view/'. $family_pass_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($family_pass_spouses['HRIS'] as $family_pass_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $family_pass_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }
                            }

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $spouse_detail['Spouse']['ic_new'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 3;
                                $medical['Medical']['name'] = $spouse_detail['Spouse']['name'];
                                $medical['Medical']['ic'] = $spouse_detail['Spouse']['ic_new'];
                                if(!empty($spouse_detail['Spouse']['dob']))
                                {
                                    $medical['Medical']['dob'] = $spouse_detail['Spouse']['dob'];
                                }
                                switch ($applyby['Staff']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['gender_id'] = 2;
                                        break;
                                    case 2:
                                        $medical['Medical']['gender_id'] = 1;
                                        break;
                                }
                                $medical['Medical']['medical_relationship_id'] = 2;
                                $medical['Medical']['medical_status_id'] = 1;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            else
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 3;
                                $medical['Medical']['name'] = $spouse_detail['Spouse']['name'];
                                $medical['Medical']['ic'] = $spouse_detail['Spouse']['ic_new'];
                                if(!empty($spouse_detail['Spouse']['dob']))
                                {
                                    $medical['Medical']['dob'] = $spouse_detail['Spouse']['dob'];
                                }
                                switch ($applyby['Staff']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['gender_id'] = 2;
                                        break;
                                    case 2:
                                        $medical['Medical']['gender_id'] = 1;
                                        break;
                                }
                                $medical['Medical']['medical_relationship_id'] = 2;
                                $medical['Medical']['medical_status_id'] = 2;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);

                                //history
                                $medical = array();
                                $medical['Medical']['id'] = $check_medical['Medical']['id'];
                                $medical['Medical']['is_active'] = 99;

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical

                            break;
                    }
                    break;
                case 20:
                    # children

                    $children = array();
                    $children['Children']['id'] = $details[$i]['Applicant']['key_id'];
                    $children['Children']['status_id'] = 10;

                    $this->Children->create();
                    $this->Children->save($children);

                    $children_detail = $this->Children->findById($details[$i]['Applicant']['key_id']);
                    
                    switch ($applyby['OrganisationType']['id']) 
                    {
                        case 1://prasarana

                            $applicant_ids = $this->Applicant->getApplicantByStaffIdModulId($staff_historys, 20);
                            $voucher_ids = $this->Voucher->findVoucherByApplicantIdVoucherTypeId($applicant_ids, 2);
                            
                            if(count($voucher_ids) < 5)
                            {
                                $hris_spouses = $this->UserRole->getStaffByRole(8);

                                if(!empty($hris_spouses))
                                {
                                     
                                    $voucher = array();

                                    $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                    $voucher['Voucher']['voucher_type_id'] = 2;
                                    $voucher['Voucher']['organisation_type_id'] = 1;
                                    $voucher['Voucher']['status_id'] = 1;
                                    $voucher['Voucher']['is_active'] = 1;
                                    $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                    $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                    $this->Voucher->create();
                                    $this->Voucher->save($voucher);
                                    $voucher_id = $this->Voucher->id;

                                    $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                    /* notification */
                                    $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                    //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/ChildrenVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                            
                                    foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                    {
                                        $notification = array();
                            
                                        $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                        $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                        $notification['Notification']['label'] = 'info-warning';
                                        $notification['Notification']['proses'] = 1;
                                        $notification['Notification']['is_read'] = 1;
                                        $notification['Notification']['subject'] = $subject;
                                        $notification['Notification']['body'] = $body;
                                        $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                        $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                            
                                        $this->Notification->create();
                                        $this->Notification->save($notification);
                                        /* notification */
                                    }
                                }
                            }

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $children_detail['Children']['ic'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 1;
                                $medical['Medical']['name'] = $children_detail['Children']['name'];
                                $medical['Medical']['ic'] = $children_detail['Children']['ic'];
                                if(!empty($children_detail['Children']['date_of_birth']))
                                {
                                    $medical['Medical']['dob'] = $children_detail['Children']['date_of_birth'];
                                }
                                $medical['Medical']['gender_id'] = $children_detail['Children']['gender_id'];

                                switch ($children_detail['Children']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['medical_relationship_id'] = 3;
                                        break;

                                    case 2:
                                        $medical['Medical']['medical_relationship_id'] = 4;
                                        break;
                                }
                                $medical['Medical']['medical_status_id'] = 1;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            else
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 1;
                                $medical['Medical']['name'] = $children_detail['Children']['name'];
                                $medical['Medical']['ic'] = $children_detail['Children']['ic'];
                                if(!empty($children_detail['Children']['date_of_birth']))
                                {
                                    $medical['Medical']['dob'] = $children_detail['Children']['date_of_birth'];
                                }
                                $medical['Medical']['gender_id'] = $children_detail['Children']['gender_id'];

                                switch ($children_detail['Children']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['medical_relationship_id'] = 3;
                                        break;

                                    case 2:
                                        $medical['Medical']['medical_relationship_id'] = 4;
                                        break;
                                }
                                $medical['Medical']['medical_status_id'] = 2;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                //history
                                $medical = array();
                                $medical['Medical']['id'] = $check_medical['Medical']['id'];
                                $medical['Medical']['is_active'] = 99;

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical

                            break;
                        
                        case 2://rapid bus

                            $applicant_ids = $this->Applicant->getApplicantByStaffIdModulId($staff_historys, 20);
                            $voucher_ids = $this->Voucher->findVoucherByApplicantIdVoucherTypeId($applicant_ids, 2);

                            if(count($voucher_ids) < 6)
                            {

                                $hris_spouses = $this->UserRole->getStaffByRole(9);

                                if(!empty($hris_spouses))
                                {
                                    $voucher = array();

                                    $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                    $voucher['Voucher']['voucher_type_id'] = 2;
                                    $voucher['Voucher']['organisation_type_id'] = 2;
                                    $voucher['Voucher']['status_id'] = 1;
                                    $voucher['Voucher']['is_active'] = 1;
                                    $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                    $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                    $this->Voucher->create();
                                    $this->Voucher->save($voucher);
                                    $voucher_id = $this->Voucher->id;

                                    $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                    /* notification */
                                    $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                    //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/ChildrenVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                            
                                    foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                    {
                                        $notification = array();
                            
                                        $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                        $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                        $notification['Notification']['label'] = 'info-warning';
                                        $notification['Notification']['proses'] = 1;
                                        $notification['Notification']['is_read'] = 1;
                                        $notification['Notification']['subject'] = $subject;
                                        $notification['Notification']['body'] = $body;
                                        $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                        $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                            
                                        $this->Notification->create();
                                        $this->Notification->save($notification);
                                        /* notification */
                                    }
                                }
                            }

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $children_detail['Children']['ic'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 2;
                                $medical['Medical']['name'] = $children_detail['Children']['name'];
                                $medical['Medical']['ic'] = $children_detail['Children']['ic'];
                                if(!empty($children_detail['Children']['date_of_birth']))
                                {
                                    $medical['Medical']['dob'] = $children_detail['Children']['date_of_birth'];
                                }
                                $medical['Medical']['gender_id'] = $children_detail['Children']['gender_id'];

                                switch ($children_detail['Children']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['medical_relationship_id'] = 3;
                                        break;

                                    case 2:
                                        $medical['Medical']['medical_relationship_id'] = 4;
                                        break;
                                }
                                $medical['Medical']['medical_status_id'] = 1;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            else
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 2;
                                $medical['Medical']['name'] = $children_detail['Children']['name'];
                                $medical['Medical']['ic'] = $children_detail['Children']['ic'];
                                if(!empty($children_detail['Children']['date_of_birth']))
                                {
                                    $medical['Medical']['dob'] = $children_detail['Children']['date_of_birth'];
                                }
                                $medical['Medical']['gender_id'] = $children_detail['Children']['gender_id'];

                                switch ($children_detail['Children']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['medical_relationship_id'] = 3;
                                        break;

                                    case 2:
                                        $medical['Medical']['medical_relationship_id'] = 4;
                                        break;
                                }
                                $medical['Medical']['medical_status_id'] = 2;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                //history
                                $medical = array();
                                $medical['Medical']['id'] = $check_medical['Medical']['id'];
                                $medical['Medical']['is_active'] = 99;

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical

                            break;
                        
                        case 3://rapid rail
                            $applicant_ids = $this->Applicant->getApplicantByStaffIdModulId($staff_historys, 20);
                            $voucher_ids = $this->Voucher->findVoucherByApplicantIdVoucherTypeId($applicant_ids, 2);
                            
                            if(count($voucher_ids) < 6)
                            {
                                
                                $hris_spouses = $this->UserRole->getStaffByRole(10);

                                if(!empty($hris_spouses))
                                {
                                    
                                    $voucher = array();

                                    $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                    $voucher['Voucher']['voucher_type_id'] = 2;
                                    $voucher['Voucher']['organisation_type_id'] = 3;
                                    $voucher['Voucher']['status_id'] = 1;
                                    $voucher['Voucher']['is_active'] = 1;
                                    $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                    $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                    $this->Voucher->create();
                                    $this->Voucher->save($voucher);
                                    $voucher_id = $this->Voucher->id;

                                    $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                    /* notification */
                                    $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                    //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/ChildrenVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                            
                                    foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                    {
                                        $notification = array();
                            
                                        $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                        $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                        $notification['Notification']['label'] = 'info-warning';
                                        $notification['Notification']['proses'] = 1;
                                        $notification['Notification']['is_read'] = 1;
                                        $notification['Notification']['subject'] = $subject;
                                        $notification['Notification']['body'] = $body;
                                        $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                        $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                            
                                        $this->Notification->create();
                                        $this->Notification->save($notification);
                                        /* notification */
                                    }
                                }
                            }

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $children_detail['Children']['ic'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 3;
                                $medical['Medical']['name'] = $children_detail['Children']['name'];
                                $medical['Medical']['ic'] = $children_detail['Children']['ic'];
                                if(!empty($children_detail['Children']['date_of_birth']))
                                {
                                    $medical['Medical']['dob'] = $children_detail['Children']['date_of_birth'];
                                }
                                $medical['Medical']['gender_id'] = $children_detail['Children']['gender_id'];

                                switch ($children_detail['Children']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['medical_relationship_id'] = 3;
                                        break;

                                    case 2:
                                        $medical['Medical']['medical_relationship_id'] = 4;
                                        break;
                                }
                                $medical['Medical']['medical_status_id'] = 1;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            else
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = 3;
                                $medical['Medical']['name'] = $children_detail['Children']['name'];
                                $medical['Medical']['ic'] = $children_detail['Children']['ic'];
                                if(!empty($children_detail['Children']['date_of_birth']))
                                {
                                    $medical['Medical']['dob'] = $children_detail['Children']['date_of_birth'];
                                }
                                $medical['Medical']['gender_id'] = $children_detail['Children']['gender_id'];

                                switch ($children_detail['Children']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['medical_relationship_id'] = 3;
                                        break;

                                    case 2:
                                        $medical['Medical']['medical_relationship_id'] = 4;
                                        break;
                                }
                                $medical['Medical']['medical_status_id'] = 2;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                //history
                                $medical = array();
                                $medical['Medical']['id'] = $check_medical['Medical']['id'];
                                $medical['Medical']['is_active'] = 99;

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical

                            break;
                    }
                    
                    break;
                case 26:
                    # death

                    $death = array();
                    $death['Death']['id'] = $details[$i]['Applicant']['key_id'];
                    $death['Death']['status_id'] = 10;

                    $this->Death->create();
                    $this->Death->save($death);


                    $check_death = $this->Death->findById($details[$i]['Applicant']['key_id']);

                    if(!empty($check_death))
                    {
                        $check_spouses = $this->Spouse->find('first', array(
                                                            'conditions' => array(
                                                                                'Spouse.ic_new' => $check_death['Death']['ic'],
                                                                                'Spouse.status_id' => 10,
                                                                                'Spouse.is_status' => 1,
                                                                                'Spouse.is_active' => 1,
                                                                                'Spouse.staff_id' => $applyby['Staff']['id'],
                                                                            )
                                                ));
                        
                        if(!empty($check_spouses))
                        {
                            $update_death = array();
                            $update_death['Spouse']['id'] = $check_spouses['Spouse']['id'];
                            $update_death['Spouse']['is_status'] = 99;

                            $this->Spouse->create();
                            $this->Spouse->save($update_death);

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $check_spouses['Spouse']['ic_new'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(!empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = $applyby['Staff']['organisation_type_id'];
                                $medical['Medical']['name'] = $check_spouses['Spouse']['name'];
                                $medical['Medical']['ic'] = $check_spouses['Spouse']['ic_new'];
                                if(!empty($check_spouses['Spouse']['dob']))
                                {
                                    $medical['Medical']['dob'] = $check_spouses['Spouse']['dob'];
                                }
                                switch ($applyby['Staff']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['gender_id'] = 2;
                                        break;
                                    case 2:
                                        $medical['Medical']['gender_id'] = 1;
                                        break;
                                }
                                $medical['Medical']['medical_relationship_id'] = 2;
                                $medical['Medical']['medical_status_id'] = 3;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical
                        }


                        $check_children = $this->Children->find('first', array(
                                                                'conditions' => array(
                                                                                    'Children.ic' => $check_death['Death']['ic'],
                                                                                    'Children.status_id' => 10,
                                                                                    'Children.is_status' => 1,
                                                                                    'Children.is_active' => 1,
                                                                                    'Children.staff_id' => $applyby['Staff']['id'],
                                                                                )
                                                    ));

                        if(!empty($check_children))
                        {
                            $update_death = array();
                            $update_death['Children']['id'] = $check_children['Children']['id'];
                            $update_death['Children']['is_status'] = 99;

                            $this->Children->create();
                            $this->Children->save($update_death);

                            //medical
                            $check_medical = $this->Medical->find('first', array(
                                                                    'conditions' => array(
                                                                                        'Medical.ic' => $check_children['Children']['ic'],
                                                                                        'Medical.is_flag' => 99,
                                                                                        'Medical.is_active' => 1,
                                                                                    )
                                                        ));
                            if(!empty($check_medical))
                            {
                                $medical = array();
                                $medical['Medical']['staff_id'] = $applyby['Staff']['id'];
                                $medical['Medical']['organisation_type_id'] = $applyby['Staff']['organisation_type_id'];
                                $medical['Medical']['name'] = $check_children['Children']['name'];
                                $medical['Medical']['ic'] = $check_children['Children']['ic'];
                                if(!empty($check_children['Children']['date_of_birth']))
                                {
                                    $medical['Medical']['dob'] = $check_children['Children']['date_of_birth'];
                                }
                                $medical['Medical']['gender_id'] = $check_children['Children']['gender_id'];

                                switch ($check_children['Children']['gender_id']) 
                                {
                                    case 1:
                                        $medical['Medical']['medical_relationship_id'] = 3;
                                        break;

                                    case 2:
                                        $medical['Medical']['medical_relationship_id'] = 4;
                                        break;
                                }
                                $medical['Medical']['medical_status_id'] = 3;
                                $medical['Medical']['is_flag'] = 1;
                                $medical['Medical']['is_active'] = 1;
                                $medical['Medical']['created_by'] = $staff['Staff']['id'];
                                $medical['Medical']['modified_by'] = $staff['Staff']['id'];

                                $this->Medical->create();
                                $this->Medical->save($medical);

                                //history
                                $medical = array();
                                $medical['Medical']['id'] = $check_medical['Medical']['id'];
                                $medical['Medical']['is_active'] = 99;

                                $this->Medical->create();
                                $this->Medical->save($medical);
                            }
                            //medical
                        }
                    }

                    switch ($applyby['OrganisationType']['id']) 
                    {
                        case 1://prasarana
                            $hris_spouses = $this->UserRole->getStaffByRole(11);

                            if(!empty($hris_spouses))
                            {
                                $voucher = array();

                                $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $voucher['Voucher']['voucher_type_id'] = 3;
                                $voucher['Voucher']['organisation_type_id'] = 1;
                                $voucher['Voucher']['status_id'] = 1;
                                $voucher['Voucher']['is_active'] = 1;
                                $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                $this->Voucher->create();
                                $this->Voucher->save($voucher);
                                $voucher_id = $this->Voucher->id;

                                $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/DeathVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }
                            }

                            break;
                        
                        case 2://rapid bus
                            $hris_spouses = $this->UserRole->getStaffByRole(12);

                            if(!empty($hris_spouses))
                            {
                                $voucher = array();

                                $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $voucher['Voucher']['voucher_type_id'] = 3;
                                $voucher['Voucher']['organisation_type_id'] = 2;
                                $voucher['Voucher']['status_id'] = 1;
                                $voucher['Voucher']['is_active'] = 1;
                                $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                $this->Voucher->create();
                                $this->Voucher->save($voucher);
                                $voucher_id = $this->Voucher->id;

                                $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/DeathVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }
                            }

                            break;
                        
                        case 3://rapid rail
                            $hris_spouses = $this->UserRole->getStaffByRole(13);

                            if(!empty($hris_spouses))
                            {
                                $voucher = array();

                                $voucher['Voucher']['applicant_id'] = $details[$i]['Applicant']['id'];
                                $voucher['Voucher']['voucher_type_id'] = 3;
                                $voucher['Voucher']['organisation_type_id'] = 3;
                                $voucher['Voucher']['status_id'] = 1;
                                $voucher['Voucher']['is_active'] = 1;
                                $voucher['Voucher']['created_by'] = $staff['Staff']['id'];
                                $voucher['Voucher']['modified_by'] = $staff['Staff']['id'];

                                $this->Voucher->create();
                                $this->Voucher->save($voucher);
                                $voucher_id = $this->Voucher->id;

                                $voucher_key = $this->Utility->encrypt($voucher_id, 'vcr');

                                /* notification */
                                $subject = $detail['ApplyBy']['name'] . " has a voucher that needs to verify entitlement.";
                                //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                                $body = "<br/>";
                                $body .= "Click <a href='".Router::url('/DeathVouchers/view/'. $voucher_key , true)."'>here</a> to view ".$detail['ApplyBy']['name']." information.";
                        
                                foreach ($hris_spouses['HRIS'] as $hris_spouse) 
                                {
                                    $notification = array();
                        
                                    $notification['Notification']['sender_id'] = $staff['Staff']['id'];
                                    $notification['Notification']['recipient_id'] = $hris_spouse['id'];
                                    $notification['Notification']['label'] = 'info-warning';
                                    $notification['Notification']['proses'] = 1;
                                    $notification['Notification']['is_read'] = 1;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    $notification['Notification']['created_by'] = $staff['Staff']['id'];
                                    $notification['Notification']['modified_by'] = $staff['Staff']['id'];
                        
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    /* notification */
                                }
                            }

                            break;
                    }

                    break;
            }
        }

        $this->Session->setFlash('Your request update is successfully sent to HCD Welfare & Benefits Team for approval.', 'success');
        $this->redirect(array('action' => 'view/'.$key));
	}

    public function details($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Education');
        $this->loadModel('Skill');
        $this->loadModel('Spouse');
        $this->loadModel('Children');
        $this->loadModel('Death');
        $this->loadModel('Applicant');
        $this->loadModel('Gender');
        $this->loadModel('National');
        $this->loadModel('State');
        $this->loadModel('Race');
        $this->loadModel('Religion');
        $this->loadModel('MaritalStatus');
        $this->loadModel('Bank');
        $this->loadModel('JobDesignation');
        $this->loadModel('OrganisationType');
        $this->loadModel('JobType');
        $this->loadModel('Location');
        $this->loadModel('Qualification');
        $this->loadModel('Relationship');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $roles = $this->UserRole->getRoleByUserId($staff['Staff']['user_id']);

        $id = $this->Utility->decrypt($key, 'app');

        $detail = $this->Applicant->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $parent = $this->Applicant->findById($detail['Applicant']['parent_id']);
        if(empty($parent))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(!empty($parent))
        {
            $parent['AssignTo']['day_by_num'] = date('d', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['month'] = date('m', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['year'] = date('Y', strtotime($parent['Applicant']['assign']));

            $parent['AssignTo']['hour'] = date('h', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['minute'] = date('i', strtotime($parent['Applicant']['assign']));
            $parent['AssignTo']['format'] = date('A', strtotime($parent['Applicant']['assign']));

            $parent['Applicant']['assign'] = date("d-m-Y",strtotime($parent['Applicant']['assign']));

            $parent['VerifiedBy']['day_by_num'] = date('d', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['month'] = date('m', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['year'] = date('Y', strtotime($parent['Applicant']['verified']));

            $parent['VerifiedBy']['hour'] = date('h', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['minute'] = date('i', strtotime($parent['Applicant']['verified']));
            $parent['VerifiedBy']['format'] = date('A', strtotime($parent['Applicant']['verified']));

            $parent['Applicant']['verified'] = date("d-m-Y",strtotime($parent['Applicant']['verified']));

            $parent['ApprovedBy']['day_by_num'] = date('d', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['month'] = date('m', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['year'] = date('Y', strtotime($parent['Applicant']['approved']));

            $parent['ApprovedBy']['hour'] = date('h', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['minute'] = date('i', strtotime($parent['Applicant']['approved']));
            $parent['ApprovedBy']['format'] = date('A', strtotime($parent['Applicant']['approved']));

            $parent['Applicant']['approved'] = date("d-m-Y",strtotime($parent['Applicant']['approved']));
        }

        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($parent['AssignTo']['id']))
        {
            $assignto = $this->Staff->findStaffSummaryById($parent['AssignTo']['id']);
            if(!empty($assignto['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$assignto['Staff']['avatar']."'/>";
            }
        }
        $parent['AssignTo']['avatar'] = $img;

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($parent['VerifiedBy']['id']))
        {
            $verifiedby = $this->Staff->findStaffSummaryById($parent['VerifiedBy']['id']);
        
            if(!empty($verifiedby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$verifiedby['Staff']['avatar']."'/>";
            }
        }
        $parent['VerifiedBy']['avatar'] = $img;

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        if(!empty($parent['ApprovedBy']['id']))
        {
            $approvedby = $this->Staff->findStaffSummaryById($parent['ApprovedBy']['id']);
        
            if(!empty($approvedby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$approvedby['Staff']['avatar']."'/>";
            }
        }
        $parent['ApprovedBy']['avatar'] = $img;

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;


        switch ($data['Submit']) 
        {
            case 'verified':
                $verified = array();

                $verified['Applicant']['id'] = $detail['Applicant']['id'];
                $verified['Applicant']['status_id'] = 7;
                $verified['Applicant']['note'] = $data['Applicant']['note'];

                $this->Applicant->set($verified);
                if($this->Applicant->validates())
                {
                    $verified['Applicant']['verified_by'] = $staff['Staff']['id'];                
                    $verified['Applicant']['verified'] = date('Y-m-d H:i:s');

                    $this->Applicant->create();
                    $this->Applicant->save($verified);

                    $this->Session->setFlash('Information successfully updated.', 'success');
                    $this->redirect(array('action' => 'details/'.$key));
                }
                else
                {
                    $this->Session->setFlash('Error! Information not successfully verified.', 'error');
                }

                break;
            case 'approved':
                    $approved = array();
    
                    $approved['Applicant']['id'] = $detail['Applicant']['id'];
                    $approved['Applicant']['status_id'] = 10;
                    $approved['Applicant']['note'] = $data['Applicant']['note'];
    
                    $this->Applicant->set($approved);
                    if($this->Applicant->validates())
                    {
                        $approved['Applicant']['approved_by'] = $staff['Staff']['id'];                
                        $approved['Applicant']['approved'] = date('Y-m-d H:i:s');
    
                        $this->Applicant->create();
                        $this->Applicant->save($approved);
    
                        $this->Session->setFlash('Information successfully updated.', 'success');
                        $this->redirect(array('action' => 'details/'.$key));
                    }
                    else
                    {
                        $this->Session->setFlash('Error! Information not successfully approved.', 'error');
                    }
    
                    break;
            case 'correction':

                $correction = array();

                $correction['Applicant']['id'] = $detail['Applicant']['id'];
                $correction['Applicant']['status_id'] = 5;
                $correction['Applicant']['note'] = $data['Applicant']['note'];

                $this->Applicant->set($correction);
                if($this->Applicant->validates())
                {
                    $correction['Applicant']['verified_by'] = null;                
                    $correction['Applicant']['verified'] = null;

                    $this->Applicant->create();
                    $this->Applicant->save($correction);

                    $this->Session->setFlash('Information successfully updated.', 'success');
                    $this->redirect(array('action' => 'details/'.$key));
                }
                else
                {
                    $this->Session->setFlash('Error! Information not successfully verified.', 'error');
                }


                break;

            case 'correction_leader':

                $correction = array();

                $correction['Applicant']['id'] = $detail['Applicant']['id'];
                $correction['Applicant']['status_id'] = 8;
                $correction['Applicant']['note'] = $data['Applicant']['note'];

                $this->Applicant->set($correction);
                if($this->Applicant->validates())
                {
                    $correction['Applicant']['verified_by'] = null;                
                    $correction['Applicant']['verified'] = null;

                    $this->Applicant->create();
                    $this->Applicant->save($correction);

                    $this->Session->setFlash('Information successfully updated.', 'success');
                    $this->redirect(array('action' => 'details/'.$key));
                }
                else
                {
                    $this->Session->setFlash('Error! Information not successfully verified.', 'error');
                }


                break;
            }
        }

        $parent_key = $this->Utility->encrypt($detail['Applicant']['parent_id'], 'app');
        $detail['Applicant']['day_by_text'] = date('D', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['day_by_num'] = date('d', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['month'] = date('m', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['year'] = date('Y', strtotime($detail['Applicant']['created']));

        $detail['Applicant']['hour'] = date('h', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['minute'] = date('i', strtotime($detail['Applicant']['created']));
        $detail['Applicant']['format'] = date('A', strtotime($detail['Applicant']['created']));

        switch ($detail['Applicant']['modul_id']) 
        {
            case 4: //Personal Details
                $person = $this->Staff->findById($detail['Applicant']['staff_id']);
                $person['Staff']['date_of_birth'] =  date("d-m-Y", strtotime($person['Staff']['date_of_birth']));
                $this->request->data = $person;
                break;
            case 8: //Education
                $education = $this->Education->findById($detail['Applicant']['key_id']);
                $education['Education']['duration_from'] =  date("d-m-Y", strtotime($education['Education']['duration_from']));
                $education['Education']['duration_to'] =  date("d-m-Y", strtotime($education['Education']['duration_to']));
                $this->request->data = $education;
                break;

            case 11: //Skill
                $skill = $this->Skill->findById($detail['Applicant']['key_id']);
                $skill['Skill']['from_year'] =  date("d-m-Y", strtotime($skill['Skill']['from_year']));
                $skill['Skill']['to_year'] =  date("d-m-Y", strtotime($skill['Skill']['to_year']));
                $this->request->data = $skill;
                break;

            case 17: //Spouse
                $spouse = $this->Spouse->findById($detail['Applicant']['key_id']);
                $spouse['Spouse']['marriage_date'] =  date("d-m-Y", strtotime($spouse['Spouse']['marriage_date']));
                $this->request->data = $spouse;
                break;

            case 20: //Children
                $children = $this->Children->findById($detail['Applicant']['key_id']);
                $children['Children']['date_of_birth'] =  date("d-m-Y", strtotime($children['Children']['date_of_birth']));
                $this->request->data = $children;
                break;

            case 26: //Death
                $death = $this->Death->findById($detail['Applicant']['key_id']);
                $death['Death']['date_of_death'] =  date("d-m-Y", strtotime($death['Death']['date_of_death']));
                $this->request->data = $death;
                break;
        }

        $disabled = 'disabled';

        $days = array(
                    'Mon' => 'Mon',
                    'Tue' => 'Tue',
                    'Wed' => 'Wed',
                    'Thu' => 'Thu',
                    'Fri' => 'Fri',
                    'Sat' => 'Sat',
                    'Sun' => 'Sun',
                );
        
        $path = Router::url('/documents/', true);

        $statuses = array(
                        1 => 'ACTIVE',
                        99 => 'INACTIVE',
                    );

        $team_checked = false;
        if($parent['Applicant']['status_id'] == 4 || $parent['Applicant']['status_id'] == 6 || $parent['Applicant']['status_id'] == 8)
        {
            $team_checked = true;
        }

        $approved = false;
        if($parent['Applicant']['status_id'] == 7 )
        {
            $approved = true;
        }

        $hrleader = false;
        if (in_array(4, $roles))
        {   
            $hrleader = true;
        }

        $assignto = false;
        if($parent['Applicant']['assign_to'] == $staff['Staff']['id'])
        {
            $assignto = true;
        }

        $genders = $this->Gender->find('list');
        $nationals = $this->National->find('list');
        $states = $this->State->find('list');
        $races = $this->Race->find('list');
        $religions = $this->Religion->find('list');
        $maritalstatus = $this->MaritalStatus->find('list');
        $banks = $this->Bank->find('list');
        $jobdesignations = $this->JobDesignation->find('list');
        $organisationtypes = $this->OrganisationType->find('list');
        $jobtypes = $this->JobType->find('list');
        $locations = $this->Location->find('list');
        $qualifications = $this->Qualification->find('list');
        $relationships = $this->Relationship->find('list');

        // debug($approved);
        // debug($hrleader);
        // debug($parent['Applicant']['status_id']);
        // debug($detail['Applicant']['status_id']);
        // die;

        // debug($team_checked);
        // debug($assignto);
        // die;

        $this->set(compact('key', 'parent_key', 'detail', 'parent', 'disabled', 'days', 'path', 'statuses', 'team_checked', 'approved', 'genders', 'nationals', 'states', 'races', 'religions', 'maritalstatus', 'banks', 'jobdesignations', 'organisationtypes', 'jobtypes', 'locations', 'qualifications', 'relationships', 'assignto', 'hrleader'));
    }
}