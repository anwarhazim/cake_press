<?php
class MyMedicalsController extends AppController
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	public $uses = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
	}

	public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Medical');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

		$conditions = array();

		$conditions['conditions'][] = array(
                                            'Medical.is_active' => 1,
                                            'Medical.staff_id' => $staff['Staff']['list_id'],
											);

        $conditions['order'] = array('Medical.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Medical'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Medical.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Medical.ic LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Medical.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Medical.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Medical'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

		$details = $this->Paginator->paginate('Medical');

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Medical']['modified'] = date("d-m-Y",strtotime($details[$i]['Medical']['modified']));

            $details[$i]['Medical']['created'] = date("d-m-Y",strtotime($details[$i]['Medical']['created']));

			$details[$i]['Medical']['id'] = $this->Utility->encrypt($details[$i]['Medical']['id'], 'mdcl');
        }
        
        $auth = $this->Utility->getUserAuth(38, $person['id']);
        if($auth == false)
        {
            $this->Session->setFlash('You do not have permission to view this function. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Users', 'action' => 'profile'));
        }

		$this->set(compact('details'));
	}

	public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Medical');
        $this->loadModel('Gender');
        $this->loadModel('Bank');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$staff = $this->Staff->findStaffByUserId($person['id']);

        $id = $this->Utility->decrypt($key, 'mdcl');

        $detail = $this->Medical->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        $baseURL = Router::url('/', true);

        $img = "";
        $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."img/users/default-avatar.jpg'/>";

        $applyby = $this->Staff->findStaffSummaryById($detail['Medical']['staff_id']);

        if(!empty($applyby))
        {
            $detail['ApplyBy'] = $applyby['Staff'];

            if(!empty($applyby['Staff']['avatar']))
            {
                $img = "<img class='img-circle img-responsive' style='width: 120px; height: 120px;' src='".$baseURL."avatars/".$applyby['Staff']['avatar']."'/>";
            }

            $detail['ApplyBy']['avatar'] = $img;
        }

        if(!empty($detail['Medical']['dob']))
        {
            $detail['Medical']['dob'] = date("d-m-Y", strtotime($detail['Medical']['dob']));
        }

        $this->request->data = $detail;

        $disabled = "disabled";

        $genders = $this->Gender->find('list');
        $banks = $this->Bank->find('list');

        $days = array(
                'Mon' => 'Mon',
                'Tue' => 'Tue',
                'Wed' => 'Wed',
                'Thu' => 'Thu',
                'Fri' => 'Fri',
                'Sat' => 'Sat',
                'Sun' => 'Sun',
            );

        $this->set(compact(
			'key',
            'detail',
            'genders',
            'banks',
			'applyby',
			'days',
            'approved',
            'pickups',
            'disabled'
		));
    }
}
