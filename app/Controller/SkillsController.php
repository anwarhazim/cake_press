<?php
class SkillsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit');
    }

    public function index()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Skill.staff_id' => $staff['Staff']['id'],
                                            'Skill.is_delete' => 99,
                                        );

        $conditions['order'] = array('Skill.from_year'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Skill'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "name")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Skill.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Skill.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Skill.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Skill'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Skill']['from_year'] = date("d-m-Y",strtotime($details[$i]['Skill']['from_year']));
            $details[$i]['Skill']['to_year'] = date("d-m-Y",strtotime($details[$i]['Skill']['to_year']));

            $details[$i]['Skill']['modified'] = date("d-m-Y",strtotime($details[$i]['Skill']['modified']));
            $details[$i]['Skill']['created'] = date("d-m-Y",strtotime($details[$i]['Skill']['created']));

            $details[$i]['Skill']['id'] = $this->Utility->encrypt($details[$i]['Skill']['id'], 'skl');
            
        }

        $update = $this->Skill->findIfUpdateByStaffId($staff['Staff']['id']);

        $this->set(compact('details', 'update'));
    }

    public function view($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $path = Router::url('/documents/', true);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'skl');

        $detail = $this->Skill->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;
        
        }
        else
        {
            $detail['Skill']['from_year'] =  date("d-m-Y", strtotime($detail['Skill']['from_year']));
            $detail['Skill']['to_year'] =  date("d-m-Y", strtotime($detail['Skill']['to_year']));

            $this->request->data = $detail;
        }

        $statuses = array(
                        1 => 'ACTIVE',
                        99 => 'INACTIVE',
                    );
        
        $disabled = "disabled";

        $update = $this->Skill->findIfUpdateByStaffId($staff['Staff']['id']);

        $this->set(compact('key', 'detail', 'path', 'statuses', 'disabled', 'update'));
    }

    public function edit($key)
    {
        $this->loadModel('Staff');
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        $path = Router::url('/documents/', true);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'skl');

        $detail = $this->Skill->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Skill']['id'] = $detail['Skill']['id'];
            $data['Skill']['is_flag'] = 1;
            $data['Skill']['is_active'] = 1;
            $data['Skill']['is_delete'] = 99;
            $data['Skill']['status_id'] = 2;
            $data['Skill']['modified_by'] = $staff['Staff']['id'];
            $data['Skill']['modified'] = date('Y-m-d H:i:s');

            $this->Skill->set($data);
            if($this->Skill->validates())
            {
                $this->Skill->create();
                $this->Skill->save($data);

                $path = WWW_ROOT . 'documents'.DS;

                foreach ($data['Skill']['attachments'] as $attachment) 
                {
                    if($attachment['error'] == 0)
                    {
                        // <!-- start create personal forder by staff no -->

                        //check and create folder by staff no
                        $filename_main = $path.$staff['Staff']['staff_no'];
                        $this->Utility->createFolder($filename_main);

                        //check and create folder by moduls
                        $filename_main = $filename_main.DS.'SKILLS';
                        $this->Utility->createFolder($filename_main);

                        // <!-- end create personal forlder by staff no -->

                        $file = array();

                        $file['Attachment']['key_id'] = $detail['Skill']['id'];
                        $file['Attachment']['name'] = $attachment['name'];
                        $file['Attachment']['modul_id'] = 11; // check table moduls to check modul id
                        $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                        $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                        $file['Attachment']['created_by'] = $staff['Staff']['id'];
                        $file['Attachment']['created'] = date('Y-m-d H:i:s');

                        $this->Attachment->create();
                        $this->Attachment->save($file);

                        $attachment_id = $this->Attachment->id;

                        $temp = explode(".", $attachment['name']);
                        $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                        move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);

                        $file = array();

                        $file['Attachment']['id'] = $attachment_id;
                        $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'SKILLS'.DS.$newfilename;

                        $this->Attachment->create();
                        $this->Attachment->save($file);
                    }
                }

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated. Please try again!', 'error');
            }
        }
        else
        {
            $detail['Skill']['to_year'] =  date("d-m-Y", strtotime($detail['Skill']['to_year']));
            $detail['Skill']['from_year'] =  date("d-m-Y", strtotime($detail['Skill']['from_year']));

            $this->request->data = $detail;
        }

        $statuses = array(
                        1 => 'ACTIVE',
                        99 => 'INACTIVE',
                    );

        $update = $this->Skill->findIfUpdateByStaffId($staff['Staff']['id']);
        
        if($update == false)
        {
            $this->Session->setFlash('You cannot make any changes within this period. Please contact system administrator for help', 'info');
            $this->redirect(array('action' => '/'));
        }

        $this->set(compact('key', 'detail', 'path', 'statuses'));
    }

    public function add()
    {
        $this->loadModel('Staff');
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $staff = $this->Staff->findStaffByUserId($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Skill']['staff_id'] = $staff['Staff']['id'];
            $data['Skill']['is_flag'] = 1;
            $data['Skill']['is_active'] = 1;
            $data['Skill']['is_delete'] = 99;
            $data['Skill']['status_id'] = 1;
            $data['Skill']['modified_by'] = $staff['Staff']['id'];
            $data['Skill']['modified'] = date('Y-m-d H:i:s');
            $data['Skill']['created_by'] = $staff['Staff']['id'];
            $data['Skill']['created'] = date('Y-m-d H:i:s');
            
            $this->Skill->set($data);
            if($this->Skill->validates())
            {
                $this->Skill->create();
                $this->Skill->save($data);

                $skill_id = $this->Skill->id;

                $path = WWW_ROOT . 'documents'.DS;

                foreach ($data['Skill']['attachments'] as $attachment) 
                {
                    // <!-- start create personal forder by staff no -->

                    //check and create folder by staff no
                    $filename_main = $path.$staff['Staff']['staff_no'];
                    $this->Utility->createFolder($filename_main);

                    //check and create folder by moduls
                    $filename_main = $filename_main.DS.'SKILLS';
                    $this->Utility->createFolder($filename_main);

                    // <!-- end create personal forlder by staff no -->

                    $file = array();

                    $file['Attachment']['key_id'] = $skill_id;
                    $file['Attachment']['name'] = $attachment['name'];
                    $file['Attachment']['modul_id'] = 11; // check table moduls to check modul id
                    $file['Attachment']['modified_by'] = $staff['Staff']['id'];
                    $file['Attachment']['modified'] = date('Y-m-d H:i:s');
                    $file['Attachment']['created_by'] = $staff['Staff']['id'];
                    $file['Attachment']['created'] = date('Y-m-d H:i:s');

                    $this->Attachment->create();
                    $this->Attachment->save($file);

                    $attachment_id = $this->Attachment->id;

                    $temp = explode(".", $attachment['name']);
                    $newfilename = $this->Utility->encrypt($attachment_id, 'AtCh') . '.' . end($temp);
                    move_uploaded_file($attachment["tmp_name"], $filename_main.DS.$newfilename);

                    $file = array();

                    $file['Attachment']['id'] = $attachment_id;
                    $file['Attachment']['path'] = $staff['Staff']['staff_no'].DS.'SKILLS'.DS.$newfilename;

                    $this->Attachment->create();
                    $this->Attachment->save($file);
                }

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }

        $statuses = array(
                            1 => 'ACTIVE',
                            99 => 'INACTIVE',
                        );

        $update = $this->Skill->findIfUpdateByStaffId($staff['Staff']['id']);

        if($update == false)
        {
            $this->Session->setFlash('You cannot make any changes within this period. Please contact system administrator for help', 'info');
            $this->redirect(array('action' => '/'));
        }

        $this->set(compact('statuses'));
    }

    public function attachment($id=null)
    {
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $path = WWW_ROOT . 'documents'.DS;

        if(empty($id))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $detail = $this->Attachment->findById($id);
 
        $directory = $path.$detail['Attachment']['path'];

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            $key = $this->Utility->encrypt($detail['Attachment']['key_id'], 'skl');

            if($this->Attachment->delete($id))
            {
                $directory = $path.$detail['Attachment']['path'];
                unlink($directory);

                $this->Session->setFlash('Attachment successfully deleted.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Attachment not successfully deleted. Please try again!', 'error');
                $this->redirect(array('action' => 'edit/'.$key));
            }
        }
    }

    public function delete($key=null)
    {
        $this->loadModel('Attachment');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $path = WWW_ROOT . 'documents'.DS;

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'edu');

        $detail = $this->Skill->findById($id);

        //$directory = $path.$detail['Attachment']['path'];

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            if($detail['Skill']['status_id'] < 3)
            {

                if($this->Skill->delete($id))
                {
                    foreach ($detail['Attachment'] as $attachment) 
                    {
                        $this->Attachment->delete($attachment['id']);

                        $directory = $path.$attachment['path'];
                        unlink($directory);
                    }

                    $this->Session->setFlash('Information successfully deleted.', 'success');
                    $this->redirect(array('action' => '/'));
                }
                else
                {
                    $this->Session->setFlash('Error! Information not successfully deleted. Please try again!', 'error');
                    $this->redirect(array('action' => '/'));
                }
            
            }
            else
            {
                $this->Session->setFlash('Error! You cannot delete this information. Please contact system administrator for help.', 'error');
                $this->redirect(array('action' => '/'));
            }

        }
    }
}