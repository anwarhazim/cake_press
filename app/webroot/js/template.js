$(function(){

    $(document).ready(function () 
    {
      var baseURL = "";
      
      if($("#baseUrl").length != 0) 
      {
        baseUrl = $("#baseUrl").val();
      }

      if($("#notification_count").length != 0) 
      {
          $.ajax({
          type: "POST",
          url: baseUrl+"Notifications/getNotificationCountByStaffId/",
          success: function(result)
            {
              if(result > 0)
              {
                var $update = $('#notification_count');
                $update.addClass("badge bg-danger-400");
                $update.text(result);
              }
            }
          });
      }

      if($("#notifications").length != 0) 
      {
          $.ajax({
          type: "POST",
          url: baseUrl+"Notifications/getNotificationByStaffId/",
          success: function(result)
            {
              var $notification = $('#notifications');
              $notification.empty();
              $.each(JSON.parse(result),function(key, value) 
              {
                $notification.append(value);
              });
            }
          });
      }

      if($("#update_count").length != 0) 
      {
          $.ajax({
          type: "POST",
          url: baseUrl+"Applicants/getUpdateCountByStaffId/",
          success: function(result)
            {
              if(result > 0)
              {
                var $update = $('#update_count');
                $update.addClass("badge bg-danger-400");
                $update.text(result);
              }
            }
          });
      }

      if($("#updates").length != 0) 
      {
          $.ajax({
          type: "POST",
          url: baseUrl+"Applicants/getUpdateByStaffId/",
          success: function(result)
            {
              var $update = $('#updates');
              $update.empty();
              $.each(JSON.parse(result),function(key, value) 
              {
                $update.append(value);
              });

            }
          });
      }

      if($("#job_designation_id").length != 0) 
      {
        $("#job_designation_id").on("change", function() 
        {
          var designation_id = $(this).val();
          $.ajax({
          type: "POST",
          url: baseUrl+"JobGrades/getJobGradeByDesignationId/"+designation_id,
          success: function(result)
            {
              var $select = $('#job_grade_id');
              $select.empty();
              $select.append('<option value="">PLEASE SELECT...</option>');
              $.each(JSON.parse(result),function(key, value) 
              {
                $select.append('<option value=' + key + '>' + value + '</option>');
              });

            }
          });

        }).change();
      }

      if($("#organisation_type_id").length != 0) 
      {
        $("#organisation_type_id").on("change", function() 
        {
          var organisation_type_id= $(this).val();
          $.ajax({
          type: "POST",
          url: baseUrl+"Organisations/getOrganisationByOrganisationTypeId/"+organisation_type_id,
          success: function(result)
            {
              var $select = $('#organisation_id');
              $select.empty();
              $select.append('<option value="">PLEASE SELECT...</option>');
              $.each(JSON.parse(result),function(key, value) 
              {
                $select.append('<option value=' + key + '>' + value + '</option>');
              });

            }
          });

        }).change();
      }

      if($("#organisation_id").length != 0) 
      {
        $("#organisation_id").on("change", function() 
        {
          var organisation_id= $(this).val();
          $.ajax({
          type: "POST",
          url: baseUrl+"Organisations/getDivisionByOrganisationId/"+organisation_id,
          success: function(result)
            {
              var $select = $('#division_id');
              $select.empty();
              $select.append('<option value="">PLEASE SELECT...</option>');
              $.each(JSON.parse(result),function(key, value) 
              {
                $select.append('<option value=' + key + '>' + value + '</option>');
              });

            }
          });

        }).change();
      }

      if($("#division_id").length != 0) 
      {
        $("#division_id").on("change", function() 
        {
          var division_id= $(this).val();
          $.ajax({
          type: "POST",
          url: baseUrl+"Organisations/getDepartmentByDivisionId/"+division_id,
          success: function(result)
            {
              var $select = $('#department_id');
              $select.empty();
              $select.append('<option value="">PLEASE SELECT...</option>');
              $.each(JSON.parse(result),function(key, value) 
              {
                $select.append('<option value=' + key + '>' + value + '</option>');
              });

            }
          });

        }).change();
      }

      if($("#department_id").length != 0) 
      {
        $("#department_id").on("change", function() 
        {
          var department_id= $(this).val();
          $.ajax({
          type: "POST",
          url: baseUrl+"Organisations/getSectionByDepartmentId/"+department_id,
          success: function(result)
            {
              var $select = $('#section_id');
              $select.empty();
              $select.append('<option value="">PLEASE SELECT...</option>');
              $.each(JSON.parse(result),function(key, value) 
              {
                $select.append('<option value=' + key + '>' + value + '</option>');
              });

            }
          });

        }).change();
      }

      if($("#section_id").length != 0) 
      {
        $("#section_id").on("change", function() 
        {
          var section_id= $(this).val();
          $.ajax({
          type: "POST",
          url: baseUrl+"Organisations/getUnitBySectiontId/"+section_id,
          success: function(result)
            {
              var $select = $('#unit_id');
              $select.empty();
              $select.append('<option value="">PLEASE SELECT...</option>');
              $.each(JSON.parse(result),function(key, value) 
              {
                $select.append('<option value=' + key + '>' + value + '</option>');
              });

            }
          });

        }).change();
      }

      if($("#unit_id").length != 0) 
      {
        $("#unit_id").on("change", function() 
        {
          var section_id= $(this).val();
          $.ajax({
          type: "POST",
          url: baseUrl+"Organisations/getSubUnitByUnitId/"+unit_id,
          success: function(result)
            {
              var $select = $('#subunit_id');
              $select.empty();
              $select.append('<option value="">PLEASE SELECT...</option>');
              $.each(JSON.parse(result),function(key, value) 
              {
                $select.append('<option value=' + key + '>' + value + '</option>');
              });

            }
          });

        }).change();
      }

    });

    $(function(){
        $("#nestable").nestable({
            maxDepth: 10,
        }).on("change", function() {
        var json_text = $(".dd").nestable("serialize");
        $.post(window.location.pathname, {
            data: json_text
        }, function(response){
            //alert(response);
        });
        });
    });

    $(document).ready(function () {
        if($(".pickatime").length != 0) 
        {
            $(".pickatime").pickatime();
        }
    });


    if($(".date").length != 0) 
    {
        $('.date').pickadate({
            selectYears: true,
            selectMonths: true,
            format: 'dd-mm-yyyy',
        });
    }

if($(".start_date").length != 0 && $(".end_date").length != 0) 
{
    // Year selector
    $('.start_date').pickadate({
        selectYears: true,
        selectMonths: true,
        format: 'dd-mm-yyyy',
    });

    $('.end_date').pickadate({
        selectYears: true,
        selectMonths: true,
        format: 'dd-mm-yyyy',
    });

    var from_$input = $('.start_date').pickadate(),
    from_picker = from_$input.pickadate('picker')

    var to_$input = $('.end_date').pickadate(),
        to_picker = to_$input.pickadate('picker')


    // Check if there’s a “from” or “to” date to start with.
    if ( from_picker.get('value') ) {
      to_picker.set('min', from_picker.get('select'))
    }
    if ( to_picker.get('value') ) {
      from_picker.set('max', to_picker.get('select'))
    }

    // When something is selected, update the “from” and “to” limits.
    from_picker.on('set', function(event) {
      if ( event.select ) {
        to_picker.set('min', from_picker.get('select'))    
      }
      else if ( 'clear' in event ) {
        to_picker.set('min', false)
      }
    })
    to_picker.on('set', function(event) {
      if ( event.select ) {
        from_picker.set('max', to_picker.get('select'))
      }
      else if ( 'clear' in event ) {
        from_picker.set('max', false)
      }
    })
}

if($(".start_monthyear").length != 0 && $(".end_monthyear").length != 0) 
{
    // Year selector
    $('.start_monthyear').pickadate({
        selectYears: true,
        selectMonths: true,
        format: 'mmm yyyy',
        onClose: function(dateText, inst) { 
          var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
          var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
          $(this).datepicker('setDate', new Date(year, month, 1));
      }
    });

    $('.end_monthyear').pickadate({
        selectYears: true,
        selectMonths: true,
        format: 'mmm yyyy',
        onClose: function(dateText, inst) { 
          var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
          var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
          $(this).datepicker('setDate', new Date(year, month, 1));
      }
    });

    var from_$input = $('.start_monthyear').pickadate(),
    from_picker = from_$input.pickadate('picker')

    var to_$input = $('.end_monthyear').pickadate(),
        to_picker = to_$input.pickadate('picker')


    // Check if there’s a “from” or “to” date to start with.
    if ( from_picker.get('value') ) {
      to_picker.set('min', from_picker.get('select'))
    }
    if ( to_picker.get('value') ) {
      from_picker.set('max', to_picker.get('select'))
    }

    // When something is selected, update the “from” and “to” limits.
    from_picker.on('set', function(event) {
      if ( event.select ) {
        to_picker.set('min', from_picker.get('select'))    
      }
      else if ( 'clear' in event ) {
        to_picker.set('min', false)
      }
    })
    to_picker.on('set', function(event) {
      if ( event.select ) {
        from_picker.set('max', to_picker.get('select'))
      }
      else if ( 'clear' in event ) {
        from_picker.set('max', false)
      }
    })

}

  // Checkbox Plugin
  $("#jstree_checkbox").jstree(
  {
      "checkbox" : {
          "keep_selected_style" : false,
          "three_state" : false,
      },
      "plugins" : [ "checkbox", "search" ]
  }).bind('check_node.jstree', function(e, data) 
  {
      var currentNode = data.rslt.obj.attr("id");
      var parentNode = data.inst._get_parent(data.rslt.obj).attr("id");
     jQuery.jstree._reference($("#jstree_checkbox")).check_node('#'+parentNode);
  });

  var to = false;
    $('#jstree_checkbox_search').keyup(function () 
    {
      if(to) { clearTimeout(to); }
      to = setTimeout(function () {
        var v = $('#jstree_checkbox_search').val();
        $('#jstree_checkbox').jstree(true).search(v);
      }, 250);
    });

  $('#jstree_checkbox').on("changed.jstree", function (e, data) 
  {
    document.getElementById('jsfields').value = data.selected;
  });

  if($( '#jsfields' ).length)
  {
      var val = $( '#jsfields' ).val();
      var valArray = val.split(",");

      var setList = [];
      for (i = 0; i < valArray.length; i++) { 
          setList.push("#" + valArray[i]);
      }

      $('#jstree_checkbox').jstree("check_node", setList);
  }

});