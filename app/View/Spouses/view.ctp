<!-- Page header -->
<div class="page-header page-header-default">
	
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="<?php echo $this->html->url('/Spouses/index', true);?>">Spouses</a></li>
			<li class="active">View Spouse</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
	<?php 
			echo $this->Session->flash(); 

			if(!empty($this->validationErrors['Spouse']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['Spouse'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}
		?>
        <?php echo $this->Form->create('Spouse', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Spouse</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Name</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Marriage Date</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('marriage_date', array(
									'class'=>'form-control date', 
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">IC No.</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('ic_new', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Passport No.</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('passport_no', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Religion</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('religion_id', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'options'=>$religions,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Race</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('race_id', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'options'=>$races,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Nationality</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('national_id', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'options'=>$nationals,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Occupation</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('occupation', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Employer </label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('employer', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Employer Address </label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('employer_address', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'textarea',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<label class="control-label col-lg-12">Attachment</label>
				<div class="col-md-12">
					<div class="row">
						<?php
							for ($i=0; $i < count($detail['Attachment']) ; $i++) 
							{ 
						?>
							<div class="col-lg-2 col-sm-6">
								<div class="thumbnail">
									<div class="thumb">
										<img src="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" />
										<div class="caption-overflow">
											<span>
												<a href="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View"><i class="icon-eye"></i></a>
											</span>
										</div>
									</div>

									<div class="caption">
										<h6 class="no-margin"><?php echo $detail['Attachment'][$i]['name'] ?></h6>
									</div>
								</div>
							</div>
						<?php
							}							
						?>
					</div>
				</div>
			</fieldset>
			<?php
				if($update == true)
				{
			?>
            <div class="text-right">
				<a class="btn btn-warning legitRipple" href="<?php echo $this->Html->url('/Spouses/edit/'.$key, true);?>">
					Edit <i class="icon-pencil5 position-right"></i>
				</a>
			</div>
			<?php
				}
			?>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Spouses/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->