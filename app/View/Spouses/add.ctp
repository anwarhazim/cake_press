<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="<?php echo $this->html->url('/Spouses/index', true);?>">Spouses</a></li>
			<li class="active">Add New Spouse</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
		<?php 
			echo $this->Session->flash(); 

			if(!empty($this->validationErrors['Spouse']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['Spouse'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}
		
		?>

        <?php echo $this->Form->create('Spouse', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Spouse</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Name <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Marriage Date <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('marriage_date', array(
									'class'=>'form-control date', 
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">IC No. <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('ic_new', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Passport No.</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('passport_no', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Religion <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('religion_id', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'options'=>$religions,
									'empty'=>'PLEASE SELECT...',
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Race <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('race_id', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'options'=>$races,
									'empty'=>'PLEASE SELECT...',
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Nationality <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('national_id', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'options'=>$nationals,
									'empty'=>'PLEASE SELECT...',
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Occupation</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('occupation', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Employer </label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('employer', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Employer Address </label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('employer_address', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'textarea'
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<label class="control-label col-lg-12">Attachment <span class="text-danger">*</span></label>
				<div class="col-md-12">
					<?php 
						echo $this->Form->input('attachments.', array(
							'class'=>'form-control file-input',
							'label'=> false,
							'error'=>false,
							'type'=>'file',
							'multiple'=>'multiple',
							'data-preview-file-type'=>'any',
							'data-show-upload'=>false,
							'showRemove'=>false,
							'data-show-caption'=>false,
							'accept'=>'image/*',
							'maxFileSize'=>'2MB'
							)
						); 
					?>
					<span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-success legitRipple">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Spouses/add', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Spouses/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->