<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li class="active">Employment Information</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<div class="panel panel-flat">
	<div class="panel-body">
		<?php echo $this->Session->flash(); ?>

		<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Employment Details</legend>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Designation</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('job_designation_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$jobdesignations,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Job Grade</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('job_grade_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$jobgrades,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Date Entry</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('entry_date', array(
									'class'=>'form-control date',
									'label'=> false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<!--
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Business Type</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('organisation_type_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$organisationtypes,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				-->
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Company Name</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('organisation_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$organisations,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Division</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('division_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$divisions,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Department</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('department_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$departments,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Section</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('section_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$sections,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Unit</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('unit_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$units,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Sub Unit</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('sub_unit_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$subunits,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Job Type</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('job_type_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$jobtypes,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Depot / Location</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('location_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$locations,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Seniority Date</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('job_start_date', array(
									'class'=>'form-control start_date',
									'label'=> false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">End Working</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('job_end_date', array(
									'class'=>'form-control end_date', 
									'label'=> false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
		<?php echo $this->Form->end(); ?>

	</div>
</div>