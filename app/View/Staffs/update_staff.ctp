<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="<?php echo $this->html->url('/Staffs/Profile', true);?>">Personal Information</a></li>
			<li class="active">Update Personal Information</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<div class="panel panel-flat">
	<div class="panel-body">
	<?php
		echo $this->Session->flash();

		if(!empty($this->validationErrors['Staff']))
		{
		?>
			<div role="alert" class="alert alert-danger">
					<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<?php
						foreach ($this->validationErrors['Staff'] as $errors)
						{
							echo '<ul>';
							foreach ($errors as $error)
							{
								echo '<li>'.h($error).'</li>';
							}
							echo '</ul>';
						}
					?>
			</div>
		<?php
		}
	?>

		<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<?php
				echo $this->Form->input('is_flag', array(
					'class'=>'form-control',
					'label'=> false,
					'type'=>'hidden',
					)
				);
			?>
			<fieldset class="content-group">
				<legend class="text-bold">Personal Information</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Name</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">IC No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('ic_no', array(
									'class'=>'form-control',
									'maxlength'=>12,
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Staff No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('staff_no', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Gender</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('gender_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'options'=>$genders,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Nationality</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('national_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'options'=>$nationals,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Date of Birth</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('date_of_birth', array(
									'class'=>'form-control date',
									'label'=> false,
									'error'=> false,
									'type'=>'text',
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Place of Birth</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('place_birth_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'options'=>$states,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Race</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('race_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'options'=>$races,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Religion</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('religion_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'options'=>$religions,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Marital Status <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('marital_status_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'options'=>$maritalstatus,
									'empty'=>'PLEASE SELECT...',
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<!--
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Bank<br>(For salary purposes)</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('bank_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'options'=>$banks,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Bank Account No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('bank_account_no', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Income Tax No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('income_tax_no', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">EPF No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('epf_no', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Income Tax Branch</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('income_tax_branch', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">SOCSO No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('socso_no', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Tabung Haji Account No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('tabung_haji_account_no', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">ASN/ASB No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('asn_asb_no', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									'disabled'=>'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			-->
			<div class="text-right">
				<button type="submit" class="btn btn-success legitRipple">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Staffs/update_staff', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
	</div>
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/Staffs/profile', true); ?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
</div>
