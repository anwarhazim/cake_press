<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="<?php echo $this->html->url('/Staffs', true);?>">Staffs</a></li>
			<li class="active">Edit Staff</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
	<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="<?php echo $this->html->url('/Staffs/edit/'.$key, true);?>">Personal Details</a>
				</li>
				<li>
					<a href="<?php echo $this->html->url('/Staffs/administrator/'.$key, true);?>">Administrator</a>
				</li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active">
					<?php echo $this->Session->flash(); ?>

					<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
					<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Name <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Staff No. <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('staff_no', array(
										'class'=>'form-control', 
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">IC No. <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic_new', array(
										'class'=>'form-control', 
										'maxlength'=>12,
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Passport No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('passport_no', array(
										'class'=>'form-control', 
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Gender <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('gender_id', array(
										'class'=>'form-control',
										'label'=> false,
										'options'=>$genders,
										'empty'=>'PLEASE SELECT...',
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Nationality <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('national_id', array(
										'class'=>'form-control', 
										'label'=> false,
										'options'=>$nationals,
										'empty'=>'PLEASE SELECT...',
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Date of Birth <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('date_of_birth', array(
										'class'=>'form-control date',
										'label'=> false,
										'type'=>'text'
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Place of Birth <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('place_birth_id', array(
										'class'=>'form-control', 
										'label'=> false,
										'options'=>$states,
										'empty'=>'PLEASE SELECT...',
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Race <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('race_id', array(
										'class'=>'form-control',
										'label'=> false,
										'options'=>$races,
										'empty'=>'PLEASE SELECT...',
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Religion <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('religion_id', array(
										'class'=>'form-control',
										'label'=> false,
										'options'=>$religions,
										'empty'=>'PLEASE SELECT...',
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Marital Status <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('marital_status_id', array(
										'class'=>'form-control',
										'label'=> false,
										'options'=>$maritalstatus,
										'empty'=>'PLEASE SELECT...',
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Bank <span class="text-danger">*</span><br>(For salary purposes)</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('bank_id', array(
										'class'=>'form-control',
										'label'=> false,
										'options'=>$banks,
										'empty'=>'PLEASE SELECT...',
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Bank Account No. <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('bank_account_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Income Tax No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('income_tax_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">EPF No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('epf_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Income Tax Branch</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('income_tax_branch', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">SOCSO No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('socso_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Tabung Haji Account No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('tabung_haji_account_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">ASN/ASB No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('asn_asb_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">No. Tel. (Home)</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('home_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">No. Tel. (HP)</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('mobile_no', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Email <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('email', array(
										'class'=>'form-control',
										'label'=> false,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn btn-success legitRipple">
						Save <i class="icon-floppy-disk position-right"></i>
					</button>

					<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Staffs/edit/'.$key, true);?>">
						Reset <i class="icon-spinner11 position-right"></i>
					</a>
				</div>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Staffs/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->