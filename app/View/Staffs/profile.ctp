<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li class="active">Personal Information</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<div class="panel panel-flat">
	<div class="panel-body">
		<?php echo $this->Session->flash(); ?>

		<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>

			<fieldset class="content-group">
				<legend class="text-bold">Personal Information</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Name</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">IC No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('ic_no', array(
									'class'=>'form-control',
									'maxlength'=>12,
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Staff No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('staff_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Gender</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('gender_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$genders,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Nationality</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('national_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$nationals,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Date of Birth</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('date_of_birth', array(
									'class'=>'form-control date',
									'label'=> false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Place of Birth</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('place_birth_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$states,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Race</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('race_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$races,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Religion</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('religion_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$religions,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Marital Status</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('marital_status_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$maritalstatus,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<!--
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Bank<br>(For salary purposes)</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('bank_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$banks,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Bank Account No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('bank_account_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Income Tax No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('income_tax_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">EPF No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('epf_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Income Tax Branch</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('income_tax_branch', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">SOCSO No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('socso_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Tabung Haji Account No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('tabung_haji_account_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">ASN/ASB No.</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('asn_asb_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
			!-->
			<?php 
				if($update == true)
				{
			?>
			<div class="text-right">
				<a class="btn btn-warning legitRipple" href="<?php echo $this->Html->url('/Staffs/update_staff', true);?>">
					Update <i class="icon-pencil5 position-right"></i>
				</a>
			</div>
			<?php
				}
			?>
        <?php echo $this->Form->end(); ?>
	</div>
</div>
