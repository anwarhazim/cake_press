<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li class="active">Contact Information</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<div class="panel panel-flat">
	<div class="panel-body">
		<?php echo $this->Session->flash(); ?>

		<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Contact Information</legend>
				<div class="form-group">
					<label class="control-label col-lg-12">Address</label>
					<div class="col-lg-12">
						<?php 
							echo $this->Form->input('current_address_1', array(
								'class'=>'form-control',
								'label'=> false,
								'disabled'=>$disabled
								)
							); 
						?>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-12">
						<?php 
							echo $this->Form->input('current_address_2', array(
								'class'=>'form-control',
								'label'=> false,
								'disabled'=>$disabled
								)
							); 
						?>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-12">
						<?php 
							echo $this->Form->input('current_address_3', array(
								'class'=>'form-control',
								'label'=> false,
								'disabled'=>$disabled
								)
							); 
						?>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Postcode</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('current_postcode', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">State</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('current_state_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$states,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">No. Tel. (Home)</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('home_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">No. Tel. (HP)</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('mobile_no', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-lg-4">Email</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('email', array(
									'class'=>'form-control',
									'label'=> false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<?php 
				if($update == true)
				{
			?>
			<div class="text-right">
				<a class="btn btn-warning legitRipple" href="<?php echo $this->Html->url('/Staffs/update_contact', true);?>">
					Update <i class="icon-pencil5 position-right"></i>
				</a>
			</div>
			<?php
				}
			?>
		<?php echo $this->Form->end(); ?>

	</div>
</div>