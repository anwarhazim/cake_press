<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li class="active">Contact Information</li>
			<li class="active">Update Contact Infomation</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<div class="panel panel-flat">
	<div class="panel-body">
	<?php
		echo $this->Session->flash();

		if(!empty($this->validationErrors['Staff']))
		{
		?>
			<div role="alert" class="alert alert-danger">
					<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<?php
						foreach ($this->validationErrors['Staff'] as $errors)
						{
							echo '<ul>';
							foreach ($errors as $error)
							{
								echo '<li>'.h($error).'</li>';
							}
							echo '</ul>';
						}
					?>
			</div>
		<?php
		}
	?>

		<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<?php
				echo $this->Form->input('is_flag', array(
					'class'=>'form-control',
					'label'=> false,
					'type'=>'hidden',
					)
				);
			?>
			<fieldset class="content-group">
				<legend class="text-bold">Contact Information</legend>
				<fieldset class="content-group">
					<div class="form-group">
						<label class="control-label col-lg-12">Address <span class="text-danger">*</span></label>
						<div class="col-lg-12">
							<?php 
								echo $this->Form->input('current_address_1', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false
									)
								); 
							?>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-12">
							<?php 
								echo $this->Form->input('current_address_2', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false
									)
								); 
							?>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-12">
							<?php 
								echo $this->Form->input('current_address_3', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false
									)
								); 
							?>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Postcode <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('current_postcode', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=> false
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">State <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('current_state_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=> false,
										'options'=>$states,
										'empty'=>'PLEASE SELECT...'
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">No. Tel. (Home)</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('home_no', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=> false
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">No. Tel. (HP)</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('mobile_no', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=> false
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Email <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('email', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=> false
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
			</fieldset>
			<div class="text-right">
				<button type="submit" class="btn btn-success legitRipple">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Staffs/update_contact', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
		<?php echo $this->Form->end(); ?>

	</div>
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/Staffs/contact', true); ?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
</div>