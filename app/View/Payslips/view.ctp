<!-- Page header -->
<div class="page-header page-header-default">

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url('/', true);?>">Home</a></li>
            <li><a href="<?php echo $this->Html->url('/payslips/index', true);?>">List of Payslips</a></li>
            <li class="active">Payslip Details</li>
        </ul>
    </div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
    <div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('JobDesignation', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
        <fieldset class="content-group">
            <legend class="text-bold">Payslip</legend>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">STAFF'S NO.</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['User']['username'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">NRIC NO.</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['Staff']['ic_new'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">DIVISION</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['Division']['name'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">EPF NO.</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['Staff']['epf_no'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">TAX NO.</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['Staff']['income_tax_no'] ?>" disabled="disabled">
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">NAME</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['Staff']['name'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">POSITION</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['JobDesignation']['name'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">DEPARTMENT</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['Department']['name'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">SOCSO NO.</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php echo $staff['Staff']['socso_no'] ?>" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">MONTH / YEAR</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="<?php  echo strtoupper($wage_month) . ' / ' . $wage_year ?>" disabled="disabled">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-framed">
                        <thead>
                            <tr>
                                <th class="text-center bg-slate-600">INCOME</th>
                                <th class="text-center bg-slate-600">RM</th>
                                <th class="text-center bg-slate-600" colspan="2">DEDUCTION</th>
                                <th class="text-center bg-slate-600">RM</th>
                                <th class="text-center bg-slate-600">YEAR-TO-DATE</th>
                                <th class="text-center bg-slate-600">RM</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php

                            for ($i=0; $i < ($count) ; $i++) 
                            {
                        ?>
                            <tr>
                                <td>
                                    <?php 
                                        if(!empty($incomes[$i]['PayslipCategory']['wage_long_text']))
                                        {
                                            echo $incomes[$i]['PayslipCategory']['wage_long_text']; 
                                        }
                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php 
                                        if(!empty($incomes[$i]['Payslip']['wage_amount']))
                                        {
                                            echo number_format($incomes[$i]['Payslip']['wage_amount'], 2, ".", ",");
                                        }
                                    ?>
                                </td>
                                <td colspan="2">
                                    <?php 
                                        if(!empty($deductions[$i]['PayslipCategory']['wage_long_text']))
                                        {
                                            echo $deductions[$i]['PayslipCategory']['wage_long_text']; 
                                        }
                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php 
                                        if(!empty($deductions[$i]['Payslip']['wage_amount']))
                                        {
                                            echo number_format(str_replace('-', '', $deductions[$i]['Payslip']['wage_amount']), 2, ".", ","); 
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        if(!empty($employer_contr[$i]['PayslipCategory']['wage_long_text']))
                                        {
                                            echo $employer_contr[$i]['PayslipCategory']['wage_long_text']; 
                                        }
                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php 
                                        if(!empty($employer_contr[$i]['Payslip']['wage_amount']))
                                        {
                                            echo number_format($employer_contr[$i]['Payslip']['wage_amount'], 2, ".", ","); 
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        </tbody>
                        <thead>
                            <tr>
                                <th class="text-center">GROSS INCOME</th>
                                <th class="text-right bg-slate-600">
                                    <?php 
                                        if(!empty($gross_pay['Payslip']['wage_amount']))
                                        {
                                            echo number_format($gross_pay['Payslip']['wage_amount'], 2, ".", ","); 
                                        }
                                    ?>
                                </th>
                                <th class="text-center" colspan="2">TOTAL DEDUCTION</th>
                                <th class="text-right bg-slate-600">69.05</th>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-right bg-slate-600">1,201.30</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th class="text-center" style="border:1px solid white;">&nbsp;</th>
                                <th class="text-center" style="border:1px solid white;">&nbsp;</th>
                                <th class="text-center" style="border:1px solid white;border-right-color:#a4a4a4;">
                                    &nbsp;</th>
                                <th class="text-center">NET PAY</th>
                                <th class="text-right bg-slate-600">
                                    <?php 
                                        if(!empty($net_pay['Payslip']['wage_amount']))
                                        {
                                            echo number_format($net_pay['Payslip']['wage_amount'], 2, ".", ","); 
                                        }
                                    ?>
                                </th>
                                <th class="text-center" style="border:1px solid white;">&nbsp;</th>
                                <th class="text-center" style="border:1px solid white;">&nbsp;</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <br />

                <div class="table-responsive">
                    <table class="table table-bordered table-framed">
                        <thead>
                            <tr>
                                <th>EMPLOYER'S EPF :  1235687123</th>
                                <th>EMPLOYERS'S SOCSO :  90342872344</th>
                                <th>BANK & ACC. NO. : <?php echo $staff['Staff']['bank_account_no'] ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </fieldset>

        <div class="text-right">
            <?php
                echo $this->Html->link('Print <i class="icon-printer2 position-right"></i>', array('controller' => 'Payslips', 'action' => 'payslip/' . $wage_month . '/' . $wage_year, 'ext' => 'pdf'), array('class' => 'btn btn-info position-right', 'target' =>'_blank', 'escape' => false));
            ?>
        </div>

        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
    <!-- panel-footer -->
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-warning" href="<?= $this->Html->url('/Payslips/index', true);?>">
                    Back <i class="icon-arrow-left13 position-right"></i>
                </a>
            </span>
        </div>
    </div>
    <!-- /panel-footer -->
</div>
<!-- /Form horizontal -->