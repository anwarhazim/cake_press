<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li class="active">List of Payslip</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Payslip', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
            <fieldset class="content-group">
			    <legend class="text-bold">Search</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Start Date</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('start_date', array(
									'class'=>'form-control start_date', 
									'label'=> false,
									'error'=> false,
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">End Date</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('end_date', array(
									'class'=>'form-control end_date', 
									'label'=> false,
									'error'=> false,
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-primary">
					Find 
					<i class="glyphicon glyphicon-search position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Payslips/index', true);?>">
					Reset
					<i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
</div>
<!-- /Form horizontal -->

<!-- Bordered panel body table -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">List of Payslip</h5>
	</div>
	<div class="panel-body">
		<?php
			$counter = $this->Paginator->counter(array('format' =>'{:start}'));
		?>
		<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="5%" class="text-center">No</th>
						<th>Month</th>
						<th>Year</th>
						<th width="15%" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if($details)
						{
							foreach ($details as $detail) 
							{
					?>
						<tr>
							<th class="text-center">
								<?php echo $counter; ?>
							</th>
							<td>
								<?php echo $detail['Payslip']['wage_month'] ?>
							</td>
							<td>
								<?php echo $detail['Payslip']['wage_year'] ?>
							</td>
							<td class="text-center">
								<a alt="edit" href="<?php echo $this->Html->url('/Payslips/view/'.$detail['Payslip']['wage_month']."/".$detail['Payslip']['wage_year']); ?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Edit details">
									<spam class="icon-file-text2">
									</spam>
								</a>
							</td>
						</tr>
					<?php
							$counter++;
							}
						}
						else
						{
					?>
						<tr>
							<td colspan="4">
								No data...
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<br/>
		<p class="pull-right">
			<?php echo $this->Paginator->counter(array(
                                                    'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                            {:count} total, start {:start}, end at {:end}'
                                                    ));
			?>
		</p>
	</div>
	<div class="panel-footer">
		<div class="heading-elements">
			<ul class="pagination  pull-right">
				<?php 
					echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
					echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
				?>
			</ul>
		</div>
	</div>
</div>
<!-- /bordered panel body table -->