<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>PRESS | Prasarana Employee Self-Service</title>
<style>
body {
    /* margin: 0; */
    /* padding: 0; */
    /* font-size: 12px; */
    /* background-color: #FAFAFA; */
}

* {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}

@page {
    size: A4;
    margin: 1.27cm 1.27cm 1.27cm 1.27cm;
}

@media print {
    .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
    }
}

</style>

<body>
    <table style="text-align: left; width: 100%; font-size: 8px;" cellpadding="0px" cellspacing="0px">
        <tr>
            <td colspan="1" rowspan="6" style="vertical-align: top; width: 280px;">
                <img src="<?php echo $baseURL . 'img/logo_prasarana.jpg'; ?>" style="width: 135px; height: 27px;"><br>
                <p style="font-size: 14px; font-weight: bold; margin: 0;">
                    PRASARANA MALAYSIA
                </p>
                <p style="font-size: 14px; font-weight: bold; margin: 0;" class="a"> 
                    BERHAD <span style="font-size: 10px; font-weight: normal; margin: 0; padding: 0;">(467220-U)</span>
                </p>
            </td>
            <td style="vertical-align: top; width: 60px;">STAFF'S NO.<br>
            </td>
            <td style="vertical-align: top; width: 10px; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 300px;"><?php echo $staff['User']['username'] ?><br>
            </td>
            <td style="vertical-align: top; width: 52px;"><br>
            </td>
            <td style="vertical-align: top; width: 9px;"><br>
            </td>
            <td style="vertical-align: top; width: 50px;"><br>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 0;">NAME<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php echo $staff['Staff']['name'] ?><br>
            </td>
            <td style="vertical-align: top; width: 0;"><br>
            </td>
            <td style="vertical-align: top; width: 0;"><br>
            </td>
            <td style="vertical-align: top; width: 0;"><br>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 0;">NRIC NO.<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php echo $staff['Staff']['ic_new'] ?><br>
            </td>
            <td style="vertical-align: top; width: 0;"><br>
            </td>
            <td style="vertical-align: top; width: 0;"><br>
            </td>
            <td style="vertical-align: top; width: 0;"><br>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 0;">POSITION<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php echo $staff['JobDesignation']['name'] ?><br>
            </td>
            <td style="vertical-align: top; width: 0;">EPF NO.<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php echo $staff['Staff']['epf_no'] ?><br>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 0;">DIV/DEPT.<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php echo $staff['Department']['name'] ?><br>
            </td>
            <td style="vertical-align: top; width: 0;">SOCSO NO.<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php echo $staff['Staff']['socso_no'] ?><br>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 0;">MONTH/YEAR<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php  echo strtoupper($wage_month) . ' / ' . $wage_year ?><br>
            </td>
            <td style="vertical-align: top; width: 0;">TAX NO.<br>
            </td>
            <td style="vertical-align: top; width: 0; text-align: center;">:<br>
            </td>
            <td style="vertical-align: top; width: 0;"><?php echo $staff['Staff']['income_tax_no'] ?><br>
            </td>
        </tr>
    </table>

    <div style="padding-top: 5px;">
        <table style="text-align: left; width: 100%; font-size: 8px;" cellpadding="0px" cellspacing="0px">
            <tr>
                <td
                    style="vertical-align: top; border: 1px solid black; text-align: center; height: 20px; width: 179px; background-color: #a4a4a4;">
                    <div style="padding-top: 3px;">INCOME</div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; height: 20px; width: 70px; background-color: #a4a4a4;">
                    <div style="padding-top: 3px;">RM</div>
                </td>
                <td colspan="2" rowspan="1"
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; height: 20px; width: 179px; background-color: #a4a4a4;">
                    <div style="padding-top: 3px;">DEDUCTION</div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; height: 20px; width: 70px; background-color: #a4a4a4;">
                    <div style="padding-top: 3px;">RM<div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; height: 20px; width: 179px; background-color: #a4a4a4;">
                    <div style="padding-top: 3px;">YEAR-TO-DATE</div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; eight: 20px; width: 70px; background-color: #a4a4a4;">
                    <div style="padding-top: 3px;">RM<div>
                </td>
            </tr>
            <!-- FOR LOOP START HERE -->
            <?php
                for ($i=0; $i < ($count) ; $i++) 
                {
                ?>
            <tr>
                <td
                    style="vertical-align: top; border: 1px solid black; height: 10px; border-top: none; border-bottom: none;">
                    <div style="padding-bottom: 2px; padding-left: 5px;">
                        <?php 
                            if(!empty($incomes[$i]['PayslipCategory']['wage_long_text']))
                            {
                                echo $incomes[$i]['PayslipCategory']['wage_long_text']; 
                            }
                        ?>
                    </div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; height: 10px; border-top: none; border-left: none; border-bottom: none; text-align: right;">
                    <div style="padding-bottom: 2px; padding-right: 5px;">
                        <?php 
                            if(!empty($incomes[$i]['Payslip']['wage_amount']))
                            {
                                echo number_format($incomes[$i]['Payslip']['wage_amount'], 2, ".", ",");
                            }
                        ?>
                    </div>
                </td>
                <td colspan="2" rowspan="1"
                    style="vertical-align: top; border: 1px solid black; height: 10px; border-top: none; border-left: none; border-bottom: none;">
                    <div style="padding-bottom: 2px; padding-left: 5px;">
                        <?php 
                            if(!empty($deductions[$i]['PayslipCategory']['wage_long_text']))
                            {
                                echo $deductions[$i]['PayslipCategory']['wage_long_text']; 
                            }
                        ?>
                    </div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; height: 10px; border-top: none; border-left: none; text-align: right; border-bottom: none;">
                    <div style="padding-bottom: 2px; padding-right: 5px;">
                        <?php 
                            if(!empty($deductions[$i]['Payslip']['wage_amount']))
                            {
                                echo number_format(str_replace('-', '', $deductions[$i]['Payslip']['wage_amount']), 2, ".", ","); 
                            }
                        ?>
                    </div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; height: 10px; border-top: none; border-left: none; border-bottom: none;">
                    <div style="padding-bottom: 2px; padding-left: 5px;">
                        <?php 
                            if(!empty($employer_contr[$i]['PayslipCategory']['wage_long_text']))
                            {
                                echo $employer_contr[$i]['PayslipCategory']['wage_long_text']; 
                            }
                        ?>
                    </div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; height: 10px; border-top: none; border-left: none; border-bottom: none; text-align: right;">
                    <div style="padding-bottom: 2px; padding-right: 5px;">
                        <?php 
                            if(!empty($employer_contr[$i]['Payslip']['wage_amount']))
                            {
                                echo number_format($employer_contr[$i]['Payslip']['wage_amount'], 2, ".", ","); 
                            }
                        ?>
                    </div>
                </td>
            </tr>
            <?php
                }
            ?>
            <!-- FOR LOOP END HERE -->
            <tr>
                <td style="vertical-align: top; border: 1px solid black; text-align: center; height: 20px;">
                    <div style="padding-top: 3px;">GROSS INCOME</div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; background-color: #a4a4a4; background-color: #a4a4a4; text-align: right;">
                    <div style="padding-top: 3px;padding-right: 5px;">
                        <?php 
                            if(!empty($gross_pay['Payslip']['wage_amount']))
                            {
                                echo number_format($gross_pay['Payslip']['wage_amount'], 2, ".", ","); 
                            }
                        ?>
                    </div>
                </td>
                <td colspan="2" rowspan="1"
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center;">
                    <div style="padding-top: 3px;">TOTAL DEDUCTION</div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; background-color: #a4a4a4; background-color: #a4a4a4; text-align: right;">
                    <div style="padding-top: 3px;padding-right: 5px;">69.05</div>
                </td>
                <td style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center;">&nbsp;
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-left: none; text-align: center; background-color: #a4a4a4; background-color: #a4a4a4; text-align: right;">
                    <div style="padding-top: 3px;padding-right: 5px;">1,201.30</div>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top;">&nbsp;</td>
                <td style="vertical-align: top;">&nbsp;</td>
                <td style="vertical-align: top;">&nbsp;</td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-top: none; text-align: center; height: 20px;">
                    <div style="padding-top: 3px;">NET PAY</div>
                </td>
                <td
                    style="vertical-align: top; border: 1px solid black; border-top: none; border-left: none; background-color: #a4a4a4; text-align: right;">
                    <div style="padding-top: 3px;padding-right: 5px;">
                        <?php 
                            if(!empty($net_pay['Payslip']['wage_amount']))
                            {
                                echo number_format($net_pay['Payslip']['wage_amount'], 2, ".", ","); 
                            }
                        ?>
                    </div>
                </td>
                <td style="vertical-align: top;">&nbsp;</td>
                <td style="vertical-align: top;">&nbsp;</td>
            </tr>
        </table>
    </div>

    <div style="padding-top: 10px;">
        <table style="text-align: left; width: 72%; font-size: 8px;" cellpadding="0px" cellspacing="0px">
            <tr>
                <td style="vertical-align: top; border: 1px solid black; height: 20px;"><div style="padding-left: 5px; padding-top: 3px;">EMPLOYERS EPF : 1235687123</div></td>
                <td style="vertical-align: top; border: 1px solid black; height: 20px; border-left: none;"><div style="padding-left: 5px; padding-top: 3px;">EMPLOYERS SOCSO : 90342872344</div></td>
                <td style="vertical-align: top; border: 1px solid black; height: 20px; border-left: none;"><div style="padding-left: 5px; padding-top: 3px;">BANK &amp; ACC. NO. : <?php echo $staff['Staff']['bank_account_no'] ?></div></td>
            </tr>
        </table>
    </div>
</body>