<!-- Page header -->
<div class="page-header page-header-default">

<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Modules</a></li>
			<li>List of Role</li>
			<li class="active">Assign Role to Module</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Modul', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<legend class="text-bold">Assign <?php echo $detail['Role']['name']; ?> to Module</legend>
			<input type="hidden" id="jsfields" name="jsfields" value="<?= $setList ?>" />
			<div id="jstree_checkbox" class="push-down-20">
				<?=$tree ?>
			</div>

			<div class="text-right">
				<button type="submit" class="btn btn-success">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default" href="<?= $this->html->url('/Moduls/assign/'.$key, true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Moduls/roles', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->