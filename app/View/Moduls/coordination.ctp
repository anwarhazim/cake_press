<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Modules</a></li>
			<li class="active">Manage Modules</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
		<?php 
			echo $this->Session->flash();
			echo $this->Form->create('Modul', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));
		?>
		<fieldset class="content-group">
		<legend class="text-bold">Manage Modules</legend>
			<div class="col-md-12">
				<div class="dd" id="nestable">
					<ol class="dd-list">
						<?php echo $nestable ?>
					</ol>
				</div>
			</div>
		</fieldset>	
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Moduls/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->