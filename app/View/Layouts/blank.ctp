<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PRESS | Prasarana Employee Self-Service</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <?php
        echo  $this->Html->css(array(
                                'icons/icomoon/styles.css',
                                'bootstrap.css',
                                'core.css',
                                'components.css',
                                'colors.css',
                                'jstree/jstree.min.css'
                            )); 
    ?>
	<!-- /global stylesheets -->

    <!-- Core JS files -->
    <?php
        echo $this->Html->script(array(
                                '/js/plugins/loaders/pace.min',
                                '/js/core/libraries/jquery.min',
                                '/js/core/libraries/bootstrap.min',
                                '/js/plugins/loaders/blockui.min',
                            )); 
    ?>
	<!-- /core JS files -->

	<!-- Theme JS files -->
    <?php 
        echo $this->Html->script(
            array(
                '/js/plugins/forms/styling/uniform.min',
                '/js/app',
                '/js/demo_pages/login'
            ));
    ?>
	<!-- /theme JS files -->

</head>

<body class="login-container bg-slate-800">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<?= $this->fetch('content'); ?>

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>