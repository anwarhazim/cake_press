<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PRESS | Prasarana Employee Self-Service</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <?php
        echo  $this->Html->css(array(
                                'icons/icomoon/styles.css',
                                'bootstrap.css',
                                'core.css',
                                'components.css',
                                'colors.css',
                                'jstree/jstree.min.css'
                            ));
    ?>
	<!-- /global stylesheets -->

    <!-- Core JS files -->
    <?php
        echo $this->Html->script(array(
                                '/js/plugins/loaders/pace.min',
                                '/js/core/libraries/jquery.min',
                                '/js/core/libraries/bootstrap.min',
                                '/js/plugins/loaders/blockui.min',
                            ));
    ?>
	<!-- /core JS files -->

	<!-- Theme JS files -->
    <?php
        echo $this->Html->script(array(
                                '/js/plugins/ui/nicescroll.min',
                                '/js/plugins/pickers/daterangepicker',
                                '/js/plugins/pickers/anytime.min',
                                '/js/plugins/pickers/pickadate/picker',
                                '/js/plugins/pickers/pickadate/picker.date',
                                '/js/plugins/pickers/pickadate/picker.time',
                                '/js/plugins/pickers/pickadate/legacy',
                                '/js/plugins/nestable/jquery.nestable',
                                '/js/core/libraries/jasny_bootstrap.min',
                                '/js/plugins/forms/styling/uniform.min',
                                '/js/plugins/jstree/jstree.min',
                                '/js/plugins/media/fancybox.min',
                                '/js/plugins/forms/styling/switchery.min',
                                '/js/plugins/forms/inputs/touchspin.min',
                                '/js/core/libraries/jquery_ui/interactions.min.js',
                                '/js/plugins/notifications/pnotify.min.js',
                                '/js/app',
                                '/js/demo_pages/components_notifications_pnotify.js',
                                '/js/demo_pages/mail_list',
                                '/js/demo_pages/layout_fixed_custom',
                                '/js/demo_pages/gallery',
                                '/js/plugins/ui/ripple.min',
                                '/js/demo_pages/form_input_groups',
                                '/js/template'
                            ));
    ?>
	<!-- /theme JS files -->

</head>

<body class="navbar-top">
    <!-- hidden value -->
    <input type='hidden' name='baseUrl' id='baseUrl' value="<?php echo $this->Html->url('/', true); ?>"/>
    <!-- hidden value -->

	<!-- Main navbar -->
        <?php echo $this->element('navbar'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
                <?php echo $this->element('sidebar'); ?>
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

                    <?php echo $this->fetch('content'); ?>

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
    <!-- Warning modal -->
    <div id="modal_logout" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title"><span class="icon-exit2"></span> Leave site?</h6>
                </div>

                <div class="modal-body">
                    <h6 class="text-semibold">Are you sure you want to Log Out?</h6>
                    <p>Changes you made may not be saved. Please save your work.</p>
                </div>

                <div class="modal-footer">
                    <a class="btn btn-warning" href="<?php echo $this->Html->url('/Users/logout', true); ?>">Yes</a>
                    <button type="button" class="btn btn-link" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /warning modal -->
</body>
</html>
