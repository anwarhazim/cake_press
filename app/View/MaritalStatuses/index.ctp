<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Lookups</a></li>
            <li class="active">Marital Statuses</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('MaritalStatus', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
            <fieldset class="content-group">
			    <legend class="text-bold">Search</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-1">Search</label>
						<div class="col-lg-11">
							<?php
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'placeholder'=>'Name',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Start Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('start_date', array(
									'class'=>'form-control start_date',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">End Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('end_date', array(
									'class'=>'form-control end_date',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-primary">
					Find
					<i class="glyphicon glyphicon-search position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/MaritalStatuses/index', true);?>">
					Reset
					<i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
</div>
<!-- /Form horizontal -->

<!-- Bordered panel body table -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">List of Marital Status</h5>
	</div>
	<div class="panel-body">
		<?php
			$counter = $this->Paginator->counter(array('format' =>'{:start}'));
		?>
		<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="5%" class="text-center">No.</th>
						<th>Name</th>
						<th width="15%" class="text-center">Code</th>
						<th width="15%" class="text-center">Last modified</th>
						<th width="15%" class="text-center">Created At</th>
						<th width="15%" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if($details)
						{
							foreach ($details as $detail)
							{
					?>
						<tr>
							<th class="text-center">
								<?php echo $counter; ?>
							</th>
							<td>
								<?php echo $detail['MaritalStatus']['name'] ?>
							</td>
							<td class="text-center">
								<?php echo $detail['MaritalStatus']['code'] ?>
							</td>
							<td class="text-center">
								<?php echo $detail['MaritalStatus']['modified'] ?>
							</td>
							<td class="text-center">
								<?php echo $detail['MaritalStatus']['created'] ?>
							</td>
							<td class="text-center">
								<a alt="edit" href="<?php echo $this->Html->url('/MaritalStatuses/edit/'.$detail['MaritalStatus']['id']); ?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Edit details">
									<spam class="glyphicon glyphicon-pencil">
									</spam>
								</a>
							</td>
						</tr>
					<?php
							$counter++;
							}
						}
						else
						{
					?>
						<tr>
							<td colspan="3">
								No data...
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<br/>
		<p class="pull-right">
			<?php echo $this->Paginator->counter(array(
                                                    'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                            {:count} total, start {:start}, end at {:end}'
                                                    ));
			?>
		</p>
	</div>
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
                <a class="btn btn-primary" href="<?php echo $this->Html->url('/MaritalStatuses/add/', true);?>">
                    Add New Marital Status  <i class="icon-plus3 position-right"></i>
                </a>
			</span>
			<ul class="pagination  pull-right">
				<?php
					echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
					echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
				?>
			</ul>
		</div>
	</div>
</div>
<!-- /bordered panel body table -->
