<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
		<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Personal KPI</a></li>
			<li class="active">Year - End Review</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Basic tables title -->
<h6 class="content-group text-semibold">
	PART 1
	<small class="display-block">KPI PLANNING <code>Focus</code> <code>Objective</code> <code>Key Performance Indicator</code> <code>Weightage</code> <code>Performance Target</code></small>
</h6>
<!-- /basic tables title -->

<!-- Input group addons -->
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2">No</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="1">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Focus</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="SECRETARIAL">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Objective</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="To deliver service to complete">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Key Performance Indicator</label>
					<div class="col-lg-10">
						<textarea rows="5" cols="5" class="form-control" disabled="disabled">Secretarial assistance tp deputy / Division Assist to liases with all clients on presentations & meetings. Manage Calendar Deputy Chief.</textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Weightage</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="30%">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Poor (1)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="50">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Fair (2)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="70">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Target (3)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="80">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Good (4)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="90">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Excellent (5)</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="95">
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<!-- /input group addons -->

<!-- Basic tables title -->
<h6 class="content-group text-semibold">
	PART 2
	<small class="display-block">MID - YEAR REVIEW <code>Result</code> <code>Comment / Amendment</code></small>
</h6>
<!-- /basic tables title -->

<!-- Input group addons -->
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2">Result</label>
					<div class="col-lg-10">
						<textarea rows="5" cols="5" class="form-control" placeholder="Enter result" disabled="disabled">Manage schedule Deputy Chief and comply all the task</textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Comment / Amendment</label>
					<div class="col-lg-10">
						<textarea rows="5" cols="5" class="form-control" placeholder="Enter comment / amendment" disabled="disabled">Good</textarea>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<!-- /input group addons -->

<!-- Basic tables title -->
<h6 class="content-group text-semibold">
	PART 3
	<small class="display-block">YEAR END REVIEW <code>Achievement / Result</code> <code>Comment / Amendment</code> <code>Weightage</code> <code>Rating</code> <code>Total Rating</code></small>
</h6>
<!-- /basic tables title -->

<!-- Input group addons -->
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2">Achievement / Result<span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<textarea rows="5" cols="5" class="form-control" placeholder="Enter result"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Comment / Amendment<span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<textarea rows="5" cols="5" class="form-control" placeholder="Enter comment / amendment"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Weightage</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="30%">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Rating<span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select name="select" class="form-control">
							<option value="opt1">Please select</option>
							<option value="opt2">5</option>
							<option value="opt3">4</option>
							<option value="opt4">3</option>
							<option value="opt5">2</option>
							<option value="opt6">1</option>
							<option value="opt7">0</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Total Rating</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" disabled="disabled" value="0.00">
					</div>
				</div>
			</fieldset>

			<div class="text-right" style="padding-top:40px;">
				<a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/yearendreview', true);?>">
					<i class=" icon-arrow-left16 position-left"></i> Previous
				</a>
				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/kpis/addnewmidyearreview', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
				<a class="btn btn-success position-right"  href="<?php echo $this->Html->url('/kpis/yearendreview', true);?>">
					Save <i class="icon-drawer-out position-right position-right"></i>
				</a>
			</div>
		</form>
	</div>

	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->

</div>
<!-- /input group addons -->
