<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">PMS</a></li>
			<li class="#">List of PMS</li>
			<li class="#">Incumbent Data</li>
			<li class="#">Part 1 - Kpi Planning</li>
			<li class="#">Part 2 - Mid Year Review</li>
			<li class="active">Part 3 - Mid Year Review</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
		<?php

		echo $this->Session->flash();

			if(!empty($this->validationErrors['Kpi']))
				{
			?>
				<div role="alert" class="alert alert-danger">
				<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<?php
					foreach ($this->validationErrors['Kpi'] as $errors)
					{
						echo '<ul>';
						foreach ($errors as $error)
						{
							echo '<li>'.h($error).'</li>';
						}
						echo '</ul>';
					}
				?>
				</div>
			<?php
		}
		?>	

		<?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal wizard clearfix', 'novalidate'=>'novalidate'));?>
		<div class="steps clearfix">
            <ul role="tablist">
                <li role="tab" class="done" aria-disabled="false" aria-selected="true">
                    <a href="#">
                        <span class="number">1</span>
                        INCUMBENT DATA
                    </a>
                </li>
				<li role="tab" class="done" aria-disabled="true">
                    <a href="#">
                        <span class="number">2</span>
                        PART 1 : KPI PLANNING
                    </a>
                </li>
                <li role="tab" class="done" aria-disabled="true">
                    <a href="#">
                        <span class="number">3</span>
                        PART 2 : MID - YEAR REVIEW
                    </a>
                </li>
                <li role="tab" class="current" aria-disabled="true">
                    <a href="#" >
                        <span class="number">4</span>
                        PART 3 : YEAR - END REVIEW
                    </a>
                </li>
            </ul>
        </div>
			<fieldset class="content-group">
				<legend class="text-bold">SECTION 2 : COMPETENCY ASSESSMENT</legend>

				<!--START TABALE-->
				<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="80%" class="left">Behaviour</th>
						<th class="text-center">Level</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="left" >Problem Assessment   <button type="button" class="btn btn-primary btn-sm" data-popup="tooltip" title="LEVEL 1 : Reviews the current situation in contrast to the standard condition and requires assistance to recognise the abnormality.
																		Uses basic technical understanding to relate to issues at hand.
																		Rely solely on the descriptions of others to understand why the current situation exists.
																		Clusters relevant information in a generally organised manner.
																		Describes the problem and encourages others to recommend solution to the problem." data-placement="right">level 1</button>
																<button type="button" class="btn btn-primary btn-sm" data-popup="tooltip" title="LEVEL 2 : Undertakes a complex task by breaking it down into manageable parts in a systematic, detailed way.
																		Readily grasps the significance of complex issues based on sound technical knowledge.
																		Rapidly identifies key issues and inconsistencies/discrepancies in a situation and take steps to avoid recurrence.
																		Sees most of the main issues (forces, events, entities, people etc.) and makes a systematic comparisons of alternative solutions.
																		Makes simple causal links or pros-and-cons lists, analysis or decision criteria." data-placement="right">level 2</button>
																<button type="button" class="btn btn-primary btn-sm" data-popup="tooltip" title="LEVEL 3 : Identifies issues and arranges information into logical sequence and makes effective interpretations.
																		Uses technical expertise and wide experience to analyse relationship among several parts of a problem.
																		Understands the impact that the current condition/situation may have on related processes and areas.
																		Highlights significant issues and study their impact and implications within the context of overall his/her departmental objectives.
																		Weighs the costs, benefits, risks and chances for success, in recommending alternative solutions." data-placement="right">level 3</button>
						</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td style = "vertical-align:top" class="left">Continous Learning</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td style = "vertical-align:top" class="left">Work Planning & Organising</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td style = "vertical-align:top" class="left">Achievement Orientation</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td style = "vertical-align:top" class="left">Communication</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td style = "vertical-align:top" class="left">Teamwork</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td style = "vertical-align:top" class="left">Attention To Detail</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td class="left">Service Orientation   <button type="button" class="btn btn-primary btn-sm" data-popup="tooltip" title="LEVEL 1 : Seeks information about the real underlying needs of the customer, beyond those expressed initially.
																		Closely follow up on services, solutions or products to ensure that their needs have been correctly and effectively met.
																		Takes a proactive approach to resolve customer issues/problems." data-placement="right">level 1</button>
																<button type="button" class="btn btn-primary btn-sm" data-popup="tooltip" title="LEVEL 2 : Works to adapt current service offering  to meet customer expectations.
																		Analyses symptoms and get to the root cause of current service delivery problems and recommends/makes  improvements
																		Encourages co-workers and teams to achieve a high standard of service excellence." data-placement="right">level 2</button>
																<button type="button" class="btn btn-primary btn-sm" data-popup="tooltip" title="LEVEl 3 : Tracks trends and developments that will affect own organizationÃ‚â€™s ability to meet current and future customer needs.
																		Redefines service standards and orchestrates the necessary enablers (resource, processes, systems) to meet those higher standards.
																		Emphasizes and drives a culture of service excellence. " data-placement="right">level 3</button>
						</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Level</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</td>
					</tr>

					<tr>
						<td class="text-right">Competency Score Obtained</td>
						<td class="text-center">
												<?php
													echo $this->Form->input('CompetencyScoreObtained', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'total all level',
														)
													);
												?>
						</td>
					</tr>

					<tr>
						<td class="text-right">Final Competency Rating</td>
						<td class="text-center">
												<?php
													echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'(CompetencyScoreObtained / total competency score (24) ) * 5',
														)
													);
												?>
						</td>
					</tr>

			</tbody>
			</table>

			</fieldset>

			<fieldset class="content-group">
				<legend class="text-bold">SECTION 3 : Merit</legend>

				<!--START TABALE-->
				<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="80%" class="text-center">List of Archivement<br>(Please Refer To The MERIT Guidelines)<br>1 Score Per Participant (Max Total Score = 10)</th>
						<th class="text-center">Total Score</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php
								echo $this->Form->input('CompetencyScoreObtained', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'placeholder'=>'List of Archivement',
									)
								);
							?>
						</td>
						<td class="text-center">
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Score</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</td>
					</tr>

					<tr>
						<td>
							<?php
								echo $this->Form->input('CompetencyScoreObtained', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'placeholder'=>'List of Archivement',
									)
								);
							?>
						</td>
						<td>
							<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Score</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</td>
					</tr>

					<tr>
					<td>
						<?php
							echo $this->Form->input('CompetencyScoreObtained', array(
								'class'=>'form-control',
								'label'=> false,
								'error'=>false,
								'type'=>'text',
								'placeholder'=>'List of Archivement',
								)
							);
						?>
						</td>
						<td class="text-center">
						<select name="focus" data-placeholder="Select Level" class="form-control">
								<option value="">Select Score</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="text-right">Merit Score Obtained</td>
						<td class="text-center">
												<?php
													echo $this->Form->input('MeritScoreObtained', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'total all Score',
														)
													);
												?>
						</td>
					</tr>

					<tr>
						<td class="text-right">Final Merit Score</td>
						<td class="text-center">
												<?php
													echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'MeritScoreObtained / Final Merit Score',
														)
													);
												?>
						</td>
					</tr>

			</tbody>
			</table>

			</fieldset>

			<fieldset class="content-group">
				<legend class="text-bold">OVERALL PERFORMANCE ASSESSMENT</legend>

				<!--START TABALE-->
				<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<tbody>
					<tr>
						<td colspan = '1' class="text-left">Final KPI Rating</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'Final KPI Rating',
														)
													);
												?>
						</td>
						<td colspan = '1' class="text-left">X Weightage (%)</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
						<td colspan = '1' class="text-left">=</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
					</tr>
					<tr>
						<td colspan = '1' class="text-left">Final Competency Rating</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'Final Competency Rating',
														)
													);
												?>
						</td>
						<td colspan = '1' class="text-left">X Weightage (%)</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
						<td colspan = '1' class="text-left">=</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
					</tr>

					<tr>
						<td colspan = '5' class="text-left"></td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
					</tr>
					<tr>
						<td colspan = '1' class="text-left">Merit</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'Merit',
														)
													);
												?>
						</td>
						<td colspan = '1' class="text-left">X Weightage (%)</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
						<td colspan = '1' class="text-left">=</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
					</tr>
					<tr>
						<td colspan = '5' class="text-left"></td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'in Percent',
														)
													);
												?>
						</td>
					</tr>
					<tr>
						<td colspan = '5' class="text-right">Overall Performance Rating</td>
						<td colspan = '1' class="text-left">
												<?php
														echo $this->Form->input('FinalCompetencyRating', array(
														'class'=>'form-control',
														'label'=> false,
														'error'=>false,
														'type'=>'text',
														'placeholder'=>'Overall Performance Rating',
														)
													);
												?>
						</td>
					</tr>

			</tbody>
			</table>
			<br>
			<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-4">Appraisee Comments<span class="text-danger">*</span></label>
			        	<textarea rows="5" cols="5" placeholder="enter Appraisee Comments here" class="form-control"></textarea>
					</div>
			</div>
			<br>
			<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-4">Appraiser Comments<span class="text-danger">*</span></label>
			        	<textarea rows="5" cols="5" placeholder="enter Appraiser Comments here" class="form-control"></textarea>
					</div>
			</div>
			<br>
			<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-4">Reviewer Comments<span class="text-danger">*</span></label>
			        	<textarea rows="5" cols="5" placeholder="enter Reviewer Comments here" class="form-control"></textarea>
					</div>
			</div>
			</fieldset>

            <div class="text-right">
			<a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/yearendreview', true);?>">
			<i class="icon-arrow-left16 position-left"></i>Previous
				</a>
				<a class="btn btn-warning position-right" href="#" data-toggle="modal" data-target="#modal_submit">
					Submit <i class="icon-drawer-out position-right"></i>
			</a>										
				<!--<a class="btn btn-success position-right" href="<?php echo $this->Html->url('/kpis/yearendreview', true);?>">
					Next <i class="icon-arrow-right16 position-right"></i>
			</a>-->

			</div>

        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- Warning modal -->
<div id="modal_submit" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-drawer-out"></span> Submit?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Confirm</h6>
				<p>Please make sure this information is correct. Once it is submitted it will be not editable.test</p>
			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-warning" >
				Yes
				</button>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->
