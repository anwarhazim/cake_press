<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
		<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Personal KPI</a></li>
			<li class="active">Year - End Review</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">

	<div class="panel-body">

		<form class="form-horizontal wizard clearfix" action="#">
			<div class="steps clearfix">
				<ul role="tablist">
					<li role="tab" class="done" aria-disabled="false" aria-selected="true">
						<a href="#">
							<span class="number">1</span>
							INCUMBENT DATA
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#">
							<span class="number">2</span>
							KPI PLANNING
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#">
							<span class="number">3</span>
							MID - YEAR REVIEW
						</a>
					</li>
					<li role="tab" class="current" aria-disabled="true">
						<a href="#" >
							<span class="number">4</span>
							YEAR - END REVIEW
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#" >
							<span class="number">5</span>
							COMPETENCY
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#" >
							<span class="number">6</span>
							MERIT
						</a>
					</li>
				</ul>
			</div>

			<fieldset class="content-group">
				<legend class="text-bold">&nbsp;</legend>
			</fieldset>

			<div class="table-responsive">
				<table class="table table-bordered table-framed table-striped">
					<thead>
						<tr>
							<th colspan = '10' class="text-center"><label><strong>PART 1 : KPI PLANNING</strong></label></th>
							<th colspan = '2' class="text-center"><label><strong>PART 2 : MID YEAR REVIEW</strong></label></th>
							<th colspan = '5' class="text-center"><label><strong>PART 3 : YEAR END REVIEW</strong></label></th>
							<th rowspan = '4' class="text-center"><label style="width:80px;"><strong>ACTION</strong></label></th>
						</tr>
						<tr>
							<th rowspan = '2' class="text-center"><label><strong>NO</strong></label></th>
							<th rowspan = '2' class="text-center"><label style="width:100px;"><strong>FOCUS</strong></label></th>
							<th rowspan = '2' class="text-center"><label style="width:250px;"><strong>OBJECTIVE</strong></label></th>
							<th rowspan = '2' class="text-center"><label style="width:400px;"><strong>KEY PERFORMANCE INDICATOR</strong></label></th>
							<th rowspan = '2' class="text-center"><label style="width:100px;"><strong>WEIGHTAGE (%)</strong></label></th>
							<th colspan = '5' class="text-center"><label><strong>PERFORMANCE TARGET</strong></label></th>
							<th rowspan = '2' class="text-center"><label style="width:400px;"><strong>RESULT</strong></label></th>
							<th rowspan = '2' class="text-center"><label style="width:400px;"><strong>COMMENT/ AMENDMENT</strong></label></th>
							<th rowspan = '4' class="text-center"><label style="width:400px;"><strong>ACHIEVEMENT / RESULT</strong></label></th>
							<th rowspan = '4' class="text-center"><label style="width:400px;"><strong>COMMENT / AMENDMENT</strong></label></th>
							<th rowspan = '4' class="text-center"><label style="width:110px;"><strong>WEIGHTAGE (%)</strong></label></th>
							<th rowspan = '4' class="text-center"><label style="width:110px;"><strong>RATING</strong></label></th>
							<th rowspan = '4' class="text-center"><label style="width:110px;"><strong>TOTAL RATING</strong></label></th>
						</tr>
						<tr>
							<th colspan = '1' class="text-center"><label style="width:100px;"><strong>POOR (1)</strong></label></th>
							<th colspan = '1' class="text-center"><label style="width:100px;"><strong>FAIR (2)</strong></label></th>
							<th colspan = '1' class="text-center"><label style="width:100px;"><strong>TARGET (3)</strong></label></th>
							<th colspan = '1' class="text-center"><label style="width:100px;"><strong>GOOD (4)</strong></label></th>
							<th colspan = '1' class="text-center"><label style="width:100px;"><strong>EXCELLENT (5)</strong></label></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">1</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">SECRETARIAL</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Secretarial assistance tp deputy / Division Assist to liases with all clients on presentations & meetings. Manage Calendar Deputy Chief.</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">30</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Manage schedule Deputy Chief and comply all the task</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">30</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">2</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">PEOPLE</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Identified and proposed solution for issue related to clerical works at CSHE division Liase with all staffs on office matters. Displayed good behavior and able to plan a tasks within the given timeline.</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">To make sure all the task given by staff not delayed and be completed.</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">3</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Documentation</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Administration works. Compile all the related document for Deputy review and signature. Distribute all related dic to PIC. Organize record incoming & outgoing correspondence / email.</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">To make sure all the process running smoothly and do not delayed.</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">4</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Development and Operational Excellent</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Submit monthly summary Claims for the division to HR within the time required.</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">10</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">Not following day</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Maintain timeline required.</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">10</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Monitoring</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Summary staff attendance and staff movement</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Weekly report</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">6</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">INNOVATION & LEARNING</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">% of Staff Meet 32 hrs Training</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Employee Development (Staff attended 32hrs training)</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">100</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Achieve target</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">7</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">ENVIRONMENT & QUALITY</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">5S Program (Audit Rating)</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Rating</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (score 60% and below)</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (min score 61%)</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 71% score)</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 81% score)</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">3 Star Rating (min 91% score)</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Ongoing</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td style = "vertical-align:top" colspan ='1' class="text-center">8</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Arrangement of Meeting</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">To arrange meeting internal / external parties</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">Smoothly on process</td>
							<td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center"></td>
							<td style = "vertical-align:top" colspan ='1' class="text-center">
								<a href="<?php echo $this->Html->url('/kpis/addnewendyearreview');?>" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
							</td>
						</tr>
						<tr>
							<td colspan="4" rowspan="1" style="vertical-align: top;" class="text-right"><strong>TOTAL WEIGHTAGE</strong></td>
							<td style="vertical-align: top;" class="text-center"><strong>100</strong></td>
							<td colspan="9" rowspan="1" style="vertical-align: top;" class="text-right"><strong>TOTAL WEIGHTAGE</strong></td>
							<td colspan="1" rowspan="1" style="vertical-align: top;" class="text-center"><strong>100</strong></td>
							<td colspan="1" rowspan="1" style="vertical-align: top;" class="text-right"><strong>FINAL KPI RATING</strong></td>
							<td colspan="1" rowspan="1" style="vertical-align: top;" class="text-center"><label><strong>0.00</strong></label></td>
							<td colspan="1" rowspan="1" style="vertical-align: top;" class="text-center">&nbsp;</td>
						</tr>
						<tr>
							<td colspan ='18'>No Data</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="text-right" style="padding-top:40px;">
				<a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/midyearreview', true);?>">
					<i class="icon-arrow-left16 position-left"></i>Previous
				</a>
				<a class="btn btn-success position-right" href="<?php echo $this->Html->url('/kpis/competencyassessment', true);?>">
					Next <i class="icon-arrow-right16 position-right"></i>
				</a>
			</div>

			<!-- Warning modal -->
			<div id="modal_submit" class="modal fade" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header bg-warning">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h6 class="modal-title"><span class="icon-drawer-out"></span> Submit?</h6>
						</div>

						<div class="modal-body">
							<h6 class="text-semibold">Confirm</h6>
							<p>Please make sure this information is correct. Once it is submitted it will be not editable</p>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-warning">Yes</button>
							<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /warning modal -->

		</form>
	</div>

	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->

</div>
<!-- /form horizontal -->