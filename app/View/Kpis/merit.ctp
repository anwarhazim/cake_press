<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
		<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Personal KPI</a></li>
			<li class="active">Merit</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">

	<div class="panel-body">

		<form class="form-horizontal wizard clearfix" action="#">
			<div class="steps clearfix">
				<ul role="tablist">
					<li role="tab" class="done" aria-disabled="false" aria-selected="true">
						<a href="#">
							<span class="number">1</span>
							INCUMBENT DATA
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#">
							<span class="number">2</span>
							KPI PLANNING
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#">
							<span class="number">3</span>
							MID - YEAR REVIEW
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#" >
							<span class="number">4</span>
							YEAR - END REVIEW
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#" >
							<span class="number">5</span>
							COMPETENCY
						</a>
					</li>
					<li role="tab" class="current" aria-disabled="true">
						<a href="#" >
							<span class="number">6</span>
							MERIT
						</a>
					</li>
				</ul>
			</div>

			<fieldset class="content-group">
				<legend class="text-bold">&nbsp;</legend>
			</fieldset>

			<div class="table-responsive">
                <table class="table table-bordered table-framed table-striped">
					<thead>
						<tr>
                            <th width="80%" class="text-center">List of Achievements<br>(Please refer to the Merit Guidelines)<br>1 score per participants (Max total score = 10)</th>
							<th class="text-center">Total Score</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<?php
									echo $this->Form->input('CompetencyScoreObtained', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'placeholder'=>'List of achievements',
										)
									);
								?>
							</td>
							<td class="text-center">
                                <select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Score</option>
									<option value="10">10</option>
									<option value="9">9</option>
									<option value="8">8</option>
									<option value="7">7</option>
									<option value="6">6</option>
									<option value="5">5</option>
									<option value="4">4</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<?php
									echo $this->Form->input('CompetencyScoreObtained', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'placeholder'=>'List of achievements',
										)
									);
								?>
							</td>
							<td class="text-center">
                                <select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Score</option>
									<option value="10">10</option>
									<option value="9">9</option>
									<option value="8">8</option>
									<option value="7">7</option>
									<option value="6">6</option>
									<option value="5">5</option>
									<option value="4">4</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
						    <td>
                                <?php
                                    echo $this->Form->input('CompetencyScoreObtained', array(
                                        'class'=>'form-control',
                                        'label'=> false,
                                        'error'=>false,
                                        'type'=>'text',
                                        'placeholder'=>'List of achievements',
                                        )
                                    );
                                ?>
							</td>
							<td class="text-center">
							    <select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Score</option>
									<option value="10">10</option>
									<option value="9">9</option>
									<option value="8">8</option>
									<option value="7">7</option>
									<option value="6">6</option>
									<option value="5">5</option>
									<option value="4">4</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="text-right">Merit Score Obtained</td>
							<td class="text-center"><strong>0</strong></td>
						</tr>
						<tr>
							<td class="text-right"><i class="icon-info22 text-danger-600" data-toggle="modal" data-target="#modal_theme_bg_custom" style="cursor: pointer;"></i> Final Merit Score</td>
							<td class="text-center"><strong>0.00</strong></td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- Warning modal -->
			<div id="modal_submit" class="modal fade" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header bg-warning">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h6 class="modal-title"><span class="icon-drawer-out"></span> Submit?</h6>
						</div>

						<div class="modal-body">
							<h6 class="text-semibold">Confirm</h6>
							<p>Please make sure this information is correct. Once it is submitted it will be not editable</p>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-warning">Yes</button>
							<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /warning modal -->

			<!-- Custom background color -->
			<div id="modal_theme_bg_custom" class="modal fade" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content bg-teal-300">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h5 class="modal-title">Final Merit Score</h5>
						</div>

						<div class="modal-body">
							<table style="width:100%;border:1px solid #fff;" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Merit Score Obtained<var></td>
										<td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>m<var></td>
										<td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>=<var></td>
										<td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>t<var></td>
									</tr>
									<tr>
										<td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Total Merit Score<var></td>
										<td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>10<var><br>
										</td>
									</tr>
								</tbody>
							</table>
							<br>
							<small>
								<var>Legend:<var>
								<p>
									<var>m : Merit Score Obtained<var>
									<br>
									<var>t : Total Merit Score<var>
								</p>
							</small>
						</div>
						<hr>
						<div class="modal-footer">
							<button type="button" class="btn btn-link text-white" data-dismiss="modal">Close <i class="icon-cross"></i></button>
						</div>
					</div>
				</div>
			</div>
			<!-- /custom background color -->

		</form>
	</div>
</div>
<!-- /form horizontal -->

<!-- Basic tables title -->
<h6 class="content-group text-semibold">
    OVERALL PERFORMANCE ASSESSMENT
	<small class="display-block">OVERALL PERFORMANCE RATING <code>Result</code> <code>Comment</code></small>
</h6>
<!-- /basic tables title -->

<!-- Input group addons -->
<div class="panel panel-flat">
	<div class="panel-body">
        <div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<tbody>
					<tr>
						<td colspan = '1' class="text-left">Final KPI Rating</td>
						<td colspan = '1' class="text-center">0.00</td>
						<td colspan = '1' class="text-center"><var>X<var>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weightage (%)</td>
						<td colspan = '1' class="text-center">70%</td>
						<td colspan = '1' class="text-center">=</td>
						<td colspan = '1' class="text-center">0.00</td>
					</tr>
					<tr>
						<td colspan = '1' class="text-left">Final Competency Rating</td>
						<td colspan = '1' class="text-center">0.00</td>
						<td colspan = '1' class="text-center"><var>X<var>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weightage (%)</td>
						<td colspan = '1' class="text-center">30%</td>
						<td colspan = '1' class="text-center">=</td>
						<td colspan = '1' class="text-center">0.00</td>
					</tr>
					<tr>
						<td colspan = '5' class="text-left">&nbsp;</td>
						<td colspan = '1' class="text-center">0.00</td>
					</tr>
					<tr>
						<td colspan = '1' class="text-left">Merit</td>
						<td colspan = '1' class="text-center">0.00</td>
						<td colspan = '1' class="text-center"><var>X<var>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weightage (%)</td>
						<td colspan = '1' class="text-center">10%</td>
						<td colspan = '1' class="text-center">=</td>
						<td colspan = '1' class="text-center">0.00</td>
					</tr>
					<tr>
						<td colspan = '5' class="text-left">&nbsp;</td>
						<td colspan = '1' class="text-center">0.00</td>
					</tr>
					<tr>
						<td colspan = '5' class="text-right">Overall Performance Rating</td>
						<td colspan = '1' class="text-center"><strong>1</strong></td>
					</tr>
			    </tbody>
			</table>
        </div>

		<div class="text-right" style="padding-top:40px;">
			<a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/yearendreview', true);?>">
				<i class="icon-arrow-left16 position-left"></i>Previous
			</a>
			<a class="btn btn-success position-right" href="<?php echo $this->Html->url('/kpis/overview', true);?>">
				Next <i class="icon-arrow-right16 position-right"></i>
			</a>
		</div>

	</div>

	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->

</div>
<!-- /input group addons -->