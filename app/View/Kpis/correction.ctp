<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Personal KPI</a></li>
			<li>Incumbent Data</li>
			<li class="active">Correction</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">

	<div class="panel-body">
		<?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal wizard clearfix', 'novalidate'=>'novalidate'));?>
		
			<fieldset class="content-group">
				<legend class="text-bold">Please Choose For Correction</legend>

				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox">
							<label><input type="checkbox" class="styled">Joining Date</label>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox">
							<label><input type="checkbox" class="styled">Position</label>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox">
							<label><input type="checkbox" class="styled">Division</label>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox">
							<label><input type="checkbox" class="styled">Department</label>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox">
							<label><input type="checkbox" class="styled">Section</label>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox">
							<label><input type="checkbox" class="styled">Unit</label>
						</div>
					</div>
				</div>
			</fieldset>

			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2">Remarks</label>
					<div class="col-lg-10">
						<textarea rows="5" cols="5" class="form-control" placeholder="Enter your remarks"></textarea>
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/incumbentdata', true);?>"">
					<i class="icon-arrow-left16 position-left"></i>Previous
				</a>
				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/kpis/correction', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
				<a class="btn btn-success position-right" href="#" data-toggle="modal" data-target="#modal_submit">
					Submit <i class="icon-drawer-out position-right"></i>
				</a>
			</div>

			<!-- Warning modal -->
			<div id="modal_submit" class="modal fade" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header bg-warning">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h6 class="modal-title"><span class="icon-drawer-out"></span> Submit?</h6>
						</div>

						<div class="modal-body">
							<h6 class="text-semibold">Confirm</h6>
							<p>Please make sure this information is correct. Once it is submitted it will be not editable.</p>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-warning" >
							Yes
							</button>
							<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /warning modal -->
			
		<?php echo $this->Form->end(); ?>
	</div>

	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->

</div>
<!-- /form horizontal -->
