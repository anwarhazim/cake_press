<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
		<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Personal KPI</a></li>
			<li class="active">Declaration</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Declaration</h5>
    </div>

    <div class="panel-body">
        <p class="content-group-lg">
            I, <code><var><strong>Mohd Nizar Bin Abd Azik</strong><var></code> with Staff No. <code><var><strong>10010630</strong><var></code> hereby declare the information regarding the Key Performance Indicator (KPI) and results or achievements given by me in this return form is true, correct and complete.
        </p>

        <p class="content-group-lg text-danger">
            <small><i>You are wholly responsible for the return furnished or information declared. Penalty will be imposed for incorrect return or incorrect information given.</i></small>
        </p>

        <p class="content-group-lg">
            <small><i>Date : 02/09/2019</i></small>
        </p>

        <form class="form-horizontal" action="#">
            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2">Appraisee Comments</label>
                    <div class="col-lg-10">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter appraisee comments" style="text-align:justify;"></textarea>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/overview', true);?>">
					<i class="icon-arrow-left16 position-left"></i>Previous
				</a>
                <a class="btn btn-warning position-right" href="<?php echo $this->Html->url('/kpis/correction', true);?>">
					Print Form Draft <i class="icon-printer position-right"></i>
				</a>
				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/kpis/declaration', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
                <button type="button" class="btn btn-success position-right" data-toggle="modal" data-target="#modal_theme_custom">Sign and Submit <i class=" icon-pen position-right"></i></button>
			</div>

            <!-- Custom header color -->
            <div id="modal_theme_custom" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-brown">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h6 class="modal-title">Sign</h6>
                        </div>

                        <div class="modal-body">
                            <p>Please key in Staff No and Password</p>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Staff No</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Enter staff no">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Password</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Enter password">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Cancel <i class="icon-cross"></i></button>
                            <button type="button" class="btn bg-brown">Sign <i class="icon-pen position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /custom header color -->

        </form>
    </div>

    <!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
    
</div>
<!-- /form horizontal -->