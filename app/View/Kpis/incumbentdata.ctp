<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Personal KPI</a></li>
			<li class="active">Incumbent Data</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">

		<?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal wizard clearfix', 'novalidate'=>'novalidate'));?>
			<div class="steps clearfix">
				<ul role="tablist">
					<li role="tab" class="current" aria-disabled="false" aria-selected="true">
						<a href="#">
							<span class="number">1</span>
							INCUMBENT DATA
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#">
							<span class="number">2</span>
							KPI PLANNING
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#">
							<span class="number">3</span>
							MID - YEAR REVIEW
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#" >
							<span class="number">4</span>
							YEAR - END REVIEW
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#" >
							<span class="number">5</span>
							COMPETENCY
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#" >
							<span class="number">6</span>
							MERIT
						</a>
					</li>
				</ul>
			</div>
			<fieldset class="content-group">
				<legend class="text-bold">&nbsp;</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Year<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('year', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> '2019',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Period Start Date<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('period_start_date', array(
									'class'	=>'form-control date',
									'label'	=> false,
									'error'	=>false,
									'type'	=>'text'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Period End Date<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('period_end_date', array(
									'class'	=>'form-control date',
									'label'	=> false,
									'error'	=>false,
									'type'	=>'text'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Joining Date<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('joining_date', array(
									'class'	=>'form-control date',
									'label'	=> false,
									'error'	=>false,
									'type'	=>'text'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Position<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('position', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> 'Assistant Vice President',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Staff No<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('staff_no', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> '10010630',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Division<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('division', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> 'Digital & Technology',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Department<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('department', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'value'		=> 'Technology Innovation',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Section<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('section', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> 'System Development (Digitisation)',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Unit<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('unit', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> '',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Sub Unit<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('subunit', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> '',
									'disabled'	=> 'disabled'
									)
								);
							?>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Supervisor<span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('subunit', array(
									'class'		=>'form-control',
									'label'		=> false,
									'error'		=>false,
									'type'		=>'text',
									'value'		=> '',
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<a class="btn btn-warning position-right" href="<?php echo $this->Html->url('/kpis/correction', true);?>">
					Correction <i class="icon-eraser position-right"></i>
				</a>
				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/kpis/incumbentdata', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
				<a class="btn btn-success position-right" href="<?php echo $this->Html->url('/kpis/kpiplanning', true);?>">
					Next <i class=" icon-arrow-right16 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->

	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->

</div>
<!-- /Form horizontal -->
