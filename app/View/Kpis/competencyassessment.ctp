<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
		<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Personal KPI</a></li>
			<li class="active">Competency</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">

	<div class="panel-body">

		<form class="form-horizontal wizard clearfix" action="#">
			<div class="steps clearfix">
				<ul role="tablist">
					<li role="tab" class="done" aria-disabled="false" aria-selected="true">
						<a href="#">
							<span class="number">1</span>
							INCUMBENT DATA
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#">
							<span class="number">2</span>
							KPI PLANNING
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#">
							<span class="number">3</span>
							MID - YEAR REVIEW
						</a>
					</li>
					<li role="tab" class="done" aria-disabled="true">
						<a href="#" >
							<span class="number">4</span>
							YEAR - END REVIEW
						</a>
					</li>
					<li role="tab" class="current" aria-disabled="true">
						<a href="#" >
							<span class="number">5</span>
							COMPETENCY
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a href="#" >
							<span class="number">6</span>
							MERIT
						</a>
					</li>
				</ul>
			</div>

			<fieldset class="content-group">
				<legend class="text-bold">&nbsp;</legend>
			</fieldset>

			<div class="table-responsive">
				<table class="table table-bordered table-framed table-striped">
					<thead>
						<tr>
							<th class="left"><label style="width:400px;"><strong>Behaviour</strong></label></th>
							<th class="text-center"><label style="width:110px;"><strong>Behaviour Details</strong></label></th>
							<th class="text-center"><label style="width:100px;"><strong>Level</strong></label></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="left" >Problem Assessment</td>
							<td class="text-center">
								<span class="label label-danger" data-toggle="modal" data-target="#modal_small" style="cursor: pointer;">Level 1</span>
								<span class="label label-success" data-toggle="modal" data-target="#modal_small" style="cursor: pointer;">Level 2</span>
								<span class="label label-primary" data-toggle="modal" data-target="#modal_small" style="cursor: pointer;">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Continous Learning</td>
							<td class="text-center">
								<span class="label label-danger">Level 1</span>
								<span class="label label-success">Level 2</span>
								<span class="label label-primary">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Work Planning & Organising</td>
							<td class="text-center">
								<span class="label label-danger">Level 1</span>
								<span class="label label-success">Level 2</span>
								<span class="label label-primary">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Achievement Orientation</td>
							<td class="text-center">
								<span class="label label-danger">Level 1</span>
								<span class="label label-success">Level 2</span>
								<span class="label label-primary">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Communication</td>
							<td class="text-center">
								<span class="label label-danger">Level 1</span>
								<span class="label label-success">Level 2</span>
								<span class="label label-primary">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Teamwork</td>
							<td class="text-center">
								<span class="label label-danger">Level 1</span>
								<span class="label label-success">Level 2</span>
								<span class="label label-primary">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Attention To Detail</td>
							<td class="text-center">
								<span class="label label-danger">Level 1</span>
								<span class="label label-success">Level 2</span>
								<span class="label label-primary">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Service Orientation</td>
							<td class="text-center">
								<span class="label label-danger">Level 1</span>
								<span class="label label-success">Level 2</span>
								<span class="label label-primary">Level 3</span>
							</td>
							<td class="text-center">
								<select name="focus" data-placeholder="Select Level" class="form-control">
									<option value="">Select Level</option>
									<option value="3">3</option>
									<option value="2">2</option>
									<option value="1">1</option>
									<option value="0">0</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="text-right">Competency Score Obtained</td>
							<td class="text-center"><label style="width:100px;"><strong>0</strong></label></td>
						</tr>
						<tr>
							<td colspan="2" class="text-right"><i class="icon-info22 text-danger-600" data-toggle="modal" data-target="#modal_theme_bg_custom" style="cursor: pointer;"></i> Final Competency Rating</td>
							<td class="text-center"><label style="width:100px;"><strong>0.00</strong></label></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="text-right" style="padding-top:40px;">
				<a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/yearendreview', true);?>">
					<i class="icon-arrow-left16 position-left"></i>Previous
				</a>
				<a class="btn btn-success position-right" href="<?php echo $this->Html->url('/kpis/merit', true);?>">
					Next <i class="icon-arrow-right16 position-right"></i>
				</a>
			</div>

			<!-- Warning modal -->
			<div id="modal_submit" class="modal fade" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header bg-warning">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h6 class="modal-title"><span class="icon-drawer-out"></span> Submit?</h6>
						</div>

						<div class="modal-body">
							<h6 class="text-semibold">Confirm</h6>
							<p>Please make sure this information is correct. Once it is submitted it will be not editable</p>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-warning">Yes</button>
							<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /warning modal -->

			<!-- Small modal -->
			<div id="modal_small" class="modal fade" tabindex="-1">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header bg-danger">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h5 class="modal-title">Level 1</h5>
						</div>

						<div class="modal-body">
							<h6 class="text-semibold"><i class="icon-book"></i> &nbsp;Behaviour Details</h6>
							<p style="text-align: justify;">
								Reviews the current situation in contrast to the standard condition and requires assistance to recognise the abnormality.
								Uses basic technical understanding to relate to issues at hand.
								Rely solely on the descriptions of others to understand why the current situation exists.
								Clusters relevant information in a generally organised manner.
								Describes the problem and encourages others to recommend solution to the problem.
							</p>
						</div>

						<hr>

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /small modal -->

			<!-- Custom background color -->
			<div id="modal_theme_bg_custom" class="modal fade" tabindex="-1">
				<div class="modal-dialog">
					<div class="modal-content bg-teal-300">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h5 class="modal-title">Final Competency Rating</h5>
						</div>

						<div class="modal-body">
							<table style="width:100%;border:1px solid #fff;" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Competency Score Obtained<var></td>
										<td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>c<var></td>
										<td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>X</var></td>
										<td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>5<var></td>
										<td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>=<var></td>
										<td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>t<var></td>
									</tr>
									<tr>
										<td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Total Competency Score<var></td>
										<td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>24<var><br>
										</td>
									</tr>
								</tbody>
							</table>
							<br>
							<small>
								<var>Legend:<var>
								<p>
									<var>c : Competency Score Obtained<var>
									<br>
									<var>t : Total Competency Score<var>
								</p>
							</small>
						</div>
						<hr>
						<div class="modal-footer">
							<button type="button" class="btn btn-link text-white" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /custom background color -->

		</form>
	</div>

	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->

</div>
<!-- /form horizontal -->
