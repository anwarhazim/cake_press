<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Hris</li>
			<li>View Request Update</li>
			<li class="active">Request Update Details</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Detailed task -->
<div class="row">
	<div class="col-lg-9">
		<!-- Task overview -->
		<div class="panel panel-flat">
			<div class="panel-heading mt-5">
				<h5 class="panel-title">#<?php echo $detail['Applicant']['reference_no']; ?>: <?php echo $detail['Modul']['name']; ?> from <?php echo $detail['CreatedBy']['name']; ?></h5>
				<div class="heading-elements">
					<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"><?php echo $detail['Status']['name']; ?> <b><i class="icon-alarm-check"></i></b></a>
				</div>
			</div>

			<div class="panel-body">
			<?php 
				echo $this->Session->flash(); 

				if(!empty($this->validationErrors['Applicant']))
				{
				?>
					<div role="alert" class="alert alert-danger">
							<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<?php
								foreach ($this->validationErrors['Applicant'] as $errors) 
								{
									echo '<ul>';
									foreach ($errors as $error) 
									{
										echo '<li>'.h($error).'</li>';
									}
									echo '</ul>';
								}
							?>
					</div>
				<?php
				}

				$alert_label = "alert-info";
				$text_label = "NOTE";
				if($detail['Applicant']['status_id'] == 5)
				{
					$alert_label = "alert-danger";
					$text_label = "CORRECTION";
				}

				if($detail['Applicant']['status_id'] >= 5)
				{
				?>
					<div role="alert" class="alert <?php echo $alert_label ?>">
							<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<?php
								echo "<p><u>".$text_label."</u></p>"; 
								echo nl2br($detail['Applicant']['note']); 
							?>
					</div>
				<?php
				}

				switch ($detail['Applicant']['modul_id']) 
				{
					case 4:
			?>
						<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
							<fieldset class="content-group">
								<legend class="text-bold">Personal Information</legend>
								<?php 
									echo $this->Form->input('id', array(
										'type'=>'hidden',
										'label'=> false,
										)
									); 
								?>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Name <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('name', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Staff No.</label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('staff_no', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>'disabled'
													)
												);
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">IC No. <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('ic_new', array(
													'class'=>'form-control',
													'maxlength'=>12,
													'label'=> false,
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Passport No.</label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('passport_no', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Gender <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('gender_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$genders,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Nationality <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('national_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$nationals,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Date of Birth <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('date_of_birth', array(
													'class'=>'form-control date',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Place of Birth <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('place_birth_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$states,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">Race <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('race_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$races,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">Religion <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('religion_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$religions,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">Marital Status <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php
												echo $this->Form->input('marital_status_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$maritalstatus,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												);
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<legend class="text-bold">Contact Information</legend>
								<div class="form-group">
									<label class="control-label col-lg-12">Address <span class="text-danger">*</span></label>
									<div class="col-lg-12">
										<?php 
											echo $this->Form->input('current_address_1', array(
												'class'=>'form-control',
												'label'=> false,
												'disabled'=>$disabled
												)
											); 
										?>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="form-group">
									<div class="col-lg-12">
										<?php 
											echo $this->Form->input('current_address_2', array(
												'class'=>'form-control',
												'label'=> false,
												'disabled'=>$disabled
												)
											); 
										?>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="form-group">
									<div class="col-lg-12">
										<?php 
											echo $this->Form->input('current_address_3', array(
												'class'=>'form-control',
												'label'=> false,
												'disabled'=>$disabled
												)
											); 
										?>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Postcode <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('current_postcode', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">State <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('current_state_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$states,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">No. Tel. (Home)</label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('home_no', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">No. Tel. (HP)</label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('mobile_no', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">Email <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('email', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<?php
								if(empty($disabled))
								{
							?>
							<div class="text-right">
								<?php
									echo $this->Form->button('Save', array(
										'type'=>'submit',
										'value'=>'staff',
										'name'=>'Submit',
										'class'=>'btn btn-success legitRipple',
										'escape'=>true
									));
								?>

								<a class="btn btn-default legitRipple" href="<?php echo $this->Html->url('/Applicants/details/'.$detail['Applicant']['id'], true);?>">
									Reset
								</a>
							</div>
							<?php
								}
							?>
						<?php echo $this->Form->end(); ?>
			<?php
						break;

					case 8:
			?>
						<?php echo $this->Form->create('Education', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
							<fieldset class="content-group">
								<legend class="text-bold">Education</legend>
								<?php 
									echo $this->Form->input('id', array(
										'type'=>'hidden',
										'label'=> false,
										)
									); 
								?>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Date Started <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('duration_from', array(
													'class'=>'form-control start_date',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Date Completed <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('duration_to', array(
													'class'=>'form-control end_date', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-lg-2">School / <span class="text-danger">*</span><br/>Institution Name</label>
										<div class="col-lg-10">
											<?php 
												echo $this->Form->input('institution', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Qualification <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('qualification_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$qualifications,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Result <span class="text-danger">*</span><br/>(CGPA/Grade/Etc.) </label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('result', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<label class="control-label col-lg-12">Attachment <span class="text-danger">*</span></label>
								<div class="col-md-12">
									<div class="row">
										<?php
											for ($i=0; $i < count($this->request->data['Attachment']) ; $i++) 
											{ 
										?>
											<div class="col-lg-2 col-sm-6">
												<div class="thumbnail">
													<div class="thumb">
														<img src="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" />
														<div class="caption-overflow">
															<span>
																<a href="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View" ><i class="icon-eye"></i></a>
																<?php
																	if(empty($disabled))
																	{
																		echo $this->html->link(
																			'<spam class="icon-bin"></spam>', '/Applicants/attachment/'.$this->request->data['Attachment'][$i]['id'].'/'.$key,
																			array('class' => 'btn border-white text-white btn-flat btn-icon btn-rounded', 'title' => 'Delete', 'confirm' => 'Are you sure you want to delete this attachment?', 'escape' => false)
																		);
																	}
																?>
															</span>
														</div>
													</div>
				
													<div class="caption">
														<h6 class="no-margin"><?php echo $this->request->data['Attachment'][$i]['name'] ?></h6>
													</div>
												</div>
											</div>
										<?php
											}							
										?>
									</div>
								</div>
								<?php
									if(empty($disabled))
									{
								?>
								<div class="col-md-12">
									<?php 
										echo $this->Form->input('attachments.', array(
											'class'=>'form-control file-input',
											'label'=> false,
											'error'=>false,
											'type'=>'file',
											'multiple'=>'multiple',
											'data-preview-file-type'=>'any',
											'data-show-upload'=>false,
											'showRemove'=>false,
											'data-show-caption'=>false,
											'accept'=>'image/*',
											'maxFileSize'=>'2MB'
											)
										); 
									?>
									<span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
								</div>
								<?php
									}
								?>
							</fieldset>
							<?php
								if(empty($disabled))
								{
							?>
							<div class="text-right">
								<?php
									echo $this->Form->button('Save', array(
										'type'=>'submit',
										'value'=>'education',
										'name'=>'Submit',
										'class'=>'btn btn-success legitRipple',
										'escape'=>true
									));
								?>
								<a class="btn btn-default legitRipple" href="<?php echo $this->Html->url('/Applicants/details/'.$detail['Applicant']['id'], true);?>">
									Reset
								</a>
							</div>
							<?php
								}
							?>
						<?php echo $this->Form->end(); ?>
			<?php
						break;

					case 11:
			?>
						<?php echo $this->Form->create('Skill', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
							<fieldset class="content-group">
								<legend class="text-bold">Professional Skill</legend>
								<?php 
									echo $this->Form->input('id', array(
										'type'=>'hidden',
										'label'=> false,
										)
									); 
								?>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Start Date <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('from_year', array(
													'class'=>'form-control start_date',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">End Date <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('to_year', array(
													'class'=>'form-control end_date', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-lg-2">Profesional Association Membership <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<?php 
												echo $this->Form->input('name', array(
													'class'=>'form-control',
													'label'=> false,
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-lg-2">Status <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<?php 
												echo $this->Form->input('is_status', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$statuses,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<label class="control-label col-lg-12">Attachment <span class="text-danger">*</span></label>
								<div class="col-md-12">
									<div class="row">
										<?php
											for ($i=0; $i < count($this->request->data['Attachment']) ; $i++) 
											{ 
										?>
											<div class="col-lg-2 col-sm-6">
												<div class="thumbnail">
													<div class="thumb">
														<img src="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" />
														<div class="caption-overflow">
															<span>
																<a href="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View" ><i class="icon-eye"></i></a>
																<?php
																	if(empty($disabled))
																	{
																		echo $this->html->link(
																			'<spam class="icon-bin"></spam>', '/Applicants/attachment/'.$this->request->data['Attachment'][$i]['id'].'/'.$key,
																			array('class' => 'btn border-white text-white btn-flat btn-icon btn-rounded', 'title' => 'Delete', 'confirm' => 'Are you sure you want to delete this attachment?', 'escape' => false)
																		);
																	}
																?>
															</span>
														</div>
													</div>
				
													<div class="caption">
														<h6 class="no-margin"><?php echo $this->request->data['Attachment'][$i]['name'] ?></h6>
													</div>
												</div>
											</div>
										<?php
											}							
										?>
									</div>
								</div>
								<?php
									if(empty($disabled))
									{
								?>
								<div class="col-md-12">
									<?php 
										echo $this->Form->input('attachments.', array(
											'class'=>'form-control file-input',
											'label'=> false,
											'error'=>false,
											'type'=>'file',
											'multiple'=>'multiple',
											'data-preview-file-type'=>'any',
											'data-show-upload'=>false,
											'showRemove'=>false,
											'data-show-caption'=>false,
											'accept'=>'image/*',
											'maxFileSize'=>'2MB'
											)
										); 
									?>
									<span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
								</div>
								<?php
									}
								?>
							</fieldset>
							<?php
								if(empty($disabled))
								{
							?>
							<div class="text-right">
								<?php
									echo $this->Form->button('Save', array(
										'type'=>'submit',
										'value'=>'skill',
										'name'=>'Submit',
										'class'=>'btn btn-success legitRipple',
										'escape'=>true
									));
								?>

								<a class="btn btn-default legitRipple" href="<?php echo $this->Html->url('/Applicants/details/'.$detail['Applicant']['id'], true);?>">
									Reset
								</a>
							</div>
							<?php
								}
							?>
						<?php echo $this->Form->end(); ?>
			<?php
						break;
					case 17 :
			?>
						<?php echo $this->Form->create('Spouse', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
							<fieldset class="content-group">
								<legend class="text-bold">Spouse</legend>
								<?php 
									echo $this->Form->input('id', array(
										'type'=>'hidden',
										'label'=> false,
										)
									); 
								?>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Name <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('name', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Marriage Date <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('marriage_date', array(
													'class'=>'form-control date', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">IC No. <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('ic_new', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Passport No.</label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('passport_no', array(
													'class'=>'form-control', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">Religion <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('religion_id', array(
													'class'=>'form-control', 
													'label'=> false,
													'options'=>$religions,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">Race <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('race_id', array(
													'class'=>'form-control', 
													'label'=> false,
													'options'=>$races,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-lg-4">Nationality <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('national_id', array(
													'class'=>'form-control', 
													'label'=> false,
													'options'=>$nationals,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Occupation <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('occupation', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Employer</label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('employer', array(
													'class'=>'form-control', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-lg-2">Employer Address </label>
										<div class="col-lg-10">
											<?php 
												echo $this->Form->input('employer_address', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'textarea',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<label class="control-label col-lg-12">Attachment <span class="text-danger">*</span></label>
								<div class="col-md-12">
									<div class="row">
										<?php
											for ($i=0; $i < count($this->request->data['Attachment']) ; $i++) 
											{ 
										?>
											<div class="col-lg-2 col-sm-6">
												<div class="thumbnail">
													<div class="thumb">
														<img src="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" />
														<div class="caption-overflow">
															<span>
																<a href="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View" ><i class="icon-eye"></i></a>
																<?php
																	if(empty($disabled))
																	{
																		echo $this->html->link(
																			'<spam class="icon-bin"></spam>', '/Applicants/attachment/'.$this->request->data['Attachment'][$i]['id'].'/'.$key,
																			array('class' => 'btn border-white text-white btn-flat btn-icon btn-rounded', 'title' => 'Delete', 'confirm' => 'Are you sure you want to delete this attachment?', 'escape' => false)
																		);
																	}
																?>
															</span>
														</div>
													</div>
				
													<div class="caption">
														<h6 class="no-margin"><?php echo $this->request->data['Attachment'][$i]['name'] ?></h6>
													</div>
												</div>
											</div>
										<?php
											}							
										?>
									</div>
								</div>
								<?php
									if(empty($disabled))
									{
								?>
								<div class="col-md-12">
									<?php 
										echo $this->Form->input('attachments.', array(
											'class'=>'form-control file-input',
											'label'=> false,
											'error'=>false,
											'type'=>'file',
											'multiple'=>'multiple',
											'data-preview-file-type'=>'any',
											'data-show-upload'=>false,
											'showRemove'=>false,
											'data-show-caption'=>false,
											'accept'=>'image/*',
											'maxFileSize'=>'2MB'
											)
										); 
									?>
									<span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
								</div>
								<?php
									}
								?>
							</fieldset>
							<?php
								if(empty($disabled))
								{
							?>
							<div class="text-right">
								<?php
									echo $this->Form->button('Save', array(
										'type'=>'submit',
										'value'=>'spouse',
										'name'=>'Submit',
										'class'=>'btn btn-success legitRipple',
										'escape'=>true
									));
								?>

								<a class="btn btn-default legitRipple" href="<?php echo $this->Html->url('/Applicants/details/'.$detail['Applicant']['id'], true);?>">
									Reset
								</a>
							</div>
							<?php
								}
							?>
						<?php echo $this->Form->end(); ?>
			<?php
						break;
					case 20 :
			?>
						<?php echo $this->Form->create('Children', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
							<fieldset class="content-group">
								<?php 
									echo $this->Form->input('id', array(
										'type'=>'hidden',
										'label'=> false,
										)
									); 
								?>
								<legend class="text-bold">Children</legend>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Name <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('name', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">MyKID / IC No. <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('ic', array(
													'class'=>'form-control', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Gender <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('gender_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$genders,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Date of Birth <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('date_of_birth', array(
													'class'=>'form-control date', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Occupation</label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('occupation', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Institution/ Employer </label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('institution_employer', array(
													'class'=>'form-control', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<label class="control-label col-lg-12">Attachment <span class="text-danger">*</span></label>
								<div class="col-md-12">
									<div class="row">
										<?php
											for ($i=0; $i < count($this->request->data['Attachment']) ; $i++) 
											{ 
										?>
											<div class="col-lg-2 col-sm-6">
												<div class="thumbnail">
													<div class="thumb">
														<img src="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" />
														<div class="caption-overflow">
															<span>
																<a href="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View" ><i class="icon-eye"></i></a>
																<?php
																	if(empty($disabled))
																	{
																		echo $this->html->link(
																			'<spam class="icon-bin"></spam>', '/Applicants/attachment/'.$this->request->data['Attachment'][$i]['id'].'/'.$key,
																			array('class' => 'btn border-white text-white btn-flat btn-icon btn-rounded', 'title' => 'Delete', 'confirm' => 'Are you sure you want to delete this attachment?', 'escape' => false)
																		);
																	}
																?>
															</span>
														</div>
													</div>
				
													<div class="caption">
														<h6 class="no-margin"><?php echo $this->request->data['Attachment'][$i]['name'] ?></h6>
													</div>
												</div>
											</div>
										<?php
											}							
										?>
									</div>
								</div>
								<?php
									if(empty($disabled))
									{
								?>
								<div class="col-md-12">
									<?php 
										echo $this->Form->input('attachments.', array(
											'class'=>'form-control file-input',
											'label'=> false,
											'error'=>false,
											'type'=>'file',
											'multiple'=>'multiple',
											'data-preview-file-type'=>'any',
											'data-show-upload'=>false,
											'showRemove'=>false,
											'data-show-caption'=>false,
											'accept'=>'image/*',
											'maxFileSize'=>'2MB'
											)
										); 
									?>
									<span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
								</div>
								<?php
									}
								?>
							</fieldset>
							<?php
								if(empty($disabled))
								{
							?>
							<div class="text-right">
								<?php
									echo $this->Form->button('Save', array(
										'type'=>'submit',
										'value'=>'children',
										'name'=>'Submit',
										'class'=>'btn btn-success legitRipple',
										'escape'=>true
									));
								?>

								<a class="btn btn-default legitRipple" href="<?php echo $this->Html->url('/Applicants/details/'.$detail['Applicant']['id'], true);?>">
									Reset
								</a>
							</div>
							<?php
								}
							?>
						<?php echo $this->Form->end(); ?>
			<?php
						break;
					case 26 :
			?>
						<?php echo $this->Form->create('Death', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
							<fieldset class="content-group">
								<legend class="text-bold">Death</legend>
								<?php 
									echo $this->Form->input('id', array(
										'type'=>'hidden',
										'label'=> false,
										)
									); 
								?>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Name <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('name', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">IC No. <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('ic', array(
													'class'=>'form-control', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Relationship <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('relationship_id', array(
													'class'=>'form-control',
													'label'=> false,
													'options'=>$relationships,
													'empty'=>'PLEASE SELECT...',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-lg-4">Date of Death <span class="text-danger">*</span></label>
										<div class="col-lg-8">
											<?php 
												echo $this->Form->input('date_of_death', array(
													'class'=>'form-control date', 
													'label'=> false,
													'type'=>'text',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-lg-2">Cause of Death <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<?php 
												echo $this->Form->input('cause_of_death', array(
													'class'=>'form-control',
													'label'=> false,
													'type'=>'textarea',
													'disabled'=>$disabled
													)
												); 
											?>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset class="content-group">
								<label class="control-label col-lg-12">Attachment <span class="text-danger">*</span></label>
								<div class="col-md-12">
									<div class="row">
										<?php
											for ($i=0; $i < count($this->request->data['Attachment']) ; $i++) 
											{ 
										?>
											<div class="col-lg-2 col-sm-6">
												<div class="thumbnail">
													<div class="thumb">
														<img src="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" />
														<div class="caption-overflow">
															<span>
																<a href="<?php echo $path.$this->request->data['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View" ><i class="icon-eye"></i></a>
																<?php
																	if(empty($disabled))
																	{
																		echo $this->html->link(
																			'<spam class="icon-bin"></spam>', '/Applicants/attachment/'.$this->request->data['Attachment'][$i]['id'].'/'.$key,
																			array('class' => 'btn border-white text-white btn-flat btn-icon btn-rounded', 'title' => 'Delete', 'confirm' => 'Are you sure you want to delete this attachment?', 'escape' => false)
																		);
																	}
																?>
															</span>
														</div>
													</div>
				
													<div class="caption">
														<h6 class="no-margin"><?php echo $this->request->data['Attachment'][$i]['name'] ?></h6>
													</div>
												</div>
											</div>
										<?php
											}							
										?>
									</div>
								</div>
								<?php
									if(empty($disabled))
									{
								?>
								<div class="col-md-12">
									<?php 
										echo $this->Form->input('attachments.', array(
											'class'=>'form-control file-input',
											'label'=> false,
											'error'=>false,
											'type'=>'file',
											'multiple'=>'multiple',
											'data-preview-file-type'=>'any',
											'data-show-upload'=>false,
											'showRemove'=>false,
											'data-show-caption'=>false,
											'accept'=>'image/*',
											'maxFileSize'=>'2MB'
											)
										); 
									?>
									<span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
								</div>
								<?php
									}
								?>
							</fieldset>
							<?php
								if(empty($disabled))
								{
							?>
							<div class="text-right">
								<?php
									echo $this->Form->button('Save', array(
										'type'=>'submit',
										'value'=>'death',
										'name'=>'Submit',
										'class'=>'btn btn-success legitRipple',
										'escape'=>true
									));
								?>

								<a class="btn btn-default legitRipple" href="<?php echo $this->Html->url('/Applicants/details/'.$detail['Applicant']['id'], true);?>">
									Reset
								</a>
							</div>
							<?php
								}
							?>
						<?php echo $this->Form->end(); ?>
			<?php
						break;
				}
			?>	
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<span class="heading-text">
						<a class="btn btn-warning" href="<?php echo $this->Html->url('/Applicants/view/'.$parent_key, true);?>">
							Back <i class="icon-arrow-left13 position-right"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
		<!-- /task overview -->

	</div>

	<div class="col-lg-3">

		<!-- Timer -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-watch position-left"></i> Apply Date Detail</h6>
			</div>

			<div class="panel-body">
				<ul class="timer-weekdays mb-10">
					<?php
						foreach ($days as $day_key => $day_value) 
						{
							if($detail['Applicant']['day_by_text'] == $day_key)
							{
					?>
							<li class="active"><a href="#" class="label label-danger"><?php echo $day_value; ?></a></li>
					<?php
							}
							else
							{
					?>
							<li><a href="#" class="label label-default"><?php echo $day_value; ?></a></li>
					<?php
							}
						}
					?>
				</ul>

				<ul class="timer mb-10">
					<li>
						<span>
							<strong>
								<?php echo $detail['Applicant']['day_by_num']?> /
							</strong>	
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Applicant']['month']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Applicant']['year']?>
							</strong>
						</span>
					</li>
				</ul>

				<ul class="timer mb-10">
					<li>
						<?php echo $detail['Applicant']['hour']?> <span>Hour</span>
					</li>
					<li class="dots">:</li>
					<li>
						<?php echo $detail['Applicant']['minute']?> <span>Minute</span>
					</li>
					<li class="dots"></li>
					<li>
						<?php echo $detail['Applicant']['format']?> <span>Period</span>
					</li>
				</ul>
			</div>
		</div>
		<!-- /timer -->
		
		<?php
			if($detail['Applicant']['status_id'] <= 3)
			{
		?>
		<!-- Task settings -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-task position-left"></i> Task Assign</h6>
			</div>

			<div class="panel-body">
				<form action="#">
					<div class="form-group">
						<div class="checkbox checkbox-switchery checkbox-right switchery-xs">
							<label class="display-block">
								<Strong><?php echo $session['Staff']['name']; ?></Strong>, Do you want to take this task?
							</label>	
						</div>
					</div>

					<div class="row">

						<div class="col-md-12">
							<a class="btn btn-primary btn-sm btn-block" href="#" data-toggle="modal" data-target="#modal_accept">
								Accept
							</a>
						</div>

					</div>
				</form>
			</div>
		</div>
		<!-- /task settings -->
		<?php
			}
			else
			{
		?>
			<!-- User details (with sample pattern) -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> Assigned To</h6>
				</div>
				<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<a href="#" class="display-inline-block content-group-sm">
						<?php echo $parent['AssignTo']['avatar']; ?>
					</a>

					<div class="content-group-sm">
						<h5 class="text-semibold no-margin-bottom">
							<?php echo $parent['AssignTo']['name'] ?>
						</h5>
					</div>

					<ul class="timer mb-10">
						<li>
							<span>
								<strong>
									<?php echo $parent['AssignTo']['day_by_num']?> /
								</strong>	
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $parent['AssignTo']['month']?> /
								</strong>
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $parent['AssignTo']['year']?>
								</strong>
							</span>
						</li>
					</ul>

					<ul class="timer mb-10">
						<li>
							<?php echo $parent['AssignTo']['hour']?> <span>Hour</span>
						</li>
						<li class="dots">:</li>
						<li>
							<?php echo $parent['AssignTo']['minute']?> <span>Minute</span>
						</li>
						<li class="dots"></li>
						<li>
							<?php echo $parent['AssignTo']['format']?> <span>Period</span>
						</li>
					</ul>
				</div>

			</div>
			<!-- /user details (with sample pattern) -->
		<?php
				if($team_checked == true && $assignto == true)
				{	
					if(($detail['Applicant']['status_id'] >= 4 && $detail['Applicant']['status_id'] <= 5) || $detail['Applicant']['status_id'] == 7)
					{
		?>
			<!-- Task settings -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> Verify / Correction</h6>
				</div>

				<div class="panel-body">
					<?php echo $this->Form->create('Applicant', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
						<?php
							echo $this->Form->input('id', array(
								'class'=>'form-control',
								'label'=> false,
								'type'=>'hidden',
								'value'=>$detail['Applicant']['key_id']
								)
							);

							echo $this->Form->input('modul_id', array(
								'class'=>'form-control',
								'label'=> false,
								'type'=>'hidden',
								'value'=>$detail['Applicant']['modul_id']
								)
							);
						?>
						<div class="form-group">
							<label class="control-label col-lg-12">Note</label>
							<div class="col-lg-12">
								<?php
									echo $this->Form->input('note', array(
										'class'=>'form-control',
										'label'=> false,
										'type'=>'textarea',
										'error'=>false,
										'value'=>$detail['Applicant']['note']
										)
									);
								?>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<?php
									echo $this->Form->button('Verify', array(
										'type'=>'submit',
										'value'=>'verified',
										'name'=>'Submit',
										'class'=>'btn btn-primary btn-sm btn-block',
										'escape'=>true
									));
								?>
							</div>
							<div class="col-md-6">
								<?php
									echo $this->Form->button('Correction', array(
										'type'=>'submit',
										'value'=>'correction',
										'name'=>'Submit',
										'class'=>'btn btn-danger btn-sm btn-block',
										'escape'=>true
									));
								?>
							</div>
						</div>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
			<!-- /task settings -->
		<?php
					}
				}

				if($parent['Applicant']['status_id'] >= 7)
				{
			?>
				<!-- User details (with sample pattern) -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title"><i class="icon-task position-left"></i> Verified</h6>
					</div>
					<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
						<a href="#" class="display-inline-block content-group-sm">
							<?php echo $parent['VerifiedBy']['avatar']; ?>
						</a>

						<div class="content-group-sm">
							<h5 class="text-semibold no-margin-bottom">
								<?php echo $parent['VerifiedBy']['name'] ?>
							</h5>
						</div>

						<ul class="timer mb-10">
							<li>
								<span>
									<strong>
										<?php echo $parent['VerifiedBy']['day_by_num']?> /
									</strong>	
								</span>
							</li>
							<li>
								<span>
									<strong>
										<?php echo $parent['VerifiedBy']['month']?> /
									</strong>
								</span>
							</li>
							<li>
								<span>
									<strong>
										<?php echo $parent['VerifiedBy']['year']?>
									</strong>
								</span>
							</li>
						</ul>

						<ul class="timer mb-10">
							<li>
								<?php echo $parent['VerifiedBy']['hour']?> <span>Hour</span>
							</li>
							<li class="dots">:</li>
							<li>
								<?php echo $parent['VerifiedBy']['minute']?> <span>Minute</span>
							</li>
							<li class="dots"></li>
							<li>
								<?php echo $parent['VerifiedBy']['format']?> <span>Period</span>
							</li>
						</ul>
					</div>

				</div>
				<!-- /user details (with sample pattern) -->
			<?php
				}

				if($approved == true)
				{		
					if($parent['Applicant']['status_id'] != 10 && ($detail['Applicant']['status_id'] == 7 || $detail['Applicant']['status_id'] == 10))
					{
		?>
			<!-- Task settings -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> Approve / Reject</h6>
				</div>

				<div class="panel-body">
					<?php echo $this->Form->create('Applicant', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
						<?php
							echo $this->Form->input('id', array(
								'class'=>'form-control',
								'label'=> false,
								'type'=>'hidden',
								'value'=>$detail['Applicant']['key_id']
								)
							);

							echo $this->Form->input('modul_id', array(
								'class'=>'form-control',
								'label'=> false,
								'type'=>'hidden',
								'value'=>$detail['Applicant']['modul_id']
								)
							);
						?>
						<div class="form-group">
							<label class="control-label col-lg-12">Note</label>
							<div class="col-lg-12">
								<?php
									echo $this->Form->input('note', array(
										'class'=>'form-control',
										'label'=> false,
										'type'=>'textarea',
										'error'=>false,
										'value'=>$detail['Applicant']['note']
										)
									);
								?>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<?php
									echo $this->Form->button('Approved', array(
										'type'=>'submit',
										'value'=>'approved',
										'name'=>'Submit',
										'class'=>'btn btn-primary btn-sm btn-block',
										'escape'=>true
									));
								?>
							</div>
							<div class="col-md-6">
								<?php
									echo $this->Form->button('Correction', array(
										'type'=>'submit',
										'value'=>'correction',
										'name'=>'Submit',
										'class'=>'btn btn-danger btn-sm btn-block',
										'escape'=>true
									));
								?>
							</div>
						</div>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
			<!-- /task settings -->
		<?php
					}
				}

				if($parent['Applicant']['status_id'] == 10)
				{
			?>
				<!-- User details (with sample pattern) -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title"><i class="icon-task position-left"></i> Approved</h6>
					</div>
					<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
						<a href="#" class="display-inline-block content-group-sm">
							<?php echo $parent['ApprovedBy']['avatar']; ?>
						</a>

						<div class="content-group-sm">
							<h5 class="text-semibold no-margin-bottom">
								<?php echo $parent['ApprovedBy']['name'] ?>
							</h5>
						</div>

						<ul class="timer mb-10">
							<li>
								<span>
									<strong>
										<?php echo $parent['ApprovedBy']['day_by_num']?> /
									</strong>	
								</span>
							</li>
							<li>
								<span>
									<strong>
										<?php echo $parent['ApprovedBy']['month']?> /
									</strong>
								</span>
							</li>
							<li>
								<span>
									<strong>
										<?php echo $parent['ApprovedBy']['year']?>
									</strong>
								</span>
							</li>
						</ul>

						<ul class="timer mb-10">
							<li>
								<?php echo $parent['ApprovedBy']['hour']?> <span>Hour</span>
							</li>
							<li class="dots">:</li>
							<li>
								<?php echo $parent['ApprovedBy']['minute']?> <span>Minute</span>
							</li>
							<li class="dots"></li>
							<li>
								<?php echo $parent['ApprovedBy']['format']?> <span>Period</span>
							</li>
						</ul>
					</div>

				</div>
				<!-- /user details (with sample pattern) -->
			<?php
				}
			}
		?>
	</div>
</div>
<!-- /detailed task -->

<!-- Warning modal -->
<div id="modal_accept" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-task"></span> Accept?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Are you sure?</h6>
				<p>Do you want to accept this task? Just needed to double check.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Hris/accept/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->