<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Request Updates</li>
            <li class="active">List of Update</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Bordered panel body table -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">List of Update</h5>
	</div>
	<div class="panel-body">
	<?php echo $this->Session->flash(); ?>
		<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="5%" class="text-center">No.</th>
						<th width="">Module</th>
						<th width="15%" class="text-center">Status</th>
						<th width="15%" class="text-center">Date</th>
						<th width="20%" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php

						if($details)
						{
							$counter = 1;

							foreach ($details['Update'] as $detail) 
							{
					?>
						<tr>
							<td valign="top" class="text-center">
								<?php echo $counter; ?>
							</td>
							<td>
								<?php echo $detail['modul'] ?>
							</td>
							<td class="text-center">
								<?php echo $detail['status'] ?>
							</td>
							<td>
								<?php echo $detail['modified'] ?>
							</td>
							<td class="text-center">
								<a alt="View" href="<?php echo $this->Html->url($detail['view'].$detail['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="View details">
									<spam class="glyphicon glyphicon-file">
									</spam>
								</a>
								
								<a alt="Edit" href="<?php echo $this->Html->url($detail['edit'].$detail['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Edit details">
									<spam class="icon-wrench">
									</spam>
								</a>
							</td>
						</tr>
					<?php
							$counter++;
							}
						}
						else
						{
					?>
						<tr>
							<td colspan="5">
								No data...
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<?php
			if(!empty($details))
			{
		?>
			<br/>
			<div class="text-right">
				<a class="btn btn-warning position-right" href="#" data-toggle="modal" data-target="#modal_submit">
					Submit
					<i class="icon-drawer-out position-right"></i>
				</a>
			</div>
		<?php
			}
		?>
	</div>
</div>
<!-- /bordered panel body table -->

<!-- Warning modal -->
<div id="modal_submit" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-drawer-out"></span> Submit?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Confirm</h6>
				<p>Please make sure this information is correct. Once it is submitted it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Applicants/submit', true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->