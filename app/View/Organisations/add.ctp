<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Organizations</a></li>
			<li class="active">Add New Business Unit</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Organisation', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Organization</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Business Type<span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('organisation_type_id', array(
									'class'=>'form-control', 
									'label'=> false,
									'options'=>$organisationtypes,
									'empty'=>'PLEASE SELECT...',
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-success legitRipple">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Organisations/add', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Organisations/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->