<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">KPI Approval</a></li>
            <li class="active">List of Departments</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
            <fieldset class="content-group">
			    <legend class="text-bold">Search</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-1">Search</label>
						<div class="col-lg-11">
							<?php
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'placeholder'=>'Department name',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Period</label>
						<div class="col-lg-10">
							<select name="select" class="form-control">
								<option value="0">Select Period</option>
								<option value="3">Year - End Review</option>
								<option value="2">Mid - Year Review</option>
								<option value="1">KPI Planning</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Status</label>
						<div class="col-lg-10">
							<select name="select" class="form-control">
								<option value="0">Select Status</option>
								<option value="2">Completed</option>
								<option value="1">Incomplete</option>
							</select>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-primary">
					Find
					<i class="glyphicon glyphicon-search position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Kpis/index', true);?>">
					Reset
					<i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
</div>
<!-- /Form horizontal -->

<!-- Bordered panel body table -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">List of Departments for Year 2019</h5>
	</div>
	<div class="panel-body">

		<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="5%" class="text-center">No</th>
						<th class="text-left">Department</th>
						<th class="text-center">Period</th>
						<th class="text-center">Status</th>
						<th width="15%" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
                    <tr>
						<td colspan ='1' class="text-center">1</td>
						<td colspan ='1' class="text-left">TECHNOLOGY & INNOVATION</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-success-400">Completed</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">2</td>
						<td colspan ='1' class="text-left">BIG DATA ANALYSIS / AI / IoT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
                    <tr>
						<td colspan ='1' class="text-center">3</td>
						<td colspan ='1' class="text-left">SYSTEM DEVELOPMENT (DIGITISATION)</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
                    <tr>
						<td colspan ='1' class="text-center">4</td>
						<td colspan ='1' class="text-left">CYBERSECURITY INFRA / BLOCKCHAIN</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
                    <tr>
						<td colspan ='1' class="text-center">5</td>
						<td colspan ='1' class="text-left">TICKETING & PAYMENT SOLUTIONS</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
                    <tr>
						<td colspan ='1' class="text-center">6</td>
						<td colspan ='1' class="text-left">BUSINESS IT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
                    <tr>
						<td colspan ='1' class="text-center">7</td>
						<td colspan ='1' class="text-left">ENTERPRISE APPLICATION</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">8</td>
						<td colspan ='1' class="text-left">INFRASTRUCTURE SUPPORT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">9</td>
						<td colspan ='1' class="text-left">RAIL</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">10</td>
						<td colspan ='1' class="text-left">BUS</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">11</td>
						<td colspan ='1' class="text-left">GOVERNANCE</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">12</td>
						<td colspan ='1' class="text-left">FINANCIAL MANAGEMENT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">13</td>
						<td colspan ='1' class="text-left">ASSET MANAGEMENT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">14</td>
						<td colspan ='1' class="text-left">INFOSEC ENGAGEMENT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">15</td>
						<td colspan ='1' class="text-left">SERVICE DELIVERY & PROGRAMME MANAGEMENT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">16</td>
						<td colspan ='1' class="text-left">SERVICE DELIVERY</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan ='1' class="text-center">17</td>
						<td colspan ='1' class="text-left">PROGRAMME MANAGEMENT</td>
						<td colspan ='1' class="text-center">KPI Planning</td>
						<td colspan ='1' class="text-center"><span class="label bg-grey-400">Incomplete</span></td>
						<td colspan ='1' class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/staff');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpiapprovals/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->

</div>
<!-- /bordered panel body table -->
