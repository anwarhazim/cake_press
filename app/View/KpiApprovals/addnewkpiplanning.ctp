<!-- Page header -->
<div class="page-header page-header-default">

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li><a href="#">KPI</a></li>
            <li class="#">List of KPI's</li>
            <li class="#">Incumbent Data</li>
            <li class="#">Part 1 - KPI Planning</li>
            <li class="active">Add New Part 1 - KPI Planning</li>
        </ul>
    </div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
    <div class="panel-body">

        <?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal wizard clearfix', 'novalidate'=>'novalidate'));?>
        <div class="steps clearfix">
            <ul role="tablist">
                <li role="tab" class="done" aria-disabled="false" aria-selected="true">
                    <a href="#">
                        <span class="number">1</span>
                        INCUMBENT DATA
                    </a>
                </li>
                <li role="tab" class="current" aria-disabled="true">
                    <a href="#">
                        <span class="number">2</span>
                        PART 1 : KPI PLANNING
                    </a>
                </li>
                <li role="tab" class="disabled" aria-disabled="true">
                    <a href="#">
                        <span class="number">3</span>
                        PART 2 : MID - YEAR REVIEW
                    </a>
                </li>
                <li role="tab" class="disabled" aria-disabled="true">
                    <a href="#">
                        <span class="number">4</span>
                        PART 3 : YEAR - END REVIEW
                    </a>
                </li>
            </ul>
        </div>

        <form class="form-horizontal" action="#">
            <fieldset class="content-group">
                <legend class="text-bold">KPI PLANNING</legend>

                <div class="form-group">
                    <label  class="control-label col-lg-12">Focus<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <select name="focus" class="form-control">
                            <option value="0">Select Focus</option>
                            <option value="1">Customer</option>
                            <option value="2">Innovation And Learning</option>
                            <option value="3">Environment And Community</option>
                            <option value="4">Internal Business Process</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label  class="control-label col-lg-12">Objective<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Objective"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label  class="control-label col-lg-12">Key Performance Indicator<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Key Performance Indicator"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label  class="control-label col-lg-12">Weightage<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="in Percent (%)">
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">PERFORMANCE TARGET</legend>

                <div class="form-group">
                    <label  class="control-label col-lg-12">Poor (1)<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Poor rate"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="control-label col-lg-12">Fair (2)<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Fair rate"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="control-label col-lg-12">Target (3)<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Target rate"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="control-label col-lg-12">Good (4)<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Good rate"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="control-label col-lg-12">Excellent (5)<span class="text-danger">*</span></label>
                    <div class="col-lg-12">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Excellent rate"></textarea>
                    </div>
                </div>
            </fieldset>
        </form>
       
        <div class="text-right">
            <a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/kpiplanning', true);?>">
                Previous <i class=" icon-arrow-left16 position-right"></i>
            </a>
            <a class="btn btn-default position-right"
                href="<?php echo $this->Html->url('/kpis/addnewkpiplanning', true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
            <a class="btn btn-success position-right" href="<?php echo $this->Html->url('/kpis/kpiplanning', true);?>">
                Save <i class="icon-drawer-out position-right position-right"></i>
            </a>

        </div>

        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
    <!-- panel-footer -->
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
                    Back <i class="icon-arrow-left13 position-right"></i>
                </a>
            </span>
        </div>
    </div>
    <!-- /panel-footer -->
</div>
<!-- /Form horizontal -->