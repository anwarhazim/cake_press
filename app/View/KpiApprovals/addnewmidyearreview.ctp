<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">PMS</a></li>
			<li class="#">List of PMS</li>
			<li class="#">Incumbent Data</li>
			<li class="#">Part 1 - Kpi Planning</li>
			<li ></li>Part 2 - Mid Year Review</li>
			<li class="active">Add New Part 2 - Mid Year Review</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">


		<?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal wizard clearfix', 'novalidate'=>'novalidate'));?>
		<div class="steps clearfix">
            <ul role="tablist">
                <li role="tab" class="done" aria-disabled="false" aria-selected="true">
                    <a href="#">
                        <span class="number">1</span>
                        INCUMBENT DATA
                    </a>
                </li>
				<li role="tab" class="done" aria-disabled="true">
                    <a href="#">
                        <span class="number">2</span>
                        PART 1 : KPI PLANNING
                    </a>
                </li>
                <li role="tab" class="current" aria-disabled="true">
                    <a href="#">
                        <span class="number">3</span>
                        PART 2 : MID - YEAR REVIEW
                    </a>
                </li>
                <li role="tab" class="disabled" aria-disabled="true">
                    <a href="#" >
                        <span class="number">4</span>
                        PART 3 : YEAR - END REVIEW
                    </a>
                </li>
            </ul>
        </div>
			<fieldset class="content-group">
				<legend class="text-bold">PART 1 : KPI PLANNIING</legend>
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label col-lg-4">Focus</label>
						<div class="col-lg-8">
							<?php
								echo $this->Form->input('Weightage', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'placeholder'=>'base on DB',
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-4">Objective<span class="text-danger">*</span></label>
			       	 <textarea rows="5" cols="5" placeholder="base on DB" class="form-control"></textarea>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-4">Key Performance Indicator<span class="text-danger">*</span></label>
			        	<textarea rows="5" cols="5" placeholder="base on DB" class="form-control"></textarea>
					</div>
				</div>

				<legend class="text-bold">Part 2 : Mid Year Review ADD</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Result<span class="text-danger">*</span></label>
			        	<textarea rows="5" cols="5" placeholder="enter here" class="form-control"></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Comment/  Ammendment<span class="text-danger">*</span></label>
			        	<textarea rows="5" cols="5" placeholder="enter here" class="form-control"></textarea>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
			<a class="btn btn-danger position-right" href="<?php echo $this->Html->url('/kpis/midyearreview', true);?>">
					Previous <i class=" icon-arrow-left16 position-right"></i>
				</a>
				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/kpis/addnewmidyearreview', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
				<a class="btn btn-success position-right"  href="<?php echo $this->Html->url('/kpis/midyearreview', true);?>">
					Save <i class="icon-drawer-out position-right position-right"></i>
				</a>

			</div>

        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->
