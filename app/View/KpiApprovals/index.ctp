<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">KPI Approval</a></li>
            <li class="active">List of KPI's</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- simple statistics -->
<div class="row">
	<div class="col-sm-6 col-md-10">
		<div class="alert alert-info alert-styled-left alert-arrow-left">
			<h6 class="alert-heading text-semibold">Announcement</h6>
			PMS Online System review period will be start from <code>1st July 2019</code> until <code>31st July 2019</code>.
			Please complete your KPI at early stage to avoid any difficulties before the end of the closing date.
		</div>
	</div>
	<div class="col-sm-6 col-md-2">
		<div class="panel panel-body bg-danger-400 has-bg-image" style="height:100px;">
			<div class="media no-margin">
				<div class="media-body">
					<h1 class="no-margin">14</h3>
					<span class="text-uppercase text-size-mini">days</span>
				</div>

				<div class="media-right media-middle">
					<i class="icon-alarm icon-3x opacity-75"></i>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /simple statistics -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
            <fieldset class="content-group">
			    <legend class="text-bold">Search</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-1">Search</label>
						<div class="col-lg-11">
							<?php
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'placeholder'=>'Year',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Start Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('start_date', array(
									'class'=>'form-control start_date',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">End Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('end_date', array(
									'class'=>'form-control end_date',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-primary">
					Find
					<i class="glyphicon glyphicon-search position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Kpis/index', true);?>">
					Reset
					<i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
</div>
<!-- /Form horizontal -->

<!-- Bordered panel body table -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">List of KPI's</h5>
	</div>
	<div class="panel-body">

		<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Year</th>
						<th colspan="3" class="text-center">Period</th>
						<th class="text-center">Status</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="1" class="text-center">1</td>
						<td colspan="1" class="text-center">2019</td>
						<td colspan="1" class="text-center">01/01/2019</td>
						<td colspan="1" class="text-center">31/12/2019</td>
						<td colspan="1" class="text-center">KPI Planning</td>
						<td colspan="1" class="text-center">
							<span class="label bg-grey-400">Incomplete</span>
						</td>
						<td colspan="1" class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/organization');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="text-center">2</td>
						<td colspan="1" class="text-center">2018</td>
						<td colspan="1" class="text-center">01/01/2018</td>
						<td colspan="1" class="text-center">31/12/2018</td>
						<td colspan="1" class="text-center">Mid - Year Review</td>
						<td colspan="1" class="text-center">
							<span class="label bg-success-400">Completed</span>
						</td>
						<td colspan="1" class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/organization');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="text-center">3</td>
						<td colspan="1" class="text-center">2017</td>
						<td colspan="1" class="text-center">01/01/2017</td>
						<td colspan="1" class="text-center">31/12/2017</td>
						<td colspan="1" class="text-center">Year - End Review</td>
						<td colspan="1" class="text-center">
							<span class="label bg-success-400">Completed</span>
						</td>
						<td colspan="1" class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/organization');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan="1" class="text-center">4</td>
						<td colspan="1" class="text-center">2016</td>
						<td colspan="1" class="text-center">01/01/2016</td>
						<td colspan="1" class="text-center">31/12/2016</td>
						<td colspan="1" class="text-center">Year - End Review</td>
						<td colspan="1" class="text-center">
							<span class="label bg-success-400">Completed</span>
						</td>
						<td colspan="1" class="text-center">
							<a href="<?php echo $this->Html->url('/kpiapprovals/organization');?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-file-eye"></i></a>
						</td>
					</tr>
					<tr>
						<td colspan="7">No Data</td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div>
</div>
<!-- /bordered panel body table -->
