<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">KPI Approval</a></li>
			<li class="active">Appraiser Comment</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Basic accordion -->
<div class="panel-group content-group-lg" id="accordion1">

    <!-- Incumbent Data -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion1" href="#accordion-group1">Incumbent Data</a>
            </h6>
        </div>
        <div id="accordion-group1" class="panel-collapse collapse in">
            <div class="panel-body">
                <form class="form-horizontal" action="#">
                    <div class="col-md-12">
                        <div class="form-group">
                        <label class="control-label col-lg-2">Name<span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" disabled="disabled" value="Mohd Nizar Bin Abd Azik">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Year<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="2018">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Period Start Date<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="01-01-2018">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Period End Date<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="31-12-2018">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Joining Date<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="04-08-2018">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Position<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="Vice President II">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Staff No<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="10010630">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Division<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="Digital & Technology">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Department<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="Technology Innovation">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Section<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="System Development (Digitisation)">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Unit<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value=" ">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Sub Unit<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value=" ">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Supervisor<span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" disabled="disabled" value="Hamid Bin Salikin">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- /Incumbent Data -->

    <!-- KPI Planning -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion-group2">KPI Planning</a>
            </h6>
        </div>
        <div id="accordion-group2" class="panel-collapse collapse">
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table class="table table-bordered table-framed table-striped">
                        <thead>
                            <tr>
                                <th colspan ='11' class="text-center"><label><strong>PART 1 : KPI PLANNING</strong></td>
                            </tr>
                            <tr>
                                <th rowspan = '2' class="text-center"><label><strong>NO</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:100px;"><strong>FOCUS</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:250px;"><strong>OBJECTIVE</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:400px;"><strong>KEY PERFORMANCE INDICATOR</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:100px;"><strong>WEIGHTAGE (%)</strong></label></th>
                                <th colspan = '5' class="text-center"><label><strong>PERFORMANCE TARGET</strong></label></th>
                            </tr>
                            <tr>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>POOR (1)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>FAIR (2)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>TARGET (3)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>GOOD (4)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>EXCELLENT (5)</strong></label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">SECRETARIAL</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Secretarial assistance tp deputy / Division Assist to liases with all clients on presentations & meetings. Manage Calendar Deputy Chief.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">30</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">PEOPLE</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Identified and proposed solution for issue related to clerical works at CSHE division Liase with all staffs on office matters. Displayed good behavior and able to plan a tasks within the given timeline.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">3</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Documentation</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Administration works. Compile all the related document for Deputy review and signature. Distribute all related dic to PIC. Organize record incoming & outgoing correspondence / email.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">4</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Development and Operational Excellent</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Submit monthly summary Claims for the division to HR within the time required.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">10</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Not following day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Monitoring</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Summary staff attendance and staff movement</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">6</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">INNOVATION & LEARNING</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">% of Staff Meet 32 hrs Training</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Employee Development (Staff attended 32hrs training)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">100</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">7</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ENVIRONMENT & QUALITY</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">5S Program (Audit Rating)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Rating</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (score 60% and below)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (min score 61%)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 71% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 81% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">3 Star Rating (min 91% score)</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">8</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Arrangement of Meeting</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To arrange meeting internal / external parties</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                            </tr>
                            <tr>
                                <td colspan="4" rowspan="1" style="vertical-align: top;" class="text-right"><strong>TOTAL WEIGHTAGE</strong></td>
                                <td style="vertical-align: top;" class="text-center"><strong>100</strong></td>
                                <td colspan="6" rowspan="1" style="vertical-align: top;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan ='10'>No Data</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!-- /KPI Planning -->

    <!-- Mid - Year Review -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion-group3">Mid - Year Review</a>
            </h6>
        </div>
        <div id="accordion-group3" class="panel-collapse collapse">
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table class="table table-bordered table-framed table-striped">
                        <thead>
                            <tr>
                                <th colspan = '10' class="text-center"><label><strong>PART 1 : KPI PLANNING</strong></label></th>
                                <th colspan = '2' class="text-center"><label><strong>PART 2 : MID YEAR REVIEW</strong></label></th>
                            </tr>
                            <tr>
                                <th rowspan = '2' class="text-center"><label><strong>NO</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:100px;"><strong>FOCUS</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:250px;"><strong>OBJECTIVE</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:400px;"><strong>KEY PERFORMANCE INDICATOR</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:100px;"><strong>WEIGHTAGE (%)</strong></label></th>
                                <th colspan = '5' class="text-center"><label><strong>PERFORMANCE TARGET</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:400px;"><strong>RESULT</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:400px;"><strong>COMMENT/ AMENDMENT</strong></label></th>
                            </tr>
                            <tr>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>POOR (1)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>FAIR (2)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>TARGET (3)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>GOOD (4)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>EXCELLENT (5)</strong></label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">SECRETARIAL</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Secretarial assistance tp deputy / Division Assist to liases with all clients on presentations & meetings. Manage Calendar Deputy Chief.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">30</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Manage schedule Deputy Chief and comply all the task</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">PEOPLE</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Identified and proposed solution for issue related to clerical works at CSHE division Liase with all staffs on office matters. Displayed good behavior and able to plan a tasks within the given timeline.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To make sure all the task given by staff not delayed and be completed.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">3</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Documentation</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Administration works. Compile all the related document for Deputy review and signature. Distribute all related dic to PIC. Organize record incoming & outgoing correspondence / email.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To make sure all the process running smoothly and do not delayed.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">4</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Development and Operational Excellent</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Submit monthly summary Claims for the division to HR within the time required.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">10</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Not following day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Maintain timeline required.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Monitoring</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Summary staff attendance and staff movement</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Weekly report</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">6</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">INNOVATION & LEARNING</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">% of Staff Meet 32 hrs Training</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Employee Development (Staff attended 32hrs training)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">100</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Achieve target</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">7</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ENVIRONMENT & QUALITY</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">5S Program (Audit Rating)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Rating</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (score 60% and below)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (min score 61%)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 71% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 81% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">3 Star Rating (min 91% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Ongoing</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">8</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Arrangement of Meeting</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To arrange meeting internal / external parties</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Smoothly on process</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                            </tr>
                            <tr>
                                <td colspan="4" rowspan="1" style="vertical-align: top;" class="text-right"><strong>TOTAL WEIGHTAGE</strong></td>
                                <td style="vertical-align: top;" class="text-center"><strong>100</strong></td>
                                <td colspan="8" rowspan="1" style="vertical-align: top;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan ='12'>No Data</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!-- /Mid - Year Review -->

    <!-- Year - End Review -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion-group4">Year - End Review</a>
            </h6>
        </div>
        <div id="accordion-group4" class="panel-collapse collapse">
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table class="table table-bordered table-framed table-striped">
                        <thead>
                            <tr>
                                <th colspan = '10' class="text-center"><label><strong>PART 1 : KPI PLANNING</strong></label></th>
                                <th colspan = '2' class="text-center"><label><strong>PART 2 : MID YEAR REVIEW</strong></label></th>
                                <th colspan = '5' class="text-center"><label><strong>PART 3 : YEAR END REVIEW</strong></label></th>
                            </tr>
                            <tr>
                                <th rowspan = '2' class="text-center"><label><strong>NO</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:100px;"><strong>FOCUS</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:250px;"><strong>OBJECTIVE</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:400px;"><strong>KEY PERFORMANCE INDICATOR</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:100px;"><strong>WEIGHTAGE (%)</strong></label></th>
                                <th colspan = '5' class="text-center"><label><strong>PERFORMANCE TARGET</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:400px;"><strong>RESULT</strong></label></th>
                                <th rowspan = '2' class="text-center"><label style="width:400px;"><strong>COMMENT/ AMENDMENT</strong></label></th>
                                <th rowspan = '4' class="text-center"><label style="width:400px;"><strong>ACHIEVEMENT / RESULT</strong></label></th>
                                <th rowspan = '4' class="text-center"><label style="width:400px;"><strong>COMMENT / AMENDMENT</strong></label></th>
                                <th rowspan = '4' class="text-center"><label style="width:110px;"><strong>WEIGHTAGE (%)</strong></label></th>
                                <th rowspan = '4' class="text-center"><label style="width:110px;"><strong>RATING</strong></label></th>
                                <th rowspan = '4' class="text-center"><label style="width:110px;"><strong>TOTAL RATING</strong></label></th>
                            </tr>
                            <tr>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>POOR (1)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>FAIR (2)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>TARGET (3)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>GOOD (4)</strong></label></th>
                                <th colspan = '1' class="text-center"><label style="width:100px;"><strong>EXCELLENT (5)</strong></label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">SECRETARIAL</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Secretarial assistance tp deputy / Division Assist to liases with all clients on presentations & meetings. Manage Calendar Deputy Chief.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">30</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Manage schedule Deputy Chief and comply all the task</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">30</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">PEOPLE</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To deliver service to complete</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Identified and proposed solution for issue related to clerical works at CSHE division Liase with all staffs on office matters. Displayed good behavior and able to plan a tasks within the given timeline.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To make sure all the task given by staff not delayed and be completed.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">3</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Documentation</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Administration works. Compile all the related document for Deputy review and signature. Distribute all related dic to PIC. Organize record incoming & outgoing correspondence / email.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To make sure all the process running smoothly and do not delayed.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">20</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">4</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Development and Operational Excellent</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Submit monthly summary Claims for the division to HR within the time required.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">10</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Not following day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Following date</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">Same day</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Maintain timeline required.</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">10</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Monitoring</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Summary staff attendance and staff movement</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Weekly report</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">6</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">INNOVATION & LEARNING</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">% of Staff Meet 32 hrs Training</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Employee Development (Staff attended 32hrs training)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">85</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">100</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Achieve target</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">7</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ENVIRONMENT & QUALITY</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">5S Program (Audit Rating)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Rating</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (score 60% and below)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">1 Star Rating (min score 61%)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 71% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">2 Star Rating (min 81% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">3 Star Rating (min 91% score)</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Ongoing</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">8</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">ADMINISTRATION</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Arrangement of Meeting</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">To arrange meeting internal / external parties</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">50</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">70</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">80</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">90</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">95</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">Smoothly on process</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-left">good</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center">5</td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                                <td style = "vertical-align:top" colspan ='1' class="text-center"></td>
                            </tr>
                            <tr>
                                <td colspan="4" rowspan="1" style="vertical-align: top;" class="text-right"><strong>TOTAL WEIGHTAGE</strong></td>
                                <td style="vertical-align: top;" class="text-center"><strong>100</strong></td>
                                <td colspan="9" rowspan="1" style="vertical-align: top;" class="text-right"><strong>TOTAL WEIGHTAGE</strong></td>
                                <td colspan="1" rowspan="1" style="vertical-align: top;" class="text-center"><strong>100</strong></td>
                                <td colspan="1" rowspan="1" style="vertical-align: top;" class="text-right"><strong>FINAL KPI RATING</strong></td>
                                <td colspan="1" rowspan="1" style="vertical-align: top;" class="text-center"><label><strong>0.00</strong></label></td>
                            </tr>
                            <tr>
                                <td colspan ='17'>No Data</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
				                                             
            </div>
        </div>
    </div>
    <!-- /Year - End Review -->

    <!-- Competency -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion-group5">Competency</a>
            </h6>
        </div>
        <div id="accordion-group5" class="panel-collapse collapse">
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table class="table table-bordered table-framed table-striped">
                        <thead>
                            <tr>
                                <th class="left"><label style="width:400px;"><strong>Behaviour</strong></label></th>
                                <th class="text-center"><label style="width:110px;"><strong>Behaviour Details</strong></label></th>
                                <th class="text-center"><label style="width:100px;"><strong>Level</strong></label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="left" >Problem Assessment</td>
                                <td class="text-center">
                                    <span class="label label-danger" data-toggle="modal" data-target="#modal_small" style="cursor: pointer;">Level 1</span>
                                    <span class="label label-success" data-toggle="modal" data-target="#modal_small" style="cursor: pointer;">Level 2</span>
                                    <span class="label label-primary" data-toggle="modal" data-target="#modal_small" style="cursor: pointer;">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="left">Continous Learning</td>
                                <td class="text-center">
                                    <span class="label label-danger">Level 1</span>
                                    <span class="label label-success">Level 2</span>
                                    <span class="label label-primary">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="left">Work Planning & Organising</td>
                                <td class="text-center">
                                    <span class="label label-danger">Level 1</span>
                                    <span class="label label-success">Level 2</span>
                                    <span class="label label-primary">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="left">Achievement Orientation</td>
                                <td class="text-center">
                                    <span class="label label-danger">Level 1</span>
                                    <span class="label label-success">Level 2</span>
                                    <span class="label label-primary">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="left">Communication</td>
                                <td class="text-center">
                                    <span class="label label-danger">Level 1</span>
                                    <span class="label label-success">Level 2</span>
                                    <span class="label label-primary">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="left">Teamwork</td>
                                <td class="text-center">
                                    <span class="label label-danger">Level 1</span>
                                    <span class="label label-success">Level 2</span>
                                    <span class="label label-primary">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="left">Attention To Detail</td>
                                <td class="text-center">
                                    <span class="label label-danger">Level 1</span>
                                    <span class="label label-success">Level 2</span>
                                    <span class="label label-primary">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="left">Service Orientation</td>
                                <td class="text-center">
                                    <span class="label label-danger">Level 1</span>
                                    <span class="label label-success">Level 2</span>
                                    <span class="label label-primary">Level 3</span>
                                </td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">Competency Score Obtained</td>
                                <td class="text-center"><label style="width:100px;"><strong>0</strong></label></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right"><i class="icon-info22 text-danger-600" data-toggle="modal" data-target="#modal_theme_bg_custom" style="cursor: pointer;"></i> Final Competency Rating</td>
                                <td class="text-center"><label style="width:100px;"><strong>0.00</strong></label></td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- Small modal -->
                    <div id="modal_small" class="modal fade" tabindex="-1">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header bg-danger">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">Level 1</h5>
                                </div>

                                <div class="modal-body">
                                    <h6 class="text-semibold"><i class="icon-book"></i> &nbsp;Behaviour Details</h6>
                                    <p style="text-align: justify;">
                                        Reviews the current situation in contrast to the standard condition and requires assistance to recognise the abnormality.
                                        Uses basic technical understanding to relate to issues at hand.
                                        Rely solely on the descriptions of others to understand why the current situation exists.
                                        Clusters relevant information in a generally organised manner.
                                        Describes the problem and encourages others to recommend solution to the problem.
                                    </p>
                                </div>

                                <hr>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /small modal -->

                    <!-- Custom background color -->
                    <div id="modal_theme_bg_custom" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content bg-teal-300">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">Final Competency Rating</h5>
                                </div>

                                <div class="modal-body">
                                    <table style="width:100%;border:1px solid #fff;" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Competency Score Obtained<var></td>
                                                <td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>c<var></td>
                                                <td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>X</var></td>
                                                <td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>5<var></td>
                                                <td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>=<var></td>
                                                <td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>t<var></td>
                                            </tr>
                                            <tr>
                                                <td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Total Competency Score<var></td>
                                                <td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>24<var><br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <small>
                                        <var>Legend:<var>
                                        <p>
                                            <var>c : Competency Score Obtained<var>
                                            <br>
                                            <var>t : Total Competency Score<var>
                                        </p>
                                    </small>
                                </div>
                                <hr>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link text-white" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /custom background color -->
                </div>

            </div>
        </div>
    </div>
    <!-- /Competency -->

    <!-- Merit -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion-group6">Merit</a>
            </h6>
        </div>
        <div id="accordion-group6" class="panel-collapse collapse">
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table class="table table-bordered table-framed table-striped">
                        <thead>
                            <tr>
                                <th width="80%" class="text-center">List of Achievements<br>(Please refer to the Merit Guidelines)<br>1 score per participants (Max total score = 10)</th>
                                <th class="text-center">Total Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>List of achievements</td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td>List of achievements</td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td>List of achievements</td>
                                <td class="text-center">0</td>
                            </tr>
                            <tr>
                                <td class="text-right">Merit Score Obtained</td>
                                <td class="text-center"><strong>0</strong></td>
                            </tr>
                            <tr>
                                <td class="text-right"><i class="icon-info22 text-danger-600" data-toggle="modal" data-target="#modal_theme_bg_custom_merit" style="cursor: pointer;"></i> Final Merit Score</td>
                                <td class="text-center"><strong>0.00</strong></td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- Custom background color -->
                    <div id="modal_theme_bg_custom_merit" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content bg-teal-300">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">Final Merit Score</h5>
                                </div>

                                <div class="modal-body">
                                    <table style="width:100%;border:1px solid #fff;" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Merit Score Obtained<var></td>
                                                <td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>m<var></td>
                                                <td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>=<var></td>
                                                <td colspan="1" rowspan="2" style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>t<var></td>
                                            </tr>
                                            <tr>
                                                <td style="height:35px;border:1px solid #fff;padding-left:5px;"><var>Total Merit Score<var></td>
                                                <td style="height:35px;border:1px solid #fff;width:40px;" class="text-center"><var>10<var><br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <small>
                                        <var>Legend:<var>
                                        <p>
                                            <var>m : Merit Score Obtained<var>
                                            <br>
                                            <var>t : Total Merit Score<var>
                                        </p>
                                    </small>
                                </div>
                                <hr>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link text-white" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /custom background color -->
                </div>

                <!-- Basic tables title -->
                <h6 class="content-group text-semibold">
                    OVERALL PERFORMANCE ASSESSMENT
                    <small class="display-block">OVERALL PERFORMANCE RATING <code>Result</code> <code>Comment</code></small>
                </h6>
                <!-- /basic tables title -->

                <!-- Input group addons -->
                <div class="table-responsive">
                    <table class="table table-bordered table-framed table-striped">
                        <tbody>
                            <tr>
                                <td colspan = '1' class="text-left">Final KPI Rating</td>
                                <td colspan = '1' class="text-center">0.00</td>
                                <td colspan = '1' class="text-center"><var>X<var>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weightage (%)</td>
                                <td colspan = '1' class="text-center">70%</td>
                                <td colspan = '1' class="text-center">=</td>
                                <td colspan = '1' class="text-center">0.00</td>
                            </tr>
                            <tr>
                                <td colspan = '1' class="text-left">Final Competency Rating</td>
                                <td colspan = '1' class="text-center">0.00</td>
                                <td colspan = '1' class="text-center"><var>X<var>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weightage (%)</td>
                                <td colspan = '1' class="text-center">30%</td>
                                <td colspan = '1' class="text-center">=</td>
                                <td colspan = '1' class="text-center">0.00</td>
                            </tr>
                            <tr>
                                <td colspan = '5' class="text-left">&nbsp;</td>
                                <td colspan = '1' class="text-center">0.00</td>
                            </tr>
                            <tr>
                                <td colspan = '1' class="text-left">Merit</td>
                                <td colspan = '1' class="text-center">0.00</td>
                                <td colspan = '1' class="text-center"><var>X<var>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weightage (%)</td>
                                <td colspan = '1' class="text-center">10%</td>
                                <td colspan = '1' class="text-center">=</td>
                                <td colspan = '1' class="text-center">0.00</td>
                            </tr>
                            <tr>
                                <td colspan = '5' class="text-left">&nbsp;</td>
                                <td colspan = '1' class="text-center">0.00</td>
                            </tr>
                            <tr>
                                <td colspan = '5' class="text-right">Overall Performance Rating</td>
                                <td colspan = '1' class="text-center"><strong>1</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /input group addons -->
            </div>
        </div>
    </div>
    <!-- /Merit -->

</div>
<!-- /basic accordion -->

<!-- Basic tables title -->
<h6 class="content-group text-semibold">
	APPRAISER
	<small class="display-block">Approval and comments for <code>KPI Planning</code> <code>2019</code></small>
</h6>
<!-- /basic tables title -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
    <div class="panel-body">

        <?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal wizard clearfix', 'novalidate'=>'novalidate'));?>
        <form class="form-horizontal" action="#">

            <fieldset class="content-group">
                <div class="row">
                    <div class="col-md-6">
                        <label  class="control-label col-lg-12">Appraisee Comments<span class="text-danger">*</span></label>
                        <div class="col-lg-12">
                            <textarea rows="5" cols="5" class="form-control" placeholder="Enter appraisee comments" disabled="disabled" style="text-align:justify;">This past year my team worked on the main areas that were very critical to the company. The key social responsibility committee and the customer service process. Our team emerged the best in successfully completing the challenging task and doing so promptly</textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label  class="control-label col-lg-12">Appraiser Comments<span class="text-danger">*</span></label>
                        <div class="col-lg-12">
                            <textarea rows="5" cols="5" class="form-control" placeholder="Enter appraiser comments" style="text-align:justify;"></textarea>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-6">
                        <label  class="control-label col-lg-12">Reviewer Comments<span class="text-danger">*</span></label>
                        <div class="col-lg-12">
                            <textarea rows="5" cols="5" class="form-control" placeholder="Reviewer comments will be available on the next enchancement." disabled="disabled" style="text-align:justify;"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label  class="control-label col-lg-12">Approval<span class="text-danger">*</span></label>
                        <div class="col-lg-12">
                            <select name="focus" class="form-control">
                                <option value="0">Please Select</option>
                                <option value="2">Approved</option>
                                <option value="1">Reject</option>
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>

            <br>
        
            <div class="text-right">
                <a class="btn btn-default position-right"
                    href="<?php echo $this->Html->url('/kpiapprovals/incumbentdata', true);?>">
                    Reset <i class="icon-spinner11 position-right"></i>
                </a>
                <a class="btn btn-success position-right" href="<?php echo $this->Html->url('/kpiapprovals/incumbentdata', true);?>">
                    Save <i class="icon-drawer-out position-right position-right"></i>
                </a>
                <button type="button" class="btn btn-danger position-right" data-toggle="modal" data-target="#modal_theme_custom">Sign and Submit <i class=" icon-pen position-right"></i></button>
            </div>

            <!-- Custom header color -->
            <div id="modal_theme_custom" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h6 class="modal-title">Sign</h6>
                        </div>

                        <div class="modal-body">
                            <p>Please key in Staff No and Password</p>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Staff No</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Enter staff no">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Password</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Enter password">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Cancel <i class="icon-cross"></i></button>
                            <button type="button" class="btn bg-danger">Sign <i class="icon-pen position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /custom header color -->

        </form>
    </div>
    <!-- /panel-body -->

    <!-- panel-footer -->
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-warning" href="<?php echo $this->Html->url('/kpiapprovals/staff', true);?>">
                    Back <i class="icon-arrow-left13 position-right"></i>
                </a>
            </span>
        </div>
    </div>
    <!-- /panel-footer -->

</div>
<!-- /Form horizontal -->
