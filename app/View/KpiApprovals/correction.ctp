<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">PMS</a></li>
			<li>List of PMS</li>
			<li class>Incumbent Data</li>
			<li class="active"><a href="#">Correction</a></li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">

	<?php
			echo $this->Session->flash();

			if(!empty($this->validationErrors['Kpi']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['Kpi'] as $errors)
							{
								echo '<ul>';
								foreach ($errors as $error)
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}

		?>
		<?php echo $this->Form->create('Kpi', array('class'=>'form-horizontal wizard clearfix', 'novalidate'=>'novalidate'));?>
		<div class="steps clearfix">
            <ul role="tablist">
                <li role="tab" class="current" aria-disabled="false" aria-selected="true">
                    <a href="#">
                        <span class="number">1</span>
                        INCUMBENT DATA
                    </a>
                </li>
                <li role="tab" class="disabled" aria-disabled="true">
                    <a href="#">
                        <span class="number">2</span>
                        PART 1 : KPI PLANNING
                    </a>
                </li>
                <li role="tab" class="disabled" aria-disabled="true">
                    <a href="#">
                        <span class="number">3</span>
                        PART 2 : MID - YEAR REVIEW
                    </a>
                </li>
                <li role="tab" class="disabled" aria-disabled="true">
                    <a href="#" >
                        <span class="number">4</span>
                        PART 3 : YEAR - END REVIEW
                    </a>
                </li>
            </ul>
        </div>
			<fieldset class="content-group">
				<legend class="text-bold">Please Tick What Need To Be Corrected</legend>
				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox checkbox-switchery">
							<label>
									<input type="checkbox" class="switchery">
									Joining Date
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox checkbox-switchery">
							<label>
									<input type="checkbox" class="switchery">
									Position
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox checkbox-switchery">
							<label>
									<input type="checkbox" class="switchery">
									Division
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox checkbox-switchery">
							<label>
									<input type="checkbox" class="switchery">
									Department
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox checkbox-switchery">
							<label>
									<input type="checkbox" class="switchery">
									Section
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="checkbox checkbox-switchery">
							<label>
									<input type="checkbox" class="switchery">
									Unit
							</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="checkbox checkbox-switchery">
							<label>
									<input type="checkbox" class="switchery">
									Supervisor
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Remarks:<span class="text-danger">*</span></label>
			        <textarea rows="5" cols="5" placeholder="enter your remarks here" class="form-control"></textarea>
				</div>
			</fieldset>
            <div class="text-right">
			<a class="btn btn-warning position-right" href="#" data-toggle="modal" data-target="#modal_submit">
					Submit
					<i class="icon-drawer-out position-right"></i>
				</a>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/kpis/correction', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
			<!-- Warning modal -->
<div id="modal_submit" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-drawer-out"></span> Submit?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Confirm</h6>
				<p>Please make sure this information is correct. Once it is submitted it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-warning" >
				Yes
				</button>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->



	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/kpis/add', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->
