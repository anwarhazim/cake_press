<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Lookups</a></li>
			<li>Statuses</li>
			<li class="active">Edit KPI Status</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
	<?php
			echo $this->Session->flash();

			if(!empty($this->validationErrors['KpiStatus']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['KpiStatus'] as $errors)
							{
								echo '<ul>';
								foreach ($errors as $error)
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}

		?>

        <?php echo $this->Form->create('KpiStatus', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Status</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-success legitRipple">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?= $this->Html->url('/KpiStatuses/edit/'.$key, true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?= $this->Html->url('/KpiStatuses/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->
