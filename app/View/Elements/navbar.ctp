<div class="navbar navbar-default navbar-fixed-top header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo $this->html->url('/', true); ?>">
            <?php echo $this->Html->image('/img/logo_prasarana_light_white.png', array('alt' => 'PRESS | Prasarana Employee Self-Service')); ?>
        </a>

        <ul class="nav navbar-nav visible-xs-block">
            <li>
                <a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a>
            </li>
            <li>
                <a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li>
                <a class="sidebar-control sidebar-main-toggle hidden-xs">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <div class="navbar-right">
            <p class="navbar-text">Welcome, <?php echo $session['Staff']['name']; ?>!</p>
            <ul class="nav navbar-nav">
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-drawer-in"></i>
                        <span class="visible-xs-inline-block position-right">List of Update</span>
                        <span id="update_count"></span>
                    </a>
                    
                    <div class="dropdown-menu dropdown-content width-350">
                        <div class="dropdown-content-heading">
                            List of Update
                        </div>

                        <ul id="updates" class="media-list dropdown-content-body">
                            <li class="media">
                                <div class="media-body">
                                    <span class="text-muted">No changes has been made...</span>
                                </div>
                            </li>
                        </ul>

                        <div class="dropdown-content-footer">
                            <a href="<?php echo $this->html->url('/Applicants/updates', true);?>" data-popup="tooltip" title="Show List of Update"><i class="icon-menu display-block"></i></a>
                        </div>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-bell2"></i>
                        <span class="visible-xs-inline-block position-right">Notifications</span>
                        <span id="notification_count"></span>
                    </a>
                    
                    <div class="dropdown-menu dropdown-content width-350">
                        <div class="dropdown-content-heading">
                            Notifications
                        </div>

                        <ul id="notifications" class="media-list dropdown-content-body">
                            <li class="media">
                                <div class="media-body">
                                    <span class="text-muted">No notification...</span>
                                </div>
                            </li>
                        </ul>

                        <div class="dropdown-content-footer">
                            <a href="<?php echo $this->html->url('/', true);?>" data-popup="tooltip" title="All notifications"><i class="icon-menu display-block"></i></a>
                        </div>
                    </div>
                </li>
                <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-gear"></i> <span class="visible-xs-inline-block position-right"> Options</span></a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="<?php echo $this->html->url('/Users/avatar', true); ?>"><i class="icon-image2"></i> Update Profile Picture</a></li>
						<li><a href="<?php echo $this->html->url('/Users/password', true); ?>"><i class="icon-key"></i> Change Password</a></li>
						<li class="divider"></li>
						<li><a href="#" data-toggle="modal" data-target="#modal_logout"><i class="icon-switch2"></i> Log Out</a></li>
					</ul>
				</li>					
            </ul>
        </div>
    </div>
</div>