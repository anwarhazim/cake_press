<div class="sidebar sidebar-main sidebar-fixed">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    <a href="<?php echo $this->html->url('/Users/profile', true); ?>">
                        <?php

                            if(!empty($session['User']['avatar']))
                            {
                                echo $this->Html->image($session['Path']['url'] . 'avatars/' . $session['User']['avatar'], array('alt' => 'PRESS | Prasarana Employee Self-Service'));
                            }
                            else
                            {
                                echo $this->Html->image('/img/users/default-avatar.jpg', array('alt' => 'PRESS | Prasarana Employee Self-Service'));
                            }

                        ?>
                    </a>
                    <h6><?php echo $session['Staff']['name']; ?></h6>
                    <span class="text-size-small"><?php echo $session['OrganisationType']['name']; ?></span>
                    <?php
                        if(!empty($session['Roles']))
                        {
                            $roles = "";
                            foreach ($session['Roles'] as $role)
                            {
                                if($roles == "")
                                {
                                    $roles = $role['name'];
                                }
                                else
                                {
                                    $roles .= " / ".$role['name'];
                                }
                            }
                            echo '<h6 class="text-size-small">( ' .$roles. ' )</h6>';
                        }
                    ?>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li>
                        <a href="<?php echo $this->html->url('/', true); ?>">
                            <i class="icon-home4"></i> <span>Home Page</span>
                        </a>
                    </li>
                    <?php echo $session['Sidebar']; ?>
                    <!--
                    <li class="navigation-header"><span>HCD Welfare & Benefits Team</span> <i class="icon-menu" title="HCD Welfare & Benefits Team"></i></li>
                    <li>
                        <a href="#"><i class="icon-lifebuoy"></i> <span>Medical</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/Medicals/index', true);?>">List of Medical</a>
                            </li>
                            <li>
                                <a href="<?php echo $this->html->url('/Batchs/index', true);?>">List of Batch</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Family Pass -->
                    <!--
                    <li class="navigation-header"><span>HCD Welfare & Benefits Team</span> <i class="icon-menu" title="HCD Welfare & Benefits Team"></i></li>
                    <li>
                        <a href="#"><i class="icon-vcard"></i> <span>Family Pass</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/FamilyPasses/index', true);?>">List of Family Task</a>
                            </li>
                        </ul>
                    </li>
                    <!-- /Family Pass -->
                    <!-- /Main -->
                    <!-- HRIS -->
                    <!--
                    <li class="navigation-header"><span>HRIS</span> <i class="icon-menu" title="HRIS"></i></li>
                    <li>
                        <a href="#"><i class="icon-task"></i> <span>Tasks</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/Hris/index', true);?>">List of Task</a>
                            </li>
                        </ul>
                    </li>
                    <!-- /HRIS -->

					<!-- BAUCER -->
                    <!--
					<li class="navigation-header"><span>VOUCHERS</span> <i class="icon-menu" title="VOUCHERS"></i></li>
							<li>
							<a href="#"><i class="icon-task"></i><span>Married Voucher</a>
								<ul>
									<li>
									<a href="<?php echo $this->html->url('/MarriedVouchers/index', true);?>">List of Married Voucher</a>
									<a href="<?php echo $this->html->url('/MarriedVouchers/add', true);?>">xxx</a>
									</li>
                        		</ul>
                    		</li>
							<li>
							<a href="#"><i class="icon-task"></i><span>Children Voucher</a>
								<ul>
									<li>
									<a href="<?php echo $this->html->url('/ChildrenVouchers/index', true);?>">List of Children Voucher</a>
									<a href="<?php echo $this->html->url('/ChildrenVouchers/add', true);?>">xxx</a>
									</li>
                        		</ul>
                    		</li>
							<li>
							<a href="#"><i class="icon-task"></i><span>Death Voucher</a>
								<ul>
									<li>
									<a href="<?php echo $this->html->url('/DeathVoucher/index', true);?>">List of Death Voucher</a>
									<a href="<?php echo $this->html->url('/DeathVoucher/add', true);?>">xxx</a>
									</li>
                        		</ul>
                    		</li>
                    <!-- /BAUCER -->

                    <!-- Configuration -->
                    <!--
                    <li class="navigation-header"><span>Administrator Settings</span> <i class="icon-menu" title="Administrator Setting"></i></li>
					<li>
                        <a href="#"><i class="icon-key"></i> <span>Users</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/Users/index', true);?>">List of User</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-users"></i> <span>Staffs</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/Staffs/index', true);?>">List of Staff</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-tree7"></i> <span>Organizations</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/Organisations/index', true);?>">List of Business Unit</a>
                            </li>
                            <li>
                                <a href="<?php echo $this->html->url('/Organisations/coordination', true);?>">Organizational Structure</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stack2"></i> <span>Modules</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/Moduls/index', true);?>">List of Module</a>
                            </li>
                            <li>
                                <a href="<?php echo $this->html->url('/Moduls/coordination', true);?>">Manage Modules</a>
                            </li>
                            <li>
                                <a href="<?php echo $this->html->url('/Moduls/roles', true);?>">Manage Permissisons</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stack2"></i> <span>Events</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/Events/index', true);?>">List of Event</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-sphere"></i> <span>Lookups</span></a>
                        <ul>
							<li>
							<a href="#">IC Colour</a>
								<ul>
									<li><a href="<?php echo $this->html->url('/Iccolors/index', true);?>">List of IC Colour</a></li>
								</ul>
							</li>
							<li>
							<a href="#">Relationship</a>
								<ul>
									<li><a href="<?php echo $this->html->url('/Relationships/index', true);?>">List of Relationship</a></li>
								</ul>
							</li>
							<li>
							<a href="#">Gender</a>
								<ul>
									<li><a href="<?php echo $this->html->url('/Genders/index', true);?>">List of Gender</a></li>
								</ul>
							</li>
							<li>
                                <a href="#">Nationality</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Nationals/index', true);?>">List of Nationality</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Roles</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Roles/index', true);?>">List of Role</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Status</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Statuses/index', true);?>">List of Status</a></li>
                                    <li><a href="<?php echo $this->html->url('/KpiStatuses/index', true);?>">List of KPI Status</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">States</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/States/index', true);?>">List of State</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Religions</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Religions/index', true);?>">List of Religion</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Marital Statuses</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/MaritalStatuses/index', true);?>">List of Marital Status</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Locations</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Locations/index', true);?>">List of Location</a></li>
                                </ul>
                            </li>
							<li>
                                <a href="#">Banks</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Banks/index', true);?>">List of Bank</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Business Types</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/OrganisationTypes/index', true);?>">List of Business Type</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Organizational Hierarchy</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/OrganisationCategories/index', true);?>">List of Organizational Hierarchy</a></li>
                                </ul>
                            </li>
							<li>
                                <a href="#">Job Types</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/JobTypes/index', true);?>">List of Job Type</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Job Grades</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/JobGrades/index', true);?>">List of Job Grade</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Job Designations</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/JobDesignations/index', true);?>">List of Designation</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Institutions</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Institutions/index', true);?>">List of Institution</a></li>
                                </ul>
                            </li>
							<li>
                                <a href="#">Qualifications</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Qualifications/index', true);?>">List of Qualification</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Majors</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Majors/index', true);?>">List of Major</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Category Events</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/CategoryEvents/index', true);?>">List of Category Event</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Projects</a>
                                <ul>
                                    <li><a href="<?php echo $this->html->url('/Projects/index', true);?>">List of Project</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    -->
                    <!-- /Configuration -->
                    
                    <!-- PMS -->
                    <!-- <li class="navigation-header"><span>Performance Management System</span> <i class="icon-menu" title="Performance Management System"></i></li>
                    <li>
                        <a href="<?php echo $this->html->url('/Kpis/index', true);?>"><i class="icon-magazine"></i> <span>Personal KPI</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-file-check"></i> <span>KPI Approval</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo $this->html->url('/KpiApprovals/index', true);?>">List of KPI's</a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- /PMS -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
