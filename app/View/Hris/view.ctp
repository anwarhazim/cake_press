<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Request Updates</li>
            <li class="active"> History of Request Update </li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Detailed task -->
<div class="row">
	<div class="col-lg-9">
		<!-- Task overview -->
		<div class="panel panel-flat">
			<div class="panel-heading mt-5">
				<h5 class="panel-title">#<?php echo $detail['Applicant']['reference_no']; ?>: Request Update from <?php echo $detail['CreatedBy']['name']; ?></h5>
				<div class="heading-elements">
					<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"><?php echo $detail['Status']['name']; ?> <b><i class="icon-alarm-check"></i></b></a>
				</div>
			</div>

			<div class="panel-body">
			<?php echo $this->Session->flash(); ?>
				<div class="form-group">
					<div class="col-lg-3">
						<a href="#" class="display-inline-block content-group-sm">
							<?php echo $detail['ApplyBy']['avatar']; ?>
						</a>
					</div>
					<div class="col-lg-9">
						<div class=" col-lg-12">
							<div class="form-group">
								<label class="text-semibold col-lg-6">Name</label>
								<span class="pull-right-sm col-lg-6"><?php echo $detail['CreatedBy']['name']; ?></span>
							</div>
						</div>
						<div class=" col-lg-12">
							<div class="form-group">
								<label class="text-semibold col-lg-6">Email</label>
								<span class="pull-right-sm col-lg-6"><?php echo $detail['CreatedBy']['email']; ?></span>
							</div>
						</div>
						<div class=" col-lg-12">
							<div class="form-group">
								<label class="text-semibold col-lg-6">Company Name</label>
								<span class="pull-right-sm col-lg-6"><?php echo $detail['CreatedBy']['Organisation']; ?></span>
							</div>
						</div>
						<div class=" col-lg-12">
							<div class="form-group">
								<label class="text-semibold col-lg-6">Location</label>
								<span class="pull-right-sm col-lg-6"><?php echo $detail['CreatedBy']['Location']; ?></span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class=" col-lg-12">
						<h6 class="text-semibold">List of Request Update</h6>
						<div class="table-responsive content-group">
							<table class="table table-framed">
								<thead>
									<tr>
										<th style="width: 20px;">#</th>
										<th class="col-xs-3">Reference No.</th>
										<th>Module</th>
										<th class="col-xs-2 text-center">Status</th>
										<th class="col-xs-3">Last updated</th>
										<?php
											if($detail['Applicant']['status_id'] > 3)
											{
										?>
											<th>Action</th>
										<?php
											}
										?>
									</tr>
								</thead>
								<tbody>
								<?php
									$i = 1;
									foreach ($details as $applicant) 
									{
								?>
									<tr>
										<td>
											<?php echo $i; ?>
										</td>
										<td>
											<?php echo $applicant['Applicant']['reference_no']; ?>
										</td>
										<td>
											<?php echo $applicant['Modul']['name']; ?>
										</td>
										<td class="text-center">
											<?php
												$label = 'label-primary';
												switch ($applicant['Status']['id']) 
												{
													case 5:
														$label = 'label-danger';
														break;
													case 7:
														$label = 'label-info';
														break;
													case 8:
														$label = 'label-danger';
														break;
													case 10:
														$label = 'label-success';
														break;
												}
											?>
											<spam class='label <?php echo $label?>'><?php echo $applicant['Status']['name']; ?></spam>
										</td>
										<td>
											<div class="input-group input-group-transparent">
												<div class="input-group-addon"><i class="icon-calendar22 position-left"></i></div>
												<span class="text-semibold"><?php echo $applicant['Applicant']['modified']; ?></span>
											</div>
										</td>
										<?php
											if($detail['Applicant']['status_id'] > 3)
											{
										?>
											<td>
											<a alt="view" href="<?php echo $this->Html->url('/Hris/details/'.$applicant['Applicant']['id']); ?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="View details">
												<spam class=" icon-file-text2">
												</spam>
											</a>
											</td>
										<?php
											}
										?>
									</tr>
								<?php
									$i++;
									}
								?>
								</tbody>
							</table>
						</div>
					<?php
						if($team_checked == true && $assignto == true)
						{
							if($correction_team == true)
							{
						?>
							<div class="text-right">
								<a class="btn btn-danger legitRipple" href="#" data-toggle="modal" data-target="#modal_correction_team">
								Send Correction
								</a>
							</div>
						<?php
							}

							if($verified == true)
							{
						?>
							<div class="text-right">
								<a class="btn btn-info legitRipple" href="#" data-toggle="modal" data-target="#modal_verified">
								Verified
								</a>
							</div>
						<?php
							}
						}


						if($approved == true && $detail['Applicant']['status_id'] < 8 )
						{
						?>
							<div class="text-right">
							<a class="btn btn-success legitRipple" href="#" data-toggle="modal" data-target="#modal_approve">
								Approve
							</a>
							</div>
						<?php
						}

						if($correction_leader == true && $hrleader == true && $detail['Applicant']['status_id'] < 8)
						{
						?>
							<div class="text-right">
								<a class="btn btn-danger legitRipple" href="#" data-toggle="modal" data-target="#modal_correction_leader">
								Send Correction
								</a>
							</div>
						<?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<span class="heading-text">
						<a class="btn btn-warning" href="<?php echo $this->Html->url('/Hris/index', true);?>">
							Back <i class="icon-arrow-left13 position-right"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-3">
		<!-- Timer -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-watch position-left"></i> Apply Date Details</h6>
			</div>

			<div class="panel-body">
				<ul class="timer-weekdays mb-10">
					<?php
						foreach ($days as $day_key => $day_value) 
						{
							if($detail['Applicant']['day_by_text'] == $day_key)
							{
					?>
							<li class="active"><a href="#" class="label label-danger"><?php echo $day_value; ?></a></li>
					<?php
							}
							else
							{
					?>
							<li><a href="#" class="label label-default"><?php echo $day_value; ?></a></li>
					<?php
							}
						}
					?>
				</ul>

				<ul class="timer mb-10">
					<li>
						<span>
							<strong>
								<?php echo $detail['Applicant']['day_by_num']?> /
							</strong>	
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Applicant']['month']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Applicant']['year']?>
							</strong>
						</span>
					</li>
				</ul>

				<ul class="timer mb-10">
					<li>
						<?php echo $detail['Applicant']['hour']?> <span>Hour</span>
					</li>
					<li class="dots">:</li>
					<li>
						<?php echo $detail['Applicant']['minute']?> <span>Minute</span>
					</li>
					<li class="dots"></li>
					<li>
						<?php echo $detail['Applicant']['format']?> <span>Period</span>
					</li>
				</ul>
			</div>
		</div>
		<!-- /timer -->

		<?php
			if($detail['Applicant']['status_id'] <= 3)
			{
		?>
		<!-- Task settings -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-task position-left"></i> Task Assign</h6>
			</div>

			<div class="panel-body">
				<form action="#">
					<div class="form-group">
						<div class="checkbox checkbox-switchery checkbox-right switchery-xs">
							<label class="display-block">
								<Strong><?php echo $session['Staff']['name']; ?></Strong>, Do you want to take this task?
							</label>	
						</div>
					</div>

					<div class="row">

						<div class="col-md-12">
							<a class="btn btn-primary btn-sm btn-block" href="#" data-toggle="modal" data-target="#modal_accept">
								Accept
							</a>
						</div>

					</div>
				</form>
			</div>
		</div>
		<!-- /task settings -->
		<?php
			}
			else
			{
		?>
			<!-- User details (with sample pattern) -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> Assigned To</h6>
				</div>
				<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<a href="#" class="display-inline-block content-group-sm">
						<?php echo $detail['AssignTo']['avatar']; ?>
					</a>

					<div class="content-group-sm">
						<h5 class="text-semibold no-margin-bottom">
							<?php echo $detail['AssignTo']['name'] ?>
						</h5>
					</div>

					<ul class="timer mb-10">
						<li>
							<span>
								<strong>
									<?php echo $detail['AssignTo']['day_by_num']?> /
								</strong>	
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['AssignTo']['month']?> /
								</strong>
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['AssignTo']['year']?>
								</strong>
							</span>
						</li>
					</ul>

					<ul class="timer mb-10">
						<li>
							<?php echo $detail['AssignTo']['hour']?> <span>Hour</span>
						</li>
						<li class="dots">:</li>
						<li>
							<?php echo $detail['AssignTo']['minute']?> <span>Minute</span>
						</li>
						<li class="dots"></li>
						<li>
							<?php echo $detail['AssignTo']['format']?> <span>Period</span>
						</li>
					</ul>
				</div>

			</div>
			<!-- /user details (with sample pattern) -->
		<?php
			}

			if($detail['Applicant']['status_id'] >= 7)
			{
		?>
			<!-- User details (with sample pattern) -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> Verified</h6>
				</div>
				<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<a href="#" class="display-inline-block content-group-sm">
						<?php echo $detail['VerifiedBy']['avatar']; ?>
					</a>

					<div class="content-group-sm">
						<h5 class="text-semibold no-margin-bottom">
							<?php echo $detail['VerifiedBy']['name'] ?>
						</h5>
					</div>

					<ul class="timer mb-10">
						<li>
							<span>
								<strong>
									<?php echo $detail['VerifiedBy']['day_by_num']?> /
								</strong>	
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['VerifiedBy']['month']?> /
								</strong>
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['VerifiedBy']['year']?>
								</strong>
							</span>
						</li>
					</ul>

					<ul class="timer mb-10">
						<li>
							<?php echo $detail['VerifiedBy']['hour']?> <span>Hour</span>
						</li>
						<li class="dots">:</li>
						<li>
							<?php echo $detail['VerifiedBy']['minute']?> <span>Minute</span>
						</li>
						<li class="dots"></li>
						<li>
							<?php echo $detail['VerifiedBy']['format']?> <span>Period</span>
						</li>
					</ul>
				</div>

			</div>
			<!-- /user details (with sample pattern) -->
		<?php
			}
		
			if($detail['Applicant']['status_id'] == 10)
			{
		?>
			<!-- User details (with sample pattern) -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> Approved</h6>
				</div>
				<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<a href="#" class="display-inline-block content-group-sm">
						<?php echo $detail['ApprovedBy']['avatar']; ?>
					</a>

					<div class="content-group-sm">
						<h5 class="text-semibold no-margin-bottom">
							<?php echo $detail['ApprovedBy']['name'] ?>
						</h5>
					</div>

					<ul class="timer mb-10">
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['day_by_num']?> /
								</strong>	
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['month']?> /
								</strong>
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['year']?>
								</strong>
							</span>
						</li>
					</ul>

					<ul class="timer mb-10">
						<li>
							<?php echo $detail['ApprovedBy']['hour']?> <span>Hour</span>
						</li>
						<li class="dots">:</li>
						<li>
							<?php echo $detail['ApprovedBy']['minute']?> <span>Minute</span>
						</li>
						<li class="dots"></li>
						<li>
							<?php echo $detail['ApprovedBy']['format']?> <span>Period</span>
						</li>
					</ul>
				</div>

			</div>
			<!-- /user details (with sample pattern) -->
		<?php
			}
		?>
	
	</div>
</div>
<!-- /detailed task -->

<!-- Warning modal -->
<div id="modal_accept" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-task"></span> Accept?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Are you sure?</h6>
				<p>Do you want to accept this task? Just needed to double check.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Hris/accept/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->

<!-- Warning modal -->
<div id="modal_correction_team" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-envelop5"></span> Send Correction?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Are you sure?</h6>
				<p>Please review before you send. Once it is submitted it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-danger" href="<?php echo $this->Html->url('/Hris/send_correction_team/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->

<!-- Warning modal -->
<div id="modal_correction_leader" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-envelop5"></span> Send Correction?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Are you sure?</h6>
				<p>Please review before you send. Once it is submitted it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-danger" href="<?php echo $this->Html->url('/Hris/send_correction_leader/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->

<!-- Warning modal -->
<div id="modal_verified" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-envelop5"></span> Verify?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Are you sure?</h6>
				<p>Please check before giving verification. Once it is verified it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-info" href="<?php echo $this->Html->url('/Hris/send_verified/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->

<!-- Warning modal -->
<div id="modal_approve" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-envelop5"></span> Approve?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Are you sure?</h6>
				<p>Please check before giving approval. Once it is approved it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-success" href="<?php echo $this->Html->url('/Hris/send_approve/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->

