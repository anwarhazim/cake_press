<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="<?php echo $this->html->url('/Educations/index', true);?>">Educations</a></li>
			<li class="active">View Education</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
		<?php 
			echo $this->Session->flash(); 

			if(!empty($this->validationErrors['Education']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['Education'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}
		
		?>

        <?php echo $this->Form->create('Education', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Education</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Date Started</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('duration_from', array(
									'class'=>'form-control start_date',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Date Completed</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('duration_to', array(
									'class'=>'form-control end_date', 
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">School /<br/>Institution Name</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('institution', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Qualification</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('qualification_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'options'=>$qualifications,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Major </label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('major_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'options'=>$majors,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Result <br/>(CGPA/Grade/Etc.) </label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('result', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<label class="control-label col-lg-12">Attachment</label>
				<div class="col-md-12">
					<div class="row">
						<?php
							for ($i=0; $i < count($detail['Attachment']) ; $i++) 
							{ 
						?>
							<div class="col-lg-2 col-sm-6">
								<div class="thumbnail">
									<div class="thumb">
										<img src="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" />
										<div class="caption-overflow">
											<span>
												<a href="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View"><i class="icon-eye"></i></a>
											</span>
										</div>
									</div>

									<div class="caption">
										<h6 class="no-margin"><?php echo $detail['Attachment'][$i]['name'] ?></h6>
									</div>
								</div>
							</div>
						<?php
							}							
						?>
					</div>
				</div>
			</fieldset>
			<?php
				if($update == true)
				{
			?>
            <div class="text-right">
				<a class="btn btn-warning legitRipple" href="<?php echo $this->Html->url('/Educations/edit/'.$key, true);?>">
					Edit <i class="icon-pencil5 position-right"></i>
				</a>
			</div>
			<?php
				}
			?>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Educations/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->