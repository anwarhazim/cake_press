<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="<?php echo $this->html->url('/Educations/index', true);?>">Educations</a></li>
			<li class="active">Edit Education</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
		<?php 
			echo $this->Session->flash(); 

			if(!empty($this->validationErrors['Education']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['Education'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}
		
		?>

        <?php echo $this->Form->create('Education', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Education</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Date Started <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('duration_from', array(
									'class'=>'form-control start_date',
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Date Completed <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('duration_to', array(
									'class'=>'form-control end_date', 
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">School /<br/>Institution Name <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('institution_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Qualification <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('qualification_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'options'=>$qualifications,
									'empty'=>'PLEASE SELECT...',
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Major <span class="text-danger">*</span></label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('major_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'options'=>$majors,
									'empty'=>'PLEASE SELECT...',
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Result <span class="text-danger">*</span> <br/>(CGPA/Grade/Etc.) </label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('result', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text'
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<label class="control-label col-lg-12">Attachment <span class="text-danger">*</span></label>
				<div class="col-md-12">
					<div class="row">
						<?php
							for ($i=0; $i < count($detail['Attachment']) ; $i++) 
							{ 
						?>
							<div class="col-lg-2 col-sm-6">
								<div class="thumbnail">
									<div class="thumb">
										<img src="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" />
										<div class="caption-overflow">
											<span>
												<a href="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View">
													<i class="icon-eye"></i>
												</a>
												<?php
													echo $this->html->link(
														'<spam class="icon-bin"></spam>', 
														array('controller' => 'Educations', 'action' => 'attachment', $detail['Attachment'][$i]['id']),
														array('class' => 'btn border-white text-white btn-flat btn-icon btn-rounded', 'title' => 'Delete', 'confirm' => 'Are you sure you want to delete this attachment?', 'escape' => false)
													);
												?>
											</span>
										</div>
									</div>

									<div class="caption">
										<h6 class="no-margin"><?php echo $detail['Attachment'][$i]['name'] ?></h6>
									</div>
								</div>
							</div>
						<?php
							}							
						?>
					</div>
				</div>
				<div class="col-md-12">
					<?php 
						echo $this->Form->input('attachments.', array(
							'class'=>'form-control file-input',
							'label'=> false,
							'error'=>false,
							'type'=>'file',
							'multiple'=>'multiple',
							'data-preview-file-type'=>'any',
							'data-show-upload'=>false,
							'showRemove'=>false,
							'data-show-caption'=>false,
							'accept'=>'image/*',
							'maxFileSize'=>'2MB'
							)
						); 
					?>
					<span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-success legitRipple">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Educations/edit/'.$key, true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Educations/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->