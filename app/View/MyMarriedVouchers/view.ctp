<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Vouchers</li>      
            <li class="active"> List of Married Voucher</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Detailed task -->
<div class="row">
	<div class="col-lg-9">
		<!-- Task overview -->
		<div class="panel panel-flat">
			<div class="panel-heading mt-5">
				<h5 class="panel-title">#<?php echo $detail['Applicant']['reference_no']; ?> : Married Voucher</h5>
				<div class="heading-elements">
					<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"><?php echo $detail['Status']['name']; ?> <b><i class="icon-alarm-check"></i></b></a>
				</div>
			</div>

			<div class="panel-body">
			<?php
				echo $this->Session->flash();

				if(!empty($this->validationErrors['Voucher']))
				{
				?>
					<div role="alert" class="alert alert-danger">
							<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<?php
								foreach ($this->validationErrors['Voucher'] as $errors)
								{
									echo '<ul>';
									foreach ($errors as $error)
									{
										echo '<li>'.h($error).'</li>';
									}
									echo '</ul>';
								}
							?>
					</div>
				<?php
				}
			?>
				<?php echo $this->Form->create('Voucher', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
				<fieldset class="content-group">
					<legend class="text-bold">Staff Information</legend>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Name</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['name']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Staff No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['staff_no']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Organisation</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('organisation_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'options'=>$organisations,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['organisation_id']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<legend class="text-bold">Spouse Information</legend>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-lg-4">Name</label>
								<div class="col-lg-8">
									<?php 
										echo $this->Form->input('name', array(
											'class'=>'form-control',
											'label'=> false,
											'error'=>false,
											'type'=>'text',
											'disabled'=>'disabled',
											'value'=>$detail['Spouse']['name']
											)
										); 
									?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-lg-4">Marriage Date</label>
								<div class="col-lg-8">
									<?php 
										echo $this->Form->input('marriage_date', array(
											'class'=>'form-control date', 
											'label'=> false,
											'error'=>false,
											'type'=>'text',
											'disabled'=>'disabled',
											'value'=>$detail['Spouse']['marriage_date']
											)
										); 
									?>
								</div>
							</div>
						</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">IC No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic_new', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['ic_new']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Passport No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('passport_no', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['passport_no']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Religion</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('religion_id', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'options'=>$religions,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['religion_id']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Race</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('race_id', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'options'=>$races,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['race_id']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-lg-4">Nationality</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('national_id', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'options'=>$nationals,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['national_id']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Occupation</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('occupation', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['occupation']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Employer </label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('employer', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['employer']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Employer Address </label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('employer_address', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'textarea',
										'disabled'=>'disabled',
										'value'=>$detail['Spouse']['employer_address']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<?php echo $this->Form->end(); ?>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<span class="heading-text">
						<a class="btn btn-warning" href="<?php echo $this->Html->url('/MyMarriedVouchers/index', true);?>">
							Back <i class="icon-arrow-left13 position-right"></i>
						</a>
					</span>
					<div class="pull-right">
						<a target="_blank" class="btn btn-info bg-blue" href="<?php echo $this->Html->url('/MyMarriedVouchers/prints/'.$key, true);?>">
							Print <i class="icon-printer position-right"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /task overview -->

	</div>

	<div class="col-lg-3">

		<!-- Timer -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-watch position-left"></i>  Apply Date Details</h6>
			</div>

			<div class="panel-body">
				<ul class="timer-weekdays mb-10">
					<?php
						foreach ($days as $day_key => $day_value)
						{
							if($detail['Voucher']['day_by_text'] == $day_key)
							{
					?>
							<li class="active"><a href="#" class="label label-danger"><?php echo $day_value; ?></a></li>
					<?php
							}
							else
							{
					?>
							<li><a href="#" class="label label-default"><?php echo $day_value; ?></a></li>
					<?php
							}
						}
					?>
				</ul>

				<ul class="timer mb-10">
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['day_by_num']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['month']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['year']?>
							</strong>
						</span>
					</li>
				</ul>

				<ul class="timer mb-10">
					<li>
						<?php echo $detail['Voucher']['hour']?> <span>Hour</span>
					</li>
					<li class="dots">:</li>
					<li>
						<?php echo $detail['Voucher']['minute']?> <span>Minute</span>
					</li>
					<li class="dots"></li>
					<li>
						<?php echo $detail['Voucher']['format']?> <span>Period</span>
					</li>
				</ul>
			</div>
		</div>
		<!-- /timer -->
		<?php

			if($detail['Voucher']['status_id'] > 2)
			{
		?>
			<!-- User details (with sample pattern) -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> <?php echo $detail['Status']['name']; ?></h6>
				</div>
				<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<a href="#" class="display-inline-block content-group-sm">
						<?php echo $detail['ApprovedBy']['avatar']; ?>
					</a>

					<div class="content-group-sm">
						<h5 class="text-semibold no-margin-bottom">
							<?php echo $detail['ApprovedBy']['name'] ?>
						</h5>
					</div>

					<ul class="timer mb-10">
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['day_by_num']?> /
								</strong>	
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['month']?> /
								</strong>
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['year']?>
								</strong>
							</span>
						</li>
					</ul>

					<ul class="timer mb-10">
						<li>
							<?php echo $detail['ApprovedBy']['hour']?> <span>Hour</span>
						</li>
						<li class="dots">:</li>
						<li>
							<?php echo $detail['ApprovedBy']['minute']?> <span>Minute</span>
						</li>
						<li class="dots"></li>
						<li>
							<?php echo $detail['ApprovedBy']['format']?> <span>Period</span>
						</li>
					</ul>
				</div>

			</div>
			<!-- /user details (with sample pattern) -->
		<?php
			}
		?>
	</div>
</div>
<!-- /detailed task -->