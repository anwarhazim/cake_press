<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Lookups</a></li>
			<li><a href="#">Projects</a></li>
			<li><a href="#">List of Events</a></li>
			<li class="active">Add New Event</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Event', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Category</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-1">Category <span class="text-danger">*</span></label>
						<div class="col-lg-11">
							<?php 
								echo $this->Form->input('category_event_id', array(
									'class'=>'form-control',
									'label'=> false,
									'options'=>$categoryevents,
									'empty'=>'PLEASE SELECT...',
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Start Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('event_start_date', array(
									'class'=>'form-control start_date',
									'label'=> false,
									'error'=> false,
									'type'=>'text'
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">End Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('event_end_date', array(
									'class'=>'form-control end_date',
									'label'=> false,
									'error'=> false,
									'type'=>'text'
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-success legitRipple">
					Save <i class="icon-floppy-disk position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Events/add', true);?>">
					Reset <i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Events/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->
