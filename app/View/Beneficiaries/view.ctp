<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="#">Next of Kin</a></li>
			<li class="active">View Next of Kin</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
	<?php 
		echo $this->Session->flash(); 

		if(!empty($this->validationErrors['Beneficiary']))
		{
		?>
			<div role="alert" class="alert alert-danger">
					<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<?php
						foreach ($this->validationErrors['Beneficiary'] as $errors) 
						{
							echo '<ul>';
							foreach ($errors as $error) 
							{
								echo '<li>'.h($error).'</li>';
							}
							echo '</ul>';
						}
					?>
			</div>
		<?php
		}
	?>

        <?php echo $this->Form->create('Beneficiary', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
		<fieldset class="content-group">
				<legend class="text-bold">Next of Kin</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Name</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Relationship</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('relationship', array(
									'class'=>'form-control',
									'label'=> false,
									'type'=>'text',
									'error'=>false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Contact No.</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('contact_no', array(
									'class'=>'form-control',
									'label'=> false,
									'type'=>'text',
									'error'=>false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Address</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('house_address', array(
									'class'=>'form-control',
									'label'=> false,
									'type'=>'textarea',
									'error'=>false,
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<a class="btn btn-warning legitRipple" href="<?php echo $this->Html->url('/Beneficiaries/edit/'.$key, true);?>">
					Edit <i class="icon-pencil5 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?= $this->Html->url('/Beneficiaries/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->