<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Vouchers</li>      
            <li class="active"> List of Khairat Kematian/ Death Contribution Claim</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Detailed task -->
<div class="row">
	<div class="col-lg-9">
		<!-- Task overview -->
		<div class="panel panel-flat">
			<div class="panel-heading mt-5">
				<h5 class="panel-title">#<?php echo $detail['Applicant']['reference_no']; ?> : Khairat Kematian/ Death Contribution Claim</h5>
				<div class="heading-elements">
					<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"><?php echo $detail['Status']['name']; ?> <b><i class="icon-alarm-check"></i></b></a>
				</div>
			</div>

			<div class="panel-body">
			<?php
				echo $this->Session->flash();

				if(!empty($this->validationErrors['Voucher']))
				{
				?>
					<div role="alert" class="alert alert-danger">
							<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<?php
								foreach ($this->validationErrors['Voucher'] as $errors)
								{
									echo '<ul>';
									foreach ($errors as $error)
									{
										echo '<li>'.h($error).'</li>';
									}
									echo '</ul>';
								}
							?>
					</div>
				<?php
				}
			?>
				<?php echo $this->Form->create('Voucher', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
				<fieldset class="content-group">
					<legend class="text-bold">Staff Information</legend>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Name</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['name']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Staff No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['staff_no']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Organisation</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('organisation_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'options'=>$organisations,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['organisation_id']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<legend class="text-bold">Khairat Kematian/Death Contribution Information</legend>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Name</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Death']['name']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">IC No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Death']['ic']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Relationship</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('relationship_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'options'=>$relationships,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled',
										'value'=>$detail['Death']['relationship_id']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Date of Death</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('date_of_death', array(
										'class'=>'form-control date', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Death']['date_of_death']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<?php echo $this->Form->end(); ?>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<span class="heading-text">
						<a class="btn btn-warning" href="<?php echo $this->Html->url('/MyDeathVouchers/index', true);?>">
							Back <i class="icon-arrow-left13 position-right"></i>
						</a>
					</span>

					<div class="pull-right">
						<a target="_blank" class="btn btn-info bg-blue" href="<?php echo $this->Html->url('/MyDeathVouchers/prints/'.$key, true);?>">
							Print <i class="icon-printer position-right"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /task overview -->

	</div>

	<div class="col-lg-3">

		<!-- Timer -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-watch position-left"></i>  Apply Date Details</h6>
			</div>

			<div class="panel-body">
				<ul class="timer-weekdays mb-10">
					<?php
						foreach ($days as $day_key => $day_value)
						{
							if($detail['Voucher']['day_by_text'] == $day_key)
							{
					?>
							<li class="active"><a href="#" class="label label-danger"><?php echo $day_value; ?></a></li>
					<?php
							}
							else
							{
					?>
							<li><a href="#" class="label label-default"><?php echo $day_value; ?></a></li>
					<?php
							}
						}
					?>
				</ul>

				<ul class="timer mb-10">
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['day_by_num']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['month']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['year']?>
							</strong>
						</span>
					</li>
				</ul>

				<ul class="timer mb-10">
					<li>
						<?php echo $detail['Voucher']['hour']?> <span>Hour</span>
					</li>
					<li class="dots">:</li>
					<li>
						<?php echo $detail['Voucher']['minute']?> <span>Minute</span>
					</li>
					<li class="dots"></li>
					<li>
						<?php echo $detail['Voucher']['format']?> <span>Period</span>
					</li>
				</ul>
			</div>
		</div>
		<!-- /timer -->
		<?php

			if($detail['Voucher']['status_id'] > 2)
			{
		?>
			<!-- User details (with sample pattern) -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> <?php echo $detail['Status']['name']; ?></h6>
				</div>
				<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<a href="#" class="display-inline-block content-group-sm">
						<?php echo $detail['ApprovedBy']['avatar']; ?>
					</a>

					<div class="content-group-sm">
						<h5 class="text-semibold no-margin-bottom">
							<?php echo $detail['ApprovedBy']['name'] ?>
						</h5>
					</div>

					<ul class="timer mb-10">
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['day_by_num']?> /
								</strong>	
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['month']?> /
								</strong>
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['year']?>
								</strong>
							</span>
						</li>
					</ul>

					<ul class="timer mb-10">
						<li>
							<?php echo $detail['ApprovedBy']['hour']?> <span>Hour</span>
						</li>
						<li class="dots">:</li>
						<li>
							<?php echo $detail['ApprovedBy']['minute']?> <span>Minute</span>
						</li>
						<li class="dots"></li>
						<li>
							<?php echo $detail['ApprovedBy']['format']?> <span>Period</span>
						</li>
					</ul>
				</div>

			</div>
			<!-- /user details (with sample pattern) -->
		<?php
			}
		?>
	</div>
</div>
<!-- /detailed task -->