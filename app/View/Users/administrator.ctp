<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li><a href="<?php echo $this->html->url('/Users', true);?>">Users</a></li>
			<li class="active">View User</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li>
					<a href="<?php echo $this->html->url('/Users/view/'.$key, true);?>">User Details</a>
				</li>
				<li class="active">
					<a href="<?php echo $this->html->url('/Users/administrator/'.$key, true);?>">Administrator</a>
				</li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active">
					<?php echo $this->Session->flash(); ?>

					<?php echo $this->Form->create('User', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
					<fieldset class="content-group">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label col-lg-2">Status  <span class="text-danger">*</span></label>
								<div class="col-lg-10">
									<?php 
										echo $this->Form->input('status_id', array(
											'class'=>'form-control',
											'label'=> false,
											'options'=>$statuses,
											'empty'=>'PLEASE SELECT...',
											)
										); 
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-2">
									Role <span class="text-danger">*</span>
								</label>
								<div class="col-lg-10">
									<?php 
										echo $this->Form->input('roles', array(
											'class'=>'checkbox', 
											'type' => 'select',
											'multiple' => 'checkbox', 
											'label'=>false,
											'required'=>'required',
											'options'=>$roles,
											'selected' => $role_selected,
											'div' => false,
											)
										);
									?>
								</div>
							</div>
						</div>
					</fieldset>
					<div class="text-right">
						<button type="submit" class="btn btn-success legitRipple">
							Save <i class="icon-floppy-disk position-right"></i>
						</button>

						<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Users/administrator/'.$key, true);?>">
							Reset <i class="icon-spinner11 position-right"></i>
						</a>
					</div>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Users/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>