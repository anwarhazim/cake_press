<!-- Password recovery -->
<?php echo $this->Form->create('User', array('novalidate'=>'novalidate')); ?>
    <div class="panel panel-body login-form">
        <?php 
            echo $this->Session->flash();

            if(!empty($this->validationErrors['User']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['User'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}
		?>

        <div class="text-center">
            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
            <h5 class="content-group">Password recovery <small class="display-block">You must choose a strong password.</small></h5>
        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('password', array('class'=>'form-control', 'placeholder'=>'New Password', 'label'=> false, 'error' => false)); ?>
            <div class="form-control-feedback">
                <i class="icon-lock2 text-muted"></i>
            </div>
        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('c_password', array('class'=>'form-control', 'placeholder'=>'Re-Enter Password', 'type' => 'password', 'label'=> false, 'error' => false)); ?>
            <div class="form-control-feedback">
                <i class="icon-lock2 text-muted"></i>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn bg-pink-400 btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
        </div>

        <div class="text-center">
			<a href="<?= $this->html->url('/', true);?>">Back</a>
		</div>
    </div>
<?php echo $this->Form->end(); ?>
<!-- /password recovery -->