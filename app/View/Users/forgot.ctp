<!-- Password recovery -->
<?php echo $this->Form->create('User', array('novalidate'=>'novalidate')); ?>
    <div class="panel panel-body login-form">
        <?php echo $this->Session->flash(); ?>

        <div class="text-center">
            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
            <h5 class="content-group">Password recovery <small class="display-block">Please enter your email and we'll send you instructions to reset your password.</small></h5>
        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('email', array('class'=>'form-control', 'placeholder'=>'Your email', 'label'=> false )); ?>
            <div class="form-control-feedback">
                <i class="icon-mail5 text-muted"></i>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn bg-pink-400 btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
        </div>

        <div class="text-center">
			<a href="<?= $this->html->url('/', true);?>">Back</a>
		</div>
    </div>
<?php echo $this->Form->end(); ?>
<!-- /password recovery -->