<!-- Toolbar -->
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li class="active">Update Profile Picture</li>
		</ul>
	</div>
</div>
<!-- /toolbar -->

<!-- User profile -->
<div class="row">
    <div class="col-lg-9">

        <?php 
			echo $this->Session->flash(); 

			if(!empty($this->validationErrors['User']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['User'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
            }
		
		?>

        <!-- Form horizontal -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php echo $this->Form->create('User', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
                <legend class="text-bold">Update Profile Picture</legend>
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="thumbnail no-padding" style="box-shadow:1px 1px 7px 1px #e6e6e6;">
                            <div class="thumb">
                                <?php 
                                    
                                    if(!empty($session['User']['avatar']))
                                    {
                                        echo $this->Html->image($path . $session['User']['avatar'], array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
                                    }
                                    else
                                    {
                                        echo $this->Html->image('/img/users/default-avatar.jpg', array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
                                    }
                                    
                                ?>
                                <div class="caption-overflow">
                                    <span>
                                        <?php 
                                        
                                        if(!empty($session['User']['avatar']))
                                        {
                                        ?>

                                            <a href="<?php echo $path . $session['User']['avatar'] ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View"><i class="icon-eye"></i></a>

                                        <?php }else{ ?>

                                            <a href="<?php echo $localhost . '/img/users/default-avatar.jpg' ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View"><i class="icon-eye"></i></a>

                                        <?php } ?>
                                    </span>
                                </div>
                            </div>

                            <div class="caption text-center">
                                <h6 class="text-semibold no-margin">
                                    <?php 
                                        echo $session['Staff']['name'];
                                        if(!empty($session['Roles']))
                                        {
                                            $roles = "";
                                            foreach ($session['Roles'] as $role) 
                                            {
                                                if($roles == "")
                                                {
                                                    $roles = $role['name'];
                                                }
                                                else
                                                {
                                                    $roles .= " / ".$role['name'];
                                                }
                                            }
                                            echo '<small class="display-block">( ' .$roles. ' )</small>';
                                        }
                                    ?>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <fieldset class="content-group">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <?php 
                                        echo $this->Form->input('avatar', array(
                                            'class'=>'form-control file-input',
                                            'label'=> false,
                                            'error'=>false,
                                            'type'=>'file',
                                            'data-preview-file-type'=>'any',
                                            'data-show-upload'=>false,
                                            'showRemove'=>false,
                                            'data-show-caption'=>false,
                                            'accept'=>'image/*',
                                            'maxFileSize'=>'2MB'
                                            )
                                        ); 
                                    ?>
                                    <span class="help-block">Only 'gif', 'bmp', 'jpeg', 'jpg' and 'png' files are allowed.</span>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-warning legitRipple">
                        Upload
                        <i class="icon-upload position-right"></i>
                    </button>

                    <a class="btn btn-default position-right"
                        href="<?php echo $this->Html->url('/Users/avatar', true);?>">
                        Reset
                        <i class="icon-spinner11 position-right"></i>
                    </a>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        <!-- /Form horizontal -->

    </div>

    <div class="col-lg-3">

        <!-- Navigation -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Navigation</h6>
            </div>

            <div class="list-group no-border no-padding-top">
                <a href="<?php echo $this->html->url('/Users/avatar', true); ?>" class="list-group-item"><i class="icon-image2"></i> Update Profile Picture</a>
                <a href="<?php echo $this->html->url('/Users/password', true); ?>" class="list-group-item"><i class="icon-key"></i> Change Password</a>
                <a href="#" class="list-group-item" data-toggle="modal" data-target="#modal_logout"><i class="icon-switch2"></i> Log Out</a>
            </div>
        </div>
        <!-- /navigation -->

    </div>
</div>
<!-- /user profile -->