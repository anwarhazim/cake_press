
<!-- Toolbar -->
<div class="navbar navbar-default navbar-component navbar-xs">
	<ul class="nav navbar-nav visible-xs-block">
		<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
	</ul>

	<div class="navbar-collapse collapse" id="navbar-filter">

		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-book"></i> User Manual<span class="visible-xs-inline-block position-right"> Options</span> <span class="caret"></span></a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-book3"></i> Married voucher</a></li>
						<li><a href="#"><i class="icon-book3"></i> Child voucher</a></li>
						<li><a href="#"><i class="icon-book3"></i> Family Pass</a></li>
						<li><a href="#"><i class="icon-book3"></i> Death Claims</a></li>
						<!-- <li><a href="#"><i class="icon-book3"></i> Death Claims</a></li> -->
						<li><a href="#"><i class="icon-book3"></i> Medical Card</a></li>
						<li><a href="#"><i class="icon-book3"></i> Payslip</a></li>
						<li><a href="#"><i class="icon-book3"></i> EA Form</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-link"></i> Related Links<span class="visible-xs-inline-block position-right"> Options</span> <span class="caret"></span></a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-link2"></i> ESS Portal</a></li>
						<li><a href="#"><i class="icon-link2"></i> Portal MyRapid</a></li>
						<li><a href="#"><i class="icon-link2"></i> Portal LRT3</a></li>
						<li><a href="#"><i class="icon-link2"></i> Rapid Penang</a></li>
						<li><a href="#"><i class="icon-link2"></i> Kelab Sukan Prasarana</a></li>
						<li><a href="#"><i class="icon-link2"></i> BAKIP</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- /toolbar -->

	<!-- User profile -->
	<div class="row">
		<div class="col-lg-9">

			<?php echo $this->Session->flash(); ?>
			<!-- Form horizontal -->
			<div class="panel panel-flat">
				<div class="panel-body">
					<?php echo $this->Form->create('Notification', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
					<fieldset class="content-group">
						<div class="form-group">
							<label class="control-label col-lg-2">Search</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('search', array(
										'class'=>'form-control',
										'placeholder'=>'Search subject or content...', 
										'label'=> false,
										'error'=> false,
										)
									); 
								?>
							</div>
						</div>
					</fieldset>

					<fieldset class="content-group">
						<div class="form-group">
							<label class="control-label col-lg-2">Start Date</label>
							<div class="col-lg-4">
								<?php 
									echo $this->Form->input('start_date', array(
										'class'=>'form-control start_date', 
										'label'=> false,
										'error'=> false,
										)
									); 
								?>
							</div>

							<label class="control-label col-lg-2">End Date</label>
							<div class="col-lg-4">
								<?php 
									echo $this->Form->input('end_date', array(
										'class'=>'form-control end_date', 
										'label'=> false,
										'error'=> false,
										)
									); 
								?>
							</div>
						</div>
					</fieldset>

					<div class="text-right">
						<button type="submit" class="btn btn-primary">
							Find 
							<i class="glyphicon glyphicon-search position-right"></i>
						</button>

						<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Users/profile', true);?>">
							Reset
							<i class="icon-spinner11 position-right"></i>
						</a>
					</div>

					<?php echo $this->Form->end(); ?>
				</div>
			</div>
			<!-- /Form horizontal -->

			<!-- Single line -->
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title">My Notifications</h6>
				</div>

				<div class="panel-toolbar panel-toolbar-inbox">
					<div class="navbar navbar-default">
						<ul class="nav navbar-nav visible-xs-block no-border">
							<li>
								<a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
									<i class="icon-circle-down2"></i>
								</a>
							</li>
						</ul>

						<div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">

							<div class="navbar-right">
								<ul class="pagination  pull-right">
									<?php 
										echo $this->Paginator->prev('' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
										echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
										echo $this->Paginator->next(__('>') . '', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="table-responsive">
					<table class="table table-inbox">
						<tbody data-link="row" class="rowlink">
							<?php
								if(!empty($details))
								{
									foreach ($details as $notification) 
									{
										$unread = "";
										if($notification['Notification']['is_read'] == 1)
										{
											$unread = "unread";
										}
										?>
										<tr class="<?php echo $unread; ?>">
											<td class="table-inbox-image rowlink-skip">
												<a href="<?php echo $this->Html->url('/Users/read/'.$notification['Notification']['id'], true);?>">
													<?php echo $notification['Sender']['avatar']; ?>
												</a>
											</td>
											<td class="table-inbox-name">
												<a href="#">
													<div  style="padding-left: 10px;" class="letter-icon-title text-default">
														<?php echo $notification['Sender']['name']; ?>
													</div>
												</a>
											</td>

											<td class="table-inbox-message">
												<div class="table-inbox-subject">
													<?php echo $notification['Notification']['subject']; ?>
												</div>
												<span class="table-inbox-preview">
													<?php echo $notification['Notification']['body']; ?>
												</span>
											</td>
											<td class="table-inbox-attachment">
												<!-- <i class="icon-attachment text-muted"></i> -->
											</td>
											<td class="table-inbox-time">
												<?php echo $notification['Notification']['created']; ?>
											</td>
										</tr>
										<?php
									}
								}
								else
								{

								}
							?>
						</tbody>
					</table>
				</div>

			</div>
			<!-- /single line -->


		</div>

		<div class="col-lg-3">

			<!-- User thumbnail -->
			<div class="thumbnail">
				<div class="thumb thumb-rounded thumb-slide">
					<?php 
					
						if(!empty($session['User']['avatar']))
						{
							echo $this->Html->image($path . 'avatars/' . $session['User']['avatar'], array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
						}
						else
						{
							echo $this->Html->image('/img/users/default-avatar.jpg', array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
						}
						
					?>
				</div>
			
				<div class="caption text-center">
					<h6 class="text-semibold no-margin">
						<?php 
							echo $session['Staff']['name'];
							if(!empty($session['Roles']))
							{
								$roles = "";
								foreach ($session['Roles'] as $role) 
								{
									if($roles == "")
									{
										$roles = $role['name'];
									}
									else
									{
										$roles .= " / ".$role['name'];
									}
								}
								echo '<small class="display-block">( ' .$roles. ' )</small>';
							}
						?>
					</h6>
				</div>
			</div>
			<!-- /user thumbnail -->


			<!-- Navigation -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Navigation</h6>
				</div>

				<div class="list-group no-border no-padding-top">
					<a href="<?php echo $this->html->url('/Users/avatar', true); ?>" class="list-group-item"><i class="icon-image2"></i> Update Profile Picture</a>
					<a href="<?php echo $this->html->url('/Users/password', true); ?>" class="list-group-item"><i class="icon-key"></i> Change Password</a>
					<a href="#" class="list-group-item" data-toggle="modal" data-target="#modal_logout"><i class="icon-switch2"></i> Log Out</a>
				</div>
			</div>
			<!-- /navigation -->

		</div>
	</div>
	<!-- /user profile -->
