<!-- Advanced login -->
<?php echo $this->Form->create('User', array('novalidate'=>'novalidate')); ?>
	<div class="panel panel-body login-form">
		<div class="text-center">
			<a href="<?php echo $this->Html->url('/', true);?>">
				<?php echo $this->Html->image('/img/logo_prasarana_small.png', array('alt' => 'PRESS | ', 'width'=>'190px', 'height'=>'38px')); ?>
			</a>
		</div>
		
		<div class="text-center">
			<div class="icon-object border-warning-400 text-warning-400"><i class="icon-reading"></i></div>
			<h5 class="content-group-lg">Log In To Your Account <small class="display-block">Enter your credentials</small></h5>
		</div>
		
		<?php echo $this->Session->flash(); ?>

		<div class="form-group has-feedback has-feedback-left">
			<?php echo $this->Form->input('username', array('class'=>'form-control', 'placeholder'=>'Staff No.', 'label'=> false )); ?>
			<div class="form-control-feedback">
				<i class="icon-user text-muted"></i>
			</div>
		</div>

		<div class="form-group has-feedback has-feedback-left">
			<?php echo $this->Form->password('password', array('class'=>'form-control', 'placeholder'=>'Password', 'label'=> false )); ?>
			<div class="form-control-feedback">
				<i class="icon-lock2 text-muted"></i>
			</div>
		</div>

		<div class="form-group">
			<button type="submit" class="btn bg-success-400 btn-block legitRipple">Login <i class="icon-circle-right2 position-right"></i></button>
		</div>

		<div class="text-center">
			<a href="<?= $this->html->url('/Users/forgot', true);?>">Forgot Password?</a>
		</div>
	</div>
<?php echo $this->Form->end(); ?>
<!-- /advanced login -->