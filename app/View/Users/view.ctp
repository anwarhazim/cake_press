<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li><a href="<?php echo $this->html->url('/Users', true);?>">Users</a></li>
			<li class="active">View User</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="<?php echo $this->html->url('/Users/view/'.$key, true);?>">User Details</a>
				</li>
				<li>
					<a href="<?php echo $this->html->url('/Users/administrator/'.$key, true);?>">Administrator</a>
				</li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active">
					<?php echo $this->Session->flash(); ?>

					<?php echo $this->Form->create('Staff', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
					<fieldset class="content-group">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-lg-4">Name</label>
								<div class="col-lg-8">
									<?php 
										echo $this->Form->input('name', array(
											'class'=>'form-control',
											'label'=> false,
											'disabled'=>$disabled
											)
										); 
									?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-lg-4">Staff No.</label>
								<div class="col-lg-8">
									<?php 
										echo $this->Form->input('staff_no', array(
											'class'=>'form-control', 
											'label'=> false,
											'disabled'=>$disabled
											)
										); 
									?>
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset class="content-group">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-lg-4">IC No.</label>
								<div class="col-lg-8">
									<?php 
										echo $this->Form->input('ic_new', array(
											'class'=>'form-control', 
											'maxlength'=>12,
											'label'=> false,
											'disabled'=>$disabled
											)
										); 
									?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-lg-4">Email</label>
								<div class="col-lg-8">
									<?php 
										echo $this->Form->input('email', array(
											'class'=>'form-control', 
											'label'=> false,
											'disabled'=>$disabled
											)
										); 
									?>
								</div>
							</div>
						</div>
					</fieldset>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Users/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>