
<!-- Toolbar -->
<div class="navbar navbar-default navbar-component navbar-xs">
	<ul class="nav navbar-nav visible-xs-block">
		<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
	</ul>

	<div class="navbar-collapse collapse" id="navbar-filter">

		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-book"></i> User Manual<span class="visible-xs-inline-block position-right"> Options</span> <span class="caret"></span></a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-book3"></i> Married voucher</a></li>
						<li><a href="#"><i class="icon-book3"></i> Child voucher</a></li>
						<li><a href="#"><i class="icon-book3"></i> Family Pass</a></li>
						<li><a href="#"><i class="icon-book3"></i> Death Claims</a></li>
						<!-- <li><a href="#"><i class="icon-book3"></i> Death Claims</a></li> -->
						<li><a href="#"><i class="icon-book3"></i> Medical Card</a></li>
						<li><a href="#"><i class="icon-book3"></i> Payslip</a></li>
						<li><a href="#"><i class="icon-book3"></i> EA Form</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-link"></i> Related Links<span class="visible-xs-inline-block position-right"> Options</span> <span class="caret"></span></a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-link2"></i> ESS Portal</a></li>
						<li><a href="#"><i class="icon-link2"></i> Portal MyRapid</a></li>
						<li><a href="#"><i class="icon-link2"></i> Portal LRT3</a></li>
						<li><a href="#"><i class="icon-link2"></i> Rapid Penang</a></li>
						<li><a href="#"><i class="icon-link2"></i> Kelab Sukan Prasarana</a></li>
						<li><a href="#"><i class="icon-link2"></i> BAKIP</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- /toolbar -->

	<!-- User profile -->
	<div class="row">
		<div class="col-lg-9">
			<?php echo $this->Session->flash(); ?>

			<!-- Single mail -->
			<div class="panel panel-white">
				<!-- Mail details -->
				<div class="media stack-media-on-mobile mail-details-read">
					<a href="#" class="media-left">
						<?php echo $detail['Sender']['avatar']; ?>
					</a>

					<div class="media-body">
						<h6 class="media-heading"><?php echo $detail['Notification']['subject']; ?></h6>
						<div class="letter-icon-title text-semibold"><?php echo $detail['Sender']['name']; ?> <a href="#">&lt;<?php echo $detail['Sender']['email']; ?>&gt;</a></div>
					</div>
				</div>
				<!-- /mail details -->

				<!-- Mail container -->
				<div class="mail-container-read">
					<?php echo $detail['Notification']['body']; ?> 
				</div>
				<!-- /mail container -->

				<!-- Attachments -->
				<div class="mail-attachments-container">
					<h6 class="text-muted text-size-small mail-attachments-heading">DISCLAIMER:</h6>
					<p class="text-muted text-size-mini">This e-mail and any attachment ("Message") are intended only for the use of the recipient(s) named above and may contain confidential information. You are hereby notified that the taking of any action in reliance upon, or any review, retransmission, dissemination, distribution, printing or copying of this Message or any part thereof by anyone other than the intended recipient(s) is prohibited. If you have received this Message in error, you should delete this Message immediately and advise the sender by return e-mail.</p>
					<p class="text-muted text-size-mini">Any opinion, view and/or other information in this Message which do not relate to the official business of PRASARANA MALAYSIA BERHAD shall not be deemed given nor endorsed by PRASARANA MALAYSIA BERHAD. PRASARANA MALAYSIA BERHAD is not responsible for any activity that might be considered to be an illegal and/or improper use of e-mail. The recipient should check this Message for the presence of viruses. PRASARANA MALAYSIA BERHAD accepts no liability for any damage caused by any virus transmitted by this Message.</p>
				</div>
				<!-- /attachments -->
				<div class="panel-footer">
					<div class="heading-elements">
						<span class="heading-text">
							<a href="<?php echo $this->html->url('/Users/profile', true); ?>" class="btn btn-warning legitRipple">
								Back <i class="icon-arrow-left13 position-right"></i>
							</a>
						</span>
					</div>
				</div>
			</div>
			<!-- /single mail -->
		</div>

		<div class="col-lg-3">

			<!-- User thumbnail -->
			<div class="thumbnail">
				<div class="thumb thumb-rounded thumb-slide">
					<?php 
						
						if(!empty($session['User']['avatar']))
						{
							echo $this->Html->image($path . 'avatars/' . $session['User']['avatar'], array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
						}
						else
						{
							echo $this->Html->image('/img/users/default-avatar.jpg', array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
						}
						
					?>
				</div>
			
				<div class="caption text-center">
					<h6 class="text-semibold no-margin">
						<?php 
							echo $session['Staff']['name'];
							if(!empty($session['Roles']))
							{
								$roles = "";
								foreach ($session['Roles'] as $role) 
								{
									if($roles == "")
									{
										$roles = $role['name'];
									}
									else
									{
										$roles .= " / ".$role['name'];
									}
								}
								echo '<small class="display-block">( ' .$roles. ' )</small>';
							}
						?>
					</h6>
				</div>
			</div>
			<!-- /user thumbnail -->


			<!-- Navigation -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Navigation</h6>
				</div>

				<div class="list-group no-border no-padding-top">
					<a href="#" class="list-group-item"><i class="icon-image2"></i> Update Profile Picture</a>
					<a href="<?php echo $this->html->url('/Users/password', true); ?>" class="list-group-item"><i class="icon-key"></i> Change Password</a>
					<a href="#" class="list-group-item" data-toggle="modal" data-target="#modal_logout"><i class="icon-switch2"></i> Log Out</a>
				</div>
			</div>
			<!-- /navigation -->

		</div>
	</div>
	<!-- /user profile -->
