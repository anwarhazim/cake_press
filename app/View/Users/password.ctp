
<!-- Toolbar -->
<div class="page-header page-header-default">
    <div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li class="active">Change Password</li>
		</ul>
	</div>
</div>
<!-- /toolbar -->

	<!-- User profile -->
	<div class="row">
		<div class="col-lg-9">

        <?php 
            echo $this->Session->flash();

            if(!empty($this->validationErrors['User']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['User'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}
		?>

			<!-- Form horizontal -->
			<div class="panel panel-flat">
				<div class="panel-body">
					<?php echo $this->Form->create('User', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
					<legend class="text-bold">Change Password</legend>
					<fieldset class="content-group">
						<div class="form-group">
							<label class="control-label col-lg-2">Old Password</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('current_password', array(
                                        'class'=>'form-control',
                                        'type'=>'password',
										'placeholder'=>'Your current password', 
										'label'=> false,
										'error'=> false,
										)
									); 
								?>
							</div>
						</div>
					</fieldset>

                    <fieldset class="content-group">
						<div class="form-group">
							<label class="control-label col-lg-2"> New Password</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('password', array(
                                        'class'=>'form-control',
                                        'type'=>'password',
										'placeholder'=>'Your new password', 
										'label'=> false,
										'error'=> false,
										)
									); 
								?>
							</div>
						</div>
					</fieldset>

                    <fieldset class="content-group">
						<div class="form-group">
							<label class="control-label col-lg-2">Re-enter Password</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('c_password', array(
                                        'class'=>'form-control',
                                        'type'=>'password',
										'placeholder'=>'Your re-enter password', 
										'label'=> false,
										'error'=> false,
										)
									); 
								?>
							</div>
						</div>
					</fieldset>

					<div class="text-right">
						<button type="submit" class="btn btn-warning legitRipple">
							Update 
							<i class="icon-pencil5 position-right"></i>
						</button>

						<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Users/password', true);?>">
							Reset
							<i class="icon-spinner11 position-right"></i>
						</a>
					</div>

					<?php echo $this->Form->end(); ?>
				</div>
			</div>
			<!-- /Form horizontal -->

		</div>

		<div class="col-lg-3">

			<!-- User thumbnail -->
			<div class="thumbnail">
				<div class="thumb thumb-rounded thumb-slide">
					<?php 
                                    
						if(!empty($session['User']['avatar']))
						{
							echo $this->Html->image($path . 'avatars/' . $session['User']['avatar'], array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
						}
						else
						{
							echo $this->Html->image('/img/users/default-avatar.jpg', array('alt' => 'PRESS | Prasarana Employee Self-Service')); 
						}
						
					?>
				</div>
			
				<div class="caption text-center">
					<h6 class="text-semibold no-margin">
						<?php 
							echo $session['Staff']['name'];
							if(!empty($session['Roles']))
							{
								$roles = "";
								foreach ($session['Roles'] as $role) 
								{
									if($roles == "")
									{
										$roles = $role['name'];
									}
									else
									{
										$roles .= " / ".$role['name'];
									}
								}
								echo '<small class="display-block">( ' .$roles. ' )</small>';
							}
						?>
					</h6>
				</div>
			</div>
			<!-- /user thumbnail -->


			<!-- Navigation -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Navigation</h6>
				</div>

				<div class="list-group no-border no-padding-top">
					<a href="<?php echo $this->html->url('/Users/avatar', true); ?>" class="list-group-item"><i class="icon-image2"></i> Update Profile Picture</a>
					<a href="<?php echo $this->html->url('/Users/password', true); ?>" class="list-group-item"><i class="icon-key"></i> Change Password</a>
					<a href="#" class="list-group-item" data-toggle="modal" data-target="#modal_logout"><i class="icon-switch2"></i> Log Out</a>
				</div>
			</div>
			<!-- /navigation -->

		</div>
	</div>
	<!-- /user profile -->
