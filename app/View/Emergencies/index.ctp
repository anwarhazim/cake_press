<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li class="active"><a href="#">Emergency Contact</a></li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Emergency', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
            <fieldset class="content-group">
			    <legend class="text-bold">Search</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-1">Search</label>
						<div class="col-lg-11">
							<?php 
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'placeholder'=>'Name', 
									'label'=> false,
									'error'=> false,
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Start Date</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('start_date', array(
									'class'=>'form-control start_date', 
									'label'=> false,
									'error'=> false,
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">End Date</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('end_date', array(
									'class'=>'form-control end_date', 
									'label'=> false,
									'error'=> false,
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-primary">
					Find 
					<i class="glyphicon glyphicon-search position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Emergencies/index', true);?>">
					Reset
					<i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
</div>
<!-- /Form horizontal -->

<!-- Bordered panel body table -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">List of Emergency Contact</h5>
	</div>
	<div class="panel-body">
		<?php
			$counter = $this->Paginator->counter(array('format' =>'{:start}'));
		?>
		<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="5%" class="text-center">No.</th>
						<th>Name</th>
						<th width="20%">Relationship</th>
						<th width="15%">Contact No.</th>
						<th width="20%" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if($details)
						{
							foreach ($details as $detail) 
							{
					?>
						<tr>
							<th class="text-center">
								<?php echo $counter; ?>
							</th>
							<td>
								<?php echo $detail['Emergency']['name'] ?>
							</td>
							<td>
								<?php echo $detail['Emergency']['relationship'] ?>
							</td>
							<td>
								<?php echo $detail['Emergency']['contact_no'] ?>
							</td>
							<td class="text-center">
								<a alt="view" href="<?php echo $this->Html->url('/Emergencies/view/'.$detail['Emergency']['id']); ?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="View details">
									<spam class=" icon-file-text2">
									</spam>
								</a>

								<a alt="edit" href="<?php echo $this->Html->url('/Emergencies/edit/'.$detail['Emergency']['id']); ?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Edit details">
									<spam class="glyphicon glyphicon-pencil">
									</spam>
								</a>

								<?php
									echo $this->html->link(
										'<spam class="icon-bin"></spam>', 
										array('controller' => 'Emergencies', 'action' => 'delete', $detail['Emergency']['id']),
										array('class' => 'btn bg-grey-300 btn-icon btn-rounded legitRipple', 'title'=> 'Delete record','confirm' => 'Are you sure you want to delete this record?', 'escape' => false)
									);
								?>
							</td>
						</tr>
					<?php
							$counter++;
							}
						}
						else
						{
					?>
						<tr>
							<td colspan="5">
								No data...
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<br/>
		<p class="pull-right">
			<?php echo $this->Paginator->counter(array(
                                                    'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                            {:count} total, start {:start}, end at {:end}'
                                                    ));
			?>
		</p>
	</div>
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
                <a class="btn btn-primary" href="<?php echo $this->Html->url('/Emergencies/add/', true);?>">
                    Add New Emergency Contact  <i class="icon-plus3 position-right"></i>
                </a>
			</span>
			<ul class="pagination  pull-right">
				<?php 
					echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
					echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
				?>
			</ul>
		</div>
	</div>
</div>
<!-- /bordered panel body table -->