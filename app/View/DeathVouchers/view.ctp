<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Vouchers</li>      
            <li class="active"> List of Khairat Kematian/ Death Contribution</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Detailed task -->
<div class="row">
	<div class="col-lg-9">
		<!-- Task overview -->
		<div class="panel panel-flat">
			<div class="panel-heading mt-5">
				<h5 class="panel-title">#<?php echo $detail['Applicant']['reference_no']; ?> : Request from Khairat Kematian/ Death Contribution</h5>
				<div class="heading-elements">
					<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"><?php echo $detail['Status']['name']; ?> <b><i class="icon-alarm-check"></i></b></a>
				</div>
			</div>

			<div class="panel-body">
			<?php
				echo $this->Session->flash();

				if(!empty($this->validationErrors['Voucher']))
				{
				?>
					<div role="alert" class="alert alert-danger">
							<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<?php
								foreach ($this->validationErrors['Voucher'] as $errors)
								{
									echo '<ul>';
									foreach ($errors as $error)
									{
										echo '<li>'.h($error).'</li>';
									}
									echo '</ul>';
								}
							?>
					</div>
				<?php
				}
			?>
			<?php echo $this->Form->create('Death', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
				<fieldset class="content-group">
					<legend class="text-bold">Staff Information</legend>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Name</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['name']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Staff No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['staff_no']
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Organisation</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('organisation_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'options'=>$organisations,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled',
										'value'=>$detail['Staff']['organisation_id']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<legend class="text-bold">Khairat Kematian/Death Contribution Information</legend>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Name</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled'
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">IC No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic', array(
										'class'=>'form-control', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled'
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Relationship</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('relationship_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'options'=>$relationships,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>'disabled'
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Date of Death</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('date_of_death', array(
										'class'=>'form-control date', 
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled'
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<!--
				<fieldset class="content-group">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Cause of Death</label>
							<div class="col-lg-10">
								<?php 
									echo $this->Form->input('cause_of_death', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'textarea',
										'disabled'=>'disabled'
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				-->
				<fieldset class="content-group">
					<label class="control-label col-lg-12">Attachment</label>
					<div class="col-md-12">
						<div class="row">
							<?php
								for ($i=0; $i < count($detail['Attachment']) ; $i++) 
								{ 
							?>
								<div class="col-lg-2 col-sm-6">
									<div class="thumbnail">
										<div class="thumb">
											<img src="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" />
											<div class="caption-overflow">
												<span>
													<a href="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View"><i class="icon-eye"></i></a>
												</span>
											</div>
										</div>

										<div class="caption">
											<h6 class="no-margin"><?php echo $detail['Attachment'][$i]['name'] ?></h6>
										</div>
									</div>
								</div>
							<?php
								}							
							?>
						</div>
					</div>
				</fieldset>
				<?php echo $this->Form->end(); ?>
				<?php echo $this->Form->create('Voucher', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
				<fieldset class="content-group">
					<legend class="text-bold">Voucher Information</legend>
					<?php
							echo $this->Form->input('id', array(
								'class'=>'form-control',
								'label'=> false,
								'type'=>'hidden',
								)
							);
					?>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Reference No.</label>
							<div class="col-lg-10">
							<?php 
									echo $this->Form->input('reference_no', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>'disabled',
										'value'=>$detail['Applicant']['reference_no']
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-12">Note 
								<?php
									if($detail['Voucher']['status_id'] < 10)
									{
								?>
									<span class="text-danger">*</span>
								<?php
									}
								?>
							</label>
							<div class="col-lg-12">
							<?php 
									echo $this->Form->input('note', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'textarea',
										'disabled'=>$disabled
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<?php
					if(empty($disabled))
					{
				?>
				<div class="text-right">
					<?php
						echo $this->Form->button('Save As Draft', array(
							'type'=>'submit',
							'value'=>'draf',
							'name'=>'Submit',
							'class'=>'btn btn-info',
							'escape'=>true
						));
					?>

					<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/DeathVouchers/view/'.$key, true);?>">
						Reset
						<i class="icon-spinner11 position-right"></i>
					</a>
				</div>
				<?php
					}
				?>
				<?php echo $this->Form->end(); ?>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<span class="heading-text">
						<a class="btn btn-warning" href="<?php echo $this->Html->url('/DeathVouchers/index', true);?>">
							Back <i class="icon-arrow-left13 position-right"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
		<!-- /task overview -->

	</div>

	<div class="col-lg-3">

		<!-- Timer -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-watch position-left"></i>  Apply Date Details</h6>
			</div>

			<div class="panel-body">
				<ul class="timer-weekdays mb-10">
					<?php
						foreach ($days as $day_key => $day_value)
						{
							if($detail['Voucher']['day_by_text'] == $day_key)
							{
					?>
							<li class="active"><a href="#" class="label label-danger"><?php echo $day_value; ?></a></li>
					<?php
							}
							else
							{
					?>
							<li><a href="#" class="label label-default"><?php echo $day_value; ?></a></li>
					<?php
							}
						}
					?>
				</ul>

				<ul class="timer mb-10">
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['day_by_num']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['month']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Voucher']['year']?>
							</strong>
						</span>
					</li>
				</ul>

				<ul class="timer mb-10">
					<li>
						<?php echo $detail['Voucher']['hour']?> <span>Hour</span>
					</li>
					<li class="dots">:</li>
					<li>
						<?php echo $detail['Voucher']['minute']?> <span>Minute</span>
					</li>
					<li class="dots"></li>
					<li>
						<?php echo $detail['Voucher']['format']?> <span>Period</span>
					</li>
				</ul>
			</div>
		</div>
		<!-- /timer -->
		<?php
			if($approved == true)
			{
		?>
		<!-- Task settings -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-task position-left"></i> Approve / Reject</h6>
			</div>

			<div class="panel-body">
				<?php echo $this->Form->create('Voucher', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
					<div class="row">
						<div class="col-md-12">
							<p>What do you want to do with this voucher?</p>
						</div>
						<div class="col-md-6">
							<a class="btn btn-success btn-sm btn-block" href="#" data-toggle="modal" data-target="#modal_approved">
								Approve
							</a>
						</div>
						<div class="col-md-6">
							<a class="btn btn-danger btn-sm btn-block" href="#" data-toggle="modal" data-target="#modal_reject">
								Reject
							</a>
						</div>
					</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
		<!-- /task settings -->
		<?php
			}

			if($detail['Voucher']['status_id'] > 2)
			{
		?>
			<!-- User details (with sample pattern) -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title"><i class="icon-task position-left"></i> <?php echo $detail['Status']['name']; ?></h6>
				</div>
				<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
					<a href="#" class="display-inline-block content-group-sm">
						<?php echo $detail['ApprovedBy']['avatar']; ?>
					</a>

					<div class="content-group-sm">
						<h5 class="text-semibold no-margin-bottom">
							<?php echo $detail['ApprovedBy']['name'] ?>
						</h5>
					</div>

					<ul class="timer mb-10">
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['day_by_num']?> /
								</strong>	
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['month']?> /
								</strong>
							</span>
						</li>
						<li>
							<span>
								<strong>
									<?php echo $detail['ApprovedBy']['year']?>
								</strong>
							</span>
						</li>
					</ul>

					<ul class="timer mb-10">
						<li>
							<?php echo $detail['ApprovedBy']['hour']?> <span>Hour</span>
						</li>
						<li class="dots">:</li>
						<li>
							<?php echo $detail['ApprovedBy']['minute']?> <span>Minute</span>
						</li>
						<li class="dots"></li>
						<li>
							<?php echo $detail['ApprovedBy']['format']?> <span>Period</span>
						</li>
					</ul>
				</div>

			</div>
			<!-- /user details (with sample pattern) -->
		<?php
			}
		?>
	</div>
</div>
<!-- /detailed task -->

<!-- Warning modal -->
<div id="modal_approved" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-drawer-out"></span> Approve?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Confirm</h6>
				<p>Please make sure this information is correct. Once it is submitted it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-success" href="<?php echo $this->Html->url('/DeathVouchers/approved/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->

<!-- Warning modal -->
<div id="modal_reject" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title"><span class="icon-drawer-out"></span> Reject?</h6>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold">Confirm</h6>
				<p>Please make sure this information is correct. Once it is submitted it will be not editable.</p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-danger" href="<?php echo $this->Html->url('/DeathVouchers/reject/'.$key, true); ?>">Yes</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- /warning modal -->
