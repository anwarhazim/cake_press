<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Notification</span> - Notification List</h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Notification</li>
        </ul>
    </div>
    
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    
    <?php echo $this->Session->flash(); ?>

    <!-- Form horizontal -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <?php echo $this->Form->create('Notification', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2">Search</label>
                    <div class="col-lg-10">
                        <?php 
                            echo $this->Form->input('search', array(
                                'class'=>'form-control',
                                'placeholder'=>'Search subject or content...', 
                                'label'=> false,
                                'error'=> false,
                                )
                            ); 
                        ?>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2">Start Date</label>
                    <div class="col-lg-4">
                        <?php 
                            echo $this->Form->input('start_date', array(
                                'class'=>'form-control start_date', 
                                'label'=> false,
                                'error'=> false,
                                )
                            ); 
                        ?>
                    </div>

                    <label class="control-label col-lg-2">End Date</label>
                    <div class="col-lg-4">
                        <?php 
                            echo $this->Form->input('end_date', array(
                                'class'=>'form-control end_date', 
                                'label'=> false,
                                'error'=> false,
                                )
                            ); 
                        ?>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
				<button type="submit" class="btn btn-primary">
					Find 
					<i class="glyphicon glyphicon-search position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Notifications/index', true);?>">
					Reset
					<i class="icon-spinner11 position-right"></i>
				</a>
			</div>

            <?php echo $this->Form->end(); ?>
        </div>
    </div>
    <!-- /Form horizontal -->

    <!-- Single line -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">My Notification</h6>
            
            <div class="heading-elements not-collapsible">
                <span class="label bg-blue heading-text"> new today</span>
            </div>
        </div>

        <div class="panel-toolbar panel-toolbar-inbox">
            <div class="navbar navbar-default">
                <ul class="nav navbar-nav visible-xs-block no-border">
                    <li>
                        <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </li>
                </ul>

                <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">

                    <div class="navbar-right">
                        <ul class="pagination  pull-right">
                            <?php 
                                echo $this->Paginator->prev('' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
                                echo $this->Paginator->next(__('>') . '', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-inbox">
                <tbody data-link="row" class="rowlink">
                    <?php
                        if(!empty($details))
                        {
                            foreach ($details as $notification) 
                            {
                                $unread = "";
                                if($notification['Notification']['is_read'] == 1)
                                {
                                    $unread = "unread";
                                }
                                ?>
                                <tr class="<?php echo $unread; ?>">
                                    <td class="table-inbox-image rowlink-skip">
                                        <a href="<?php echo $this->Html->url('/Notifications/read/'.$notification['Notification']['id'], true);?>">
                                            <?php echo $this->Html->image('/img/Users/default-male.png', array( 'class' => 'img-circle img-xs', 'alt' => 'PRESS | Prasarana Employee Self-Service')); ?>
                                        </a>
                                    </td>
                                    <td class="table-inbox-name">
                                        <a href="#">
                                            <div  style="padding-left: 10px;" class="letter-icon-title text-default">
                                                <?php echo $notification['Sender']['name']; ?>
                                            </div>
                                        </a>
                                    </td>

                                    <td class="table-inbox-message">
                                        <div class="table-inbox-subject">
                                            <?php echo $notification['Notification']['subject']; ?>
                                        </div>
                                        <span class="table-inbox-preview">
                                            <?php echo $notification['Notification']['body']; ?>
                                        </span>
                                    </td>
                                    <td class="table-inbox-attachment">
                                        <!-- <i class="icon-attachment text-muted"></i> -->
                                    </td>
                                    <td class="table-inbox-time">
                                        <?php echo $notification['Notification']['created']; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        else
                        {

                        }
                    ?>
                </tbody>
            </table>
        </div>

    </div>
    <!-- /single line -->
</div>
<!-- /Content area -->
