<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Notification</span> - Notification List</h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Notification</li>
            <li class="active">Read Notification</li>
        </ul>
    </div>
    
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    
    <?php echo $this->Session->flash(); ?>

   <!-- Single mail -->
   <div class="panel panel-white">
        <!-- Mail details -->
        <div class="media stack-media-on-mobile mail-details-read">
            <a href="#" class="media-left">
                <?php echo $this->Html->image('/img/placeholders/placeholder.jpg', array( 'class' => 'img-circle img-md', 'alt' => 'PRESS | ')); ?>
            </a>

            <div class="media-body">
                <h6 class="media-heading"><?php echo $detail['Notification']['subject']; ?></h6>
                <div class="letter-icon-title text-semibold"><?php echo $detail['Sender']['name']; ?> <a href="#">&lt;<?php echo $detail['Sender']['email']; ?>&gt;</a></div>
            </div>
        </div>
        <!-- /mail details -->

        <!-- Mail container -->
        <div class="mail-container-read">
            <?php echo $detail['Notification']['body']; ?> 
        </div>
        <!-- /mail container -->

        <!-- Attachments -->
        <div class="mail-attachments-container">
            <h6 class="text-muted text-size-small mail-attachments-heading">DISCLAIMER:</h6>
            <p class="text-muted text-size-mini">This e-mail and any attachment ("Message") are intended only for the use of the recipient(s) named above and may contain confidential information. You are hereby notified that the taking of any action in reliance upon, or any review, retransmission, dissemination, distribution, printing or copying of this Message or any part thereof by anyone other than the intended recipient(s) is prohibited. If you have received this Message in error, you should delete this Message immediately and advise the sender by return e-mail.</p>
            <p class="text-muted text-size-mini">Any opinion, view and/or other information in this Message which do not relate to the official business of PRASARANA MALAYSIA BERHAD shall not be deemed given nor endorsed by PRASARANA MALAYSIA BERHAD. PRASARANA MALAYSIA BERHAD is not responsible for any activity that might be considered to be an illegal and/or improper use of e-mail. The recipient should check this Message for the presence of viruses. PRASARANA MALAYSIA BERHAD accepts no liability for any damage caused by any virus transmitted by this Message.</p>
        </div>
        <!-- /attachments -->
        <div class="panel-footer">
            <div class="heading-elements">
                <span class="heading-text">
                    <a href="<?php echo $this->html->url('/Notifications/index', true); ?>" class="btn btn-warning legitRipple">
                        Back <i class="icon-arrow-left13 position-right"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <!-- /single mail -->

</div>
