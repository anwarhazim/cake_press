<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Medical Application</li>
			<li>List of Applicants</li>
			<li class="active">Edit Medical Application Details</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Detailed task -->
<div class="row">
	<div class="col-lg-9">
		<!-- Task overview -->
		<div class="panel panel-flat">
			<div class="panel-heading mt-5">
				<h5 class="panel-title"><?php echo $detail['Medical']['name']; ?> - <?php echo $detail['MedicalRelationship']['name']; ?></h5>
				<div class="heading-elements">
					<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"><?php echo $detail['MedicalStatus']['name']; ?> <b><i class="icon-alarm-check"></i></b></a>
				</div>
			</div>

			<div class="panel-body">
			<?php
				echo $this->Session->flash();

				if(!empty($this->validationErrors['Medical']))
				{
				?>
					<div role="alert" class="alert alert-danger">
							<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<?php
								foreach ($this->validationErrors['Medical'] as $errors)
								{
									echo '<ul>';
									foreach ($errors as $error)
									{
										echo '<li>'.h($error).'</li>';
									}
									echo '</ul>';
								}
							?>
					</div>
				<?php
				}
			?>
				<?php echo $this->Form->create('Medical', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
				<fieldset class="content-group">
					<legend class="text-bold">Medical Information</legend>
					<?php
							echo $this->Form->input('id', array(
								'class'=>'form-control',
								'label'=> false,
								'type'=>'hidden',
								)
							);
					?>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Name <span class="text-danger">*</span></label>
							<div class="col-lg-8">
							<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>$disabled,
										)
									); 
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">IC No. <span class="text-danger">*</span></label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('ic', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>$disabled,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Gender <span class="text-danger">*</span></label>
							<div class="col-lg-8">
							<?php 
									echo $this->Form->input('gender_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'options'=>$genders,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>$disabled,
										)
									);  
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">DOB</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('dob', array(
										'class'=>'form-control date',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>$disabled
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Status <span class="text-danger">*</span></label>
							<div class="col-lg-10">
							<?php 
									echo $this->Form->input('medical_status_id', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'options'=>$medicalstatuses,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>$disabled,
										)
									);  
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Bank</label>
							<div class="col-lg-8">
							<?php 
									echo $this->Form->input('bank_id', array(
										'class'=>'form-control',
										'label'=> false,
										'options'=>$banks,
										'empty'=>'PLEASE SELECT...',
										'disabled'=>$disabled,
										)
									);  
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-lg-4">Account No.</label>
							<div class="col-lg-8">
								<?php 
									echo $this->Form->input('bank_account_no', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>$disabled
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset class="content-group">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-12">Remarks</label>
							<div class="col-lg-12">
							<?php 
									echo $this->Form->input('remarks', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'textarea',
										'disabled'=>$disabled
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn btn-success legitRipple">
						Save <i class="icon-floppy-disk position-right"></i>
					</button>

					<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Medicals/edit/'.$key, true);?>">
						Reset <i class="icon-spinner11 position-right"></i>
					</a>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<span class="heading-text">
						<a class="btn btn-warning" href="<?php echo $this->Html->url('/Medicals/index', true);?>">
							Back <i class="icon-arrow-left13 position-right"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
		<!-- /task overview -->

	</div>

	<div class="col-lg-3">
		<!-- User details (with sample pattern) -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-task position-left"></i> Dependency Details</h6>
			</div>
			<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
				<a href="#" class="display-inline-block content-group-sm">
					<?php echo $detail['ApplyBy']['avatar']; ?>
				</a>

				<div class="content-group-sm">
					<h5 class="text-semibold no-margin-bottom">
						<?php echo $detail['ApplyBy']['name']; ?>
					</h5>
					<p><?php echo $detail['OrganisationType']['name']; ?></p>
					<p><?php echo $applyby['Staff']['Organisation']; ?></p>
					<p><?php echo $applyby['Staff']['Location']; ?></p>
				</div>

			</div>

		</div>
		<!-- /user details (with sample pattern) -->
	</div>
</div>
<!-- /detailed task -->
