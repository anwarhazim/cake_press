<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Medicals</li>
			<li class="active">List of Batch</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('Batch', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
            <fieldset class="content-group">
			    <legend class="text-bold">Search</legend>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-1">Search</label>
						<div class="col-lg-11">
							<?php
								echo $this->Form->input('search', array(
									'class'=>'form-control',
									'placeholder'=>'Name',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">Start Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('start_date', array(
									'class'=>'form-control start_date',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-2">End Date</label>
						<div class="col-lg-10">
							<?php
								echo $this->Form->input('end_date', array(
									'class'=>'form-control end_date',
									'label'=> false,
									'error'=> false,
									)
								);
							?>
						</div>
					</div>
				</div>
			</fieldset>
            <div class="text-right">
				<button type="submit" class="btn btn-primary">
					Find
					<i class="glyphicon glyphicon-search position-right"></i>
				</button>

				<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Batchs/index', true);?>">
					Reset
					<i class="icon-spinner11 position-right"></i>
				</a>
			</div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!-- /panel-body -->
</div>
<!-- /Form horizontal -->

<!-- Bordered panel body table -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">List of Batch</h5>
	</div>
	<div class="panel-body">
		<?php
			$counter = $this->Paginator->counter(array('format' =>'{:start}'));
		?>
		<div class="table-responsive">
			<table class="table table-bordered table-framed table-striped">
				<thead>
					<tr>
						<th width="5%" class="text-center">No.</th>
						<th>Name</th>
						<th width="15%" class="text-center">Date</th>
						<th width="25%" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php

						if($details)
						{
							$counter = 1;

							foreach ($details as $detail)
							{
					?>
						<tr>
							<td valign="top" class="text-center">
								<?php echo $counter; ?>
							</td>
							<td>
								<?php echo $detail['Batch']['name']; ?>
							</td>
							<td class="text-center">
								<?php echo $detail['Batch']['created'] ?>
							</td>
							<td class="text-center">
								<a alt="List" href="<?php echo $this->Html->url('/Batchs/lists/key:'.$detail['Batch']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Medical list">
									<spam class="icon-list">
									</spam>
								</a>
								<a alt="Download" traget="_blank" href="<?php echo $this->Html->url('/Batchs/download/'.$detail['Batch']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Medical Download">
									<spam class="icon-download4">
									</spam>
								</a>
								<a alt="View" href="<?php echo $this->Html->url('/Batchs/view/'.$detail['Batch']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="View details">
									<spam class="glyphicon glyphicon-file">
									</spam>
								</a>
								<a alt="Edit" href="<?php echo $this->Html->url('/Batchs/edit/'.$detail['Batch']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Edit details">
									<spam class="glyphicon glyphicon-pencil">
									</spam>
								</a>
							</td>
						</tr>
					<?php
							$counter++;
							}
						}
						else
						{
					?>
						<tr>
							<td colspan="4">
								No data...
							</td>
						</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<br/>
		<p class="pull-right">
			<?php echo $this->Paginator->counter(array(
                                                    'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                            {:count} total, start {:start}, end at {:end}'
                                                    ));
			?>
		</p>
	</div>
	<div class="panel-footer">
		<div class="heading-elements">
			<ul class="pagination  pull-right">
				<?php
					echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
					echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
				?>
			</ul>
		</div>
	</div>
</div>
<!-- /bordered panel body table -->
