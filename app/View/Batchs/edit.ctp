<!-- Page header -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li>Medicals</li>
			<li>List of Batch</li>
			<li class="active">Edit Batch Details</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Detailed task -->
<div class="row">
	<div class="col-lg-9">
		<!-- Task overview -->
		<div class="panel panel-flat">

			<div class="panel-body">
			<?php
				echo $this->Session->flash();

				if(!empty($this->validationErrors['Batch']))
				{
				?>
					<div role="alert" class="alert alert-danger">
							<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<?php
								foreach ($this->validationErrors['Batch'] as $errors)
								{
									echo '<ul>';
									foreach ($errors as $error)
									{
										echo '<li>'.h($error).'</li>';
									}
									echo '</ul>';
								}
							?>
					</div>
				<?php
				}
			?>
				<?php echo $this->Form->create('Batch', array('class'=>'form-horizontal', 'novalidate'=>'novalidate'));?>
				<fieldset class="content-group">
					<legend class="text-bold">Batch Information</legend>
					<?php
							echo $this->Form->input('id', array(
								'class'=>'form-control',
								'label'=> false,
								'type'=>'hidden',
								)
							);
					?>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
							<div class="col-lg-10">
							<?php 
									echo $this->Form->input('name', array(
										'class'=>'form-control',
										'label'=> false,
										'error'=>false,
										'type'=>'text',
										'disabled'=>$disabled,
										)
									); 
								?>
							</div>
						</div>
					</div>
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn btn-success legitRipple">
						Save <i class="icon-floppy-disk position-right"></i>
					</button>

					<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Batchs/edit/'.$key, true);?>">
						Reset <i class="icon-spinner11 position-right"></i>
					</a>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
			<div class="panel-footer">
				<div class="heading-elements">
					<span class="heading-text">
						<a class="btn btn-warning" href="<?php echo $this->Html->url('/Batchs/index', true);?>">
							Back <i class="icon-arrow-left13 position-right"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
		<!-- /task overview -->

	</div>

	<div class="col-lg-3">
		<!-- User details (with sample pattern) -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><i class="icon-task position-left"></i> Created By</h6>
			</div>
			<div class="panel-body border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
				<ul class="timer-weekdays mb-10">
					<?php
						foreach ($days as $day_key => $day_value) 
						{
							if($detail['Batch']['day_by_text'] == $day_key)
							{
					?>
							<li class="active"><a href="#" class="label label-danger"><?php echo $day_value; ?></a></li>
					<?php
							}
							else
							{
					?>
							<li><a href="#" class="label label-default"><?php echo $day_value; ?></a></li>
					<?php
							}
						}
					?>
				</ul>
				<a href="#" class="display-inline-block content-group-sm">
					<?php echo $detail['CreatedBy']['avatar']; ?>
				</a>

				<div class="content-group-sm">
					<h5 class="text-semibold no-margin-bottom">
						<?php echo $detail['CreatedBy']['name']; ?>
					</h5>
				</div>

				<ul class="timer mb-10">
					<li>
						<span>
							<strong>
								<?php echo $detail['Batch']['day_by_num']?> /
							</strong>	
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Batch']['month']?> /
							</strong>
						</span>
					</li>
					<li>
						<span>
							<strong>
								<?php echo $detail['Batch']['year']?>
							</strong>
						</span>
					</li>
				</ul>

				<ul class="timer mb-10">
					<li>
						<?php echo $detail['Batch']['hour']?> <span>Hour</span>
					</li>
					<li class="dots">:</li>
					<li>
						<?php echo $detail['Batch']['minute']?> <span>Minute</span>
					</li>
					<li class="dots"></li>
					<li>
						<?php echo $detail['Batch']['format']?> <span>Period</span>
					</li>
				</ul>

			</div>

		</div>
		<!-- /user details (with sample pattern) -->
	</div>
</div>
<!-- /detailed task -->
