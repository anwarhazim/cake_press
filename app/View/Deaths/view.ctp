<!-- Page header -->
<div class="page-header page-header-default">
	
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
			<li><a href="<?php echo $this->html->url('/Deaths/index', true);?>">Khairat Kematian/ Death Contribution</a></li>
			<li class="active">View Khairat Kematian/ Death Contribution</li>
		</ul>
	</div>

</div>
<!-- /page header -->

<!-- Form horizontal -->
<div class="panel panel-flat">
    <!-- panel-body -->
	<div class="panel-body">
		<?php 
			echo $this->Session->flash(); 

			if(!empty($this->validationErrors['Death']))
			{
			?>
				<div role="alert" class="alert alert-danger">
						<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<?php
							foreach ($this->validationErrors['Death'] as $errors) 
							{
								echo '<ul>';
								foreach ($errors as $error) 
								{
									echo '<li>'.h($error).'</li>';
								}
								echo '</ul>';
							}
						?>
				</div>
			<?php
			}
		?>
        <?php echo $this->Form->create('Death', array('class'=>'form-horizontal', 'novalidate'=>'novalidate', 'type'=>'file'));?>
			<fieldset class="content-group">
				<legend class="text-bold">Khairat Kematian/ Death Contribution</legend>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Name</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('name', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">IC No.</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('ic', array(
									'class'=>'form-control', 
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="content-group">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Relationship</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('relationship_id', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'options'=>$relationships,
									'empty'=>'PLEASE SELECT...',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-lg-4">Date of Death</label>
						<div class="col-lg-8">
							<?php 
								echo $this->Form->input('date_of_death', array(
									'class'=>'form-control date', 
									'label'=> false,
									'error'=>false,
									'type'=>'text',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			<!--
			<fieldset class="content-group">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-lg-2">Cause of Death</label>
						<div class="col-lg-10">
							<?php 
								echo $this->Form->input('cause_of_death', array(
									'class'=>'form-control',
									'label'=> false,
									'error'=>false,
									'type'=>'textarea',
									'disabled'=>$disabled
									)
								); 
							?>
						</div>
					</div>
				</div>
			</fieldset>
			-->
			<fieldset class="content-group">
				<label class="control-label col-lg-12">Attachment</label>
				<div class="col-md-12">
					<div class="row">
						<?php
							for ($i=0; $i < count($detail['Attachment']) ; $i++) 
							{ 
						?>
							<div class="col-lg-2 col-sm-6">
								<div class="thumbnail">
									<div class="thumb">
										<img src="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" />
										<div class="caption-overflow">
											<span>
												<a href="<?php echo $path.$detail['Attachment'][$i]['path']; ?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded" title="View"><i class="icon-eye"></i></a>
											</span>
										</div>
									</div>

									<div class="caption">
										<h6 class="no-margin"><?php echo $detail['Attachment'][$i]['name'] ?></h6>
									</div>
								</div>
							</div>
						<?php
							}							
						?>
					</div>
				</div>
			</fieldset>
			<?php
				if($update == true)
				{
			?>
            <div class="text-right">
				<a class="btn btn-warning legitRipple" href="<?php echo $this->Html->url('/Deaths/edit/'.$key, true);?>">
					Edit <i class="icon-pencil5 position-right"></i>
				</a>
			</div>
			<?php
				}
			?>
        <?php echo $this->Form->end(); ?>
    </div>
	<!-- /panel-body -->
	<!-- panel-footer -->
	<div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a class="btn btn-warning" href="<?php echo $this->Html->url('/Deaths/index', true);?>">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
	<!-- /panel-footer -->
</div>
<!-- /Form horizontal -->