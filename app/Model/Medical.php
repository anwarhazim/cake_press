<?php

App::uses('AuthComponent', 'Controller/Component');

class Medical extends AppModel
{
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
		'gender_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Gender.'
				),
			),
        'medical_status_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Status.'
                ),
            ),
    );
    
    public $belongsTo = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
        ),
        'Gender' => array(
			'className' => 'Gender',
			'fields' => array('id', 'name'),
			'foreignKey' => 'gender_id',
		),
		'Bank' => array(
			'className' => 'Bank',
			'fields' => array('id', 'name'),
			'foreignKey' => 'bank_id',
		),
        'MedicalRelationship' => array(
			'className' => 'MedicalRelationship',
			'fields' => array('id', 'name'),
			'foreignKey' => 'medical_relationship_id',
		),
        'MedicalStatus' => array(
			'className' => 'MedicalStatus',
			'fields' => array('id', 'name'),
			'foreignKey' => 'medical_status_id',
		),
		'OrganisationType' => array(
			'className' => 'OrganisationType',
			'fields' => array('id', 'name'),
			'foreignKey' => 'organisation_type_id',
		),
		'CreatedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'modified_by',
		)
    );
    
    public function beforeSave($options = array())
	{
        if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		if (!empty($this->data[$this->alias]['dob']))
		{
			$this->data[$this->alias]['dob'] = date("Y-m-d", strtotime($this->data[$this->alias]['dob']));
        }
        
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
