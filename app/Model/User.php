<?php

App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel 
{
	
	public $validate = array(
        'roles' => array(
            'rule' => array('multiple', array(
                'min' => 1,
            )),
            'message' => 'Please tick at least one role'
        ),
        'status_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select one from the list'
            ),
        ),
        'current_password' => array(
            'notBlank' => array(
                        'rule' => 'notBlank',
                        'message' => 'Please enter your old password.'
                ),
              'Length' => array(
                'rule'      => array('between', 6, 12),
                'message'   => 'Your password must contain at least 6 to 12 characters.',
            ),
            'Check'    => array(
                'rule'      => array('checkCurrentPassword'),
                'message' => 'Your old password does not match.',
            )
          ),
          'password' => array(
            'notBlank' => array(
                        'rule' => 'notBlank',
                        'message' => 'Please enter your new password.'
                ),
              'Length' => array(
                'rule'      => array('between', 6, 12),
                'message'   => 'Your password must contain at least 6 to 12 characters.',
            ),
          ),
          'c_password' => array(
            'notBlank' => array(
                        'rule' => 'notBlank',
                        'message' => 'Please re-enter your password.'
                ),
            'Length' => array(
                'rule'      => array('between', 6, 12),
                'message'   => 'Your re-enter password at least 6 to 12 characters.',
            ),
            'Compare'    => array(
                'rule'      => array('validate_passwords'),
                'message' => 'Your password does not match.',
            )
        ),
        'avatar' => array(
			'CreateNotBlank'    => array(
                'rule'      => array('create_NotBlank'),
                'on' => 'create',
                'message' => 'Please specify a file to upload.',
				'last' => false,
            ),
            'CreateNotFormat'    => array(
                'rule'      => array('create_NotFormat'),
                'on' => 'create',
                'message' => 'Your file format is invalid. Only .gif, .bmp, .jpeg, .jpg and .png files are allowed. Please try again!',
				'last' => false,
			),
            'NotSize'    => array(
                'rule'      => array('NotSize'),
                'message' => 'Your file must not exceed 20MB. Please try again!',
				'last' => false,
			),
		),
	);
	
	public $hasMany = array(
        'UserRole' => array(
            'className' => 'UserRole',
            'foreignKey' => 'user_id',
        )
    );

    public $hasAndBelongsToMany = array(
        'Roles' => array(
            'className' => 'Role',
            'joinTable' => 'user_roles',
        )
    );
    
    public function create_NotBlank($file)
    {
        if(empty($file['avatar']['name']))
        {
            return false;
        }

        return true;
    }

    public function create_NotFormat($file)
    {
        $check = false;
        
        if(!empty($file['avatar']['name']))
        {
            $file_parts = pathinfo($file['avatar']['name']);
            $supportedFileTypes =  array('gif', 'jpg', 'png', 'jpeg', 'bmp');
            if(!in_array(strtolower($file_parts['extension']), $supportedFileTypes)) 
            {
                $check = false;
            }

            $check = true;
        }
        else
        {
            $check = false;
        }

        return $check;
    }

    public function NotSize($file)
    {
        $totalFileSize = 0;
        
        if(!empty($file['avatar']['size']))
        {
            $totalFileSize = $totalFileSize + $file['avatar']['size'];
        }

        $maxFileSize = 20 * 1024 * 1024 /* 20MB */;        

        if ($totalFileSize >= $maxFileSize) 
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function beforeSave($options = array()) 
	{
		// hash our password
		if (isset($this->data[$this->alias]['password'])) 
		{
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		
		// if we get a new password, hash it
		if (isset($this->data[$this->alias]['password_update']) && !empty($this->data[$this->alias]['password_update'])) 
		{
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
		}
		// fallback to our parent
		return parent::beforeSave($options);
    }
    
    public function checkCurrentPassword($data) 
    {
        $this->id = AuthComponent::user('id');
        $password = $this->field('password');
        return(AuthComponent::password($data['current_password']) == $password);
    }

    public function validate_passwords() 
    {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['c_password'];
    }
}