<?php

App::uses('AuthComponent', 'Controller/Component');

class JobGrade extends AppModel 
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
			),
		'job_designation_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select one from the list'
				)
			),
	);

	public $belongsTo = array(
		'JobDesignation' => array(
			'className' => 'JobDesignation',
			'foreignKey' => 'job_designation_id',
		),
	);
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}
		
		// fallback to our parent
		return parent::beforeSave($options);
	}
}