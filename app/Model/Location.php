<?php

App::uses('AuthComponent', 'Controller/Component');

class Location extends AppModel 
{
	public $validate = array(
        'code' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
			),
		'name' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'This field is required'
				)
			)
	);
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}
		
		// fallback to our parent
		return parent::beforeSave($options);
	}
}