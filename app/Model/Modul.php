<?php

App::uses('AuthComponent', 'Controller/Component');

class Modul extends AppModel 
{
	public $actsAs = array('Tree');

	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
        ),
        'path' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
        ),
        'is_nav' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select one from the list'
                )
        ),
        'is_full' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select one from the list'
                )
        ),
        'is_type' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select one from the list'
                )
        ),
        'is_active' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select one from the list'
                )
        )
    );
	
    public function beforeSave($options = array()) 
	{		
		// fallback to our parent
		return parent::beforeSave($options);
	}
}