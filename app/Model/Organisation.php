<?php

App::uses('AuthComponent', 'Controller/Component');

class Organisation extends AppModel 
{
	public $actsAs = array('Tree');

	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
			),
		'organisation_type_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select one from the list'
				)
			),
		'organisation_category_id' => array(
				'notBlank' => array(
						'rule' => 'notBlank',
						'message' => 'Please select one from the list'
					)
				)
	);
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}
		
		// fallback to our parent
		return parent::beforeSave($options);
	}
}