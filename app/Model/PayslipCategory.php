<?php

App::uses('AuthComponent', 'Controller/Component');

class PayslipCategory extends AppModel
{
    public function beforeSave($options = array())
	{
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
