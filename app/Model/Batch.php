<?php

App::uses('AuthComponent', 'Controller/Component');

class Batch extends AppModel
{
	public $belongsTo = array(
		'CreatedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'modified_by',
		)
	);
	
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
	);
	 
    public function beforeSave($options = array())
	{
        if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}
        
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
