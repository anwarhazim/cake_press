<?php

App::uses('AuthComponent', 'Controller/Component');

class Payslip extends AppModel
{
    public $belongsTo = array(
		'PayslipCategory' => array(
			'className' => 'PayslipCategory',
			'fields' => array('payslip_type_id', 'wage_code', 'wage_long_text', 'wage_text'),
			'conditions' => array('`Payslip`.`wage_code` = `PayslipCategory`.`wage_code`'),
			'foreignKey' => false,
		)
	);

    public function beforeSave($options = array())
	{
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
