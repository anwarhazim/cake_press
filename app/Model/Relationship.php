<?php

App::uses('AuthComponent', 'Controller/Component');

class Relationship extends AppModel
{
	public $validate = array(
        'code' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
			),
		'name' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'This field is required'
				)
			)
	);

    public function beforeSave($options = array())
	{
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
