<?php

App::uses('AuthComponent', 'Controller/Component');

class Voucher extends AppModel
{
	public $validate = array(
        'note' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'You need to leave a note!'
				)
		),
	);

	public $belongsTo = array(
		'Applicant' => array(
			'className' => 'Applicant',
			//'fields' => array('id', 'name'),
			'foreignKey' => 'applicant_id',
		),
		'OrganisationType' => array(
				'className' => 'organisationType',
				//'fields' => array('id', 'name'),
				'foreignKey' => 'organisation_type_id',
			),
		'Status' => array(
				'className' => 'Status',
				'fields' => array('id', 'name'),
				'foreignKey' => 'status_id',
			),
		'ApprovedBy' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'approved_by',
			),
		'CreatedBy' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'created_by',
			),
		'ModifiedBy' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'modified_by',
			)
	);

	public function findVoucherByApplicantIdVoucherTypeId($applicant_ids = array(), $voucher_type_id = null)
	{
		$voucher_ids = array();

		$details = $this->find('list',
									array(
										'conditions' => array(
															'Voucher.applicant_id' => $applicant_ids,
															'Voucher.voucher_type_id' => $voucher_type_id,
															'Voucher.status_id' => array(1,10),
														),
								));

		foreach ($details as $key => $value) 
		{
			$voucher_ids[] = $key;
		}
													

		return $voucher_ids;
	}

    public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['vouchers_pick_name']))
		{
			$this->data[$this->alias]['vouchers_pick_name'] = strtoupper($this->data[$this->alias]['vouchers_pick_name']);
		}

		if (!empty($this->data[$this->alias]['vouchers_pick_date']))
		{
			$this->data[$this->alias]['vouchers_pick_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['vouchers_pick_date']));
		}

		return parent::beforeSave($options);
	}
}
