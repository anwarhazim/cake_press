<?php

App::uses('AuthComponent', 'Controller/Component');

class Skill extends AppModel 
{
	
	public $validate = array(
        'from_year' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Start Date field is required.'
            ),
        ),
        'to_year' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The End Date field  is required.'
            ),
        ),
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Profesional Association Membership field is required.'
            ),
        ),
        'is_status' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Status.'
            ),
        ),
        'attachments' => array(
			'CreateNotBlank'    => array(
                'rule'      => array('create_NotBlank'),
                'on' => 'create',
                'message' => 'Please specify a file to upload.',
				'last' => false,
            ),
            'CreateNotFormat'    => array(
                'rule'      => array('create_NotFormat'),
                'on' => 'create',
                'message' => 'Your file format is invalid. Only .gif, .bmp, .jpeg, .jpg and .png files are allowed. Please try again!',
				'last' => false,
			),
            'UpdateNotFormat'    => array(
                'rule'      => array('update_NotFormat'),
                'on' => 'update',
                'message' => 'Your file format is invalid. Only .gif, .bmp, .jpeg, .jpg and .png files are allowed. Please try again!',
				'last' => false,
			),
            'NotSize'    => array(
                'rule'      => array('NotSize'),
                'message' => 'Your file must not exceed 20MB. Please try again!',
				'last' => false,
			),
		),
    );
    
    public $belongsTo = array(
		'Status' => array(
			'className' => 'Status',
			'fields' => array('name'),
			'foreignKey' => 'status_id',
		),
		'CreatedBy' => array(
			'className' => 'Staff',
			'fields' => array('name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Staff',
			'fields' => array('name'),
			'foreignKey' => 'modified_by',
		)
    );

    public $hasMany = array(
        'Attachment' => array(
			'className' => 'Attachment',
			'fields' => array('id', 'name', 'path', 'modul_id', 'key_id'),
			'conditions' => array('modul_id' => 11),
            'foreignKey' => 'key_id',
        )
    );

    public function create_NotBlank($files)
    {
        foreach ($files['attachments'] as $file) 
        {
            if(empty($file['name']))
            {
                return false;
                break;
            }
        }

        return true;
    }

    public function create_NotFormat($files)
    {
        $check = false;

        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['name']))
            {
                $file_parts = pathinfo($file['name']);
                $supportedFileTypes =  array('gif', 'jpg', 'png', 'jpeg', 'bmp');
                if(!in_array(strtolower($file_parts['extension']), $supportedFileTypes)) 
                {
                    $check = false;
                    break;
                }

                $check = true;
            }
            else
            {
                $check = false;
            }
        }

        return $check;
    }

    public function update_NotFormat($files)
    {
        $check = true;

        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['name']))
            {
                $file_parts = pathinfo($file['name']);
                $supportedFileTypes =  array('gif', 'jpg', 'png', 'jpeg', 'bmp');
                if(!in_array(strtolower($file_parts['extension']), $supportedFileTypes)) 
                {
                    $check = false;
                    break;
                }

                $check = true;
            }
        }

        return $check;
    }

    public function NotSize($files)
    {
        $totalFileSize = 0;
        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['size']))
            {
                $totalFileSize = $totalFileSize + $file['size'];
            }
        }

        $maxFileSize = 20 * 1024 * 1024 /* 20MB */;        

        if ($totalFileSize >= $maxFileSize) 
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function findIfUpdateByStaffId($staff_id = null)
    {
        $update = true;

        $data = $this->find('count', array(
                                    'conditions' => array(
                                        'Skill.staff_id' => $staff_id,
                                        'Skill.is_active' => 1,
                                        'NOT' => array( 'Skill.status_id' => array(1,2,10) )
                                    ),
                                ));

        if($data > 0 )
        {
            $update = false;
        }


        return $update;
    }
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
        }
        
        if (!empty($this->data[$this->alias]['from_year']))
		{
			$this->data[$this->alias]['from_year'] = date("Y-m-d", strtotime($this->data[$this->alias]['from_year']));
        }
        
        if (!empty($this->data[$this->alias]['to_year']))
		{
			$this->data[$this->alias]['to_year'] = date("Y-m-d", strtotime($this->data[$this->alias]['to_year']));
        }
        
		return parent::beforeSave($options);
	}
}