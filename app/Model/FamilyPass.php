<?php

App::uses('AuthComponent', 'Controller/Component');

class FamilyPass extends AppModel
{

	public $validate = array(
		/*
		'family_pass_pick_name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please insert the Pick Up Name'
            ),
		),
		'family_pass_pick_date' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please insert the Pick Up Date'
            ),
		),
		*/
	);

	public $belongsTo = array(
		'Applicant' => array(
			'className' => 'Applicant',
			'foreignKey' => 'applicant_id',
		),
		'FamilyPassType' => array(
				'className' => 'FamilyPassType',
				'fields' => array('id', 'name'),
				'foreignKey' => 'family_pass_type_id',
			),
		'FamilyPassReason' => array(
				'className' => 'FamilyPassReason',
				'fields' => array('id', 'name'),
				'foreignKey' => 'family_pass_reason_id',
			),
		'OrganisationType' => array(
				'className' => 'organisationType',
				//'fields' => array('id', 'name'),
				'foreignKey' => 'organisation_type_id',
			),
		'Status' => array(
				'className' => 'Status',
				'fields' => array('id', 'name'),
				'foreignKey' => 'status_id',
			),
		'FamilyPassStatus' => array(
				'className' => 'FamilyPassStatus',
				'fields' => array('id', 'name'),
				'foreignKey' => 'family_pass_status_id',
			),
		'FamilyPassPick' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'family_pass_pick_id',
			),
		'ApprovedBy' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'approved_by',
			),
		'CreatedBy' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'created_by',
			),
		'ModifiedBy' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'modified_by',
			)
	);

	public function findFamilyPassByApplicantIdModulId($applicant_ids = array(), $modul_id = null)
	{
		$familypass_ids = array();

		$details = $this->find('list',
									array(
										'conditions' => array(
															'FamilyPass.applicant_id' => $applicant_ids,
															'FamilyPass.modul_id' => $modul_id,
															'FamilyPass.status_id' => 10,
															'FamilyPass.is_active' => 1
														),
								));

		foreach ($details as $key => $value) 
		{
			$familypass_ids[] = $key;
		}
													

		return $familypass_ids;
	}

    public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['family_pass_pick_name']))
		{
			$this->data[$this->alias]['family_pass_pick_name'] = strtoupper($this->data[$this->alias]['family_pass_pick_name']);
		}

		if (!empty($this->data[$this->alias]['family_pass_pick_date']))
		{
			$this->data[$this->alias]['family_pass_pick_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['family_pass_pick_date']));
		}

		return parent::beforeSave($options);
	}
}
