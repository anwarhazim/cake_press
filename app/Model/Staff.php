<?php

App::uses('AuthComponent', 'Controller/Component');

class Staff extends AppModel
{
	public $actsAs = array('Tree');

	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
		'staff_no' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Staff No. field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'The Staff No. field can only contain numbers. Please try again!',
				),
			'Maxlength' => array(
				'rule' => array('maxLength', 8),
				'message' => 'Maximum 8 digits only in Staff No. Please try again!',
				),
			'Minlength' => array(
				'rule' => array('minLength', 8),
				'message' => 'Minimum 8 digits only in Staff No. Please try again!',
				),
			'Unique'    => array(
				'rule'	=> array('create_staffNo'),
				'on' => 'create',
				'message' => 'Staff No. already been used. Please try again!',
				'last' => false,
				),
			'Update' => array(
				'on' => 'update',
				'rule' => array('update_staffNo'),
				'message' => 'Staff No. already been used. Please try again!',
				'last' => false,
				),
			),
		'ic_no' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The IC No. field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'The IC No. field can only contain numbers. Please try again!',
				),
			'Maxlength' => array(
				'rule' => array('maxLength', 12),
				'message' => 'Maximum 12 digits only in IC No. Please try again!',
				),
			'Minlength' => array(
				'rule' => array('minLength', 12),
				'message' => 'Minimum 12 digits only in IC No. Please try again!',
				),
			'Unique'    => array(
				'rule'	=> array('create_icNo'),
				'on' => 'create',
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
				),
			'Update' => array(
				'on' => 'update',
				'rule' => array('update_icNo'),
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
				),
			),
		'status_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Status.'
				),
			),
		'gender_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Gender.'
				),
			),
		'national_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Nationality.'
				),
			),
		'date_of_birth' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Date of Birth field is required.'
				),
			),
		'place_birth_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Place of Birth.'
				),
			),
		'religion_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Religion.'
				),
			),
		'race_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Race.'
				),
			),
		'marital_status_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Marital Status.'
				),
			),
		'bank_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Bank.'
				),
			),
		'bank_account_no' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Bank Account No. field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'The Bank Account No. field can only contain numbers. Please try again!',
				),
			),
		'home_no' => array(
			'Minlength' => array(
				'rule' => array('minLength', 8),
				'allowEmpty' => true,
				'message' => 'Minimum 8 digits only in No. Tel. (Home). Please try again!',
			),
			'Numeric' => array(
				'rule' => 'numeric',
				'allowEmpty' => true,
				'message' => 'The No. Tel. (Home) field can only contain numbers. Please try again!',
			),
		),
		'mobile_no' => array(
			'Minlength' => array(
				'rule' => array('minLength', 8),
				'allowEmpty' => true,
				'message' => 'Minimum 8 digits only in No. Tel. (HP). Please try again!',
			),
			'Numeric' => array(
				'rule' => 'numeric',
				'allowEmpty' => true,
				'message' => 'The No. Tel. (HP) field can only contain numbers. Please try again!',
			)
		),
		'current_address_1' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Address field is required.'
				),
			),
		'current_postcode' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Postcode field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'The Postcode field can only contain numbers. Please try again!',
				),
			'Minlength' => array(
				'rule' => array('minLength', 5),
				'message' => 'Minimum 12 digits only in Postcode. Please try again!',
				),
			),
		'current_state_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a State.'
				),
			),
		'email' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Email field is required.'
			),
			'Email' => array(
				'rule' => array('email', true),
				'message' => 'A valid email address is required.'
			),
			'Unique'    => array(
				'rule'      => array('create_email'),
				'on' => 'create',
				'message' => 'Email already been used. Please try again!',
				'last' => false,
			),
			'Update' => array(
				'on' => 'update',
				'rule' => array('update_email'),
				'message' => 'Email already been used. Please try again!',
				'last' => false,
			)
		),
		'job_designation_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Designation.'
				),
			),
		'job_grade_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Job Grade.'
				),
			),
		'entry_date' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Date Entry field is required.'
				),
			),
		'organisation_type_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Business Type.'
				),
			),
		'organisation_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select an Company Name.'
				),
			),
		'job_type_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Job Type.'
				),
			),
		'location_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Location.'
				),
			),
		'job_start_date' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Start Working field is required.'
				),
			),
	);

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'fields' => array('id','username', 'avatar', 'status_id','is_active', 'modified', 'created'),
			'foreignKey' => 'user_id',
		),
		'Gender' => array(
			'className' => 'Gender',
			'fields' => array('id', 'name'),
			'foreignKey' => 'gender_id',
		),
		'OrganisationType' => array(
			'className' => 'OrganisationType',
			'fields' => array('id', 'name'),
			'foreignKey' => 'organisation_type_id',
		),
		'Organisation' => array(
			'className' => 'Organisation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'organisation_id',
		),
		'Group' => array(
			'className' => 'Organisation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'group_id',
		),
		'Division' => array(
			'className' => 'Organisation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'division_id',
		),
		'Department' => array(
			'className' => 'Organisation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'department_id',
		),
		'Section' => array(
			'className' => 'Organisation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'section_id',
		),
		'Unit' => array(
			'className' => 'Organisation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'unit_id',
		),
		'SubUnit' => array(
			'className' => 'Organisation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'sub_unit_id',
		),
		'JobDesignation' => array(
			'className' => 'JobDesignation',
			'fields' => array('id', 'name'),
			'foreignKey' => 'job_designation_id',
		),
		'Location' => array(
			'className' => 'Location',
			'fields' => array('id', 'name'),
			'foreignKey' => 'location_id',
		),
		'Status' => array(
			'className' => 'Status',
			'fields' => array('id', 'name'),
			'foreignKey' => 'status_id',
		),
		'CreatedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'modified_by',
		)
	);

	/*
	public $hasMany = array(
        'Education' => array(
			'className' => 'Education',
			'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'institution', 'qualification_id', 'result', 'modified'),
			'conditions' => array('is_active' => 1, 'is_delete' => 99),
            'foreignKey' => 'staff_id',
		),
		'Skill' => array(
			'className' => 'Skill',
			'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'membership_no', 'from_year', 'to_year', 'status_id', 'is_status', 'is_delete', 'note', 'modified'),
			'conditions' => array('is_active' => 1, 'is_delete' => 99),
            'foreignKey' => 'staff_id',
		),
		'Emergency' => array(
			'className' => 'Emergency',
			'fields' => array('id', 'name', 'relationship', 'contact_no', 'house_address', 'is_delete', 'note', 'modified'),
			'conditions' => array('is_delete' => 99),
            'foreignKey' => 'staff_id',
		),
		'Spouse' => array(
			'className' => 'Spouse',
			'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', ' ic_new', 'is_status', 'marriage_date', 'status_id', 'passport_no', 'race_id', 'religion_id', 'national_id', 'occupation', 'employer', 'employer_address', 'office_no', 'mobile_no', 'income_tax_no', 'income_tax_branch', 'note', 'modified'),
			'conditions' => array('is_active' => 1, 'is_delete' => 99),
            'foreignKey' => 'staff_id',
		),
		'Children' => array(
			'className' => 'Children',
			'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', ' ic', 'is_status', 'date_of_birth', 'status_id', 'gender_id', 'occupation', 'institution_employer', 'note', 'modified'),
			'conditions' => array('is_active' => 1, 'is_delete' => 99),
            'foreignKey' => 'staff_id',
		),
		'Beneficiary' => array(
			'className' => 'Beneficiary',
			'fields' => array('id', 'name', ' relationship', 'contact_no', 'house_address', 'note', 'modified'),
			'conditions' => array('is_delete' => 99),
            'foreignKey' => 'staff_id',
		),
		'Death' => array(
			'className' => 'Death',
			'fields' => array('id', 'is_flag', 'is_active', 'is_delete', 'status_id', 'name', 'ic', ' relationship_id', 'date_of_death', 'cause_of_death', 'status_id', 'note', 'modified'),
			'conditions' => array('is_active' => 1, 'is_delete' => 99),
            'foreignKey' => 'staff_id',
		),
	);
	*/


	public function create_staffNo()
	{
		return ($this->find('count', array('conditions' =>array('Staff.staff_no' => $this->data[$this->alias]['staff_no'], 'Staff.is_active' => 1))) == 0);
	}

	public function update_staffNo()
	{
        return ($this->find('count', array('conditions' =>array('Staff.staff_no' => $this->data[$this->alias]['staff_no'], 'Staff.id !=' => $this->data[$this->alias]['id'], 'Staff.is_active' => 1))) == 0);
	}

	public function create_icNo()
	{
		return ($this->find('count', array('conditions' =>array('Staff.ic_no' => $this->data[$this->alias]['ic_no'], 'Staff.is_active' => 1))) == 0);
	}

	public function update_icNo()
	{
        return ($this->find('count', array('conditions' =>array('Staff.ic_no' => $this->data[$this->alias]['ic_no'], 'Staff.id !=' => $this->data[$this->alias]['id'], 'Staff.is_active' => 1))) == 0);
	}

	public function create_email()
	{
		return ($this->find('count', array('conditions' =>array('Staff.email' => $this->data[$this->alias]['email'], 'Staff.is_active' => 1))) == 0);
	}

	public function update_email()
	{
        return ($this->find('count', array('conditions' =>array('Staff.email' => $this->data[$this->alias]['email'], 'Staff.id !=' => $this->data[$this->alias]['id'], 'Staff.is_active' => 1))) == 0);
	}

	public function findStaffByUserId($user_id = null)
	{
		$data = array();

		$data = $this->find('first', array(
					'conditions' => array('Staff.user_id' => $user_id, 'Staff.is_active' => 1)
				));

		$data['Staff']['list_id'] = array();
		$staff_ids = $this->getListStaffIdByUserId($user_id);

		if(!empty($staff_ids))
		{
			$i = 0;
			foreach ($staff_ids as $staff_key => $staff_value) 
			{
				$data['Staff']['list_id'][$i] = $staff_key;
				$i++;
			}
		}
		
		return $data;
	}

	public function getListStaffIdByUserId($user_id = null)
	{
		$data = array();

		$data = $this->find('list', array(
								'conditions' => array('Staff.user_id' => $user_id)
							));

		return $data;
	}

	public function findStaffSummaryByUserId($user_id)
	{
		$data = array();

		$data = $this->find('first', array(
					'conditions' => array('Staff.user_id' => $user_id, 'Staff.is_active' => 1),
					'fields' => array('Staff.name', 'User.username', 'Staff.email')
				));

		return $data;
	}
	
    public function findIfUpdateByStaffId($staff_id = null)
    {
        $update = true;

        $data = $this->find('count', array(
                                'conditions' => array(
									'Staff.id' => $staff_id,
									'Staff.is_active' => 1,
									'NOT' => array( 'Staff.status_id' => array(1,2,10) )
								),
							));

        if($data > 0)
        {
            $update = false;
        }


        return $update;
    }

	public function findStaffSummaryById($staff_id)
	{
		$data = array();
		$sort = array();

		$data = $this->find('first', array(
					'conditions' => array('Staff.id' => $staff_id, 'Staff.is_active' => 1),
					'fields' => array('Staff.name', 'Staff.staff_no', 'Staff.email', 'User.avatar', 'OrganisationType.name', 'Organisation.name', 'Division.name', 'Department.name', 'Section.name', 'Unit.name', 'SubUnit.name', 'Location.name'),
				));

		if(!empty($data))
		{
			$sort['Staff'] = $data['Staff'];
			$sort['Staff']['OrganisationType'] = $data['OrganisationType']['name'];
			$sort['Staff']['avatar'] = $data['User']['avatar'];

			$organisation = "";

			if($data['Organisation']['name'] != "")
			{
				$organisation .= $data['Organisation']['name'];
			}

			if($data['Division']['name'] != "")
			{
				$organisation .= ' / '.$data['Division']['name'];
			}

			if($data['Department']['name'] != "")
			{
				$organisation .= ' / '.$data['Department']['name'];
			}

			if($data['Section']['name'] != "")
			{
				$organisation .= ' / '.$data['Section']['name'];
			}

			if($data['Unit']['name'] != "")
			{
				$organisation .= ' / '.$data['Unit']['name'];
			}

			if($data['SubUnit']['name'] != "")
			{
				$organisation .= ' / '.$data['SubUnit']['name'];
			}

			$sort['Staff']['Organisation'] = $organisation;
			$sort['Staff']['Location'] = $data['Location']['name'];
		}

		return $sort;
	}

	public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		if (!empty($this->data[$this->alias]['date_of_birth']))
		{
			$this->data[$this->alias]['date_of_birth'] = date("Y-m-d", strtotime($this->data[$this->alias]['date_of_birth']));
		}

		if (!empty($this->data[$this->alias]['current_address_1']))
		{
			$this->data[$this->alias]['current_address_1'] = strtoupper($this->data[$this->alias]['current_address_1']);
		}

		if (!empty($this->data[$this->alias]['current_address_2']))
		{
			$this->data[$this->alias]['current_address_2'] = strtoupper($this->data[$this->alias]['current_address_2']);
		}

		if (!empty($this->data[$this->alias]['current_address_3']))
		{
			$this->data[$this->alias]['current_address_3'] = strtoupper($this->data[$this->alias]['current_address_3']);
		}

		if (!empty($this->data[$this->alias]['entry_date']))
		{
			$this->data[$this->alias]['entry_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['entry_date']));
		}

		if (!empty($this->data[$this->alias]['job_start_date']))
		{
			$this->data[$this->alias]['job_start_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['job_start_date']));
		}

		if (!empty($this->data[$this->alias]['job_end_date']))
		{
			$this->data[$this->alias]['job_end_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['job_end_date']));
		}

		return parent::beforeSave($options);
	}

}
