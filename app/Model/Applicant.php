<?php

App::uses('AuthComponent', 'Controller/Component');

class Applicant extends AppModel 
{
	public $actsAs = array('Tree');

	public $validate = array(
        'note' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'You need to leave a note'
                )
        ),
    );

	public $belongsTo = array(
			'ApplyBy' => array(
				'className' => 'Staff',
				'fields' => array('id', 'name'),
				'foreignKey' => 'staff_id',
			),
			'Modul' => array(
					'className' => 'Modul',
					'fields' => array('id', 'name'),
					'foreignKey' => 'modul_id',
				),
			'Status' => array(
					'className' => 'Status',
					'fields' => array('id', 'name'),
					'foreignKey' => 'status_id',
				),
			'AssignTo' => array(
					'className' => 'Staff',
					'fields' => array('id', 'name'),
					'foreignKey' => 'assign_to',
				),
			'VerifiedBy' => array(
					'className' => 'Staff',
					'fields' => array('id', 'name'),
					'foreignKey' => 'verified_by',
				),
			'ApprovedBy' => array(
					'className' => 'Staff',
					'fields' => array('id', 'name'),
					'foreignKey' => 'approved_by',
				),
			'CreatedBy' => array(
					'className' => 'Staff',
					'fields' => array('id', 'name'),
					'foreignKey' => 'created_by',
				),
			'ModifiedBy' => array(
					'className' => 'Staff',
					'fields' => array('id', 'name'),
					'foreignKey' => 'modified_by',
				)
	);
	
	public function getApplicantByStaffIdModulId($staff_ids = array(), $modul_id = null )
	{
		$applicant_ids = array();

		$details = $this->find('list',
									array(
										'conditions' => array(
															'Applicant.staff_id' => $staff_ids,
															'Applicant.modul_id' => $modul_id,
															'Applicant.status_id' => 10,
														),
								));

		foreach ($details as $key => $value) 
		{
			$applicant_ids[] = $key;
		}
													

		return $applicant_ids;
	}
	
    public function beforeSave($options = array()) 
	{
		// fallback to our parent
		return parent::beforeSave($options);
	}
}