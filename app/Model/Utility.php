<?php
App::uses('AuthComponent', 'Controller/Component');

class Utility extends AppModel 
{
	public $useTable = false;

	public function cleanString($string) 
	{
		$string = str_replace(array('[\', \']'), '', $string);
	    $string = preg_replace('/\[.*\]/U', '', $string);
	    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
	    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
	    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
	    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);

		return strtolower(trim($string, '-'));
	}
	
	public function createFolder($filename=null){
		$condition = false;

		if($filename){
			if (!file_exists($filename)) {
    			mkdir($filename, 0777);
    			$condition = true;
    		}
		}
		return $condition;
	}

	function strlen($str = null, $len = 0)
    {

        if(strlen($str) > $len)
        {
            $str = substr($str,0,$len)."...";
        }

        return $str;
    }

    function datetime($created = null)
    {

        if(!empty($created))
        {
            $curr_date = strtotime(date("Y-m-d"));
            $the_date    = strtotime($created);
            $diff=floor(($curr_date-$the_date)/(60*60*24));

            switch($diff)
            {
                case 0:
                    $created = date("H:i a", strtotime($created));
                    break;
                default:
                    $created = date("d M", strtotime($created));
                    break;
            }
        }

        return $created;
    }

	function encrypt( $id, $salt) 
	{
		$qEncoded = base64_encode($id . $salt);
		return( $qEncoded );
	}
	
	function decrypt( $key, $salt ) 
	{
		$qDecoded_raw = base64_decode($key);
		$qDecoded = preg_replace(sprintf('/%s/', $salt), '', $qDecoded_raw);

		return( $qDecoded );
	}

	public function getUserAuth($modul_id, $user_id)
	{
		$Setting = ClassRegistry::init('Setting');
		$UserRole = ClassRegistry::init('UserRole');

		$auth = false;

		$setting = $Setting->find('all',
			    		array(
			    			'conditions' => array('Setting.modul_id' => $modul_id),	
			    		));
		
		$role_lists = array();
		$userroles = array();

		foreach ($setting as $value) 
		{
			$role_lists[] = $value['Setting']['role_id'];
		}

		if(!empty($role_lists))
		{
			$userroles = $UserRole->find('all',
			    		array(
			    			'conditions' => array(
			    					'UserRole.user_id' => $user_id, 
			    					'UserRole.role_id' => $role_lists
			    			),	
			    		));
		}

		if(!empty($userroles))
		{
			$auth = true;
		}

		return $auth;
	}

	public function getUserDetails($id)
	{
		$User = ClassRegistry::init('User');
		$Staff = ClassRegistry::init('Staff');
		$Role = ClassRegistry::init('Role');

		$data = array();

		$data = $User->findById($id);

		$staff = $Staff->findStaffByUserId($id);
		
		$data['Staff'] = $staff['Staff'];

		if(!empty($staff['OrganisationType']))
		{
			$data['OrganisationType'] = $staff['OrganisationType'];
		}

		if(!empty($staff['Organisation']))
		{
			$data['Organisation'] = $staff['Organisation'];
		}
		
		if(!empty($staff['Division']))
		{
			$data['Division'] = $staff['Division'];
		}

		if(!empty($staff['Department']))
		{
			$data['Department'] = $staff['Department'];
		}

		if(!empty($staff['Section']))
		{
			$data['Section'] = $staff['Section'];
		}

		if(!empty($staff['Unit']))
		{
			$data['Unit'] = $staff['Unit'];
		}

		if(!empty($staff['SubUnit']))
		{
			$data['SubUnit'] = $staff['SubUnit'];
		}

		if(!empty($staff['Location']))
		{
			$data['Location'] = $staff['Location'];
		}

		$data['Sidebar'] = $this->getSidebar($id);
	
		return $data;
	}

	public function getSidebar($id)
	{
		$Setting = ClassRegistry::init('Setting');
		$Modul = ClassRegistry::init('Modul');
		$User = ClassRegistry::init('User');

		$sidebar = "";
		$modul_selected = array();
		$settings = array();
		$moduls = array();

		$data = $User->findById($id);

		foreach ($data['UserRole'] as $role) 
		{

			$settings = $Setting->find('all',
											array(
												'conditions' => array('role_id' => $role['role_id'])	
											));

			if($settings)
			{
				foreach ($settings as $setting) 
				{
					if (!in_array($setting['Setting']['modul_id'], $modul_selected)) 
					{
						array_push($modul_selected ,$setting['Setting']['modul_id']);
					}
				}
			}
		}
    	
    	$moduls = $Modul->find('threaded',
									array(
										'conditions' => array(
															'id' => $modul_selected,
															'Modul.is_active' => 1,
															'Modul.is_nav' => 1,
														),
										'order' => array('Modul.order asc')	
									));

		$sidebar = $this->getSidebarList($moduls, count($moduls), "");
		
		return $sidebar;
	}

	function getSidebarList($arr, $max, $sidebar)
    {
    	$base_url = Router::url('/', true);

    	if ( ! is_array($arr))
	        return $sidebar;

    	for ($i=0; $i < $max ; $i++) 
    	{ 
    		if ( ! empty($arr[$i]['children']))
	        {
	        	$sidebar .= "<li class='xn-openable'>";
	        }else{
				if($arr[$i]['Modul']['is_type'] == 1)
				{
					$sidebar .= "<li class='navigation-header'>";
				}
				else
				{
					$sidebar .= "<li>";
				}
	        }

	        if($arr[$i]['Modul']['path'] == '#')
	        {
				if($arr[$i]['Modul']['is_type'] == 1)
				{
	        		$sidebar .= "<span>";
				}
				else
				{
					$sidebar .= "<a href='".$arr[$i]['Modul']['path']."'>";
				}
	        } 
	        else 
	        {
				$sidebar .= "<a href='".$base_url.$arr[$i]['Modul']['path']."'>";
			}

	    	if($arr[$i]['Modul']['icon'])
	    	{
	    		$sidebar .= "<i class='".$arr[$i]['Modul']['icon']."'></i>";
	    	}
	    	
            if ( ! empty($arr[$i]['children']))
            {
                $sidebar .= "<span class='xn-text'>".$arr[$i]['Modul']['name']."</span>";
            }
            else
            {
				if($arr[$i]['Modul']['is_type'] == 1)
				{
    		    	$sidebar .= "<span>".$arr[$i]['Modul']['name']."</span>";
				}
				else
				{
					$sidebar .= $arr[$i]['Modul']['name'];
				}
			}

			if($arr[$i]['Modul']['is_type'] == 1)
			{
    			$sidebar .= "</spam>";
			}
			else
			{
				$sidebar .= "</a>";
			}

			if ( ! empty($arr[$i]['children']))
	        {  
                $sidebar .= "<ul>";
	        	$sidebar = $this->getSidebarList($arr[$i]['children'], count($arr[$i]['children']), $sidebar);
	            $sidebar .= "</ul>";
            }
	        
	        $sidebar .= "</li>";
    	}

    	return $sidebar;
    }

	public function getNestable($arr, $max , $model, $nestable = "", $field)
	{
    	if ( ! is_array($arr))
	        return $nestable;

	    for ($i=0; $i < $max ; $i++) 
	    { 
	    	$nestable = $nestable.'<li class="dd-item" data-id="'.$arr[$i][$model]['id'].'">';

        	if(empty($arr[$i][$model]['code']))
        	{
        		$nestable = $nestable.'<div class="dd-handle"> #'.$arr[$i][$model]['id']." : ".$arr[$i][$model][$field].'</div>';
        	}
        	else 
        	{
        		$nestable = $nestable.'<div class="dd-handle"> #'.$arr[$i][$model]['id']." : ".$arr[$i][$model][$field].' ('.$arr[$i][$model]['code'].')</div>';
        	}

        	if ( ! empty($arr[$i]['children']))
	        {
	        	$nestable = $nestable.'<ol class="dd-list">';
	        	$nestable = $this->getNestable($arr[$i]['children'] , count($arr[$i]['children']), $model ,$nestable, $field);
	        	$nestable = $nestable.'</ol>';
	        }
        	$nestable = $nestable.'</li>';
	    }

	    return $nestable;
	}
	
	public function getNestableSave($arr, $max, $model, $parent_id, $counter)
    {
    	$modul = ClassRegistry::init($model);
    	for ($i=0; $i < $max ; $i++) 
    	{ 
    		$data[$model]['id'] = $arr[$i]['id'];
            $data[$model]['parent_id'] = $parent_id;
            $data[$model]['order'] = $counter;
            $modul->create();
            $modul->save($data);
            $counter++;
            if(! empty($arr[$i]['children']))
            {
            	$this->getNestableSave($arr[$i]['children'] , count($arr[$i]['children']), $model, $arr[$i]['id'], $counter);
            }
    	}
	}
	
	public function getTreeModul($arr, $max , $tree = "")
    {
    	if ( ! is_array($arr))
	        return $tree;

	    $tree .= 	'<ul>';
	    for ($i=0; $i < $max ; $i++) { 
	    	$tree .=	'<li id="'.$arr[$i]['Modul']['id'].'" class="jstree-open">';
	    	$tree .=	$arr[$i]['Modul']['name'];
	    	if ( ! empty($arr[$i]['children']))
	        {
	        	$tree = $this->getTreeModul($arr[$i]['children'] , count($arr[$i]['children']), $tree);
	        }
	    	$tree .=	'</li>';
		}
		$tree .= 	'</ul>';	    

    	return $tree;
	}
	
	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function signature($source)
	{
		return md5($source);
	}

}