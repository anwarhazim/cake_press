<?php

App::uses('AuthComponent', 'Controller/Component');

class Notification extends AppModel 
{
	public $belongsTo = array(
		'Sender' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name','email'),
			'foreignKey' => 'created_by',
		),
		'Recipient' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name','email'),
			'foreignKey' => 'created_by',
		),
		'CreatedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name','email'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name','email'),
			'foreignKey' => 'modified_by',
		)
	);
	
    public function beforeSave($options = array()) 
	{	
		// fallback to our parent
		return parent::beforeSave($options);
	}
}