<?php

App::uses('AuthComponent', 'Controller/Component');

class Event extends AppModel
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
        )
    );
    
    public $belongsTo = array(
        'CategoryEvent' => array(
			'className' => 'CategoryEvent',
			'fields' => array('id', 'name'),
			'foreignKey' => 'category_event_id',
		),
		'CreatedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'modified_by',
		)
	);

    public function beforeSave($options = array())
	{
        if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
        }

        if (!empty($this->data[$this->alias]['event_start_date']))
		{
			$this->data[$this->alias]['event_start_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['event_start_date']));
        }
        
        if (!empty($this->data[$this->alias]['event_end_date']))
		{
			$this->data[$this->alias]['event_end_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['event_end_date']));
		}
        
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
