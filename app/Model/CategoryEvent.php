<?php

App::uses('AuthComponent', 'Controller/Component');

class CategoryEvent extends AppModel
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
            ),
        'project_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Project.'
                )
        )
    );
    
    public $belongsTo = array(
        'Project' => array(
			'className' => 'Project',
			'fields' => array('id', 'name'),
			'foreignKey' => 'project_id',
		),
		'CreatedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Staff',
			'fields' => array('id', 'name'),
			'foreignKey' => 'modified_by',
		)
	);

    public function beforeSave($options = array())
	{
        if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
        }
        
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
