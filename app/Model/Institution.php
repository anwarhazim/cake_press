<?php

App::uses('AuthComponent', 'Controller/Component');

class Institution extends AppModel
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
		'code' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Code field is required.'
				)
			),
	);


    public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		if (!empty($this->data[$this->alias]['code']))
		{
			$this->data[$this->alias]['code'] = strtoupper($this->data[$this->alias]['code']);
		}

		// fallback to our parent
		return parent::beforeSave($options);
	}
}
