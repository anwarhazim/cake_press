<?php

App::uses('AuthComponent', 'Controller/Component');

class IcColor extends AppModel
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This field is required'
                )
        )
	);

    public function beforeSave($options = array())
	{
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
