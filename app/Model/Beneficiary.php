<?php

App::uses('AuthComponent', 'Controller/Component');

class Beneficiary extends AppModel 
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
		'relationship' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Relationship field is required.'
				)
			),
		'contact_no' => array(
			'notBlank' => array(
				'rule' => 'notBlank',
				'message' => 'The Contact No. field is required.'
				),
			'Minlength' => array(
				'rule' => array('minLength', 8),
				'message' => 'Minimum 8 digits only in Contact No. Please try again!',
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'Please insert number only in Contact No. Please try again!',
				),
			),
		'house_address' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Address field is required.'
				)
			),

	);
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		if (!empty($this->data[$this->alias]['relationship']))
		{
			$this->data[$this->alias]['relationship'] = strtoupper($this->data[$this->alias]['relationship']);
		}

		if (!empty($this->data[$this->alias]['house_address']))
		{
			$this->data[$this->alias]['house_address'] = strtoupper($this->data[$this->alias]['house_address']);
		}
		
		// fallback to our parent
		return parent::beforeSave($options);
	}
}